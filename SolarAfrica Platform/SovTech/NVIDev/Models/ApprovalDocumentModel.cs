﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NVIDev.Models
{
    public class ApprovalDocumentModelLayer
    {
        public IEnumerable<ApprovalDocument> AdminApprovalDocumentList { get; set; }
        public ApprovalDocumentModel AdminApprovalDocument { get; set; }
    }

    public class ApprovalDocumentModel
    {
        public Int64 ApprovalDocumentId { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Document Type")]
        public string ApprovalDocumentMenu { get; set; }        

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Document Name")]
        public string ApprovalDocumentSubMenu { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Document Description")]
        public string ApprovalDocumentDesc { get; set; }

        [Display(Name = "")]
        public string TriggerOnLoadMessage { get; set; }
        
        [Display(Name = "Specific Name")]
        public string CounSpecificName { get; set; }
       
        [Display(Name = "Specific Name")]
        public string OrgSpecificName { get; set; }

        public bool IsActive { get; set; }
    }

    public partial class ApprovalDocument
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 ApprovalDocumentId { get; set; }
        public string ApprovalDocumentMenu { get; set; }
        public string ApprovalDocumentSubMenu { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ApprovalDocumentDesc { get; set; }
        public string CounOrgSpecificName { get; set; }
    }

    public class UserDocumentModel
    {
        public string CompanyID { get; set; }
        public string UserID { get; set; }
        public string DocumentID { get; set; } 
    }

    [Table("UserApprovalDocument")]
    public partial class UserApprovalDocumentModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 UserDocumentId { get; set; }
        public string CompanyID { get; set; }
        public string UserID { get; set; }
        public string DocumentID { get; set; } 
        public DateTime CreatedOn { get; set; }
        public string AttachedBy { get; set; }
        public string AttachmentName { get; set; }
        public string IsApproved { get; set; }
        public byte[] AttachedFile { get; set; }
        public string AdminComment { get; set; }
        public string TPUserComment { get; set; }
    }

    public class AttachDocModel 
    {
        public string ApprovalDocumentId { get; set; }
        public string ApprovalDocumentMenu { get; set; }
        public string ApprovalDocumentSubMenu { get; set; }        
        public string CompanyID { get; set; }
        public string IsApproved { get; set; }
        [DataType(DataType.Upload)]
        public HttpPostedFileBase AttachmentName { get; set; }
        public string ApprovalDocumentDesc { get; set; }
        public byte[] AttachedFile { get; set; }
        public string Attachedfilename { get; set; }
        public string AdminComment { get; set; }
        public string TPUserComment { get; set; }
    }
}