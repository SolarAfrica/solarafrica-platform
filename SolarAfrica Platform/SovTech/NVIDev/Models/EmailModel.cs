﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Data;
using System.Globalization;
using iTextSharp.text;

namespace NVIDev.Models
{
    public class EmailModel
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataTable GetMailSetting()
        {
            try
            {
                var dt = new DataTable();
                var ds = new DataSet("dsMailSetting");
                ds.ReadXml(HttpRuntime.AppDomainAppPath + "/xmlDB/Email.xml");
                dt = (DataTable)ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            { throw ex; }
            finally { }
        }

        public bool SendEMail(string subject, string ToName, string To, List<ApprovalDocument> docdetails)
        {
            var status = false;
            try
            {
                var splitTo = To.Split(';');

                if (splitTo.Length > 0)
                {
                    //loop for sending mail for various multiple users in to feild
                    for (int i = 0; i < splitTo.Length; i++)
                    {
                        var from = new MailAddress("solarafricateam@gmail.com");
                        var to = new MailAddress(splitTo[i].ToString(CultureInfo.InvariantCulture));
                        var msg = new MailMessage(from, to);
                        //from.User.Replace("hr", "HR SIPL");
                        //from.DisplayName.Replace("hr", sender);

                        msg.Subject = subject;
                        msg.IsBodyHtml = true;

                        msg.Body = "<b> Dear, " + ToName + "</b>";

                        msg.Body += "<p>Please login to the Solar Africa Tool and submit the following documents</p>";
                        msg.Body += "<ul>";
                        foreach (var doc in docdetails)
                        {
                            msg.Body += "<li>" + doc.ApprovalDocumentMenu + "</li>";
                        }
                        msg.Body += "</ul>";                        
                        msg.Body += "<p>These documents will undergo a final approval.</p>";
                        msg.Body += "<p>Please contact us via the tool if you have any further concerns</p> <p>Regards,</p>  <p>Solar Africa Team</p>";
                        
                        msg.Priority = MailPriority.High;
                        var client = new SmtpClient("smtp.gmail.com", 587);
                        client.UseDefaultCredentials = false;
                        client.EnableSsl = true;
                        var theCredential = new System.Net.NetworkCredential("solarafricateam@gmail.com", "sipl12345");
                        client.Credentials = theCredential;
                        client.Send(msg);
                    }
                }
                status = true;
            }


            catch (Exception ex)
            {
                status = false;
                return status;
                throw ex;
            }
            finally
            {
            }
            return status;
        }
    
    }
}