﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVIDev.Models
{
    public class Order_Details
    {

        public ApplicationDbContext VarDB = new ApplicationDbContext();
        

        //Group items together by country of origin
        //Package those items together into 20fts
        //Devided total 20fts by 2 to get total 40fts
        //the remained either goes up to 1 x 40ft or down to 1 x 20ft

        public Order order { get; set; } //this is the order sheet with all containers
        public List<string> myCOOS { get; set; } //all countries of origin

        public List<EquipmentItem> allEquipment { get; set; }
        public void CalculateEquipment(Project project)
        {
            //get inverters, bi inverters, pv-modules , mounting structures , batteries , balance of system
            //sort them by sub category
            //find the tax information
            //pack the items per category into
            allEquipment = new List<EquipmentItem>();
            #region Get Equipment
            order = new Order();
            myCOOS = new List<string>();


            #region Get Inverters
            List<string> invIDs = (!string.IsNullOrEmpty(project.InverterIDS)) ? project.InverterIDS.Split(',').ToList() : new List<string>();
            List<string> invamounts = (!string.IsNullOrEmpty(project.InverterAmounts)) ? project.InverterAmounts.Split(',').ToList() : new List<string>();
            //List<EquipmentItem> invertEquips = new List<EquipmentItem>(); //all inverters
            try
            {
            for (int a = 0; a < invIDs.Count; a++) //list of all inverters
            {
              

                
                string id = invIDs[a].Split('!')[0];
                string config = invIDs[a].Split('!')[1];
                string dcVol = invIDs[a].Split('!')[2];
                string dcLoad = invIDs[a].Split('!')[3];
                string io = invIDs[a].Split('!')[4];
                string country = invIDs[a].Split('!')[5];

                string Amount = invamounts[a].Split('!')[0];

                Inverters inv = VarDB.Inverters.Find(id);

                EquipmentItem item = inv.ToEquipmentItem(); //inverter is now equipment item
                item.Type = EquipmentType.Inverter;
                item.Extra = string.Format("{0} {1} {2} {3} {4}", config, dcVol, dcLoad, io, country);
                item.Amount = int.Parse(Amount);
                item.CalculateSpaceUsed();

                //invertEquips.Add(item);//list of all inverters as equipment
                allEquipment.Add(item);


                if (!myCOOS.Contains(inv.CountryOfOrigin))
                {
                    myCOOS.Add(inv.CountryOfOrigin);
                }
            


            }

            invIDs.Clear();
            invIDs = null;
            invamounts.Clear();
            invamounts = null;
            //invertEquips.OrderBy(m => m.CountryOfOrigin); //order by country of origin
                        }

                catch
                {
                    //if the id is not found NVI has changed the equipment item
                }
            #endregion

            #region Get Bi Inverters
            List<string> BIinvIDs = (!string.IsNullOrEmpty(project.BiInverterIDS)) ? project.BiInverterIDS.Split(',').ToList() : new List<string>();
            List<string> biinvamounts = (!string.IsNullOrEmpty(project.BiInverterAmounts)) ? project.BiInverterAmounts.Split(',').ToList() : new List<string>();
            //List<EquipmentItem> BiinvertEquips = new List<EquipmentItem>(); //all inverters

            try
            {
                for (int a = 0; a < BIinvIDs.Count; a++)
                {
                    string id = BIinvIDs[a].Split('!')[0];
                    string coms = BIinvIDs[a].Split('!')[1];
                    string apps = BIinvIDs[a].Split('!')[2];
                    string inters = BIinvIDs[a].Split('!')[3];


                    string Amount = biinvamounts[a].Split('!')[0];

                    BiInverters biNV = VarDB.BiInverters.Find(id);

                    EquipmentItem item = biNV.ToEquipmentItem(); //inverter is now equipment item
                    item.Extra = string.Format("{0} {1} {2} ", coms, apps, inters);
                    item.Amount = int.Parse(Amount);
                    item.CalculateSpaceUsed();
                    item.Type = EquipmentType.Bi_Inverter;
                    //BiinvertEquips.Add(item);//list of all inverters as equipment
                    allEquipment.Add(item);


                    if (!myCOOS.Contains(biNV.CountryOfOrigin))
                    {
                        myCOOS.Add(biNV.CountryOfOrigin);
                    }

                    //BiinvertEquips.OrderBy(m => m.CountryOfOrigin); //order by country of origin


                }

                biinvamounts.Clear();
                biinvamounts = null;
                BIinvIDs.Clear();
                BIinvIDs = null;
            }
            catch (Exception)
            {
                //
            }


            #endregion

            #region Get Mounting
            List<string> mountIDS = (!string.IsNullOrEmpty(project.MountingIDS)) ? project.MountingIDS.Split(',').ToList() : new List<string>();
            List<string> mountAmounts = (!string.IsNullOrEmpty(project.MountingAmounts)) ? project.MountingAmounts.Split(',').ToList() : new List<string>();
            //List<EquipmentItem> MountEquips = new List<EquipmentItem>(); //all inverters

            try
            {
                for (int a = 0; a < mountIDS.Count; a++)
                {
                    string id = mountIDS[a];

                    string Amount = mountAmounts[a].Split('!')[0];


                    MountingStructures mount = VarDB.MountingStructures.Find(id);

                    EquipmentItem item = mount.ToEquipmentItem(); //inverter is now equipment item
                    item.Extra = string.Format("Mounting Structure");
                    item.Amount = int.Parse(Amount);
                    item.CalculateSpaceUsed();
                    item.Type = EquipmentType.Mounting;
                    //MountEquips.Add(item);//list of all inverters as equipment
                    allEquipment.Add(item);


                    if (!myCOOS.Contains(mount.CountryOfOrigin))
                    {
                        myCOOS.Add(mount.CountryOfOrigin);
                    }

                    //MountEquips.OrderBy(m => m.CountryOfOrigin); //order by country of origin
                }


                mountIDS.Clear();
                mountIDS = null;
                mountAmounts.Clear();
                mountAmounts = null;
            }
            catch (Exception)
            {
                //
            }

            #endregion

            #region Get PV Modules
            List<string> pvModsIDs = (!string.IsNullOrEmpty(project.PVModuleIDS)) ? project.PVModuleIDS.Split(',').ToList() : new List<string>();
            List<string> pvAmounts = (!string.IsNullOrEmpty(project.PvModuleAmounts)) ? project.PvModuleAmounts.Split(',').ToList() : new List<string>();
            //List<EquipmentItem> pvEquips = new List<EquipmentItem>(); //all inverters

            try
            {
                for (int a = 0; a < pvModsIDs.Count; a++)
                {
                    string id = pvModsIDs[a];


                    string Amount = pvAmounts[a].Split('!')[0];



                    PVModules pvmod = VarDB.PVModules.Find(id);
                    EquipmentItem item = pvmod.ToEquipmentItem(); //inverter is now equipment item
                    item.Extra = string.Format("PV Module");
                    item.Amount = int.Parse(Amount);
                    item.CalculateSpaceUsed();
                    item.Type = EquipmentType.PVModule;
                    //pvEquips.Add(item);//list of all inverters as equipment
                    allEquipment.Add(item);


                    if (!myCOOS.Contains(pvmod.CountryOfOrigin))
                    {
                        myCOOS.Add(pvmod.CountryOfOrigin);
                    }

                    //pvEquips.OrderBy(m => m.CountryOfOrigin); //order by country of origin
                }
                pvModsIDs.Clear();
                pvAmounts.Clear();
                pvModsIDs = null;
                pvAmounts = null;
            }
            catch (Exception)
            {
                //
            }

            #endregion

            #region Get Batteries
            List<string> batIds = (!string.IsNullOrEmpty(project.BatteryIDS)) ? project.BatteryIDS.Split(',').ToList() : new List<string>();
            List<string> batamounts = (!string.IsNullOrEmpty(project.BatteryAmounts)) ? project.BatteryAmounts.Split(',').ToList() : new List<string>();
            //List<EquipmentItem> batEquips = new List<EquipmentItem>(); //all inverters

            try
            {
                for (int a = 0; a < batIds.Count; a++)
                {
                    string id = batIds[a];


                    string Amount = batamounts[a].Split('!')[0];



                    Batteries bat = VarDB.Batteries.Find(id);
                    EquipmentItem item = bat.ToEquipmentItem(); //inverter is now equipment item
                    item.Extra = string.Format("Battery");
                    item.Amount = int.Parse(Amount);
                    item.CalculateSpaceUsed();
                    item.Type = EquipmentType.Battery;
                    //batEquips.Add(item);//list of all inverters as equipment
                    allEquipment.Add(item);


                    if (!myCOOS.Contains(bat.CountryOfOrigin))
                    {
                        myCOOS.Add(bat.CountryOfOrigin);
                    }

                    //batEquips.OrderBy(m => m.CountryOfOrigin); //order by country of origin
                }

                batIds.Clear();
                batamounts.Clear();
                batIds = null;
                batamounts = null;
            }
            catch (Exception)
            {
                //
            }

            #endregion

            #region Get Balance Of System
            List<string> balanceIds = (!string.IsNullOrEmpty(project.BalanceSystemIDS)) ? project.BalanceSystemIDS.Split(',').ToList() : new List<string>();
            List<string> balanceAmounts = (!string.IsNullOrEmpty(project.BalanceSystemAmounts)) ? project.BalanceSystemAmounts.Split(',').ToList() : new List<string>();
            //List<EquipmentItem> balanceEquips = new List<EquipmentItem>(); //all inverters

            try
            {
                for (int a = 0; a < balanceIds.Count; a++)
                {
                    string id = balanceIds[a].Split('!')[0];
                    string type = balanceIds[a].Split('!')[1];


                    string Amount = balanceAmounts[a].Split('!')[0];



                    BalanceSystem bal = VarDB.BalanceSystems.Find(id);
                    EquipmentItem item = bal.ToEquipmentItem(); //inverter is now equipment item
                    item.Extra = string.Format(bal.Type);
                    item.Amount = int.Parse(Amount);
                    item.CalculateSpaceUsed();
                    item.Type = EquipmentType.BOS;
                    //balanceEquips.Add(item);//list of all inverters as equipment
                    allEquipment.Add(item);


                    if (!myCOOS.Contains(bal.CountryOfOrigin))
                    {
                        myCOOS.Add(bal.CountryOfOrigin);
                    }

                    //balanceEquips.OrderBy(m => m.CountryOfOrigin); //order by country of origin
                }

                balanceIds.Clear();
                balanceAmounts.Clear();
                balanceIds = null;
                balanceAmounts = null;
            }
            catch (Exception)
            {
                //
            }
            #endregion
            #endregion

            #region Populate Containers

            allEquipment =   allEquipment.OrderBy(m => m.CountryOfOrigin).ToList();



            foreach (var coo in myCOOS)
            {
                List<Container> countryContainers = new List<Container>(); //all the containers shipped from a country
                List<EquipmentItem> equipmentToShip = allEquipment.Where(m => m.CountryOfOrigin == coo).ToList(); //all country specific equipment
                List<Pallet> pallets = new List<Pallet>();

                int index = 0;
                Container container = new Container();
                container.Size = ContainerType.ft20;
                Pallet pallet = new Pallet();


                //get port information
                Port port = VarDB.Ports.Where(m => m.Country == project.Details.P_Country).First();
                List<string> port_coos = port.COOs.Split('!').ToList();
                List<string> port_20fts = port.DeliveryCosts20.Split('!').ToList();
                List<string> port_40fts = port.DeliveryCosts40.Split('!').ToList();

                //get index of current coo
                var myCoo = port_coos.Where(m => m == coo).FirstOrDefault();

                if (myCoo == null )
                {

                }

                else
                {
                    int port_coo_index = port_coos.IndexOf(myCoo);

                    double twentyFootCosts = double.Parse(port_20fts[port_coo_index]); //get 20ft shipping costs
                    double fortFootCosts = double.Parse(port_40fts[port_coo_index]);//get 40ft shipping costs

                    //sort into pallets
                    //sort into 20ft
                    //sort into 40ft

                    //sorting items into pallets
                    foreach (var itemToShip in equipmentToShip)
                    {
                        index = equipmentToShip.IndexOf(itemToShip);
                        itemToShip.CalculateSpaceUsed();
                        pallet.CheckCapacity();

                        while (itemToShip.Amount > 0) //while there are items that are not sorted into pallets
                        {
                            if (!pallet.MaxedOut && pallet.PercentageFilled + itemToShip.palletSpaceUsed <= 1) //if the pallet has room for another equipment item
                            {
                                //add 1 item to the pallet
                                EquipmentItem eq = new EquipmentItem();

                                eq.Populate(itemToShip);
                                eq.CalculateSpaceUsed();
                                eq.Amount = 1;
                                pallet.Equipment.Add(eq);
                                pallet.Name = itemToShip.Name;
                                pallet.PalletPer20ft = itemToShip.PalletPer20ft;
                                pallet.PercentageFilled += eq.palletSpaceUsed;

                                itemToShip.Amount--; //remove 1 item from the list
                            }

                            else
                            {

                                //if the pallet is full add to list of pallets
                                pallets.Add(pallet);
                                pallet = new Pallet();
                            }


                            if (!pallet.MaxedOut && itemToShip.Amount <= 0)
                            {
                                //the pallet is not full, but it is the last item and therefore must be packed
                                pallets.Add(pallet);
                                pallet = new Pallet();
                            }


                        }

                    }

                    //sort pallets into 20fts
                    int itemsToSort = pallets.Count;

                    index = 0;

                    while (itemsToSort > 0)
                    {
                        var item = pallets[index];

                        container.CheckCapacity();
                        item.CalculateSpaceUsed();
                        if (!container.MaxedOut && container.PercentageFilled + item.ContainerSpaceUsed <= 1.00)
                        {

                            container.CountryOfOrigin = coo;
                            container.Pallets.Add(item);
                            container.FreighCost = twentyFootCosts;
                            container.PercentageFilled += item.ContainerSpaceUsed;
                            itemsToSort--;
                            index++;

                        }
                        else //if there is no more space to add a pallet
                        {
                            countryContainers.Add(container);
                            container = new Container();
                            container.Size = ContainerType.ft20;
                            container.FreighCost = twentyFootCosts;


                        }

                        if (!container.MaxedOut && itemsToSort == 0)//
                        {
                            countryContainers.Add(container);
                            container = new Container();
                            container.Size = ContainerType.ft20;


                        }
                    }


                    Container _container = new Container();
                    _container.Size = ContainerType.ft40;
                    _container.FreighCost = fortFootCosts;
                    int twentyAmount = countryContainers.Count;
                    index = 0;
                    countryContainers = countryContainers.OrderByDescending(m => m.PercentageFilled).ToList();


                    while (twentyAmount > 0)
                    {
                        var item = countryContainers[index];
                        item.CheckCapacity();
                        _container.CheckCapacity();

                        if (!_container.MaxedOut && _container.PercentageFilled + item.PercentageFilled / 2.00 <= 1.00)
                        {
                            _container.Pallets.AddRange(item.Pallets);
                            _container.PercentageFilled += item.PercentageFilled / 2.0;
                            _container.CountryOfOrigin = item.CountryOfOrigin;
                            _container.FreighCost = fortFootCosts;
                            index++;
                            twentyAmount--;
                        }

                        else
                        {
                            order.Containers.Add(_container);
                            _container = new Container()
                            {
                                Size = ContainerType.ft40,
                                FreighCost = fortFootCosts
                            };
                        }


                        if (twentyAmount == 0) //last container
                        {
                            _container.CheckCapacity();
                            if (_container.PercentageFilled <= 0.5) //make it a 20ft
                            {
                                _container.Size = ContainerType.ft20;
                                _container.PercentageFilled *= 2.0;
                                _container.FreighCost = twentyFootCosts;
                            }

                            else
                            {
                                // leave it a 40ft
                            }
                            order.Containers.Add(_container);
                            _container = new Container()
                            {
                                Size = ContainerType.ft40,
                                FreighCost = fortFootCosts
                            };

                        }
                    }
              
            }


            }

          
    


            #endregion
        }
    }
}