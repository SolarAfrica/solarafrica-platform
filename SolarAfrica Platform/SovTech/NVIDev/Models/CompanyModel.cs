﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NVIDev.Models
{
    public class CompanyModel
    {
        [Key]
        public decimal CompanyID { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Organization name")]
        [Remote("doesCompanyExist", "Account", HttpMethod = "POST", ErrorMessage = "Company Name already exists. Please enter a different Company Name.")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Address")]
        public string CompanyAddress { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Main Contact Number")]
        public string CompanyTel { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "WebSite")]
        public string CompanyWebSite { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "Company Registration No")]
        public string CompanyRegistrationNo { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "City")]
        public string City { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "Min Size KWP")]
        public string MinSizeKWP { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "Max Size KWP")]
        public string MaxSizeKWP { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "Company Icon")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase CompanyLogo { get; set; }

        [Display(Name = "Accept Term")]
        public bool IsAcceptTerm { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }

    public class Company
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string PostalCode { get; set; }
        public string CompanyTel { get; set; }
        public string CompanyWebSite { get; set; }
        public string CompanyRegistrationNo { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string MinSizeKWP { get; set; }
        public string MaxSizeKWP { get; set; }
        public byte[] CompanyLogo { get; set; }
        public Nullable<bool> IsAcceptTerm { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}