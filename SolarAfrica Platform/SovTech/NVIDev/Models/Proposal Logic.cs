﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf.draw;
using System.Web.UI.DataVisualization.Charting;
using MyColor = System.Drawing.Color;
using System.Xml;
using System.Text;
namespace NVIDev.Models
{
    public static class Proposal_Logic
    {
        static ApplicationDbContext db = new ApplicationDbContext();
        #region Font Sizes
        public static float BigText = 20f;
        public static float HeadText = 12;
        public static float MidText1 = 9;
        public static float MidText2 = 8;

        public static string[] alphabet = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
        #endregion

        #region Colors
        public static BaseColor nviDarkBlue = new BaseColor(60, 83, 111);
        public static BaseColor nviBlue = new BaseColor(149, 169, 203);
        public static BaseColor nviLightBLue = new BaseColor(227, 228, 230);
        public static BaseColor nviMidBLue = new BaseColor(145, 167, 196);
        public static BaseColor nviSemiGrey = new BaseColor(218, 225, 235);
        public static BaseColor nviGreen = new BaseColor(119, 161, 64);
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="proposalInput"></param>
        /// <returns></returns>
        public static MemoryStream Proposal(ProposalInputModel proposalInput)
        {
            proposalInput.project = db.Projects.Find(proposalInput.ProjectID);
            proposalInput.superProject = new SuperProject(proposalInput.project);
            proposalInput.preparer = db.Users.Find(proposalInput.preparerID);
            MemoryStream stream = new MemoryStream(); //output stream
            Document document = new Document(); //pdf document to write
            PdfWriter pdfwriter = PdfWriter.GetInstance(document, stream);

            document.Open();
            PdfReader pdfreader;
            double LCOE = 0.0;
            double verionInfo = 0.1;
            int offerCount = 0;

            #region Addincludemes Cheat
            if (proposalInput.TechData.SubSections.Where(m => m.IncludeMe == true).Count() > 0)
            {
                proposalInput.TechData.IncludeMe = true;
            }
            if (proposalInput.DataSheets.SubSections.Where(m => m.IncludeMe == true).Count() > 0)
            {
                proposalInput.DataSheets.IncludeMe = true;
            }
            if (proposalInput.SupplemData.SubSections.Where(m => m.IncludeMe == true).Count() > 0)
            {
                proposalInput.SupplemData.IncludeMe = true;
            }
            #endregion

            ///ORDER OF EVENTS
            ///SORT AND GENERATE COVER SHEET, WITH USER PICTURE ECT
            ///SORT AND GENERATE CONTENTS PAGE - with correct page heading numbers A-A4 ect
            ///Order of pages default:
            ///A
            ///1 project overview
            ///2 system proposal
            ///3 Financial analysis
            ///4 financed offers, PPA, PBS, GEN rental
            ///.
            ///B TECHNICAL DATA
            ///1 Bill of quantitites
            ///2 System specifications
            ///3 Single line diagram -user
            ///4 Program of works -user
            ///.
            ///C VISUAL OVERLAYS & DESIGN -ALL user
            ///1 Site layouts
            ///2 3D model rendering
            ///3 - 5 Design drawings
            ///.
            ///D DATA SHEETS & YIELD ANALYSIS - ALL User
            ///1 PVSYS YIELD MODULES
            ///2 MODULE DATA SHEETS
            ///3 INVERTER DATA SHEETS
            ///4 FRAMING DATA SHEETS
            ///.
            ///E SUPPLEMTARY DATA - ALL USER
            ///-items to be added by user

            if (proposalInput.ProposalType != "Finance Only")
            {
                //if the proposal type is Sales
                #region Generate  Proposal

                ////---------------------------------START of Generate COVER PAGE----------------------------------------------------////
                #region Generate Cover Page
                if (proposalInput.alterCover != null && proposalInput.alterCover.ContentType.Contains("pdf"))
                {
                    byte[] newCover = validPDFPage(proposalInput.alterCover);
                    pdfreader = new PdfReader(newCover);
                    PdfContentByte conByte = pdfwriter.DirectContent;
                    conByte.AddTemplate(pdfwriter.GetImportedPage(pdfreader, 1), 1f, 0, 0, 1f, 0, 0);
                    document.NewPage();
                }
                else
                {
                    //space before will determine line height
                    //leading is the line height per line
                    //line indent is horizontal space before

                    //background image last
                    string solarimagPATH = "";
                    solarimagPATH = System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/Cover full.png");
                    if (proposalInput.Type == "Financed - Direct")
                    {
                        solarimagPATH = System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/Cover full financed.png");
                    }

                    Image cover_imgCenter = Image.GetInstance(solarimagPATH);

                    //cover_imgCenter.ScaleAbsolute((document.PageSize.Width - 2 * document.RightMargin + 20), (document.PageSize.Height - (document.BottomMargin + 250 + document.TopMargin)));
                    //cover_imgCenter.ScalePercent(80);
                    cover_imgCenter.ScaleAbsolute(document.PageSize.Width - 2 * (document.LeftMargin - 16f), document.PageSize.Height - (181 + document.BottomMargin));
                    cover_imgCenter.Alignment = Image.UNDERLYING;
                    cover_imgCenter.SetAbsolutePosition(document.LeftMargin - 17, 167 + document.BottomMargin);
                    document.Add(cover_imgCenter);

                    //if the user has a pic to add to the quote
                    if (proposalInput.preparer.Image != null && proposalInput.preparer.Image.Length > 0)
                    {
                        try
                        {
                            Image cover_profileImage = Image.GetInstance(proposalInput.preparer.Image);
                            cover_profileImage.ScaleToFit(150f, 150f);
                            cover_profileImage.SetAbsolutePosition(document.Right - cover_profileImage.ScaledWidth, document.Bottom);
                            cover_profileImage.Alignment = Image.UNDERLYING;
                            document.Add(cover_profileImage);
                        }
                        catch (Exception)
                        {
                            //dont add image for some reason
                        }
                    }

                    //first back gray, then solar then main right
                    Paragraph cover_Heading = new Paragraph(94, proposalInput.CoverPageHeader.ToUpper(), new Font(Font.FontFamily.HELVETICA, 30f, Font.BOLD, nviDarkBlue));
                    cover_Heading.Alignment = Image.ALIGN_TOP;
                    cover_Heading.FirstLineIndent = 140f;

                    document.Add(cover_Heading);
                    BaseColor nviSuperDark = new BaseColor(40, 55, 74);

                    if (proposalInput.Type == "Financed - Direct")
                    {
                        int indent = 158;
                        Paragraph Scover_Heading = new Paragraph("FINANCED SOLAR SOLUTIONS", new Font(Font.FontFamily.HELVETICA, BigText, Font.BOLD, nviSuperDark));
                        Scover_Heading.Alignment = Image.ALIGN_TOP;
                        Scover_Heading.IndentationLeft = indent;
                        Scover_Heading.SpacingBefore = 435;
                        Paragraph S2cover_Heading = new Paragraph("COMMERCIAL OFFER", new Font(Font.FontFamily.HELVETICA, HeadText - 0.5f, Font.NORMAL, nviSuperDark));
                        S2cover_Heading.Alignment = Image.ALIGN_TOP;
                        S2cover_Heading.IndentationLeft = indent;
                        document.Add(Scover_Heading);
                        document.Add(S2cover_Heading);

                        Chunk cover_linebreak = new Chunk(new LineSeparator(1.5f, 106, BaseColor.GRAY, Image.ALIGN_CENTER, -17));

                        document.Add(cover_linebreak);

                        Paragraph cover_subheading = new Paragraph("FOR:", new Font(Font.FontFamily.HELVETICA, HeadText - 1, Font.BOLD, BaseColor.LIGHT_GRAY));
                        cover_subheading.FirstLineIndent = 0;
                        cover_subheading.SpacingBefore = 15;
                        cover_subheading.IndentationLeft = -15f;
                        document.Add(cover_subheading);
                    }

                    else
                    {
                        Chunk cover_linebreak = new Chunk(new LineSeparator(1f, 106, BaseColor.GRAY, Image.ALIGN_CENTER, -422));
                        document.Add(cover_linebreak);
                        Paragraph cover_subheading = new Paragraph("FOR:", new Font(Font.FontFamily.HELVETICA, HeadText - 1, Font.BOLD, BaseColor.LIGHT_GRAY));
                        cover_subheading.FirstLineIndent = 0;
                        cover_subheading.SpacingBefore = 420f;
                        cover_subheading.IndentationLeft = -15;
                        document.Add(cover_subheading);
                    }

                    Paragraph cover_companyName = new Paragraph(proposalInput.project.Details.CompanyName.ToUpper() + ".", new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD, nviDarkBlue));
                    cover_companyName.FirstLineIndent = 0;
                    cover_companyName.SpacingBefore = -6;
                    cover_companyName.IndentationLeft = -15;
                    document.Add(cover_companyName);

                    Paragraph cover_companyNameSmall = new Paragraph(proposalInput.project.Details.CompanyName.ToUpper() + ".", new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD, nviDarkBlue));
                    cover_companyNameSmall.FirstLineIndent = 0;
                    cover_companyNameSmall.SpacingBefore = 2;
                    cover_companyNameSmall.IndentationLeft = -15;
                    document.Add(cover_companyNameSmall);

                    string address = string.Format("{0},{1}.", proposalInput.project.Details.P_Road.ToUpper(), proposalInput.project.Details.P_Country.ToUpper());
                    Paragraph cover_companyAddress = new Paragraph(address, new Font(Font.FontFamily.HELVETICA, 11, Font.ITALIC, BaseColor.LIGHT_GRAY));
                    cover_companyAddress.FirstLineIndent = 0;
                    cover_companyAddress.SpacingBefore = -3;
                    cover_companyAddress.IndentationLeft = -15;
                    document.Add(cover_companyAddress);

                    Paragraph cover_dateLine = new Paragraph(DateTime.Now.ToString("MMMM dd yyyy"), new Font(Font.FontFamily.HELVETICA, 11, Font.ITALIC, nviDarkBlue));
                    cover_dateLine.FirstLineIndent = 0;
                    cover_dateLine.SpacingBefore = 2;
                    cover_dateLine.IndentationLeft = -15;
                    document.Add(cover_dateLine);

                    Paragraph cover_versionLine = new Paragraph("Version :" + proposalInput.project.Revision.ToString("0.##"), new Font(Font.FontFamily.HELVETICA, 11, Font.BOLDITALIC, BaseColor.LIGHT_GRAY));
                    cover_versionLine.FirstLineIndent = 0;
                    cover_versionLine.SpacingBefore = -4;
                    cover_versionLine.IndentationLeft = -15;
                    document.Add(cover_versionLine);
                }

                #endregion
                ////---------------------------------------------------------END of Generate COVER PAGE----------------------------------------------------////

                ////---------------------------------START of Generate Blank Title PAGE----------------------------------------------------////
                #region Generate Blank Title Page
                document.NewPage();
                Paragraph oParaForblankspace = new Paragraph(" ");
                document.Add(oParaForblankspace);

                PdfPTable blankPageTitle_table = new PdfPTable(new float[] { 220 });
                blankPageTitle_table.LockedWidth = true;
                blankPageTitle_table.TotalWidth = 250;
                blankPageTitle_table.SpacingBefore = 5;
                blankPageTitle_table.HorizontalAlignment = Image.ALIGN_RIGHT;

                PdfPTable pagetitleNest = new PdfPTable(1);

                PdfPCell blankPage_tblCompanyname = new PdfPCell(new Phrase(proposalInput.project.Details.CompanyName, new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                blankPage_tblCompanyname.Border = Image.NO_BORDER;
                blankPage_tblCompanyname.BackgroundColor = nviLightBLue;
                blankPage_tblCompanyname.Padding = 0;
                pagetitleNest.AddCell(blankPage_tblCompanyname);

                PdfPCell blankPage_tblProjectname = new PdfPCell(new Phrase(proposalInput.project.Details.ProjectName + " | " + proposalInput.project.Details.P_Road + ", " + proposalInput.project.Details.P_City, new Font(Font.FontFamily.HELVETICA, MidText1 - 1, Font.NORMAL, nviDarkBlue)));
                blankPage_tblProjectname.Border = Image.NO_BORDER;
                blankPage_tblProjectname.BackgroundColor = nviLightBLue;
                blankPage_tblProjectname.Padding = 0;
                pagetitleNest.AddCell(blankPage_tblProjectname);

                PdfPCell blankPage_tbldateandVersion = new PdfPCell(new Phrase(DateTime.Now.ToString("dd MMM yyyy") + " | Version " + verionInfo.ToString(), new Font(Font.FontFamily.HELVETICA, MidText1 - 1, Font.NORMAL, nviDarkBlue)));
                blankPage_tbldateandVersion.Border = Image.NO_BORDER;
                blankPage_tbldateandVersion.BackgroundColor = nviLightBLue;
                blankPage_tbldateandVersion.Padding = 0;
                pagetitleNest.AddCell(blankPage_tbldateandVersion);

                blankPageTitle_table.AddCell(new PdfPCell(pagetitleNest) { Border = Image.NO_BORDER, Padding = 4, PaddingBottom = 7, PaddingLeft = 10, BackgroundColor = nviLightBLue });
                document.Add(blankPageTitle_table);

                Image blankPageTitleImage = Image.GetInstance(proposalInput.preparer.Image);
                blankPageTitleImage.ScaleToFit(150f, 150f);
                blankPageTitleImage.SetAbsolutePosition(document.Right - blankPageTitleImage.ScaledWidth, document.Bottom);
                blankPageTitleImage.Alignment = Image.UNDERLYING;
                document.Add(blankPageTitleImage);
                #endregion
                ////---------------------------------------------------------END of Generate Blank Title PAGE----------------------------------------------------////

                ////---------------------------------START of Generate CONTENTS PAGE----------------------------------------------------////
                #region Generate Contents Page
                //The cotent page
                //It has a left most tilted title - solar proposal
                //It has a main heading - content
                //it has sub headings Solar Proposal A , Technical Data B, Visual Overlays and Design C, Data sheets and yield analysis D, and supp e
                //Solar Proposal A
                //Overview A1
                //System Prop A2
                //Financial analysis A3
                //solar offers presented
                //ppa
                //pbs
                //generator
                document.NewPage();
                //load side image
                string content_sidePath = "";
                content_sidePath = System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/Side Image.png");

                Paragraph cParaNest = new Paragraph();
                cParaNest.IndentationLeft = -33;
                PdfPTable contentsPTable = new PdfPTable(new float[] { 140, 600 });
                contentsPTable.TotalWidth = document.PageSize.Width - 2 * document.RightMargin;
                contentsPTable.LockedWidth = true;

                PdfPCell leftImage = new PdfPCell(FCell("SOLAR PROPOSAL", BigText + 5, false, false, true, nviDarkBlue));
                leftImage.Rotation = 90;
                leftImage.PaddingLeft = 32;
                leftImage.PaddingTop = 208;
                leftImage.PaddingBottom = 270;
                leftImage.HorizontalAlignment = Image.ALIGN_CENTER;
                PdfPCell rightNest = new PdfPCell() { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER };
                rightNest.PaddingLeft = 120;
                rightNest.PaddingRight = -30;
                rightNest.PaddingTop = 123;               

                //first back gray, then solar then main right
                Paragraph content_Heading = new Paragraph("CONTENTS", new Font(Font.FontFamily.HELVETICA, 25f, Font.BOLD, nviDarkBlue));
                //content_Heading.Alignment =  Image.LEFT_ALIGN;
                content_Heading.Alignment = Image.ALIGN_LEFT;
                content_Heading.IndentationLeft = -20;
                rightNest.AddElement(content_Heading);

                //order of business - Item, empty, letter.............repeat
                int currentIndex = 1;
                string[] letters = new string[] { "A", "B", "C", "D", "E", "F", "G" };
                int letINdex = 0;
                string currentLetter = letters[letINdex];

                string[] HeaderIndexes = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09" };
                currentLetter = HeaderIndexes[letINdex];

                int subIndex = 1;
                int offerIndex = 3;

                PdfPTable content_table = new PdfPTable(new float[] { 253, 18, 27 });
                content_table.TotalWidth = 360;
                content_table.SpacingBefore = 32f;
                content_table.LockedWidth = true;
                content_table.HorizontalAlignment = Image.ALIGN_RIGHT;
                content_table.AddCell(ContentCell("COMMERCIAL OFFER", CellInfo.Header));
                content_table.AddCell(ContentCell(null, CellInfo.Empty));
                content_table.AddCell(ContentCell("" + currentLetter, CellInfo.Letter));
                content_table.AddCell(new PdfPCell(new Phrase(" ")) { Padding = 1.5f, Colspan = 3, Border = Image.NO_BORDER });
                content_table.AddCell(ContentCell("OVERVIEW", CellInfo.Subheader));
                content_table.AddCell(ContentCell(null, CellInfo.Empty));
                content_table.AddCell(ContentCell(("" + currentIndex), CellInfo.Subletter));
                currentIndex++;

                if (proposalInput.Type != "Financed - Direct" && proposalInput.Type != "Financed - Indirect")
                {
                    content_table.AddCell(ContentCell("SYSTEM PROPOSAL", CellInfo.Subheader));
                    content_table.AddCell(ContentCell(null, CellInfo.Empty));
                    content_table.AddCell(ContentCell(("" + currentIndex), CellInfo.Subletter)); currentIndex++;

                    content_table.AddCell(ContentCell("FINANCIAL ANALYSIS", CellInfo.Subheader));
                    content_table.AddCell(ContentCell(null, CellInfo.Empty));
                    content_table.AddCell(ContentCell(("" + currentIndex), CellInfo.Subletter)); currentIndex++;
                }

                if (proposalInput.Type != "Sales - Direct")
                {
                    if (proposalInput.IncludeGen || proposalInput.IncludePBS || proposalInput.IncludePPA || proposalInput.IncludeSDS)
                    {
                        letINdex++;
                        currentLetter = letters[letINdex];
                        currentLetter = HeaderIndexes[letINdex];

                        offerIndex = currentIndex;

                        PdfPCell content_cursive = new PdfPCell(new Phrase(" ", new Font(Font.FontFamily.HELVETICA, 10, Font.ITALIC, nviDarkBlue)));

                        content_table.AddCell(ContentCell("SolarAfrica Financed Offers", CellInfo.Header));
                        content_table.AddCell(ContentCell(null, CellInfo.Empty));
                        content_table.AddCell(ContentCell("" + currentLetter, CellInfo.Letter));
                        content_table.AddCell(new PdfPCell(new Phrase(" ")) { Padding = 1.5f, Colspan = 3, Border = Image.NO_BORDER });

                        content_cursive.Border = Image.NO_BORDER;
                        content_cursive.BackgroundColor = BaseColor.WHITE;
                        content_table.AddCell(content_cursive);
                        content_table.AddCell(ContentCell(null, CellInfo.Empty));
                        content_table.AddCell(ContentCell(null, CellInfo.Empty));
                    }

                    #region Dynamic sub sections

                    //dynamically create document
                    //pbs , ppa, genr
                    if (proposalInput.IncludePPA)
                    {
                        offerCount++;
                        content_table.AddCell(ContentCell("SOLAR SERVICES OFFER (PPA)", CellInfo.Subheader));
                        content_table.AddCell(ContentCell(null, CellInfo.Empty));
                        content_table.AddCell(ContentCell(("" + subIndex), CellInfo.Subletter)); subIndex++;
                    }

                    if (proposalInput.IncludePBS)
                    {
                        offerCount++;
                        content_table.AddCell(ContentCell("SOLAR SERVICES OFFER (PBS)", CellInfo.Subheader));
                        content_table.AddCell(ContentCell(null, CellInfo.Empty));
                        content_table.AddCell(ContentCell(("" + subIndex), CellInfo.Subletter)); subIndex++;
                    }

                    if (proposalInput.IncludeGen)
                    {
                        content_table.AddCell(ContentCell("GENERATOR RENTAL OFFER", CellInfo.Subheader));
                        content_table.AddCell(ContentCell(null, CellInfo.Empty));
                        content_table.AddCell(ContentCell(("" + subIndex), CellInfo.Subletter)); subIndex++;
                    }

                }

                content_table.AddCell(ContentCell(" ", CellInfo.Row)); //empy row

                if (proposalInput.TechData.IncludeMe && proposalInput.TechData.SubSections.Where(m => m.IncludeMe == true).ToList().Count > 0) //if the user wants techdata
                {
                    currentIndex = 1;
                    letINdex++;
                    currentLetter = letters[letINdex];
                    currentLetter = HeaderIndexes[letINdex];
                    subIndex = 1;


                    content_table.AddCell(ContentCell(proposalInput.TechData.Name, CellInfo.Header));
                    content_table.AddCell(ContentCell(null, CellInfo.Empty));
                    content_table.AddCell(ContentCell(currentLetter, CellInfo.Letter));



                    foreach (var item in proposalInput.TechData.SubSections.Where(m => m.IncludeMe == true).OrderBy(a => a.LetterCode))
                    {
                        content_table.AddCell(ContentCell(item.Name, CellInfo.Subheader));
                        content_table.AddCell(ContentCell(null, CellInfo.Empty));
                        content_table.AddCell(ContentCell(("" + currentIndex), CellInfo.Subletter)); currentIndex++;
                    }
                    content_table.AddCell(ContentCell(" ", CellInfo.Row)); //empy row


                }

                //if (proposalInput.VisualData.IncludeMe && proposalInput.VisualData.SubSections.Where(m => m.IncludeMe == true).ToList().Count > 0) //if the user wants visual data
                //{
                //    currentIndex = 1;
                //    letINdex++;
                //    currentLetter = letters[letINdex];
                //    subIndex = 1;


                //    content_table.AddCell(ContentCell(proposalInput.VisualData.Name, CellInfo.Header));
                //    content_table.AddCell(ContentCell(null, CellInfo.Empty));
                //    content_table.AddCell(ContentCell(currentLetter, CellInfo.Letter));



                //    foreach (var item in proposalInput.VisualData.SubSections.Where(m => m.IncludeMe == true).OrderBy(a => a.LetterCode))
                //    {
                //        content_table.AddCell(ContentCell(item.Name, CellInfo.Subheader));
                //        content_table.AddCell(ContentCell(null, CellInfo.Empty));
                //        content_table.AddCell(ContentCell((currentLetter + currentIndex), CellInfo.Subletter)); currentIndex++;
                //    }

                //    content_table.AddCell(ContentCell(" ", CellInfo.Row)); //empy row

                //}


                if (proposalInput.DataSheets.IncludeMe && proposalInput.DataSheets.SubSections.Where(m => m.IncludeMe == true).ToList().Count > 0) //if the user wants datasheets
                {
                    currentIndex = 1;
                    letINdex++;
                    currentLetter = letters[letINdex];
                    currentLetter = HeaderIndexes[letINdex];
                    subIndex = 1;


                    content_table.AddCell(ContentCell(proposalInput.DataSheets.Name, CellInfo.Header));
                    content_table.AddCell(ContentCell(null, CellInfo.Empty));
                    content_table.AddCell(ContentCell(currentLetter, CellInfo.Letter));



                    foreach (var item in proposalInput.DataSheets.SubSections.Where(m => m.IncludeMe == true).OrderBy(a => a.LetterCode))
                    {
                        content_table.AddCell(ContentCell(item.Name, CellInfo.Subheader));
                        content_table.AddCell(ContentCell(null, CellInfo.Empty));
                        content_table.AddCell(ContentCell(("" + currentIndex), CellInfo.Subletter)); currentIndex++;
                    }

                    content_table.AddCell(ContentCell(" ", CellInfo.Row)); //empy row

                }

                if (proposalInput.SupplemData.IncludeMe && proposalInput.SupplemData.SubSections.Where(m => m.IncludeMe == true).ToList().Count > 0) //if the user wants supplementary info
                {
                    currentIndex = 1;
                    letINdex++;
                    currentLetter = letters[letINdex];
                    currentLetter = HeaderIndexes[letINdex];
                    subIndex = 1;

                    content_table.AddCell(ContentCell(proposalInput.SupplemData.Name, CellInfo.Header));
                    content_table.AddCell(ContentCell(null, CellInfo.Empty));
                    content_table.AddCell(ContentCell(currentLetter, CellInfo.Letter));

                    foreach (var item in proposalInput.SupplemData.SubSections.Where(m => m.IncludeMe == true).OrderBy(a => a.LetterCode))
                    {
                        content_table.AddCell(ContentCell(item.Name, CellInfo.Subheader));
                        content_table.AddCell(ContentCell(null, CellInfo.Empty));
                        content_table.AddCell(ContentCell(("" + currentIndex), CellInfo.Subletter)); currentIndex++;
                    }
                }
                    #endregion
                rightNest.AddElement(content_table);

                contentsPTable.AddCell(leftImage);
                contentsPTable.AddCell(rightNest);
                cParaNest.Add(contentsPTable);
                document.Add(cParaNest);


                #endregion
                ////---------------------------------------------------------END of Generate CONTENTS PAGE----------------------------------------------------////

                letINdex = 0;
                currentIndex = 1;
                currentLetter = letters[letINdex];
                currentLetter = HeaderIndexes[letINdex];

                ////---------------------------------START of Generate SECTION A----------------------------------------------------////
                #region Generate Section A

                #region Generate Overview Page A1
                document.NewPage();
                //generate the page heading table ======= header, space , letter

                Paragraph oPageContainer = new Paragraph(" ");
                document.Add(oPageContainer);

                PdfPTable overview_header_table = HeaderTable(new float[] { 560, 50, 70 });
                overview_header_table.PaddingTop = 0;
                overview_header_table.SpacingBefore = 17;

                overview_header_table.AddCell(new PdfPCell(FCell("OVERVIEW", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
                overview_header_table.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
                overview_header_table.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
                //overview_header_table.AddCell(new PdfPCell(FCell(currentLetter, BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
                document.Add(overview_header_table);
                Paragraph overview_subheading = new Paragraph("PROPOSAL SUMMARY", new Font(Font.FontFamily.HELVETICA, BigText - 3, Font.BOLD, nviDarkBlue));

                document.Add(overview_subheading);

                Paragraph overview_content_1 = new Paragraph();
                overview_content_1.FirstLineIndent = 0;
                overview_content_1.SetLeading(4, 0.8f);
                overview_content_1.SpacingBefore = 5;
                //Phrase overview_content_1_line = new Phrase("We are pleased to present our offer for the engineering, procurement, installation and commissioning of a solar photovoltaic system based on the specifications detailed below.\nThe contract price (the “Contract Price”) qouted is for the works described in the general scope of works document attached under Section F. We have also taken the opportunity to include our financed and alternative energy supply offers for your consideration below.", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue));
                Phrase overview_content_1_line = new Phrase(proposalInput.CommercialOffer, new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue));
                overview_content_1.Add(overview_content_1_line);
                document.Add(overview_content_1);

                ////Added by Prashant
                PdfPTable tbl_offerandsite = new PdfPTable(new float[] { 30f, 30f });
                tbl_offerandsite.TotalWidth = 500;
                tbl_offerandsite.LockedWidth = true;
                tbl_offerandsite.HorizontalAlignment = Image.ALIGN_LEFT;
                tbl_offerandsite.PaddingTop = 0;
                tbl_offerandsite.SpacingBefore = 20;
                tbl_offerandsite.SpacingAfter = 2;

                PdfPCell offerto_caption = MyCell("OFFER TO:", MidText1 + 1, false, false, false);
                offerto_caption.Padding = 7;
                offerto_caption.PaddingTop = 4;
                offerto_caption.HorizontalAlignment = Image.ALIGN_LEFT;
                offerto_caption.VerticalAlignment = Image.ALIGN_LEFT;
                PdfPCell offerto_details = MyCell(proposalInput.project.Details.Title + ". " + proposalInput.project.Details.Surname + " | " + proposalInput.project.Details.CompanyName + " | " + proposalInput.project.Details.P_Road + ", " + proposalInput.project.Details.P_City, MidText1 + 1, false, false, false);
                offerto_details.Padding = 7;
                offerto_details.PaddingTop = 4;
                offerto_details.HorizontalAlignment = Image.ALIGN_LEFT;
                offerto_details.VerticalAlignment = Image.ALIGN_LEFT;

                PdfPCell site_caption = MyCell("SITE:", MidText1 + 1, false, false, false);
                site_caption.Padding = 7;
                site_caption.PaddingTop = 4;
                site_caption.HorizontalAlignment = Image.ALIGN_LEFT;
                site_caption.VerticalAlignment = Image.ALIGN_LEFT;
                PdfPCell site_details = MyCell(proposalInput.project.Details.P_Site + " | " + proposalInput.project.Details.P_Road + ", " + proposalInput.project.Details.P_City, MidText1 + 1, false, false, false);
                site_details.Padding = 7;
                site_details.PaddingTop = 4;
                site_details.HorizontalAlignment = Image.ALIGN_LEFT;
                site_details.VerticalAlignment = Image.ALIGN_LEFT;

                tbl_offerandsite.AddCell(offerto_caption);
                tbl_offerandsite.AddCell(site_caption);
                tbl_offerandsite.AddCell(offerto_details);
                tbl_offerandsite.AddCell(site_details);

                document.Add(tbl_offerandsite);
                ////End of Added by Prashant

                #region Overview table
                PdfPTable overview_table_1 = new PdfPTable(new float[] { 30f, 15, 20 });
                overview_table_1.HorizontalAlignment = Image.ALIGN_LEFT;
                overview_table_1.SpacingBefore = 6;
                overview_table_1.LockedWidth = true;
                overview_table_1.TotalWidth = 80 + document.PageSize.Width / 2;
                PdfPCell row4 = new PdfPCell(new Phrase(" ", new Font(Font.FontFamily.HELVETICA, 2)));
                row4.Padding = 1;
                row4.Colspan = 3;
                row4.Border = Image.NO_BORDER;

                //FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });
                overview_table_1.AddCell(new PdfPCell(FCell("ND", MidText2, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });

                ////Nominal Power
                Phrase bigSmallPhrase = new Phrase("Nominal Power (kWp", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
                bigSmallPhrase.Add(new Phrase("DC", new Font(Font.FontFamily.HELVETICA, MidText2 - 2, Font.NORMAL, nviDarkBlue)));
                bigSmallPhrase.Add(new Phrase(")", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue)));
                //kilowats produced
                PdfPCell overview_row_NP_label = new PdfPCell(bigSmallPhrase) { PaddingTop = 0, PaddingBottom = 4 };
                PdfPCell overview_row_NP_value = new PdfPCell(new Phrase((proposalInput.superProject.prosize * 1000).ToString("#,##0.##"), new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, BackgroundColor = nviLightBLue };
                overview_row_NP_value.HorizontalAlignment = Image.ALIGN_RIGHT;
                overview_table_1.AddCell(overview_row_NP_label);
                overview_table_1.AddCell(overview_row_NP_value);
                overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });

                ////Energy Produced
                bigSmallPhrase = new Phrase("Energy Production (kWh", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
                bigSmallPhrase.Add(new Phrase("AC", new Font(Font.FontFamily.HELVETICA, MidText2 - 2, Font.NORMAL, nviDarkBlue)));
                bigSmallPhrase.Add(new Phrase(")", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue)));
                PdfPCell overview_row_EP_label = new PdfPCell(bigSmallPhrase) { PaddingTop = 0, PaddingBottom = 4 };
                PdfPCell overview_row_EP_value = new PdfPCell(new Phrase((proposalInput.superProject.prosize * 1000 * proposalInput.project.Costing.Yield).ToString("#,##0.##"), new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, BackgroundColor = nviLightBLue }; //KW * yield
                overview_row_EP_value.HorizontalAlignment = Image.ALIGN_RIGHT;
                overview_table_1.AddCell(overview_row_EP_label);
                overview_table_1.AddCell(overview_row_EP_value);
                overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });

                //Energy Yield
                bigSmallPhrase = new Phrase("Energy Yield (KWh / kWp", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
                bigSmallPhrase.Add(new Phrase("DC", new Font(Font.FontFamily.HELVETICA, MidText2 - 2, Font.NORMAL, nviDarkBlue)));
                bigSmallPhrase.Add(new Phrase(")", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue)));
                PdfPCell overview_row_EY_label = new PdfPCell(bigSmallPhrase) { PaddingTop = 0, PaddingBottom = 4 };
                PdfPCell overview_row_EY_value = new PdfPCell(new Phrase((proposalInput.project.Costing.Yield).ToString("#,##0.##"), new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, BackgroundColor = nviLightBLue }; //KW * yield
                overview_row_EY_value.HorizontalAlignment = Image.ALIGN_RIGHT;
                overview_table_1.AddCell(overview_row_EY_label);
                overview_table_1.AddCell(overview_row_EY_value);
                overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });

                double contractPriceBeforeDuitesInCCY = ((proposalInput.superProject.TotalEq_BA + proposalInput.superProject.TotalEq_A) - (proposalInput.superProject.TotalDutiesA + proposalInput.superProject.TotalDutiesBa));
                double totalDuties = (proposalInput.superProject.TotalDutiesA + proposalInput.superProject.TotalDutiesBa);
                //overview_table_1.AddCell(new PdfPCell(row4) { Padding = 2 });

                ////-------------------------------------------Add remove according to include price checkbox-----------------------------------------------////
                if (proposalInput.IncludePrice)
                {
                    ////Contract Price
                    PdfPCell overview_row_CPBD_label = new PdfPCell(new Phrase("Contract Price (Before Duties) ", new Font(Font.FontFamily.HELVETICA, MidText1, Font.BOLD, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 };
                    PdfPCell overview_row_CPBD_value = new PdfPCell(new Phrase((contractPriceBeforeDuitesInCCY).ToString("#,##0.##"), new Font(Font.FontFamily.HELVETICA, MidText1, Font.BOLD, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, BackgroundColor = nviLightBLue }; //KW * yield
                    //PdfPCell overview_row_CPBD_sign = new PdfPCell(new Phrase(proposalInput.superProject.Country.Currency, new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 }; //KW * yield
                    overview_row_CPBD_value.HorizontalAlignment = Image.ALIGN_RIGHT;
                    //overview_row_CPBD_sign.Border = Image.NO_BORDER;
                    overview_row_CPBD_label.BorderWidthBottom = 1;
                    overview_row_CPBD_value.BorderWidthBottom = 1;
                    overview_table_1.AddCell(overview_row_CPBD_label);
                    overview_table_1.AddCell(overview_row_CPBD_value);
                    overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });

                    ////Duties & Import Taxes
                    PdfPCell overview_row_Dut_label = new PdfPCell(new Phrase("Duties & Import Taxes ", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 };
                    PdfPCell overview_row_Dut_value = new PdfPCell(new Phrase((totalDuties).ToString("#,##0.##"), new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, BackgroundColor = nviLightBLue };
                    //PdfPCell overview_row_Dut_sign = new PdfPCell(new Phrase(proposalInput.superProject.Country.Currency, new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 };
                    overview_row_Dut_value.HorizontalAlignment = Image.ALIGN_RIGHT;
                    //overview_row_Dut_sign.Border = Image.NO_BORDER;
                    overview_table_1.AddCell(overview_row_Dut_label);
                    overview_table_1.AddCell(overview_row_Dut_value);
                    overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });
                    //overview_table_1.AddCell(new PdfPCell(row4) { Padding = 2 });

                    //Total Contract Price
                    PdfPCell overview_row_CPUSD_label = new PdfPCell(new Phrase("Total Contract Price (Exl. VAT)", new Font(Font.FontFamily.HELVETICA, MidText1, Font.BOLD, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 };
                    PdfPCell overview_row_CPUSD_value = new PdfPCell(new Phrase((contractPriceBeforeDuitesInCCY + totalDuties).ToString("#,##0.##"), new Font(Font.FontFamily.HELVETICA, MidText1, Font.BOLD, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, BackgroundColor = nviLightBLue };
                    //PdfPCell overview_row_CPUSD_sign = new PdfPCell(new Phrase(proposalInput.superProject.Country.Currency, new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 };
                    overview_row_CPUSD_value.HorizontalAlignment = Image.ALIGN_RIGHT;
                    overview_row_CPUSD_label.BorderWidthBottom = 1;
                    overview_row_CPUSD_value.BorderWidthBottom = 1;
                    overview_table_1.AddCell(overview_row_CPUSD_label);
                    overview_table_1.AddCell(overview_row_CPUSD_value);
                    overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });
                    //overview_table_1.AddCell(new PdfPCell(row4) { Padding = 2 });

                    ////Cost / WpDC
                    bigSmallPhrase = new Phrase("Cost / Wp", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
                    bigSmallPhrase.Add(new Phrase("DC", new Font(Font.FontFamily.HELVETICA, MidText2 - 2, Font.NORMAL, nviDarkBlue)));
                    bigSmallPhrase.Add(new Phrase(" (Excl. Taxes)", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue)));
                    double costPerWattProduced = ((contractPriceBeforeDuitesInCCY + totalDuties) / proposalInput.superProject.prosize) / 1000000.00;
                    PdfPCell overview_row_CPWP_label = new PdfPCell(bigSmallPhrase) { PaddingTop = 0, PaddingBottom = 0 };
                    PdfPCell overview_row_CPWP_value = new PdfPCell(new Phrase((costPerWattProduced).ToString("#,##0.##"), new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, HorizontalAlignment = Image.ALIGN_CENTER, BackgroundColor = nviLightBLue };
                    //PdfPCell overview_row_CPWP_sign = new PdfPCell(new Phrase(proposalInput.superProject.Country.Currency + "/Wp", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 };
                    overview_row_CPWP_value.HorizontalAlignment = Image.ALIGN_RIGHT;
                    //overview_row_CPWP_sign.Border = Image.NO_BORDER;
                    overview_table_1.AddCell(overview_row_CPWP_label);
                    overview_table_1.AddCell(new PdfPCell(overview_row_CPWP_value) { HorizontalAlignment = Image.ALIGN_RIGHT });
                    overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });
                    ////-------------------------------------------Add remove according to include price checkbox-----------------------------------------------////
                }

                overview_table_1.AddCell(new PdfPCell(row4) { Padding = 2 });
                overview_table_1.AddCell(new PdfPCell(row4) { Padding = 2 });
                
                ////MONTHLY PAYMENT OFFERS
                overview_row_NP_label = new PdfPCell(new Phrase("MONTHLY PAYMENT OFFERS:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.BOLD, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 };
                overview_row_NP_value = new PdfPCell(new Phrase("R", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, BackgroundColor = nviLightBLue };
                //PdfPCell overview_row_Dut_sign = new PdfPCell(new Phrase(proposalInput.superProject.Country.Currency, new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 };
                overview_row_NP_value.HorizontalAlignment = Image.ALIGN_RIGHT;
                //overview_row_Dut_sign.Border = Image.NO_BORDER;
                overview_table_1.AddCell(overview_row_NP_label);
                overview_table_1.AddCell(overview_row_NP_value);
                overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });

                ////PBS Lease
                overview_row_NP_label = new PdfPCell(new Phrase("PBS Lease:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 };
                overview_row_NP_value = new PdfPCell(new Phrase("1000pm", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, BackgroundColor = nviLightBLue };
                overview_row_NP_value.HorizontalAlignment = Image.ALIGN_RIGHT;
                PdfPCell overview_row_yousaved = new PdfPCell(new Phrase("You save X: pm", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 };
                overview_row_yousaved.Border = Image.NO_BORDER;
                overview_table_1.AddCell(overview_row_NP_label);
                overview_table_1.AddCell(overview_row_NP_value);
                overview_table_1.AddCell(overview_row_yousaved);

                ////Solar Services
                overview_row_NP_label = new PdfPCell(new Phrase("Solar Services:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 };
                overview_row_NP_value = new PdfPCell(new Phrase("1000pm", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, BackgroundColor = nviLightBLue };
                overview_row_yousaved = new PdfPCell(new Phrase("You save X: pm", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 };
                overview_row_NP_value.HorizontalAlignment = Image.ALIGN_RIGHT;
                overview_row_yousaved.Border = Image.NO_BORDER;
                overview_table_1.AddCell(overview_row_NP_label);
                overview_table_1.AddCell(overview_row_NP_value);
                overview_table_1.AddCell(overview_row_yousaved);

                overview_table_1.AddCell(new PdfPCell(new Phrase("See further details under section B.", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { Border = Image.NO_BORDER, Padding = 1 });
                overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });
                overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });

                //PdfPTable overview_nested_table = new PdfPTable(1);
                //PdfPCell overview_nested_1 = new PdfPCell(new Phrase("Pay-Back Period", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue)));
                //PdfPCell overview_nested_2 = new PdfPCell(new Phrase("Internal Rate of Return (15 Yrs)", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue)));
                //overview_nested_table.AddCell(overview_nested_1);
                //overview_nested_table.AddCell(overview_nested_2);
                //PdfPCell overview_nest_house = new PdfPCell(overview_nested_table);
                //overview_nest_house.Padding = 0;
                //overview_table_1.AddCell(overview_nest_house);
                //PdfPCell overview_nested_3 = new PdfPCell(new Phrase("See Financial Analysis", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue)));
                //overview_nested_3.HorizontalAlignment = Image.ALIGN_CENTER;
                //overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });
                //overview_table_1.AddCell(overview_nested_3);
                //overview_table_1.AddCell(new PdfPCell(new Phrase()) { Border = Image.NO_BORDER, Padding = 1 });
                ////--------------------------------------------------------------////

                Paragraph p = new Paragraph();
                p.IndentationLeft = 7;
                p.Add(overview_table_1);

                ////-------------SYSTEM SPECIFICATIONS-------------////
                ////Instantiating table again
                overview_table_1 = new PdfPTable(new float[] { 25, 25, 25, 25 });
                overview_table_1.HorizontalAlignment = Image.ALIGN_LEFT;
                overview_table_1.SpacingBefore = 6;
                overview_table_1.LockedWidth = true;
                overview_table_1.TotalWidth = 80 + document.PageSize.Width / 2;

                ////SYSTEM SPECIFICATIONS
                overview_row_NP_label = new PdfPCell(new Phrase("SYSTEM SPECIFICATIONS:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.BOLD, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, Colspan=4 };
                overview_row_NP_label.HorizontalAlignment = Image.ALIGN_LEFT;
                overview_row_NP_label.Border = Image.NO_BORDER;
                overview_row_NP_label.BorderWidthBottom = 1;
                overview_table_1.AddCell(overview_row_NP_label);

                ////NOMINAL POWER & NUMBER OF PV MODULES
                overview_table_1 = GlobalCreatePdfTable(overview_table_1, "NOMINAL POWER", "40 8000 kWp");
                overview_table_1 = GlobalCreatePdfTable(overview_table_1, "NUMBER OF PV MODULES", "1 Modules");
                ////INSTALLATION TYPE & ENERGY
                overview_table_1 = GlobalCreatePdfTable(overview_table_1, proposalInput.project.Costing.Type, "Grid-Tied");
                overview_table_1 = GlobalCreatePdfTable(overview_table_1, "ENERGY YIELD", "1 800 kWh / kWp");
                ////MOUNTING SYSTEM & CONNECTION
                overview_table_1 = GlobalCreatePdfTable(overview_table_1, "MOUNTING SYSTEM", "Insert Mounting System");
                overview_table_1 = GlobalCreatePdfTable(overview_table_1, "CONNECTION", "CONNECTION");
                ////SOLAR PRODUCTION & VOLTAGE SUPPLY
                overview_table_1 = GlobalCreatePdfTable(overview_table_1, "SOLAR PRODUCTION", "1 800 kWh per annum");
                overview_table_1 = GlobalCreatePdfTable(overview_table_1, "VOLTAGE SUPPLY", "3 Phase, Low Voltage");

                p.Add(overview_table_1);

                ////Bind contact details for Queries
                overview_table_1 = new PdfPTable(new float[] { 50 });
                overview_table_1.HorizontalAlignment = Image.ALIGN_LEFT;
                overview_table_1.SpacingBefore = 6;
                overview_table_1.LockedWidth = true;
                overview_table_1.TotalWidth = 80 + document.PageSize.Width / 2;
                overview_row_NP_label = new PdfPCell(new Phrase("For queries on this offer please speak to:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, Colspan = 4 };
                overview_row_NP_label.HorizontalAlignment = Image.ALIGN_LEFT;
                overview_row_NP_label.Border = Image.NO_BORDER;
                overview_table_1.AddCell(overview_row_NP_label);
                overview_row_NP_label = new PdfPCell(new Phrase(proposalInput.project.Details.Initial +" "+ proposalInput.project.Details.Surname, new Font(Font.FontFamily.HELVETICA, MidText1, Font.BOLD, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, Colspan = 4 };
                overview_row_NP_label.HorizontalAlignment = Image.ALIGN_LEFT; 
                overview_row_NP_label.Border = Image.NO_BORDER;
                overview_table_1.AddCell(overview_row_NP_label);
                overview_row_NP_label = new PdfPCell(new Phrase(proposalInput.project.Details.CompanyName, new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, Colspan = 4 };
                overview_row_NP_label.HorizontalAlignment = Image.ALIGN_LEFT;
                overview_row_NP_label.Border = Image.NO_BORDER;
                overview_table_1.AddCell(overview_row_NP_label);
                overview_row_NP_label = new PdfPCell(new Phrase(proposalInput.preparer.PhoneNumber, new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, Colspan = 4 };
                overview_row_NP_label.HorizontalAlignment = Image.ALIGN_LEFT;
                overview_row_NP_label.Border = Image.NO_BORDER;
                overview_table_1.AddCell(overview_row_NP_label);
                overview_row_NP_label = new PdfPCell(new Phrase(proposalInput.preparer.Email, new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, Colspan = 4 };
                overview_row_NP_label.HorizontalAlignment = Image.ALIGN_LEFT;
                overview_row_NP_label.Border = Image.NO_BORDER;
                overview_table_1.AddCell(overview_row_NP_label);
                p.Add(overview_table_1);

                /////Adding paragraph in document...very important
                document.Add(p);

                //Paragraph overview_terms = new Paragraph("This offer is subject to the General Conditions stated in Section G.", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
                //document.Add(overview_terms);
                #endregion

                #region About ABC Solar
                PdfPTable abc_solar_table = HeaderTable(new float[] { 560, 50, 70 });
                abc_solar_table.PaddingTop = 0;
                abc_solar_table.SpacingBefore = 17;

                abc_solar_table.AddCell(new PdfPCell(FCell("ABOUT ABC SOLAR", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
                abc_solar_table.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
                abc_solar_table.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
                //overview_header_table.AddCell(new PdfPCell(FCell(currentLetter, BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
                document.Add(abc_solar_table);
                #endregion                
                
                //PdfContentByte foot = pdfwriter.DirectContent;
                //foot.SetFontAndSize(FontFactory.GetFont(FontFactory.HELVETICA).BaseFont, MidText1);
                //foot.SetColorFill(nviDarkBlue);
                //foot.BeginText();
                //foot.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "This Proposal is confidential and intended only for the prospective customer", document.PageSize.Width / 2, document.BottomMargin - 7, 0);
                //foot.EndText();
                #endregion

                if (proposalInput.Type == "Sales - Direct" || proposalInput.Type == "Sales + Financed - Direct")
                {
                    #region Generate Commercial Offer Page
                    document.NewPage();

                    #region Details

                    currentIndex++;
                    PdfPTable ComOffer_header_table = HeaderTable(new float[] { 520, 40, 90 });
                    //Top heading
                    ComOffer_header_table.AddCell(new PdfPCell(HeaderCell("COMMERCIAL OFFER", CellInfo.Header)) { PaddingBottom = 8 });
                    ComOffer_header_table.AddCell(ContentCell(" ", CellInfo.Empty));
                    ComOffer_header_table.AddCell(new PdfPCell(HeaderCell(currentLetter, CellInfo.Letter)) { PaddingBottom = 8 });
                    document.Add(ComOffer_header_table);

                    Paragraph ComOffer_offerTo = new Paragraph(new Phrase("OFFER TO:", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue)));
                    ComOffer_offerTo.SpacingBefore = 25;
                    ComOffer_offerTo.SpacingAfter = -4;
                    ComOffer_offerTo.SetLeading(2, 0.4f);

                    PdfPTable ComOffer_version = new PdfPTable(1);
                    ComOffer_version.TotalWidth = 140;
                    ComOffer_version.LockedWidth = true;
                    ComOffer_version.HorizontalAlignment = Image.ALIGN_RIGHT;
                    ComOffer_version.PaddingTop = 0;
                    ComOffer_version.SpacingBefore = 10;
                    ComOffer_version.SpacingAfter = -50;

                    PdfPCell ComOffer_version_cell = MyCell("Version " + verionInfo.ToString(), HeadText + 4, true, false, true);
                    ComOffer_version_cell.Padding = 7;
                    ComOffer_version_cell.PaddingTop = 4;
                    ComOffer_version_cell.HorizontalAlignment = Image.ALIGN_CENTER;
                    ComOffer_version_cell.VerticalAlignment = Image.ALIGN_CENTER;

                    ComOffer_version.AddCell(ComOffer_version_cell);

                    document.Add(ComOffer_version);
                    document.Add(ComOffer_offerTo);

                    //Table with details of person the proposal is addressed to
                    PdfPTable ComOffer_offerTo_Table = new PdfPTable(new float[] { 270 });
                    ComOffer_offerTo_Table.LockedWidth = true;
                    ComOffer_offerTo_Table.TotalWidth = 290;
                    ComOffer_offerTo_Table.SpacingBefore = 5;
                    ComOffer_offerTo_Table.HorizontalAlignment = Image.ALIGN_LEFT;

                    PdfPTable comONest = new PdfPTable(1);

                    PdfPCell ComOffer_offerTo_Table_person = new PdfPCell(new Phrase(proposalInput.project.Details.Title + ". " + proposalInput.project.Details.Surname, new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue)));
                    ComOffer_offerTo_Table_person.Border = Image.NO_BORDER;
                    ComOffer_offerTo_Table_person.BackgroundColor = nviLightBLue;
                    ComOffer_offerTo_Table_person.Padding = 0;
                    comONest.AddCell(ComOffer_offerTo_Table_person);

                    PdfPCell ComOffer_offerTo_Table_company = new PdfPCell(new Phrase(proposalInput.project.Details.CompanyName, new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    ComOffer_offerTo_Table_company.Border = Image.NO_BORDER;
                    ComOffer_offerTo_Table_company.BackgroundColor = nviLightBLue;
                    ComOffer_offerTo_Table_company.Padding = 0;

                    comONest.AddCell(ComOffer_offerTo_Table_company);

                    PdfPCell ComOffer_offerTo_Table_address = new PdfPCell(new Phrase(proposalInput.project.Details.P_Road + ", " + proposalInput.project.Details.P_City, new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    ComOffer_offerTo_Table_address.Border = Image.NO_BORDER;
                    ComOffer_offerTo_Table_address.BackgroundColor = nviLightBLue;
                    ComOffer_offerTo_Table_address.Padding = 0;

                    comONest.AddCell(ComOffer_offerTo_Table_address);

                    ComOffer_offerTo_Table.AddCell(new PdfPCell(comONest) { Border = Image.NO_BORDER, Padding = 4, PaddingBottom = 7, PaddingLeft = 10, BackgroundColor = nviLightBLue });
                    document.Add(ComOffer_offerTo_Table);
                    #endregion

                    #region LCOE Table
                    Paragraph Comoff_Pleased = new Paragraph(new Phrase("We are pleased to present our proposal for a Solar photovoltaic system, installed at the following Site under our turnkey contract:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue)));
                    Comoff_Pleased.SpacingBefore = 4f;
                    Comoff_Pleased.SetLeading(4, 0.8f);
                    document.Add(Comoff_Pleased);


                    PdfPTable ComOffer_LCOE_Table = new PdfPTable(new float[] { 200, 100, 110 });
                    ComOffer_LCOE_Table.TotalWidth = document.PageSize.Width - 2 * document.RightMargin;
                    ComOffer_LCOE_Table.LockedWidth = true;
                    ComOffer_LCOE_Table.SpacingBefore = 5;


                    PdfPTable nester = new PdfPTable(1);

                    PdfPCell ComOffer_LCOE_Table_siteH = MyCell("SITE:", HeadText - 2, true, false, false);
                    ComOffer_LCOE_Table_siteH.PaddingBottom = 2;

                    PdfPCell ComOffer_LCOE_Table_LCOEH = MyCell("Levelised Cost of Energy:", HeadText - 2, true, false, false);
                    ComOffer_LCOE_Table_LCOEH.PaddingBottom = 0;
                    ComOffer_LCOE_Table_LCOEH.HorizontalAlignment = Image.ALIGN_CENTER;


                    ComOffer_LCOE_Table.AddCell(ComOffer_LCOE_Table_siteH);
                    ComOffer_LCOE_Table.AddCell(ContentCell(" ", CellInfo.Empty));
                    ComOffer_LCOE_Table.AddCell(ComOffer_LCOE_Table_LCOEH);



                    PdfPTable ComOffer_LCOE_Table_Nest_table = new PdfPTable(1);


                    PdfPCell ComOffer_LCOE_Table_Site = MyCell(proposalInput.project.Details.P_Site, MidText1 + 1, true, false, true);
                    ComOffer_LCOE_Table_Site.Padding = 0;
                    PdfPCell ComOffer_LCOE_Table_road = MyCell(proposalInput.project.Details.P_Road, MidText1 + 1, false, false, true);
                    ComOffer_LCOE_Table_road.Padding = 0;


                    ComOffer_LCOE_Table_Nest_table.AddCell(ComOffer_LCOE_Table_Site);
                    ComOffer_LCOE_Table_Nest_table.AddCell(ComOffer_LCOE_Table_road);

                    PdfPCell ComOffer_LCOE_Table_Nest = new PdfPCell(ComOffer_LCOE_Table_Nest_table);
                    ComOffer_LCOE_Table_Nest.Border = Image.NO_BORDER;
                    ComOffer_LCOE_Table_Nest.BackgroundColor = nviLightBLue;
                    ComOffer_LCOE_Table_Nest.Padding = 4;
                    ComOffer_LCOE_Table_Nest.PaddingBottom = 7;
                    ComOffer_LCOE_Table_Nest.PaddingLeft = 10;

                    PdfPCell ComOffer_LCOE_Table_LCOE = MyCell(string.Format("{0} {1} / kWh", proposalInput.superProject.Country.Currency, LCOE.ToString()), HeadText, false, true, false);
                    ComOffer_LCOE_Table_LCOE.HorizontalAlignment = Image.ALIGN_CENTER;
                    ComOffer_LCOE_Table_LCOE.Padding = 7;
                    ComOffer_LCOE_Table_LCOE.PaddingBottom = 5;

                    ComOffer_LCOE_Table.AddCell(ComOffer_LCOE_Table_Nest);
                    ComOffer_LCOE_Table.AddCell(ContentCell(" ", CellInfo.Empty));
                    ComOffer_LCOE_Table.AddCell(ComOffer_LCOE_Table_LCOE);


                    document.Add(ComOffer_LCOE_Table);

                    #endregion

                    #region System Specs Table

                    Paragraph ComOffer_OverviewSysSpecs = new Paragraph("OVERVIEW OF SYSTEM SPECIFICATIONS", new Font(Font.FontFamily.HELVETICA, HeadText - 1, Font.BOLD, nviDarkBlue));
                    ComOffer_OverviewSysSpecs.SpacingBefore = 5;
                    document.Add(ComOffer_OverviewSysSpecs);

                    PdfPTable sysNest = new PdfPTable(1);
                    sysNest.TotalWidth = document.PageSize.Width - 2 * document.RightMargin;
                    sysNest.LockedWidth = true;
                    sysNest.SpacingBefore = 2;

                    PdfPTable ComOffer_SysSpec_Table = new PdfPTable(new float[] { 180, 195, 205, 200 });

                    //NOMINAL POWER
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell("Nominal Power:", true));
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell((proposalInput.superProject.prosize * 1000.00).ToString("#,##") + " kWp", false)); //MW * 1000

                    //NUMBER OF PV MODULES
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell("Number of PV Modules:", true));
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell((proposalInput.superProject._pvmodules.Count).ToString() + " Modules", false));


                    //INSTALLATION TYPE
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell("Installation Type:", true));
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell((proposalInput.project.Costing.Type), false));


                    //ENERGY YIELD
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell("Energy Yield:", true));
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell((proposalInput.project.Costing.Yield).ToString("#,##") + " kWh / kWp", false)); //Y

                    //TODO add mounting system type
                    //MOUNTING SYSTEM
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell("Mounting System:", true));
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell(("Insert Mounting System"), false));


                    //CONNECTION
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell("Connection:", true));
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell("400 V (Low Voltage)", false));

                    //SOLAR PRODUCTION PER ANNUM
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell("Solar Production:", true));
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell((proposalInput.project.Costing.Yield).ToString("#,##") + " kWh per annum", false));

                    //VOLTAGE SUPPLY
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell("Voltage Supply:", true));
                    ComOffer_SysSpec_Table.AddCell(SysSpecCell("Three Phase, Low Voltage", false));

                    sysNest.AddCell(new PdfPCell(ComOffer_SysSpec_Table) { Border = Image.NO_BORDER, Padding = 4, PaddingLeft = 10, PaddingBottom = 7, HorizontalAlignment = Image.ALIGN_CENTER, BackgroundColor = nviLightBLue });
                    document.Add(sysNest);

                    #endregion

                    #region System Costs
                    Paragraph ComOffer_SysCostsH = new Paragraph();

                    ComOffer_SysCostsH.Add(new Phrase("SYSTEM COSTS", new Font(Font.FontFamily.HELVETICA, HeadText - 1, Font.BOLD, nviDarkBlue)));
                    Paragraph syscosH = new Paragraph("\nCosts for the construction & installation of the solar system, delivered on a turnkey basis is set out below. The Contract Price is subject to concluding a " + proposalInput.project.Costing.Term + " year maintenance and monitoring plan. Services included in our maintenance and monitoring plan are set out in the scope of work annexure to this offer.", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
                    syscosH.SetLeading(4, 0.8f);
                    syscosH.SpacingBefore = -10;
                    ComOffer_SysCostsH.Add(syscosH);

                    document.Add(ComOffer_SysCostsH);
                    #endregion

                    #region Contract Price
                    //CONTRACT PRICE (FOR OUTRIGHT PURCHASE)
                    Paragraph ComOffer_ContractPriceH = new Paragraph(new Phrase("CONTRACT PRICE (FOR OUTRIGHT PURCHASE)", new Font(Font.FontFamily.HELVETICA, HeadText - 1, Font.BOLD, nviDarkBlue)));
                    ComOffer_ContractPriceH.SpacingBefore = 3;
                    document.Add(ComOffer_ContractPriceH);


                    PdfPTable ComOffer_contractPriceTable_nesttable = new PdfPTable(1);
                    ComOffer_contractPriceTable_nesttable.TotalWidth = document.PageSize.Width - 2 * document.RightMargin;
                    ComOffer_contractPriceTable_nesttable.LockedWidth = true;
                    ComOffer_contractPriceTable_nesttable.SpacingBefore = 4;


                    PdfPTable ComOffer_contractPriceTable = new PdfPTable(new float[] { 300, 150, 200 });


                    PdfPCell ComOffer_contractPriceTable_TCPL = MyCell("Total Contract Price (Excl. of VAT):*", MidText1, true, false, true);
                    ComOffer_contractPriceTable_TCPL.Padding = 1;
                    PdfPCell ComOffer_contractPriceTable_TCPV = MyCell(proposalInput.superProject.Country.Currency + " " + (proposalInput.superProject.TotalEq_A + proposalInput.superProject.TotalEq_BA).ToString("#,##0.00"), MidText1, true, false, false);
                    ComOffer_contractPriceTable_TCPV.HorizontalAlignment = Image.ALIGN_RIGHT;
                    ComOffer_contractPriceTable_TCPV.Padding = 1;
                    ComOffer_contractPriceTable_TCPV.BackgroundColor = BaseColor.WHITE;
                    ComOffer_contractPriceTable_TCPV.PaddingBottom = 2;


                    PdfPCell ComOffer_contractPriceTable_TCPS = MyCell("(Including Duties & Import Taxes)", MidText1, false, false, true);
                    ComOffer_contractPriceTable_TCPS.Padding = 1;
                    ComOffer_contractPriceTable_TCPS.PaddingTop = 0;
                    ComOffer_contractPriceTable_TCPS.PaddingLeft = 2;
                    ComOffer_contractPriceTable.AddCell(ComOffer_contractPriceTable_TCPL);
                    ComOffer_contractPriceTable.AddCell(ComOffer_contractPriceTable_TCPV);
                    ComOffer_contractPriceTable.AddCell(ComOffer_contractPriceTable_TCPS);

                    PdfPCell ComOffer_contractPriceTable_TCPL_TNC = MyCell("* Subject to concluding a minimum 36 month Maintenance Contract.", MidText1, false, false, true);
                    ComOffer_contractPriceTable_TCPL_TNC.Colspan = 3;
                    ComOffer_contractPriceTable_TCPL_TNC.Padding = 1;
                    ComOffer_contractPriceTable_TCPL_TNC.PaddingTop = 8;
                    ComOffer_contractPriceTable_TCPL_TNC.PaddingBottom = 3;

                    ComOffer_contractPriceTable.AddCell(ComOffer_contractPriceTable_TCPL_TNC);



                    PdfPCell ComOffer_contractPriceTable_nestcell = new PdfPCell(ComOffer_contractPriceTable);
                    ComOffer_contractPriceTable_nestcell.Padding = 7;
                    ComOffer_contractPriceTable_nestcell.PaddingBottom = 7;
                    ComOffer_contractPriceTable_nestcell.PaddingLeft = 10;

                    ComOffer_contractPriceTable_nestcell.BackgroundColor = nviLightBLue;
                    ComOffer_contractPriceTable_nestcell.Border = Image.NO_BORDER;

                    ComOffer_contractPriceTable_nesttable.AddCell(ComOffer_contractPriceTable_nestcell);
                    document.Add(ComOffer_contractPriceTable_nesttable);
                    #endregion

                    #region PREVENTATIVE MAINTENANCE COSTS

                    Paragraph ComOffer_MaintenanceH = new Paragraph(new Phrase("PREVENTATIVE MAINTENANCE COSTS", new Font(Font.FontFamily.HELVETICA, HeadText - 1, Font.BOLD, nviDarkBlue)));
                    ComOffer_MaintenanceH.SpacingBefore = 3;
                    document.Add(ComOffer_MaintenanceH);


                    PdfPTable ComOffer_MaintenanceTable_nesttable = new PdfPTable(1);
                    ComOffer_MaintenanceTable_nesttable.TotalWidth = document.PageSize.Width - 2 * document.RightMargin;
                    ComOffer_MaintenanceTable_nesttable.LockedWidth = true;
                    ComOffer_MaintenanceTable_nesttable.SpacingBefore = 4;


                    PdfPTable ComOffer_MaintenanceTable = new PdfPTable(new float[] { 300, 150, 200 });


                    PdfPCell ComOffer_MaintenanceTable_FMPL = MyCell("Fixed Monthly Payment (Excl. of VAT): *", MidText1, true, false, true);
                    ComOffer_MaintenanceTable_FMPL.Padding = 1;

                    PdfPCell ComOffer_MaintenanceTable_FMPV = MyCell(proposalInput.superProject.Country.Currency + " " + (proposalInput.superProject.OM + proposalInput.superProject.Admin).ToString("#,##0.00") + " / Mnth", MidText1, true, false, false);
                    ComOffer_MaintenanceTable_FMPV.HorizontalAlignment = Image.ALIGN_RIGHT;
                    ComOffer_MaintenanceTable_FMPV.Padding = 1;
                    ComOffer_MaintenanceTable_FMPV.PaddingBottom = 2;
                    ComOffer_MaintenanceTable_FMPV.BackgroundColor = BaseColor.WHITE;


                    ComOffer_MaintenanceTable.AddCell(ComOffer_MaintenanceTable_FMPL);
                    ComOffer_MaintenanceTable.AddCell(ComOffer_MaintenanceTable_FMPV);
                    ComOffer_MaintenanceTable.AddCell(new PdfPCell(new Phrase("")) { Border = Image.NO_BORDER });


                    PdfPCell ComOffer_MaintenanceTable_CPMSVL = MyCell("Charges per preventative maintenance site visit:", MidText1, true, false, true);
                    ComOffer_MaintenanceTable_CPMSVL.Padding = 0;
                    ComOffer_MaintenanceTable_CPMSVL.PaddingTop = 12;
                    PdfPCell ComOffer_MaintenanceTable_CPMSV = MyCell("Included", MidText1, true, false, false);
                    ComOffer_MaintenanceTable_CPMSV.HorizontalAlignment = Image.ALIGN_RIGHT;
                    ComOffer_MaintenanceTable_CPMSV.Padding = 0;
                    ComOffer_MaintenanceTable_CPMSV.PaddingTop = 12;

                    ComOffer_MaintenanceTable.AddCell(ComOffer_MaintenanceTable_CPMSVL);
                    ComOffer_MaintenanceTable.AddCell(ComOffer_MaintenanceTable_CPMSV);
                    ComOffer_MaintenanceTable.AddCell(new PdfPCell(new Phrase("")) { Border = Image.NO_BORDER });


                    PdfPCell ComOffer_MaintenanceTable_ADDL = MyCell("Additional Charges (Excl. VAT) for unscheduled site:", MidText1, true, false, true);
                    ComOffer_MaintenanceTable_ADDL.Padding = 0;
                    PdfPCell ComOffer_MaintenanceTable_ADDV = MyCell("(Scope Dependent)", MidText1, true, false, false);
                    ComOffer_MaintenanceTable_ADDV.Padding = 0;
                    ComOffer_MaintenanceTable_ADDV.HorizontalAlignment = Image.ALIGN_RIGHT;


                    ComOffer_MaintenanceTable.AddCell(ComOffer_MaintenanceTable_ADDL);
                    ComOffer_MaintenanceTable.AddCell(ComOffer_MaintenanceTable_ADDV);
                    ComOffer_MaintenanceTable.AddCell(new PdfPCell(new Phrase("")) { Border = Image.NO_BORDER });


                    PdfPCell ComOffer_MaintenanceTable_CHARGE = MyCell("* All charges increase annually at CPI.", MidText1, false, false, true);
                    ComOffer_MaintenanceTable_CHARGE.Padding = 0;
                    ComOffer_MaintenanceTable_CHARGE.PaddingTop = 3;
                    ComOffer_MaintenanceTable_CHARGE.PaddingBottom = 6;
                    ComOffer_MaintenanceTable_CHARGE.Colspan = 3;

                    ComOffer_MaintenanceTable.AddCell(ComOffer_MaintenanceTable_CHARGE);

                    PdfPCell ComOffer_cMaintenanceTable_nestcell = new PdfPCell(ComOffer_MaintenanceTable);

                    ComOffer_cMaintenanceTable_nestcell.BackgroundColor = nviLightBLue;
                    ComOffer_cMaintenanceTable_nestcell.Border = Image.NO_BORDER;
                    ComOffer_cMaintenanceTable_nestcell.Padding = 7;
                    ComOffer_cMaintenanceTable_nestcell.PaddingBottom = 7;
                    ComOffer_cMaintenanceTable_nestcell.PaddingLeft = 10;

                    ComOffer_contractPriceTable_nestcell.BackgroundColor = nviLightBLue;
                    ComOffer_contractPriceTable_nestcell.Border = Image.NO_BORDER;

                    ComOffer_MaintenanceTable_nesttable.AddCell(ComOffer_cMaintenanceTable_nestcell);

                    document.Add(ComOffer_MaintenanceTable_nesttable);

                    #endregion

                    #region PERFORMANCE GUARANTEE


                    Paragraph ComOffer_PerfGH = new Paragraph(new Phrase("PERFORMANCE GUARANTEE", new Font(Font.FontFamily.HELVETICA, HeadText - 1, Font.BOLD, nviDarkBlue)));
                    ComOffer_PerfGH.SpacingBefore = 3;
                    document.Add(ComOffer_PerfGH);


                    PdfPTable ComOffer_PerfGHTable = new PdfPTable(1);
                    ComOffer_PerfGHTable.TotalWidth = document.PageSize.Width - 2 * document.RightMargin;
                    ComOffer_PerfGHTable.LockedWidth = true;
                    ComOffer_PerfGHTable.SpacingBefore = 4;

                    // 
                    Phrase ComOffer_PerfGHTable_1P = new Phrase();
                    Chunk ComOffer_PerfGHTable_1PH = new Chunk("Equipment Guarantee:", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue));
                    Chunk ComOffer_PerfGHTable_1PC = new Chunk(" We procure from the most respected manufacturers in the industry, all equipment guarantees are passed directly on to you. Please see equipment specification sheets for more information on individual manufacturer's warranties.", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue));

                    ComOffer_PerfGHTable_1P.Add(ComOffer_PerfGHTable_1PH);
                    ComOffer_PerfGHTable_1P.Add(ComOffer_PerfGHTable_1PC);


                    PdfPCell ComOffer_PerfGHTable_1 = new PdfPCell(ComOffer_PerfGHTable_1P);
                    ComOffer_PerfGHTable_1.Border = Image.NO_BORDER;
                    ComOffer_PerfGHTable_1.BackgroundColor = nviLightBLue;
                    ComOffer_PerfGHTable_1.Padding = 4;
                    ComOffer_PerfGHTable_1.PaddingLeft = 10;
                    ComOffer_PerfGHTable.AddCell(ComOffer_PerfGHTable_1);



                    Phrase ComOffer_PerfGHTable_2P = new Phrase();
                    Chunk ComOffer_PerfGHTable_2PH = new Chunk("Performance Ratio Guarantee: The Contract Price includes", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue));
                    Chunk ComOffer_PerfGHTable_2PC = new Chunk(" our Solar4Africa Performance Guarantee, we guarantee the performance ratio of the plant for the first year. Should performance be lower, you will be compensated for lower energy production up to a maximum of 5% of the Contract Price. Our Solar4Africa performance guarantee is subject to concluding a maintenance contract for the period above.", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue));

                    ComOffer_PerfGHTable_2P.Add(ComOffer_PerfGHTable_2PH);
                    ComOffer_PerfGHTable_2P.Add(ComOffer_PerfGHTable_2PC);


                    PdfPCell ComOffer_PerfGHTable_2 = new PdfPCell(ComOffer_PerfGHTable_2P);
                    ComOffer_PerfGHTable_2.Border = Image.NO_BORDER;
                    ComOffer_PerfGHTable_2.BackgroundColor = nviLightBLue;
                    ComOffer_PerfGHTable_2.Padding = 4;
                    ComOffer_PerfGHTable_2.PaddingLeft = 10;
                    ComOffer_PerfGHTable.AddCell(ComOffer_PerfGHTable_2);


                    document.Add(ComOffer_PerfGHTable);

                    #endregion

                    #region Queries

                    Paragraph ComOffer_queriesH = new Paragraph("For queries on this offer please speak to:", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue));
                    document.Add(ComOffer_queriesH);


                    PdfPTable Comoff_QueriesTableMain = new PdfPTable(1);
                    Comoff_QueriesTableMain.HorizontalAlignment = Image.ALIGN_LEFT;
                    Comoff_QueriesTableMain.SpacingBefore = 10;
                    Comoff_QueriesTableMain.TotalWidth = document.PageSize.Width / 2 - 60;
                    Comoff_QueriesTableMain.LockedWidth = true;

                    PdfPTable Comoff_QueriesTableNested = new PdfPTable(new float[] { 50, 200 });

                    PdfPCell Comoff_QueriesTableNested_person = MyCell(proposalInput.preparer.UserName.ToUpper(), HeadText, true, false, false);
                    Comoff_QueriesTableNested_person.Padding = 1;
                    Comoff_QueriesTableNested_person.Colspan = 2;

                    Comoff_QueriesTableNested.AddCell(Comoff_QueriesTableNested_person);

                    PdfPCell Comoff_QueriesTableNested_company = MyCell(proposalInput.preparer.Company, HeadText, true, false, false);
                    Comoff_QueriesTableNested_company.Padding = 1;
                    Comoff_QueriesTableNested_company.Colspan = 2;

                    Comoff_QueriesTableNested.AddCell(Comoff_QueriesTableNested_company);


                    PdfPCell Comoff_QueriesTableNested_telL = MyCell("TEL:", HeadText - 2, false, false, false);
                    Comoff_QueriesTableNested_telL.Padding = 1;
                    Comoff_QueriesTableNested.AddCell(Comoff_QueriesTableNested_telL);


                    PdfPCell Comoff_QueriesTableNested_telV = MyCell(proposalInput.preparer.PhoneNumber, HeadText - 2, false, false, false);
                    Comoff_QueriesTableNested_telV.Padding = 1;
                    Comoff_QueriesTableNested.AddCell(Comoff_QueriesTableNested_telV);

                    PdfPCell Comoff_QueriesTableNested_EmailL = MyCell("EMAIL:", HeadText - 2, false, false, false);
                    Comoff_QueriesTableNested_EmailL.Padding = 1;

                    Comoff_QueriesTableNested.AddCell(Comoff_QueriesTableNested_EmailL);


                    PdfPCell Comoff_QueriesTableNested_EmailV = MyCell(proposalInput.preparer.Email, HeadText - 2, false, false, false);
                    Comoff_QueriesTableNested_EmailV.Padding = 1;
                    Comoff_QueriesTableNested.AddCell(Comoff_QueriesTableNested_EmailV);

                    PdfPCell Comoff_QueriesTableNested_Cell = new PdfPCell(Comoff_QueriesTableNested);
                    Comoff_QueriesTableNested_Cell.Padding = 3;
                    Comoff_QueriesTableNested_Cell.PaddingLeft = 10;
                    Comoff_QueriesTableNested_Cell.PaddingBottom = 10;
                    Comoff_QueriesTableMain.AddCell(Comoff_QueriesTableNested_Cell);


                    document.Add(Comoff_QueriesTableMain);
                    Chunk lineBr = new Chunk(new LineSeparator(0.5f, 100, BaseColor.BLACK, Image.ALIGN_CENTER, -10));
                    document.Add(lineBr);
                    #endregion
                    #endregion

                    #region Generate Financial Review A3
                    //idea create this page into new pdf document, access the stream
                    //reader get the stream into new pdf, use stamper to rotate, add pdf to current pdf
                    document.NewPage();
                    currentIndex++;
                    pdfreader = new PdfReader(createFinancialAnalysis(currentLetter, currentIndex, proposalInput.superProject));
                    PdfImportedPage financeAnalysis = pdfwriter.GetImportedPage(pdfreader, 1);
                    PdfContentByte pwrote = pdfwriter.DirectContent;
                    PdfDictionary dict = pdfreader.GetPageN(1);
                    dict.Put(PdfName.ROTATE, new PdfNumber(270));
                    PdfContentByte waas = pdfwriter.DirectContent;
                    waas.AddTemplate(pdfwriter.GetImportedPage(pdfreader, 1), 1f, 0, 0, 1f, 0, 0);
                    currentIndex++;
                    //add fucntions here then bring back
                    #endregion
                }


                #region Generate Finance Offers A4-1 to 3 PBS   PPA   GEN

                subIndex = 1;
                if (proposalInput.IncludePPA)
                {
                    Project ppaProject = proposalInput.project;
                    ppaProject.Costing.Plan = "Power Purchase Agreement";
                    pdfreader = new PdfReader(generateOffer("SSA", "02", currentIndex, subIndex, ppaProject));

                    PdfContentByte cbPPA = pdfwriter.DirectContent;

                    for (int a = 1; a <= pdfreader.NumberOfPages; a++)
                    {
                        document.NewPage();
                        cbPPA.AddTemplate(pdfwriter.GetImportedPage(pdfreader, a), 1f, 0, 0, 1f, 0, 0);
                    }
                    subIndex++;
                }
                if (proposalInput.IncludePBS)
                {
                    document.NewPage();

                    Project ppaProject = proposalInput.project;
                    ppaProject.Costing.Plan = "Pay-By-Solar";
                    pdfreader = new PdfReader(generateOffer("PBS", "02", currentIndex, subIndex, ppaProject));

                    PdfContentByte cbPPA = pdfwriter.DirectContent;

                    for (int a = 1; a <= pdfreader.NumberOfPages; a++)
                    {
                        document.NewPage();
                        cbPPA.AddTemplate(pdfwriter.GetImportedPage(pdfreader, a), 1f, 0, 0, 1f, 0, 0);
                    }
                    subIndex++;
                }
                #endregion

                #endregion
                ////---------------------------------------------------------END of Generate SECTION A----------------------------------------------------////

                letINdex++;
                currentLetter = letters[letINdex];
                currentLetter = HeaderIndexes[letINdex];
                currentIndex = 1;

                ////---------------------------------START of Generate TECHNICAL DATA----------------------------------------------------////
                #region Generation  Technical  Data

                PdfContentByte externalPages = pdfwriter.DirectContent;
                if (proposalInput.TechData.IncludeMe)
                {
                    //generate Technical Data + BOS if required

                    #region Tech data main page
                    document.NewPage();
                    //PdfPTable header_table = new PdfPTable(new float[] { 90, 40, 520 });
                    //header_table.TotalWidth = document.PageSize.Width - (document.RightMargin * 2);
                    //header_table.LockedWidth = true;
                    //header_table.PaddingTop = 0;
                    ////Top heading
                    ////header_table.AddCell(new PdfPCell(MyCell(currentLetter, BigText, true, false, true)) { HorizontalAlignment = Image.ALIGN_CENTER });
                    //header_table.AddCell(ContentCell(" ", CellInfo.Empty));
                    //header_table.AddCell(ContentCell(" ", CellInfo.Empty));
                    //header_table.AddCell(MyCell("TECHNICAL DATA", BigText, true, false, true));
                    //document.Add(header_table);
                    PdfPTable Eheader_table = HeaderTable(new float[] { 50, 560 });
                    Eheader_table.PaddingTop = 0;
                    Eheader_table.SpacingBefore = 17;
                    Eheader_table.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
                    Eheader_table.AddCell(new PdfPCell(FCell("TECHNICAL DATA", BigText + 2, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
                    document.Add(Eheader_table);

                    PdfPTable nestTable = new PdfPTable(1);
                    nestTable.TotalWidth = 420;
                    nestTable.LockedWidth = true;
                    nestTable.HorizontalAlignment = Image.ALIGN_RIGHT;
                    nestTable.SpacingBefore = 15;

                    PdfPCell nestCell = new PdfPCell();
                    nestCell.HorizontalAlignment = Image.ALIGN_CENTER;
                    nestCell.Padding = 5;

                    PdfPTable table = new PdfPTable(new float[] { 250, 150 });
                    table.TotalWidth = 420;
                    table.LockedWidth = true;
                    Paragraph specialContentGraph = new Paragraph();
                    specialContentGraph.Add(new Phrase("System Size (Nominal Power", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    specialContentGraph.Add(new Phrase(" DC", new Font(Font.FontFamily.HELVETICA, MidText2 - 2, Font.NORMAL, nviDarkBlue)));
                    specialContentGraph.Add(new Phrase("):", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    PdfPCell lable = new PdfPCell(specialContentGraph);
                    lable.Padding = 5;
                    lable.Border = Image.NO_BORDER;

                    PdfPCell val = new PdfPCell(new Phrase((proposalInput.superProject.prosize * 1000).ToString("#,##") + " kWp", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    val.HorizontalAlignment = Image.ALIGN_RIGHT;
                    val.Border = Image.NO_BORDER;
                    val.Padding = 5;

                    table.AddCell(lable);
                    table.AddCell(val);

                    lable = new PdfPCell(new Phrase("Solar Energy Production (Yr 1)", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    lable.Padding = 5;
                    lable.Border = Image.NO_BORDER;

                    val = new PdfPCell(new Phrase((proposalInput.superProject.prosize * 1000 * proposalInput.project.Costing.Yield).ToString("#,##") + " kWh", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    val.HorizontalAlignment = Image.ALIGN_RIGHT;
                    val.Border = Image.NO_BORDER;
                    val.Padding = 5;

                    table.AddCell(lable);
                    table.AddCell(val);

                    lable = new PdfPCell(new Phrase("Daily Energy Production (Yr 1)", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    lable.Padding = 5;
                    lable.Border = Image.NO_BORDER;

                    val = new PdfPCell(new Phrase((proposalInput.superProject.prosize * 1000 / 365 * proposalInput.project.Costing.Yield).ToString("#,##") + " kWh", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    val.HorizontalAlignment = Image.ALIGN_RIGHT;
                    val.Border = Image.NO_BORDER;
                    val.Padding = 5;

                    table.AddCell(lable);
                    table.AddCell(val);

                    lable = new PdfPCell(new Phrase("Energy Storage Capacity (kWh)", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    lable.Padding = 5;
                    lable.Border = Image.NO_BORDER;

                    //convert bat capp Watts to kW
                    string vaStr = proposalInput.superProject.BattCap == 0.0 ? "-" : (proposalInput.superProject.BattCap / 1000).ToString("#,##0.0");
                    val = new PdfPCell(new Phrase(vaStr + " kWh", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    val.HorizontalAlignment = Image.ALIGN_RIGHT;
                    val.Border = Image.NO_BORDER;
                    val.Padding = 5;

                    table.AddCell(lable);
                    table.AddCell(val);

                    lable = new PdfPCell(new Phrase("Days Autonomy (24 hrs)", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    lable.Padding = 5;
                    lable.Border = Image.NO_BORDER;

                    //convert bat capp Watts to kW
                    vaStr = proposalInput.superProject.BattCap == 0.0 ? "-" : (proposalInput.superProject.BattCap / 1000 / (proposalInput.superProject.prosize * proposalInput.project.Costing.Yield * 1000)).ToString("#,##0.00");
                    val = new PdfPCell(new Phrase(vaStr + " Days", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    val.HorizontalAlignment = Image.ALIGN_RIGHT;
                    val.Border = Image.NO_BORDER;
                    val.Padding = 5;

                    table.AddCell(lable);
                    table.AddCell(val);

                    lable = new PdfPCell(new Phrase("Annual Module Degredation", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    lable.Padding = 5;
                    lable.Border = Image.NO_BORDER;

                    val = new PdfPCell(new Phrase(proposalInput.superProject.Df.ToString("#,##0.##") + "%", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    val.HorizontalAlignment = Image.ALIGN_RIGHT;
                    val.Border = Image.NO_BORDER;
                    val.Padding = 5;

                    table.AddCell(lable);
                    table.AddCell(val);

                    nestCell.AddElement(table);
                    nestTable.AddCell(nestCell);
                    document.Add(nestTable);

                    #endregion

                    foreach (var item in proposalInput.TechData.SubSections.Where(m => m.IncludeMe == true).OrderBy(a => a.LetterCode))
                    {
                        if (item.Name == "BILL OF QUANTITIES (BOQ)")
                        {
                            document.NewPage();
                            #region Header
                            ////generate bill of quantities
                            //PdfPTable BOQheader_table = new PdfPTable(new float[] { 520, 40, 90 });
                            //BOQheader_table.TotalWidth = document.PageSize.Width - (document.RightMargin * 2);
                            //BOQheader_table.LockedWidth = true;
                            //BOQheader_table.PaddingTop = 0;
                            ////Top heading
                            //BOQheader_table.AddCell(MyCell("BILL OF QUANTITIES (BOQ)", BigText, true, false, true));
                            //BOQheader_table.AddCell(ContentCell(" ", CellInfo.Empty));
                            //BOQheader_table.AddCell(ContentCell(" ", CellInfo.Empty));
                            ////BOQheader_table.AddCell(new PdfPCell(MyCell("" + currentIndex, BigText, true, false, true)) { HorizontalAlignment = Image.ALIGN_CENTER });
                            //document.Add(BOQheader_table);
                            PdfPTable Eheader_tableBOQ = HeaderTable(new float[] { 50, 560 });
                            Eheader_tableBOQ.PaddingTop = 0;
                            Eheader_tableBOQ.SpacingBefore = 17;
                            Eheader_tableBOQ.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
                            Eheader_tableBOQ.AddCell(new PdfPCell(FCell("BILL OF QUANTITIES (BOQ)", BigText + 2, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
                            document.Add(Eheader_tableBOQ);
                            #endregion

                            #region Subheader Table
                            PdfPTable BOQnestTable = new PdfPTable(1);
                            BOQnestTable.TotalWidth = 320;
                            BOQnestTable.LockedWidth = true;
                            BOQnestTable.HorizontalAlignment = Image.ALIGN_LEFT;
                            BOQnestTable.SpacingBefore = 15;


                            PdfPCell BOQnestCell = new PdfPCell();
                            BOQnestCell.HorizontalAlignment = Image.ALIGN_CENTER;
                            BOQnestCell.Padding = 5;


                            PdfPTable BOQtable = new PdfPTable(new float[] { 250, 150 });
                            BOQtable.TotalWidth = 320;
                            BOQtable.LockedWidth = true;


                            lable = new PdfPCell(new Phrase("Project:", new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));

                            lable.Padding = 2;
                            lable.Border = Image.NO_BORDER;

                            val = new PdfPCell(new Phrase((proposalInput.project.Details.ProjectName), new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));
                            val.HorizontalAlignment = Image.ALIGN_RIGHT;
                            val.Border = Image.NO_BORDER;
                            val.Padding = 2;


                            BOQtable.AddCell(lable);
                            BOQtable.AddCell(val);



                            lable = new PdfPCell(new Phrase("Location:", new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));

                            lable.Padding = 2;
                            lable.Border = Image.NO_BORDER;

                            val = new PdfPCell(new Phrase((proposalInput.project.Details.P_City + "," + proposalInput.project.Details.P_Road), new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));
                            val.HorizontalAlignment = Image.ALIGN_RIGHT;
                            val.Border = Image.NO_BORDER;
                            val.Padding = 2;







                            lable = new PdfPCell(new Phrase("Project Terms:", new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));

                            lable.Padding = 2;
                            lable.Border = Image.NO_BORDER;

                            val = new PdfPCell(new Phrase(("EPC Turn Key"), new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));
                            val.HorizontalAlignment = Image.ALIGN_RIGHT;
                            val.Border = Image.NO_BORDER;
                            val.Padding = 2;

                            BOQtable.AddCell(lable);
                            BOQtable.AddCell(val);


                            lable = new PdfPCell(new Phrase("Installed Capacity (Panels):", new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));

                            lable.Padding = 2;
                            lable.Border = Image.NO_BORDER;
                            vaStr = proposalInput.superProject.prosize * 1000 == 0.0 ? "-" : (proposalInput.superProject.prosize * 1000).ToString("#,##0.##");

                            val = new PdfPCell(new Phrase((proposalInput.superProject.prosize * 1000).ToString("#,##0.##") + " kWp", new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));
                            val.HorizontalAlignment = Image.ALIGN_RIGHT;
                            val.Border = Image.NO_BORDER;
                            val.Padding = 2;


                            BOQtable.AddCell(lable);
                            BOQtable.AddCell(val);



                            lable = new PdfPCell(new Phrase("Installed Capacity (Inverters):", new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));

                            lable.Padding = 2;
                            lable.Border = Image.NO_BORDER;
                            vaStr = proposalInput.superProject.InverterWatt * 1000.00 == 0.0 ? "-" : (proposalInput.superProject.InverterWatt * 1000.00).ToString("#,##0.##");

                            val = new PdfPCell(new Phrase((vaStr + " kWp"), new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));
                            val.HorizontalAlignment = Image.ALIGN_RIGHT;
                            val.Border = Image.NO_BORDER;
                            val.Padding = 2;





                            BOQtable.AddCell(lable);
                            BOQtable.AddCell(val);

                            vaStr = proposalInput.superProject.BattCap == 0.0 ? "-" : (proposalInput.superProject.BattCap / 1000.00).ToString("#,##0.##");
                            lable = new PdfPCell(new Phrase("Installed Capacity (Autonomous):", new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));

                            lable.Padding = 2;
                            lable.Border = Image.NO_BORDER;

                            val = new PdfPCell(new Phrase((vaStr) + " kWp", new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));
                            val.HorizontalAlignment = Image.ALIGN_RIGHT;
                            val.Border = Image.NO_BORDER;
                            val.Padding = 2;


                            BOQtable.AddCell(lable);
                            BOQtable.AddCell(val);


                            vaStr = proposalInput.superProject.BattCap == 0.0 ? "-" : (proposalInput.superProject.BattCap / 1000.00).ToString("#,##0.##");
                            lable = new PdfPCell(new Phrase("Energy Storage:", new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));

                            lable.Padding = 2;
                            lable.Border = Image.NO_BORDER;

                            val = new PdfPCell(new Phrase((vaStr) + " kWh", new Font(Font.FontFamily.HELVETICA, MidText2, Font.BOLD, nviDarkBlue)));
                            val.HorizontalAlignment = Image.ALIGN_RIGHT;
                            val.Border = Image.NO_BORDER;
                            val.Padding = 2;


                            BOQtable.AddCell(lable);
                            BOQtable.AddCell(val);



                            BOQnestCell.AddElement(BOQtable);
                            BOQnestTable.AddCell(BOQnestCell);
                            document.Add(BOQnestTable);
                            string _s4africaPATH = System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/nvi Pdf Logo.png");

                            Image s4africa = Image.GetInstance(_s4africaPATH);
                            s4africa.ScaleAbsolute(120, 120);
                            s4africa.SetAbsolutePosition(document.Right - s4africa.ScaledWidth - 20, document.Top - s4africa.ScaledHeight - 30);

                            document.Add(s4africa);
                            #endregion

                            #region Equipment Table

                            PdfPTable _EquipmentTableNest = new PdfPTable(1);
                            _EquipmentTableNest.TotalWidth = document.PageSize.Width - (2 * document.RightMargin);
                            _EquipmentTableNest.LockedWidth = true;
                            _EquipmentTableNest.SpacingBefore = 20;

                            //item number , name , description ,unit of measure , qty
                            PdfPTable equipmentTable = new PdfPTable(new float[] { 50, 200, 400, 50, 100 });
                            equipmentTable.TotalWidth = document.PageSize.Width - (2 * document.RightMargin);
                            equipmentTable.LockedWidth = true;

                            //add all the equipment items as phrases into single cell houses, then add the cell to the table

                            int mIndex = 1;
                            int mSubIndex = 1;
                            int mSubSubindex = 1;
                            int mSubSubSubindex = 1;

                            //one cell for every column

                            PdfPCell colEmpty = new PdfPCell(new Phrase(" "));
                            colEmpty.BorderColor = nviDarkBlue;
                            colEmpty.Colspan = 5;
                            PdfPCell colCell1, colCell2, colCell3, colCell4, colCell5;

                            colCell1 = EquipmentCell(EquipmentPhrase("Item", true, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                            colCell2 = EquipmentCell(EquipmentPhrase("Name", true, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                            colCell3 = EquipmentCell(EquipmentPhrase("Description", true, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                            colCell4 = EquipmentCell(EquipmentPhrase("UOM", true, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                            colCell5 = EquipmentCell(EquipmentPhrase("Qty", true, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_RIGHT);

                            equipmentTable.AddCell(colCell1);
                            equipmentTable.AddCell(colCell2);
                            equipmentTable.AddCell(colCell3);
                            equipmentTable.AddCell(colCell4);
                            equipmentTable.AddCell(colCell5);


                            colCell1 = EquipmentCell(EquipmentPhrase("1", true, MidText2, BaseColor.WHITE), EquipmentCellInfo.Header, Image.ALIGN_LEFT);
                            colCell2 = EquipmentCell(EquipmentPhrase("Equipment Total", true, MidText2, BaseColor.WHITE), EquipmentCellInfo.Header, Image.ALIGN_LEFT);
                            colCell2.Colspan = 4;

                            equipmentTable.AddCell(colCell1);
                            equipmentTable.AddCell(colCell2);

                            #region PV MODS

                            if (proposalInput.superProject._pvmodules.Count > 0)
                            {
                                colCell1 = EquipmentCell(EquipmentPhrase(mIndex + "." + mSubIndex, true, MidText2, nviDarkBlue), EquipmentCellInfo.Subheader, Image.ALIGN_LEFT);
                                colCell2 = EquipmentCell(EquipmentPhrase("Panels", true, MidText2, nviDarkBlue), EquipmentCellInfo.Subheader, Image.ALIGN_LEFT);
                                colCell2.Colspan = 4;

                                equipmentTable.AddCell(colCell1);
                                equipmentTable.AddCell(colCell2);
                                foreach (var equip in proposalInput.superProject._pvmodules)
                                {
                                    colCell1 = EquipmentCell(EquipmentPhrase(mIndex + "." + mSubIndex + "." + mSubSubindex, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                    colCell2 = EquipmentCell(EquipmentPhrase(equip.Id.Replace('_', ' '), false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                    colCell3 = EquipmentCell(EquipmentPhrase(equip.Description, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                    colCell4 = EquipmentCell(EquipmentPhrase(equip.UOM, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_RIGHT);
                                    colCell5 = EquipmentCell(EquipmentPhrase(equip.Amount.ToString(), false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_RIGHT);

                                    equipmentTable.AddCell(colCell1);
                                    equipmentTable.AddCell(colCell2);
                                    equipmentTable.AddCell(colCell3);
                                    equipmentTable.AddCell(colCell4);
                                    equipmentTable.AddCell(colCell5);
                                    mSubSubindex++;
                                }

                                equipmentTable.AddCell(colEmpty);

                            }
                            #endregion

                            //mounting excl for now

                            #region Inverters
                            if (proposalInput.superProject._inverters.Count > 0)
                            {
                                mSubIndex++;
                                colCell1 = EquipmentCell(EquipmentPhrase(mIndex + "." + mSubIndex, true, MidText2, nviDarkBlue), EquipmentCellInfo.Subheader, Image.ALIGN_LEFT);
                                colCell2 = EquipmentCell(EquipmentPhrase("Inverters", true, MidText2, nviDarkBlue), EquipmentCellInfo.Subheader, Image.ALIGN_LEFT);
                                colCell2.Colspan = 4;

                                equipmentTable.AddCell(colCell1);
                                equipmentTable.AddCell(colCell2);
                                foreach (var equip in proposalInput.superProject._inverters)
                                {
                                    colCell1 = EquipmentCell(EquipmentPhrase(mIndex + "." + mSubIndex + "." + mSubSubindex, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                    colCell2 = EquipmentCell(EquipmentPhrase(equip.Id.Replace('_', ' '), false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                    colCell3 = EquipmentCell(EquipmentPhrase(equip.Description, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                    colCell4 = EquipmentCell(EquipmentPhrase(equip.UOM, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_RIGHT);
                                    colCell5 = EquipmentCell(EquipmentPhrase(equip.Amount.ToString(), false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_RIGHT);

                                    equipmentTable.AddCell(colCell1);
                                    equipmentTable.AddCell(colCell2);
                                    equipmentTable.AddCell(colCell3);
                                    equipmentTable.AddCell(colCell4);
                                    equipmentTable.AddCell(colCell5);
                                    mSubSubindex++;
                                }
                                equipmentTable.AddCell(colEmpty);

                            }

                            #endregion

                            #region Bi Inverters
                            if (proposalInput.superProject._biInverters.Count > 0)
                            {
                                mSubIndex++;
                                colCell1 = EquipmentCell(EquipmentPhrase(mIndex + "." + mSubIndex, true, MidText2, nviDarkBlue), EquipmentCellInfo.Subheader, Image.ALIGN_LEFT);
                                colCell2 = EquipmentCell(EquipmentPhrase("Bi-Inverters", true, MidText2, nviDarkBlue), EquipmentCellInfo.Subheader, Image.ALIGN_LEFT);
                                colCell2.Colspan = 4;

                                equipmentTable.AddCell(colCell1);
                                equipmentTable.AddCell(colCell2);
                                foreach (var equip in proposalInput.superProject._biInverters)
                                {
                                    colCell1 = EquipmentCell(EquipmentPhrase(mIndex + "." + mSubIndex + "." + mSubSubindex, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                    colCell2 = EquipmentCell(EquipmentPhrase(equip.Id.Replace('_', ' '), false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                    colCell3 = EquipmentCell(EquipmentPhrase(equip.Description, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                    colCell4 = EquipmentCell(EquipmentPhrase(equip.UOM, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_RIGHT);
                                    colCell5 = EquipmentCell(EquipmentPhrase(equip.Amount.ToString(), false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_RIGHT);

                                    equipmentTable.AddCell(colCell1);
                                    equipmentTable.AddCell(colCell2);
                                    equipmentTable.AddCell(colCell3);
                                    equipmentTable.AddCell(colCell4);
                                    equipmentTable.AddCell(colCell5);
                                    mSubSubindex++;
                                }

                                equipmentTable.AddCell(colEmpty);

                            }
                            #endregion

                            #region Batteries
                            if (proposalInput.superProject._batteries.Count > 0)
                            {
                                mSubIndex++;
                                colCell1 = EquipmentCell(EquipmentPhrase(mIndex + "." + mSubIndex, true, MidText2, nviDarkBlue), EquipmentCellInfo.Subheader, Image.ALIGN_LEFT);
                                colCell2 = EquipmentCell(EquipmentPhrase("Batteries", true, MidText2, nviDarkBlue), EquipmentCellInfo.Subheader, Image.ALIGN_LEFT);
                                colCell2.Colspan = 4;

                                equipmentTable.AddCell(colCell1);
                                equipmentTable.AddCell(colCell2);
                                foreach (var equip in proposalInput.superProject._batteries)
                                {
                                    colCell1 = EquipmentCell(EquipmentPhrase(mIndex + "." + mSubIndex + "." + mSubSubindex, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                    colCell2 = EquipmentCell(EquipmentPhrase(equip.Id.Replace('_', ' '), false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                    colCell3 = EquipmentCell(EquipmentPhrase(equip.Description, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                    colCell4 = EquipmentCell(EquipmentPhrase(equip.UOM, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_RIGHT);
                                    colCell5 = EquipmentCell(EquipmentPhrase(equip.Amount.ToString(), false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_RIGHT);

                                    equipmentTable.AddCell(colCell1);
                                    equipmentTable.AddCell(colCell2);
                                    equipmentTable.AddCell(colCell3);
                                    equipmentTable.AddCell(colCell4);
                                    equipmentTable.AddCell(colCell5);
                                    mSubSubindex++;
                                }
                                equipmentTable.AddCell(colEmpty);

                            }
                            #endregion

                            #region Balance Of System
                            if (proposalInput.superProject._balanceSystems.Count > 0)
                            {

                                List<string> balanceCategories = new List<string>();
                                foreach (string category in proposalInput.superProject._balanceSystems.Select(m => m.ParentList))
                                {
                                    if (!balanceCategories.Contains(category))
                                    {
                                        balanceCategories.Add(category);
                                    }
                                }
                                mSubIndex++;
                                colCell1 = EquipmentCell(EquipmentPhrase(mIndex + "." + mSubIndex, true, MidText2, nviDarkBlue), EquipmentCellInfo.Subheader, Image.ALIGN_LEFT);
                                colCell2 = EquipmentCell(EquipmentPhrase("Balance Of System", true, MidText2, nviDarkBlue), EquipmentCellInfo.Subheader, Image.ALIGN_LEFT);
                                colCell2.Colspan = 4;

                                mSubSubSubindex = 1;
                                mSubSubindex = 1;
                                equipmentTable.AddCell(colCell1);
                                equipmentTable.AddCell(colCell2);

                                foreach (var cat in balanceCategories)
                                {
                                    colCell1 = EquipmentCell(EquipmentPhrase(mIndex + "." + mSubIndex + "." + mSubSubindex, true, MidText2, nviDarkBlue), EquipmentCellInfo.SubSubHeader, Image.ALIGN_LEFT);
                                    colCell2 = EquipmentCell(EquipmentPhrase(cat, true, MidText2, nviDarkBlue), EquipmentCellInfo.SubSubHeader, Image.ALIGN_LEFT);
                                    colCell2.Colspan = 4;

                                    equipmentTable.AddCell(colCell1);
                                    equipmentTable.AddCell(colCell2);
                                    mSubSubSubindex = 1;

                                    foreach (var equip in proposalInput.superProject._balanceSystems)
                                    {
                                        if (equip.ParentList == cat)
                                        {
                                            colCell1 = EquipmentCell(EquipmentPhrase(mIndex + "." + mSubIndex + "." + mSubSubindex + "." + mSubSubSubindex, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                            colCell2 = EquipmentCell(EquipmentPhrase(equip.Id.Replace('_', ' '), false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                            colCell3 = EquipmentCell(EquipmentPhrase(equip.Description, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_LEFT);
                                            colCell4 = EquipmentCell(EquipmentPhrase(equip.UOM, false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_RIGHT);
                                            colCell5 = EquipmentCell(EquipmentPhrase(equip.Amount.ToString(), false, MidText2, nviDarkBlue), EquipmentCellInfo.Item, Image.ALIGN_RIGHT);

                                            equipmentTable.AddCell(colCell1);
                                            equipmentTable.AddCell(colCell2);
                                            equipmentTable.AddCell(colCell3);
                                            equipmentTable.AddCell(colCell4);
                                            equipmentTable.AddCell(colCell5);
                                            mSubSubSubindex++;
                                        }


                                    }

                                    mSubSubindex++;
                                }

                            }
                            #endregion

                            _EquipmentTableNest.AddCell(new PdfPCell(equipmentTable) { BorderColor = nviDarkBlue });
                            document.Add(_EquipmentTableNest);

                            #endregion
                        }
                        else
                        {
                            if (item.PDF != null)
                            {
                                byte[] filearrs = validPDFPage(item.PDF);
                                if (filearrs != null)
                                {
                                    pdfreader = new PdfReader(filearrs);

                                    for (int a = 1; a <= pdfreader.NumberOfPages; a++)
                                    {
                                        document.NewPage();
                                        externalPages.AddTemplate(pdfwriter.GetImportedPage(pdfreader, a), 1f, 0, 0, 1f, 0, 0);


                                    }
                                    subIndex++;
                                }
                            }


                        }
                    }


                    letINdex++;

                }

                #endregion
                ////---------------------------------------------------------END of Generate TECHNICAL DATA----------------------------------------------------////


                ////---------------------------------START of Generate OTHER DATA----------------------------------------------------////
                ////Commented b'cos not in use
                #region Generate Other Data            

                #region DATA SHEETS
                //if (proposalInput.DataSheets.IncludeMe)
                //{
                //    currentLetter = letters[letINdex];
                //    currentLetter = HeaderIndexes[letINdex];
                //    //generate Technical Data + BOS if required


                //    pdfreader = new PdfReader(holderPage("DATA SHEETS & YIELD ANALYSIS", currentLetter));
                //    document.NewPage();
                //    externalPages.AddTemplate(pdfwriter.GetImportedPage(pdfreader, 1), 1f, 0, 0, 1f, 0, 0);

                //    foreach (var item in proposalInput.DataSheets.SubSections.Where(m => m.IncludeMe == true).OrderBy(a => a.LetterCode))
                //    {
                //        if (item.PDF != null)
                //        {
                //            byte[] filearrs = validPDFPage(item.PDF);
                //            if (filearrs != null)
                //            {
                //                pdfreader = new PdfReader(filearrs);

                //                for (int a = 1; a <= pdfreader.NumberOfPages; a++)
                //                {
                //                    document.NewPage();
                //                    externalPages.AddTemplate(pdfwriter.GetImportedPage(pdfreader, a), 1f, 0, 0, 1f, 0, 0);


                //                }
                //            }
                //        }
                //        subIndex++;
                //    }

                //    letINdex++;
                //}

                #endregion

                #region SUPPLEMENTAL INFO
                //if (proposalInput.SupplemData.IncludeMe)
                //{
                //    currentLetter = letters[letINdex];
                //    currentLetter = HeaderIndexes[letINdex];
                //    //generate Technical Data + BOS if required


                //    pdfreader = new PdfReader(holderPage("SUPPLEMENTAL INFO", currentLetter));
                //    document.NewPage();
                //    externalPages.AddTemplate(pdfwriter.GetImportedPage(pdfreader, 1), 1f, 0, 0, 1f, 0, 0);

                //    foreach (var item in proposalInput.SupplemData.SubSections.Where(m => m.IncludeMe == true).OrderBy(a => a.LetterCode))
                //    {
                //        if (item.PDF != null)
                //        {
                //            byte[] filearrs = validPDFPage(item.PDF);
                //            if (filearrs != null)
                //            {
                //                pdfreader = new PdfReader(filearrs);

                //                for (int a = 1; a <= pdfreader.NumberOfPages; a++)
                //                {
                //                    document.NewPage();
                //                    externalPages.AddTemplate(pdfwriter.GetImportedPage(pdfreader, a), 1f, 0, 0, 1f, 0, 0);


                //                }
                //            }
                //        }
                //        subIndex++;
                //    }

                //    letINdex++;
                //}

                #endregion

                #endregion
                ////---------------------------------------------------------END of Generate OTHER DATA----------------------------------------------------////

                #endregion

                #region GENERAL TERMS ALL CONTRACTS

                document.NewPage();
                pdfreader = new PdfReader(GeneralTerms(proposalInput.superProject.Country.Currency, proposalInput.superProject.Country.Currency_Per_Dollar));

                PdfContentByte cbGENERAL = pdfwriter.DirectContent;

                cbGENERAL.AddTemplate(pdfwriter.GetImportedPage(pdfreader, 1), 1f, 0, 0, 1f, 0, 0);
                #endregion

                #region FinanceTerms

                if (proposalInput.IncludePPA)
                {
                    document.NewPage();

                    pdfreader = new PdfReader(PPATerms());

                    cbGENERAL = pdfwriter.DirectContent;

                    cbGENERAL.AddTemplate(pdfwriter.GetImportedPage(pdfreader, 1), 1f, 0, 0, 1f, 0, 0);
                }

                if (proposalInput.IncludePBS)
                {
                    document.NewPage();

                    pdfreader = new PdfReader(PBSTerms());

                    cbGENERAL = pdfwriter.DirectContent;

                    cbGENERAL.AddTemplate(pdfwriter.GetImportedPage(pdfreader, 1), 1f, 0, 0, 1f, 0, 0);
                }



                //specific terms
                document.NewPage();

                pdfreader = new PdfReader(SpecificTerms(proposalInput.Exclusions.Select(m => m.Name).ToList()));

                cbGENERAL = pdfwriter.DirectContent;

                cbGENERAL.AddTemplate(pdfwriter.GetImportedPage(pdfreader, 1), 1f, 0, 0, 1f, 0, 0);

                #endregion
            }

            else
            {
                #region Generate Proposal For Finance Only
                FinanceOnlyProposal(proposalInput, document, verionInfo, pdfwriter);
                #endregion
            }
            ////---------------------------------------------------------End of Generate Proposal----------------------------------------------------////


            pdfwriter.CloseStream = false; //dont close the stream or the file will break
            document.Close();
            return stream; //this will be the stream of the file
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string dashString(double content, string format)
        {
            if (content == 0.0)
            {
                return "-";
            }

            else
            {
                return content.ToString(format);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string dashString(int content, string format)
        {
            if (content == 0.0)
            {
                return "-";
            }

            else
            {
                return content.ToString(format);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="equipment"></param>
        /// <returns></returns>
        public static string EquiplistString(List<string> equipment)
        {

            List<string> items = new List<string>();

            foreach (var equip in equipment)
            {
                if (!items.Contains(equip.Replace('_', ' ')))
                {
                    items.Add(equip.Replace('_', ' '));
                }
            }
            string content = "";

            content = string.Join(", ", items);

            if (!string.IsNullOrWhiteSpace(content))
            {
                content += '.';
            }

            else
            {
                content = "-";
            }
            return content;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="letter"></param>
        /// <returns></returns>
        private static byte[] holderPage(string Name, string letter)
        {
            MemoryStream stream = new MemoryStream();
            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, stream);
            document.Open();


            PdfPTable header_table = new PdfPTable(new float[] { 90, 40, 520 });
            header_table.TotalWidth = document.PageSize.Width - (document.RightMargin * 2);
            header_table.LockedWidth = true;
            header_table.PaddingTop = 0;
            //Top heading              
            header_table.AddCell(new PdfPCell(MyCell(letter, BigText, true, false, true)) { HorizontalAlignment = Image.ALIGN_CENTER });
            header_table.AddCell(ContentCell(" ", CellInfo.Empty));
            header_table.AddCell(MyCell(Name, BigText, true, false, true));

            document.Add(header_table);

            writer.CloseStream = false;

            document.Close();
            stream.Position = 0;
            return stream.GetBuffer();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private static byte[] validPDFPage(HttpPostedFileBase file)
        {
            MemoryStream strm = new MemoryStream();
            if (file.ContentType.Contains("pdf"))
            {
                file.InputStream.CopyTo(strm);

                strm.Position = 0;
                return strm.GetBuffer();
            }
            else
            {
                return null;
            }
        }  

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="currencyInDollars"></param>
        /// <returns></returns>
        private static byte[] GeneralTerms(string currency, double currencyInDollars)
        {

            MemoryStream stream = new MemoryStream(); //output stream
            Document document = new Document(); //pdf document to write
            PdfWriter pdfwriter = PdfWriter.GetInstance(document, stream);
            document.Open();


            Paragraph empty = new Paragraph(0, " ");
            empty.SpacingBefore = 1;
            empty.SpacingAfter = 25;
            document.Add(empty);

            PdfPTable container = new PdfPTable(1);
            container.TotalWidth = document.PageSize.Width - 40;
            container.HorizontalAlignment = Image.ALIGN_CENTER;
            container.LockedWidth = true;
            PdfPCell mcontainerCell = new PdfPCell();
            mcontainerCell.HorizontalAlignment = Image.ALIGN_CENTER;
            mcontainerCell.Padding = 30;
            mcontainerCell.PaddingTop = 25;
            mcontainerCell.PaddingBottom = 20;
            #region Header

            PdfPTable Eheader_table = HeaderTable(new float[] { 50, 560 });
            Eheader_table.PaddingTop = 0;
            Eheader_table.SpacingBefore = 17;

            Eheader_table.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
            Eheader_table.AddCell(new PdfPCell(FCell("TERMS & CONDITIONS", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
            document.Add(Eheader_table);

            //Paragraph header = new Paragraph("TERMS & CONDITIONS", new Font(Font.FontFamily.HELVETICA, HeadText + 2, Font.BOLD, nviDarkBlue));
            //mcontainerCell.AddElement(header);

            Paragraph subheader = new Paragraph("GENERAL CONDITIONS - ALL CONTRACTS", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue));
            subheader.SpacingBefore = 10;
            mcontainerCell.AddElement(subheader);

            subheader = new Paragraph("This quotation in provided subject to the following assumptions and standard exclusions, unless amended, replaced or excluded under section:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
            subheader.Add(new Phrase("SPECIFIC CONDITIONS.", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.ITALIC, nviDarkBlue)));
            subheader.SpacingBefore = 10;

            mcontainerCell.AddElement(subheader);
            string currencyFormat = string.Format("{0} {1}/ US$", currency, currencyInDollars.ToString("0.00"));
            Paragraph exchanger = new Paragraph("All pricing has been based on an exchange rate of: " + currencyFormat, new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
            exchanger.SpacingBefore = 10;

            mcontainerCell.AddElement(exchanger);
            #endregion

            #region General Terms

            //general: header
            subheader = new Paragraph("General:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.BOLD, nviDarkBlue));
            subheader.SpacingBefore = 8;
            mcontainerCell.AddElement(subheader);


            PdfPTable termTable = new PdfPTable(new float[] { 30, 600 });
            termTable.LockedWidth = true;
            termTable.TotalWidth = document.PageSize.Width - 100;
            termTable.SpacingBefore = 8;

            int letindex = 0;
            #region General
            // letter first then
            Paragraph paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Validity of Offer:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" This offer is valid for 30 days, subject to exchange rate quoted above. Confirmed order pricing is held for 7 days.	", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Exchange Rate:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Confirmed offers will be held for 7 days, thereafter we reserve a right to amend our pricing to account for exchange rate movements that materially affect our costs.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Payment Terms:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Our standard payment terms for outright purchase and payment of the Contract Price are: 20% on Purchase Order confirmation, 50% on equipment delivery and 30% on completion. If we have agreed alternative payment terms with you these will be listed under Specific Conditions.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Taxes & Duties:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" All pricing exclude VAT and include duties and taxes unless stated otherwise. ", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;



            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Scope of Works", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" : Except for any specific terms agreed with you, our Contract Price includes design, procurement, delivery, installation and commissioning of the solar plant consisting of components as per the bill of quantities. Any changes in the scope of works or bill of materials will be treated as a variation. ", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Enabling Works:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Our offer excludes site enabling works expressly excluded form our scope under the Specific Conditions of this offer.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;

            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Security & Safety: ", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" On delivery of supplies to the site you will be responsible for providing the same level of security as provided for your own assets, to the solar assests kept at the site during construction and installation.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Grid Connection Costs", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Our quotation excludes any grid connection fees, where these apply.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;



            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Site Survey", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" In order for us to confirm any offer we must perform a site assessment (if not already concluded). Depending on the location of the site, a fee may apply for us to perform site assessment and audit. A quote will be provided if required.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;

            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Structural Conditions", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" The Proposal excludes costs relating to testing or upgrading structural integrity of existing buildings (roof mountings).	", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Energy Yield", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Although we take care to present an accurate energy yield assessment in our offer letters, this is an estimate only and site-specific adjustments may apply when concluding final design.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;



            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Site Access:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Right of way and access to the site must be granted and maintained during the period of construction (and during any sunsequent contractual periods) during which we are responsible for execution of works at the site.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;

            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Insurance", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Our offer includes provision for all marine and cargo insurance up to the delivery of equipment to the site. Adequate contractors all-risk insurance shall be maintained by us during the installation and commissioning of the system by us (or our contractors). If you have elected a financing plan (where ownership of the solar system remains with us and we retain certain obligations to insure the system), all risk and rewards of ownership will pass to you and suitable asset insurance must be taken out from the commissioning date onwards.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;

            mcontainerCell.AddElement(termTable);


            #endregion

            #region Equipment & Performance Guarantees

            subheader = new Paragraph("Equipment & Performance Guarantees:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.BOLD, nviDarkBlue));
            mcontainerCell.AddElement(subheader);


            termTable = new PdfPTable(new float[] { 30, 600 });
            termTable.LockedWidth = true;
            termTable.TotalWidth = document.PageSize.Width - 100;
            termTable.SpacingBefore = 8;
            letindex = 0;

            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Equipment Warranties & Workmanship Guarantees:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" In the case of an outright purchase, all our manufacturer warranties are passes through to you. These include both workmanship and production guarantees.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Performance Guarantees", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Solar4Africa will guarantee the system performance ratio of your plant and make any necessary repairs or replacements in order to ensure that the plant is performing as required. In the event that the plants performance can’t be rectified, we will pay an amount as compensation for loss of production, up to a maximum of 5% of the contract price (unless agreed otherwise with you in the Specific Conditions).", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;




            mcontainerCell.AddElement(termTable);

            #endregion

            #region Maintenance Terms:
            subheader = new Paragraph("Maintenance Terms:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.BOLD, nviDarkBlue));
            mcontainerCell.AddElement(subheader);


            termTable = new PdfPTable(new float[] { 30, 600 });
            termTable.LockedWidth = true;
            termTable.TotalWidth = document.PageSize.Width - 100;
            termTable.SpacingBefore = 8;
            letindex = 0;

            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Maintenance Contract:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Our Contract Price is subject to concluding a Maintenance Contract for the period specified in this offer.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Included:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" : Maintenance services include scheduled annual maintenance visits but exclude cost of any equipment items replaced outside of warranty.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Payment Terms", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Maintenance fees are invoiced monthly in advance. Payable in 7 days from date of invoice.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            mcontainerCell.AddElement(termTable);
            #endregion


            #endregion

            container.AddCell(new PdfPCell(mcontainerCell) { Border = Image.NO_BORDER });
            document.Add(container);
            pdfwriter.CloseStream = false;
            document.Close();

            stream.Position = 0;
            stream.Flush();

            return stream.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static byte[] PPATerms()
        {
            MemoryStream stream = new MemoryStream(); //output stream
            Document document = new Document(); //pdf document to write
            PdfWriter pdfwriter = PdfWriter.GetInstance(document, stream);
            document.Open();

            Paragraph empty = new Paragraph(0, " ");
            empty.SpacingBefore = 1;
            empty.SpacingAfter = 25;
            document.Add(empty);

            PdfPTable container = new PdfPTable(1);
            container.TotalWidth = document.PageSize.Width - 40;
            container.HorizontalAlignment = Image.ALIGN_CENTER;
            container.LockedWidth = true;
            PdfPCell mcontainerCell = new PdfPCell();
            mcontainerCell.HorizontalAlignment = Image.ALIGN_CENTER;
            mcontainerCell.Padding = 30;
            mcontainerCell.PaddingTop = 25;
            mcontainerCell.PaddingBottom = 20;

            #region Header
            PdfPTable Eheader_table = HeaderTable(new float[] { 50, 560 });
            Eheader_table.PaddingTop = 0;
            Eheader_table.SpacingBefore = 17;
            Eheader_table.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
            Eheader_table.AddCell(new PdfPCell(FCell("SOLAR SERVICES CONDITIONS:", BigText - 3, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
            document.Add(Eheader_table);

            Paragraph subheader = new Paragraph("The following additional terms apply to our Solar Services offer and are supplemental to our General Conditions.", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
            subheader.SpacingBefore = 10;
            mcontainerCell.AddElement(subheader);
            #endregion

            PdfPTable termTable = new PdfPTable(new float[] { 30, 600 });
            termTable.LockedWidth = true;
            termTable.TotalWidth = document.PageSize.Width - 100;
            termTable.SpacingBefore = 8;

            int letindex = 0;
            #region Terms
            // letter first then
            Paragraph paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Agreement:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Our offer to provide solar services over the agreed contract length is subject to concluding our Solar Services Agreement (“SSA”).", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;

            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Early Termination Fee:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Our Standard SSA includes the right to terminate the contract from yaer 5 onwards. Early termination is subject to payment of a Termination Amount, based on the costs incurred due to termination. The Termination Amount will decrease each year.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;

            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Energy Production:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" You will be charged for all electrical energy produced by the system and used by you (“Net Energy”), on a monthly basis.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;

            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("System Repair Guarantee:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" The system will be owned, monitored, maintained and insured by us during the agreement term. If the system stops working we will repair or replace any part at our cost.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Purchase Option:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" We provided you the option to purchase the system from year 5 at the higher of a fair market value (determined by an independent valuator) or the Termination Amount.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Right of Use:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Our solar services offer is subject to a right being granted to us to place our system at the installation site, for the length of the SSA, at no charge to us.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Quote Finalization:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Our quote pricing is subject to credit approval.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;

            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Annual Escalations:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Monthly Lease Payments will escalate annually at CPI.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            PdfPCell bottomfiller = new PdfPCell(new Phrase(" ")) { Colspan = 2, Border = Image.NO_BORDER };
            bottomfiller.PaddingBottom = (document.PageSize.Height - termTable.TotalHeight) - 280;
            termTable.AddCell(bottomfiller);

            mcontainerCell.AddElement(termTable);
            #endregion
            container.AddCell(new PdfPCell(mcontainerCell) { Border = Image.NO_BORDER });
            document.Add(container);
            pdfwriter.CloseStream = false;
            document.Close();

            stream.Position = 0;
            stream.Flush();

            return stream.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static byte[] PBSTerms()
        {
            MemoryStream stream = new MemoryStream(); //output stream
            Document document = new Document(); //pdf document to write
            PdfWriter pdfwriter = PdfWriter.GetInstance(document, stream);
            document.Open();

            Paragraph empty = new Paragraph(0, " ");
            empty.SpacingBefore = 1;
            empty.SpacingAfter = 25;
            document.Add(empty);

            PdfPTable container = new PdfPTable(1);
            container.TotalWidth = document.PageSize.Width - 40;
            container.HorizontalAlignment = Image.ALIGN_CENTER;
            container.LockedWidth = true;
            PdfPCell mcontainerCell = new PdfPCell();
            mcontainerCell.HorizontalAlignment = Image.ALIGN_CENTER;
            mcontainerCell.Padding = 30;
            mcontainerCell.PaddingTop = 25;
            mcontainerCell.PaddingBottom = 20;

            #region Header

            PdfPTable Eheader_table = HeaderTable(new float[] { 50, 560 });
            Eheader_table.PaddingTop = 0;
            Eheader_table.SpacingBefore = 17;

            Eheader_table.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
            Eheader_table.AddCell(new PdfPCell(FCell("PAY-BY-SOLAR CONTRACT TERMS & CONDITIONS:", BigText - 3, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
            document.Add(Eheader_table);

            //Paragraph header = new Paragraph("PAY-BY-SOLAR CONTRACT TERMS & CONDITIONS:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.BOLD, nviDarkBlue));
            //mcontainerCell.AddElement(header);

            Paragraph subheader = new Paragraph("The following additional terms apply to our Pay-By-Solar leasing offer and are supplemental to our General Conditions.", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
            subheader.SpacingBefore = 10;
            mcontainerCell.AddElement(subheader);


            #endregion


            PdfPTable termTable = new PdfPTable(new float[] { 30, 600 });
            termTable.LockedWidth = true;
            termTable.TotalWidth = document.PageSize.Width - 100;
            termTable.SpacingBefore = 8;

            int letindex = 0;
            #region Terms
            // letter first then
            Paragraph paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Upfront Payment:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" The Upfront Payment is billable 50% on Purchase Order receipt, 30% on equipment delivery and 20% on completion.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;

            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Monthly Lease Payment:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" You will be billed a monthly lease payment (“Lease Payment”) that will be adjusted (monthly) for under/over energy production by the system (adjusted on a monthly basis).", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;

            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Energy Production:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" A monthly adjustment to the Lease Payment ensures you only pay in the event that the system is working as agreed (or for the portion that is working). You are only required to pay the Lease Payment if the system has produced energy, unless the solar plant's operations have been effected by reasons outside of yout control (e.g. theft, shading caused by you, damange caused by you). Under such circumstances you will still be charged the Lease Payment, which will not be adjusted for under-production until the system has restored to normal operating conditions.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;

            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("System Repair Guarantee:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" The system will be  monitored, maintained and insured by us during the agreement term. If the system stops working due to technical fault we will repair or replace any part at our cost.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("External Damage:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" If the system is damaged for reasons other than a technical failure (example: by falling tree), we will use th insurance proceeds to repair the system to normal working condition. If the damage is not caused by us, you will still be charged the Lease Payment during the period the system is out of order, but will not be charged for the repair work, unless caused by you directly.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;



            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Quote Finalization:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Our quote pricing is subject to credit approval.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;

            paragraph = new Paragraph();
            paragraph.Add(EquipmentPhrase("Annual Escalations:", true, MidText1, nviDarkBlue));
            paragraph.Add(EquipmentPhrase(" Monthly Lease Payments will escalate annually at CPI.", false, MidText1, nviDarkBlue));
            termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
            termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;


            PdfPCell bottomfiller = new PdfPCell(new Phrase(" ")) { Colspan = 2, Border = Image.NO_BORDER };
            bottomfiller.PaddingBottom = (document.PageSize.Height - termTable.TotalHeight) - 280;
            termTable.AddCell(bottomfiller);

            mcontainerCell.AddElement(termTable);
            #endregion
            container.AddCell(new PdfPCell(mcontainerCell) { Border = Image.NO_BORDER });
            document.Add(container);
            pdfwriter.CloseStream = false;
            document.Close();

            stream.Position = 0;
            stream.Flush();

            return stream.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Exclusions"></param>
        /// <returns></returns>
        private static byte[] SpecificTerms(List<string> Exclusions)
        {
            MemoryStream stream = new MemoryStream(); //output stream
            Document document = new Document(); //pdf document to write
            PdfWriter pdfwriter = PdfWriter.GetInstance(document, stream);
            document.Open();

            Paragraph empty = new Paragraph(0, " ");
            empty.SpacingBefore = 1;
            empty.SpacingAfter = 25;
            document.Add(empty);

            PdfPTable container = new PdfPTable(1);
            container.TotalWidth = document.PageSize.Width - 40;
            container.HorizontalAlignment = Image.ALIGN_CENTER;
            container.LockedWidth = true;
            PdfPCell mcontainerCell = new PdfPCell();
            mcontainerCell.HorizontalAlignment = Image.ALIGN_CENTER;
            mcontainerCell.Padding = 30;
            mcontainerCell.PaddingTop = 25;
            mcontainerCell.PaddingBottom = 20;
            Paragraph paragraph = new Paragraph();

            #region Header

            PdfPTable Eheader_table = HeaderTable(new float[] { 50, 560 });
            Eheader_table.PaddingTop = 0;
            Eheader_table.SpacingBefore = 17;

            Eheader_table.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
            Eheader_table.AddCell(new PdfPCell(FCell("TERM SHEETS", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
            document.Add(Eheader_table);

            //Paragraph TitlePara = new Paragraph("TERM SHEETS", new Font(Font.FontFamily.HELVETICA, BigText, Font.BOLD, nviDarkBlue));
            //TitlePara.Alignment = Image.ALIGN_LEFT;
            //document.Add(TitlePara);

            Paragraph header = new Paragraph("SPECIFIC TERMS AGREED FOR THIS OFFER", new Font(Font.FontFamily.HELVETICA, MidText1, Font.BOLD, nviDarkBlue));
            mcontainerCell.AddElement(header);

            Paragraph subheader = new Paragraph("The following additional terms apply to this offer:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
            subheader.SpacingBefore = 10;
            mcontainerCell.AddElement(subheader);


            #endregion

            if (Exclusions.Count <= 0 || Exclusions == null)
            {
                subheader = new Paragraph("None", new Font(Font.FontFamily.HELVETICA, MidText1, Font.BOLD, nviDarkBlue));
                subheader.SpacingBefore = 10;
                mcontainerCell.AddElement(subheader);
            }

            else
            {
                PdfPTable termTable = new PdfPTable(new float[] { 30, 600 });
                termTable.LockedWidth = true;
                termTable.TotalWidth = document.PageSize.Width - 100;
                termTable.SpacingBefore = 8;

                int letindex = 0;
                #region Terms
                // letter first then
                for (int a = 0; a < Exclusions.Count; a++)
                {

                    if (!string.IsNullOrWhiteSpace(Exclusions[a]))
                    {


                        string[] shplit = Exclusions[a].Split(':');
                        paragraph = new Paragraph();

                        if (shplit.Count() > 1)
                        {
                            paragraph.Add(EquipmentPhrase(shplit[0], true, MidText1, nviDarkBlue));
                            paragraph.Add(EquipmentPhrase(":" + shplit[1], false, MidText1, nviDarkBlue));
                        }

                        else
                        {
                            paragraph.Add(EquipmentPhrase(shplit[0], false, MidText1, nviDarkBlue));

                        }

                        termTable.AddCell(new PdfPCell(EquipmentPhrase("(" + alphabet[letindex] + ")", false, MidText1, nviDarkBlue)) { Border = Image.NO_BORDER });
                        termTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER }); letindex++;
                    }
                }



                mcontainerCell.AddElement(termTable);



                #endregion
            }

            subheader = new Paragraph("Client Comments:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
            subheader.SpacingBefore = 10;
            subheader.SpacingAfter = 20;

            mcontainerCell.AddElement(subheader);
            for (int b = 0; b < 5; b++)
            {
                Chunk line = new Chunk(new LineSeparator(0.5f, 100f, BaseColor.BLACK, Element.ALIGN_CENTER, 7));
                mcontainerCell.AddElement(line);
            }

            subheader = new Paragraph("I/We hereby accept this offer, subject to the general and specific conditions stated above and wish to proceed with final contract negotiations.", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
            subheader.SpacingBefore = 15;
            subheader.SpacingAfter = 20;
            mcontainerCell.AddElement(subheader);

            subheader = new Paragraph("Signature:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
            subheader.SpacingBefore = 10;
            subheader.SpacingAfter = 20;
            mcontainerCell.AddElement(subheader);

            Chunk sigLine = new Chunk(new LineSeparator(0.5f, 30f, BaseColor.BLACK, Element.ALIGN_LEFT, 10));
            mcontainerCell.AddElement(sigLine);

            subheader = new Paragraph("Print Name:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
            subheader.SpacingBefore = 10;
            mcontainerCell.AddElement(subheader);

            subheader = new Paragraph("Position:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
            mcontainerCell.AddElement(subheader);
            subheader = new Paragraph("Date:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
            mcontainerCell.AddElement(subheader);

            paragraph = new Paragraph(" ");
            paragraph.SpacingAfter = (document.PageSize.Height - mcontainerCell.Height) - 620;
            mcontainerCell.AddElement(paragraph);

            container.AddCell(new PdfPCell(mcontainerCell) { Border = Image.NO_BORDER });
            document.Add(container);
            pdfwriter.CloseStream = false;
            document.Close();

            stream.Position = 0;
            stream.Flush();

            return stream.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="letter"></param>
        /// <param name="index"></param>
        /// <param name="super"></param>
        /// <returns></returns>
        public static byte[] createFinancialAnalysis(string letter, int index, SuperProject super)
        {
            MemoryStream stream = new MemoryStream(); //output stream
            Document document = new Document(); //pdf document to write
            PdfWriter pdfwriter = PdfWriter.GetInstance(document, stream);
            document.Open();

            //make a landscape A4 page to rotate later
            document.SetPageSize(PageSize.A4.Rotate());

            document.NewPage();

            Paragraph empty = new Paragraph(0, " ");

            document.Add(empty);

            PdfPTable header_table = new PdfPTable(new float[] { 550, 40, 60 });
            header_table.TotalWidth = document.PageSize.Width - (document.RightMargin * 2);
            header_table.LockedWidth = true;
            header_table.PaddingTop = -30;
            header_table.SpacingBefore = -50;
            //Top heading
            header_table.AddCell(new PdfPCell(MyCell("FINANCIAL REVIEW (OUTRIGHT PURCHASE)", MidText1, true, false, true)) { PaddingTop = 4, PaddingBottom = 4 });
            header_table.AddCell(ContentCell(" ", CellInfo.Empty));
            header_table.AddCell(new PdfPCell(MyCell(letter + index, MidText1, true, false, true)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = 4, PaddingTop = 4 });
            document.Add(header_table);


            PdfPTable Finance_Main_Table = new PdfPTable(1);
            Finance_Main_Table.TotalWidth = document.PageSize.Width - (document.RightMargin * 2);
            Finance_Main_Table.SpacingBefore = 4f;
            Finance_Main_Table.LockedWidth = true;


            //calculate all variable lists 
            double enCost = super.ExistingPower;

            double[] encost20 = new double[20];
            encost20[0] = enCost;

            double[] supOMADMIN20 = new double[20];
            //(OM+Admin)/1000*(1+CPI)^C17

            double[] enSave20 = new double[20];


            double[] totalWithoutSolar = new double[15];


            double[] totalWithSolar = new double[15];
            //temp
            //((((Demand/1000)-solarpro)*currentEc)+solarmac)

            for (int o = 1; o < 16; o++)
            {
                enCost = enCost * (1 + super.Country.UtilityInflation / 100);
                encost20[o - 1] = enCost;
                supOMADMIN20[o - 1] = ((super.OM * 1000 + super.Admin) / 1000.00 * Math.Pow((1 + super.Country.CPI / 100.00), o - 1.0));
                enSave20[o - 1] = encost20[o - 1] * super.SolarProduction20[o - 1];
                totalWithoutSolar[o - 1] = super.Project.Costing.Demand * encost20[o - 1] / 1000.00;

                totalWithSolar[o - 1] = ((((super.Project.Costing.Demand / 1000) - super.SolarProduction20[o - 1]) * encost20[o - 1]) + supOMADMIN20[o - 1]);

            }

            #region Finanace Row 1

            PdfPTable Finace_Row1_Nest = new PdfPTable(new float[] { 335, 50, 300, 90, 130, 30, 48, 45 });



            PdfPTable Finace_KeyInputs_Table = new PdfPTable(new float[] { 300, 140 });

            PdfPCell Finace_KeyInputs_Header = FCell("KEY INPUTS:", MidText2 + 0.2f, true, false, true, BaseColor.WHITE);
            Finace_KeyInputs_Header.Colspan = 2;
            Finace_KeyInputs_Header.BackgroundColor = nviDarkBlue;
            Finace_KeyInputs_Header.PaddingBottom = 5;
            Finace_KeyInputs_Table.AddCell(Finace_KeyInputs_Header);


            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell("System Size:", MidText2, false, false, false, nviDarkBlue)) { PaddingTop = 5 });
            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell((super.prosize * 1000).ToString("#,##0.##") + " kWp", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingTop = 5 });
            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell("Total Contract Costs (Excl. VAT):", MidText2, false, false, false, nviDarkBlue)) { PaddingTop = 5 });
            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell(super.Country.Currency + " " + (super.TotalEq_A + super.TotalEq_BA).ToString("#,##0.##"), MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingTop = 5 });
            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell("Mainenance Costs (Monthly):", MidText2, false, false, false, nviDarkBlue)) { PaddingTop = 5 });
            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell(super.Country.Currency + " " + (super.OM + super.Admin) + "/mnth", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingTop = 5 });
            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell("Annual Solar Production Degradation (-):", MidText2, false, false, false, nviDarkBlue)) { PaddingTop = 5 });
            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell(super.Df.ToString("0.##") + " %", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingTop = 5 });
            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell("Maintenance Cost Increases(CPI):", MidText2, false, false, false, nviDarkBlue)) { PaddingTop = 5 });
            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell(super.Country.CPI.ToString("0.##") + " %", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingTop = 5 });
            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell("Your Current Unit Cost of Energy:", MidText2, false, false, false, nviDarkBlue)) { PaddingTop = 5 });
            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell(super.Country.Currency + " " + super.ExistingPower.ToString("#,##0.##"), MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingTop = 5 });
            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell("Annual Electricity Consumption:", MidText2, false, false, false, nviDarkBlue)) { PaddingTop = 5 });
            Finace_KeyInputs_Table.AddCell(new PdfPCell(FCell(super.Project.Costing.Demand.ToString() + " kWh", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingTop = 5 });

            Finace_Row1_Nest.AddCell(new PdfPCell(Finace_KeyInputs_Table) { Border = Image.NO_BORDER });



            PdfPTable Finace_KeyOutputs_Table = new PdfPTable(new float[] { 350, 120 });

            PdfPCell Finace_KeyOutputs_Header = FCell("KEY OUTPUTS:", MidText2 + 0.2f, true, false, true, BaseColor.WHITE);
            Finace_KeyOutputs_Header.Colspan = 2;
            Finace_KeyOutputs_Header.BackgroundColor = nviDarkBlue;
            Finace_KeyOutputs_Header.PaddingBottom = 5;
            Finace_KeyOutputs_Table.AddCell(Finace_KeyOutputs_Header);

            //empty row
            Finace_KeyOutputs_Table.AddCell(new PdfPCell(FCell(" ", MidText2 + 0.2f, true, false, false, BaseColor.WHITE)) { PaddingTop = 5 });
            Finace_KeyOutputs_Table.AddCell(new PdfPCell(FCell(" ", MidText2 + 0.2f, true, false, false, BaseColor.WHITE)) { PaddingTop = 5 });


            Finace_KeyOutputs_Table.AddCell(new PdfPCell(FCell("First Year Electricity Savings:", MidText2, true, false, false, nviDarkBlue)) { PaddingTop = 5 });
            Finace_KeyOutputs_Table.AddCell(new PdfPCell(FCell(super.Country.Currency + " " + (enSave20[0] * 1000).ToString("#,##"), MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingTop = 5 });
            Finace_KeyOutputs_Table.AddCell(new PdfPCell(FCell("Daily Electricity Savings", MidText2, true, false, false, nviDarkBlue)) { PaddingTop = 5 });
            Finace_KeyOutputs_Table.AddCell(new PdfPCell(FCell(super.Country.Currency + " " + (enSave20[0] / 365 * 1000).ToString("#,##"), MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingTop = 5 });
            Finace_KeyOutputs_Table.AddCell(new PdfPCell(FCell("Payback Period", MidText2, true, false, false, nviDarkBlue)) { PaddingTop = 5 });
            Finace_KeyOutputs_Table.AddCell(new PdfPCell(FCell(super.Project.Costing.Term + " Years", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingTop = 5 });
            Finace_KeyOutputs_Table.AddCell(new PdfPCell(FCell("IRR (20 Years)", MidText2, true, false, false, nviDarkBlue)) { PaddingTop = 5 });
            Finace_KeyOutputs_Table.AddCell(new PdfPCell(FCell("Need Details", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingTop = 5 });




            Finace_Row1_Nest.AddCell(ContentCell("", CellInfo.Empty));
            Finace_Row1_Nest.AddCell(new PdfPCell(Finace_KeyOutputs_Table) { Border = Image.NO_BORDER });




            PdfPTable Finance_SupplyEnergy_Table = new PdfPTable(1);

            Finance_SupplyEnergy_Table.AddCell(new PdfPCell(FCell("SUPPLY OF ENERGY", MidText1 + 1, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER });


            double tSolar = super.prosize * super.Project.Costing.Yield;

            double exist = super.Project.Costing.Demand / 1000 - tSolar;

            double totS = tSolar + exist;

            Image supplyEnergyPie = Image.GetInstance(GeneratePieChart(85, 85, Convert.ToInt32(tSolar / totS * 100), Convert.ToInt32(exist / totS * 100)));

            //Finance_SupplyEnergy_Table.AddCell(new PdfPCell(FCell("INSERT GRAPH IMAGE HERE", MidText1, true, false, false, nviBlue)));
            Finance_SupplyEnergy_Table.AddCell(new PdfPCell(supplyEnergyPie) { Border = Image.NO_BORDER });

            Finace_Row1_Nest.AddCell(ContentCell("", CellInfo.Empty));
            Finace_Row1_Nest.AddCell(new PdfPCell(Finance_SupplyEnergy_Table) { Border = Image.NO_BORDER });




            Finace_Row1_Nest.AddCell(ContentCell("", CellInfo.Empty));

            PdfPTable Finance_SupplyEnergy_Table2 = new PdfPTable(1);
            Finance_SupplyEnergy_Table2.AddCell(ContentCell("", CellInfo.Empty));
            Finance_SupplyEnergy_Table2.AddCell(ContentCell("", CellInfo.Empty));

            PdfPCell rowSub = FCell("SOLAR", MidText2 - 2, false, false, true, BaseColor.WHITE);
            rowSub.HorizontalAlignment = Image.ALIGN_CENTER;
            rowSub.BackgroundColor = nviGreen;
            rowSub.PaddingBottom = 4;
            rowSub.PaddingTop = 4;

            Finance_SupplyEnergy_Table2.AddCell(rowSub);

            rowSub = FCell((Math.Round(tSolar / totS * 100).ToString() + "%"), MidText2 - 2, false, false, true, nviDarkBlue);
            rowSub.HorizontalAlignment = Image.ALIGN_CENTER;
            rowSub.BackgroundColor = BaseColor.WHITE;
            rowSub.PaddingBottom = 4;
            rowSub.PaddingTop = 3;

            Finance_SupplyEnergy_Table2.AddCell(rowSub);

            rowSub = FCell("EXISTING", MidText2 - 2, true, false, true, nviDarkBlue);
            rowSub.HorizontalAlignment = Image.ALIGN_CENTER;
            rowSub.BackgroundColor = nviLightBLue;
            rowSub.PaddingBottom = 4;
            rowSub.PaddingTop = 4;

            Finance_SupplyEnergy_Table2.AddCell(rowSub);

            rowSub = FCell((Math.Round(exist / totS * 100).ToString() + "%"), MidText2 - 2, false, false, true, nviDarkBlue);
            rowSub.HorizontalAlignment = Image.ALIGN_CENTER;
            rowSub.BackgroundColor = BaseColor.WHITE;
            rowSub.PaddingBottom = 4;
            rowSub.PaddingTop = 3;
            Finance_SupplyEnergy_Table2.AddCell(rowSub);





            Finace_Row1_Nest.AddCell(new PdfPCell(Finance_SupplyEnergy_Table2) { Border = Image.NO_BORDER });
            Finace_Row1_Nest.AddCell(new PdfPCell(new Phrase("")) { Border = Image.NO_BORDER });
            Finance_Main_Table.AddCell(new PdfPCell(Finace_Row1_Nest) { Border = Image.NO_BORDER });


            #endregion


            #region Finance Row 2

            PdfPTable Finance_Row2_nest = new PdfPTable(new float[] { 170, 50, 720 });
            Finance_Row2_nest.SpacingBefore = 5;


            float[] Table15Widths = new float[] { 110, 110, 110, 110, 110, 110, 110, 110, 110, 110, 110, 110, 110, 110, 110 };
            float Table15a = 580f;
            PdfPCell Finance_Row2_nest_header = FCell("CASH FLOW ANALYSIS ('000)", MidText2 + 0.2f, true, false, true, BaseColor.WHITE);
            Finance_Row2_nest_header.PaddingBottom = 4;
            Finance_Row2_nest_header.Colspan = 3;
            Finance_Row2_nest_header.BackgroundColor = nviDarkBlue;
            Finance_Row2_nest.AddCell(Finance_Row2_nest_header);


            PdfPCell Finance_Row2_nest_headerSub = FCell("(All amounts are stated in '000, other than those shown as unit costs)", MidText2 + 0.2f, false, false, false, nviDarkBlue);
            Finance_Row2_nest_headerSub.Colspan = 3;
            Finance_Row2_nest_headerSub.PaddingBottom = 10;
            Finance_Row2_nest.AddCell(new PdfPCell(Finance_Row2_nest_headerSub) { Border = Image.NO_BORDER });

            Finance_Row2_nest.AddCell(new PdfPCell(new Phrase("")) { Border = Image.NO_BORDER, Colspan = 3 });

            //CELL _ CELL _ TABLE (inside cell)


            PdfPCell Cashflow_Label = FCell("Years", MidText2 + 0.2f, true, false, false, nviDarkBlue);
            PdfPCell Cashflow_Char = FCell(" ", MidText2 + 0.2f, false, false, false, nviDarkBlue);
            Cashflow_Char.HorizontalAlignment = Image.ALIGN_CENTER;
            PdfPCell Cashflow_NestTable = new PdfPCell();
            Cashflow_NestTable.HorizontalAlignment = Image.ALIGN_RIGHT;
            Cashflow_NestTable.BorderColor = nviDarkBlue;


            PdfPTable Cashflow_15_Table = new PdfPTable(Table15Widths);
            Cashflow_15_Table.HorizontalAlignment = Image.ALIGN_RIGHT;
            Cashflow_15_Table.TotalWidth = Table15a;
            Cashflow_15_Table.LockedWidth = true;


            //Years 1 to 15

            for (int a = 1; a < 16; a++)
            {
                Cashflow_15_Table.AddCell(new PdfPCell(FCell(a.ToString(), MidText2 + 0.2f, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 0, PaddingLeft = -3 });
            }




            Cashflow_NestTable.AddElement(Cashflow_15_Table);
            Finance_Row2_nest.AddCell(Cashflow_Label);
            Finance_Row2_nest.AddCell(Cashflow_Char);
            Finance_Row2_nest.AddCell(Cashflow_NestTable);





            //Electricity cosr escl.
            Cashflow_Label = FCell("Electricity Cost Escl.(%)", MidText2 + 0.2f, false, false, false, nviDarkBlue);
            Cashflow_Char = FCell(" ", MidText2 + 0.2f, true, false, false, nviDarkBlue);
            Cashflow_Char.HorizontalAlignment = Image.ALIGN_CENTER;

            Cashflow_NestTable = new PdfPCell() { Border = Image.NO_BORDER };
            Cashflow_15_Table = new PdfPTable(Table15Widths);
            Cashflow_15_Table.HorizontalAlignment = Image.ALIGN_RIGHT;
            Cashflow_15_Table.TotalWidth = Table15a;
            Cashflow_15_Table.LockedWidth = true;
            Cashflow_NestTable.HorizontalAlignment = Image.ALIGN_RIGHT;

            for (int a = 1; a < 16; a++)
            {
                Cashflow_15_Table.AddCell(new PdfPCell(new Phrase(super.Country.UtilityInflation.ToString() + "%", new Font(Font.FontFamily.HELVETICA, MidText2 + 0.2f, Font.ITALIC, nviDarkBlue))) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 0, Border = Image.NO_BORDER, PaddingBottom = 5, PaddingLeft = -3 });
            }


            Cashflow_NestTable.AddElement(Cashflow_15_Table);
            Finance_Row2_nest.AddCell(Cashflow_Label);
            Finance_Row2_nest.AddCell(Cashflow_Char);
            Finance_Row2_nest.AddCell(Cashflow_NestTable);

            //add space



            //Current electricty cost (KWh Unit)
            Cashflow_Label = FCell("Current Electricity Cost (/KWh Unit)", MidText2 + 0.2f, false, false, false, nviDarkBlue);
            Cashflow_Label.PaddingTop = 5;
            Cashflow_Char = new PdfPCell(new Phrase("A", new Font(Font.FontFamily.HELVETICA, MidText2 + 0.2f, Font.ITALIC, nviDarkBlue))) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.NO_BORDER };
            Cashflow_Char.HorizontalAlignment = Image.ALIGN_CENTER;
            Cashflow_Char.PaddingTop = 5;
            Cashflow_NestTable = new PdfPCell() { BorderColorBottom = BaseColor.WHITE, BorderWidthBottom = 0 };
            Cashflow_15_Table = new PdfPTable(Table15Widths);
            Cashflow_15_Table.HorizontalAlignment = Image.ALIGN_RIGHT;
            Cashflow_15_Table.TotalWidth = Table15a;
            Cashflow_15_Table.LockedWidth = true;
            Cashflow_NestTable.HorizontalAlignment = Image.ALIGN_RIGHT;

            for (int a = 1; a < 16; a++)
            {

                Cashflow_15_Table.AddCell(new PdfPCell((FCell((encost20[a - 1]).ToString("0.##"), MidText2 + 0.2f, false, false, false, nviDarkBlue))) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 0 });

            }


            Cashflow_NestTable.AddElement(Cashflow_15_Table);
            Finance_Row2_nest.AddCell(Cashflow_Label);
            Finance_Row2_nest.AddCell(Cashflow_Char);
            Finance_Row2_nest.AddCell(Cashflow_NestTable);


            //Solar Maitenance 

            Cashflow_Label = FCell("Solar Maintenance Cost*", MidText2 + 0.2f, false, false, false, nviDarkBlue);
            Cashflow_Char = new PdfPCell(new Phrase("B", new Font(Font.FontFamily.HELVETICA, MidText2 + 0.2f, Font.ITALIC, nviDarkBlue))) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.NO_BORDER };
            Cashflow_Char.HorizontalAlignment = Image.ALIGN_CENTER;

            Cashflow_NestTable = new PdfPCell() { BorderColorTop = BaseColor.WHITE, BorderWidthTop = 0 };
            Cashflow_15_Table = new PdfPTable(Table15Widths);
            Cashflow_15_Table.HorizontalAlignment = Image.ALIGN_RIGHT;
            Cashflow_15_Table.TotalWidth = Table15a;
            Cashflow_15_Table.LockedWidth = true;
            Cashflow_15_Table.PaddingTop = -2;
            Cashflow_NestTable.HorizontalAlignment = Image.ALIGN_RIGHT;


            for (int a = 1; a < 16; a++)
            {
                Cashflow_15_Table.AddCell(new PdfPCell(FCell(supOMADMIN20[a - 1].ToString("#,##0.#"), MidText2 + 0.2f, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 0, PaddingLeft = 0 });
            }


            Cashflow_NestTable.AddElement(Cashflow_15_Table);
            Finance_Row2_nest.AddCell(Cashflow_Label);
            Finance_Row2_nest.AddCell(Cashflow_Char);
            Finance_Row2_nest.AddCell(Cashflow_NestTable);


            //empty row


            Cashflow_Label = FCell("Solar Production (KWh)*", MidText2 + 0.2f, false, false, false, nviDarkBlue);
            Cashflow_Label.PaddingTop = 5;
            Cashflow_Char = new PdfPCell(new Phrase("C", new Font(Font.FontFamily.HELVETICA, MidText2 + 0.2f, Font.ITALIC, nviDarkBlue))) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER };
            Cashflow_Char.HorizontalAlignment = Image.ALIGN_CENTER;
            Cashflow_Char.PaddingBottom = 2;
            Cashflow_Char.BorderWidthLeft = 0;
            Cashflow_Char.BorderWidthRight = 0;
            Cashflow_Char.BorderWidthTop = 0;
            Cashflow_Char.PaddingTop = 3;
            Cashflow_NestTable = new PdfPCell() { BorderColorTop = BaseColor.WHITE, BorderWidthTop = 0, BorderWidthLeft = 0, BorderWidthRight = 0 };
            Cashflow_15_Table = new PdfPTable(Table15Widths);
            Cashflow_15_Table.HorizontalAlignment = Image.ALIGN_RIGHT;
            Cashflow_15_Table.TotalWidth = Table15a;
            Cashflow_15_Table.LockedWidth = true;
            Cashflow_NestTable.HorizontalAlignment = Image.ALIGN_RIGHT;


            for (int a = 1; a < 16; a++)
            {
                Cashflow_15_Table.AddCell(new PdfPCell(FCell(super.SolarProduction20[a - 1].ToString("0."), MidText2 + 0.2f, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 0, PaddingTop = 0 });
            }


            Cashflow_NestTable.AddElement(Cashflow_15_Table);
            Finance_Row2_nest.AddCell(Cashflow_Label);
            Finance_Row2_nest.AddCell(Cashflow_Char);
            Finance_Row2_nest.AddCell(Cashflow_NestTable);






            //Energy Costs Saved
            //A * C

            Cashflow_Label = FCell("Energy Costs Saved:", MidText2 + 0.2f, false, false, false, nviDarkBlue);
            Cashflow_Char = new PdfPCell(new Phrase("A * C", new Font(Font.FontFamily.HELVETICA, MidText2 + 0.2f, Font.ITALIC, nviDarkBlue))) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.NO_BORDER };
            Cashflow_Char.HorizontalAlignment = Image.ALIGN_CENTER;

            Cashflow_NestTable = new PdfPCell() { BorderColorBottom = BaseColor.WHITE, BorderWidthBottom = 0, BorderWidthRight = 0, BorderWidthLeft = 0, BorderWidthTop = 0 };
            Cashflow_15_Table = new PdfPTable(Table15Widths);
            Cashflow_15_Table.HorizontalAlignment = Image.ALIGN_RIGHT;
            Cashflow_15_Table.TotalWidth = Table15a;
            Cashflow_15_Table.LockedWidth = true;
            Cashflow_NestTable.HorizontalAlignment = Image.ALIGN_RIGHT;

            for (int a = 1; a < 16; a++)
            {
                Cashflow_15_Table.AddCell(new PdfPCell(FCell(enSave20[a - 1].ToString("#,##0.#"), MidText2 + 0.2f, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 0, PaddingLeft = 0 });
            }


            Cashflow_NestTable.AddElement(Cashflow_15_Table);
            Finance_Row2_nest.AddCell(Cashflow_Label);
            Finance_Row2_nest.AddCell(Cashflow_Char);
            Finance_Row2_nest.AddCell(Cashflow_NestTable);





            //Less Solar Maintenance Costs *
            Cashflow_Label = FCell("Less Solar Maintenance Costs *", MidText2 + 0.2f, false, false, false, nviDarkBlue);
            Cashflow_Char = new PdfPCell(new Phrase("B", new Font(Font.FontFamily.HELVETICA, MidText2 + 0.2f, Font.ITALIC, nviDarkBlue))) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER };
            Cashflow_Char.HorizontalAlignment = Image.ALIGN_CENTER;

            Cashflow_Char.BorderWidthLeft = 0;
            Cashflow_Char.BorderWidthRight = 0;
            Cashflow_Char.BorderWidthTop = 0;

            Cashflow_NestTable = new PdfPCell() { BorderColorTop = BaseColor.WHITE, BorderWidthTop = 0, BorderWidthLeft = 0, BorderWidthRight = 0 };
            Cashflow_15_Table = new PdfPTable(Table15Widths);
            Cashflow_15_Table.HorizontalAlignment = Image.ALIGN_RIGHT;
            Cashflow_15_Table.TotalWidth = Table15a;
            Cashflow_15_Table.LockedWidth = true;
            Cashflow_NestTable.HorizontalAlignment = Image.ALIGN_RIGHT;

            for (int a = 1; a < 16; a++)
            {
                Cashflow_15_Table.AddCell(new PdfPCell(FCell("(" + (supOMADMIN20[a - 1]).ToString("#,##0.#") + ")", MidText2 + 0.2f, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 0, PaddingLeft = 0 });
            }


            Cashflow_NestTable.AddElement(Cashflow_15_Table);
            Finance_Row2_nest.AddCell(Cashflow_Label);
            Finance_Row2_nest.AddCell(Cashflow_Char);
            Finance_Row2_nest.AddCell(Cashflow_NestTable);




            //Net Cash Savings (Per Yr)

            Cashflow_Label = FCell("Net Cash Savings (Per Yr)", MidText2 + 0.2f, true, false, false, nviDarkBlue);
            Cashflow_Char = ContentCell(" ", CellInfo.Empty);
            Cashflow_Char.HorizontalAlignment = Image.ALIGN_CENTER;

            Cashflow_NestTable = new PdfPCell() { BorderWidthBottom = 1.5f, BorderWidthTop = 0, BorderWidthLeft = 0, BorderWidthRight = 0, BorderColorLeft = nviLightBLue, BorderColorRight = nviLightBLue, BorderColorTop = nviLightBLue, BackgroundColor = nviLightBLue };
            Cashflow_15_Table = new PdfPTable(Table15Widths);
            Cashflow_15_Table.HorizontalAlignment = Image.ALIGN_RIGHT;
            Cashflow_15_Table.TotalWidth = Table15a;
            Cashflow_15_Table.LockedWidth = true;
            Cashflow_NestTable.HorizontalAlignment = Image.ALIGN_RIGHT;

            for (int a = 1; a < 16; a++)
            {
                Cashflow_15_Table.AddCell(new PdfPCell(FCell((enSave20[a - 1] - supOMADMIN20[a - 1]).ToString("0."), MidText2 + 0.2f, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 0 });
            }


            Cashflow_NestTable.AddElement(Cashflow_15_Table);
            Finance_Row2_nest.AddCell(Cashflow_Label);
            Finance_Row2_nest.AddCell(Cashflow_Char);
            Finance_Row2_nest.AddCell(Cashflow_NestTable);

            //empty row
            Finance_Row2_nest.AddCell(ContentCell(" ", CellInfo.Row));

            Finance_Main_Table.AddCell(new PdfPCell(Finance_Row2_nest) { Border = Image.NO_BORDER });


            #endregion



            #region Finanace Row 3
            PdfPTable Finance_Row3_nest = new PdfPTable(new float[] { 170, 50, 720 });
            PdfPCell Finance_Row3_nest_header = FCell("OVERALL OUTCOME ('000)", MidText2 + 0.2f, true, false, true, BaseColor.WHITE);
            Finance_Row3_nest_header.PaddingBottom = 4;
            Finance_Row3_nest_header.Colspan = 3;
            Finance_Row3_nest_header.BackgroundColor = nviDarkBlue;
            Finance_Row3_nest.AddCell(Finance_Row3_nest_header);
            Finance_Row3_nest.SpacingBefore = 1;



            PdfPCell OverallOutcome_Label = FCell("Total Costs (Withour Solar PV)", MidText2 + 0.2f, false, false, false, nviDarkBlue);
            PdfPCell OverallOutcome_Char = new PdfPCell(new Phrase("A", new Font(Font.FontFamily.HELVETICA, MidText2 + 0.2f, Font.ITALIC, nviDarkBlue))) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.NO_BORDER };
            OverallOutcome_Char.HorizontalAlignment = Image.ALIGN_CENTER;
            PdfPCell OverallOutcome_NestTable = new PdfPCell();
            OverallOutcome_NestTable.Border = Image.NO_BORDER;
            OverallOutcome_NestTable.HorizontalAlignment = Image.ALIGN_RIGHT;

            PdfPTable OverallOutcome_15_Table = new PdfPTable(Table15Widths);
            OverallOutcome_15_Table.HorizontalAlignment = Image.ALIGN_RIGHT;
            OverallOutcome_15_Table.TotalWidth = Table15a;
            OverallOutcome_15_Table.LockedWidth = true;



            //temp

            for (int a = 1; a < 16; a++)
            {
                OverallOutcome_15_Table.AddCell(new PdfPCell(FCell(totalWithoutSolar[a - 1].ToString("0."), MidText2 + 0.2f, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT });
            }




            OverallOutcome_NestTable.AddElement(OverallOutcome_15_Table);
            Finance_Row3_nest.AddCell(OverallOutcome_Label);
            Finance_Row3_nest.AddCell(OverallOutcome_Char);
            Finance_Row3_nest.AddCell(OverallOutcome_NestTable);





            OverallOutcome_Label = FCell("Total Costs (With Solar PV)", MidText2 + 0.2f, false, false, false, nviDarkBlue);
            OverallOutcome_Char = new PdfPCell(new Phrase("B", new Font(Font.FontFamily.HELVETICA, MidText2 + 0.2f, Font.ITALIC, nviDarkBlue))) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.NO_BORDER };
            OverallOutcome_Char.HorizontalAlignment = Image.ALIGN_CENTER;
            OverallOutcome_NestTable = new PdfPCell();
            OverallOutcome_NestTable.BorderWidthLeft = 0;
            OverallOutcome_NestTable.BorderWidthRight = 0;
            OverallOutcome_NestTable.BorderWidthTop = 0;
            OverallOutcome_NestTable.HorizontalAlignment = Image.ALIGN_RIGHT;

            OverallOutcome_Char.BorderWidthLeft = 0;
            OverallOutcome_Char.BorderWidthRight = 0;
            OverallOutcome_Char.BorderWidthTop = 0;

            OverallOutcome_15_Table = new PdfPTable(Table15Widths);
            OverallOutcome_15_Table.HorizontalAlignment = Image.ALIGN_RIGHT;
            OverallOutcome_15_Table.TotalWidth = Table15a;
            OverallOutcome_15_Table.LockedWidth = true;


            for (int a = 1; a < 16; a++)
            {
                OverallOutcome_15_Table.AddCell(new PdfPCell(FCell(totalWithSolar[a - 1].ToString("0."), MidText2 + 0.2f, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT });
            }




            OverallOutcome_NestTable.AddElement(OverallOutcome_15_Table);
            Finance_Row3_nest.AddCell(OverallOutcome_Label);
            Finance_Row3_nest.AddCell(OverallOutcome_Char);
            Finance_Row3_nest.AddCell(OverallOutcome_NestTable);





            OverallOutcome_Label = FCell("Net Cash Savings (Per Yr)", MidText2 + 0.2f, true, false, false, nviDarkBlue);
            OverallOutcome_Char = new PdfPCell(new Phrase("A - B", new Font(Font.FontFamily.HELVETICA, MidText2 + 0.2f, Font.ITALIC, nviDarkBlue))) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.NO_BORDER };
            OverallOutcome_Char.HorizontalAlignment = Image.ALIGN_CENTER;
            OverallOutcome_NestTable = new PdfPCell() { BorderWidthBottom = 1.5f, BorderWidthTop = 0, BorderWidthLeft = 0, BorderWidthRight = 0, BorderColorLeft = nviLightBLue, BorderColorRight = nviLightBLue, BorderColorTop = nviLightBLue, BackgroundColor = nviLightBLue };
            OverallOutcome_NestTable.BorderWidthLeft = 0;
            OverallOutcome_NestTable.BorderWidthRight = 0;
            OverallOutcome_NestTable.BorderWidthTop = 0;
            OverallOutcome_NestTable.BorderWidthBottom = 1.5f;
            OverallOutcome_NestTable.HorizontalAlignment = Image.ALIGN_RIGHT;

            OverallOutcome_15_Table = new PdfPTable(Table15Widths);
            OverallOutcome_15_Table.HorizontalAlignment = Image.ALIGN_RIGHT;
            OverallOutcome_15_Table.TotalWidth = Table15a;
            OverallOutcome_15_Table.LockedWidth = true;


            //temp

            for (int a = 1; a < 16; a++)
            {
                OverallOutcome_15_Table.AddCell(new PdfPCell(FCell((totalWithoutSolar[a - 1] - totalWithSolar[a - 1]).ToString("0."), MidText2 + 0.2f, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = 2 });
            }




            OverallOutcome_NestTable.AddElement(OverallOutcome_15_Table);
            Finance_Row3_nest.AddCell(OverallOutcome_Label);
            Finance_Row3_nest.AddCell(OverallOutcome_Char);
            Finance_Row3_nest.AddCell(OverallOutcome_NestTable);


            PdfPTable BarGraphLabelNest = new PdfPTable(new float[] { 30, 50, 10 });
            BarGraphLabelNest.AddCell(new PdfPCell(FCell("Notes:", MidText2 + -2, true, false, false, BaseColor.BLACK)) { Colspan = 3, PaddingTop = 5 });
            BarGraphLabelNest.AddCell(new PdfPCell(new Phrase("* Estimated Annual Maintenance Cost ('000) escl. at CPI", new Font(Font.FontFamily.HELVETICA, MidText2 - 3, Font.ITALIC, nviDarkBlue))) { Border = Image.NO_BORDER, Colspan = 3 });
            BarGraphLabelNest.AddCell(ContentCell(" ", CellInfo.Empty));
            BarGraphLabelNest.AddCell(ContentCell(" ", CellInfo.Empty));
            BarGraphLabelNest.AddCell(ContentCell(" ", CellInfo.Empty));





            rowSub = FCell("New Costs", MidText2 + 0.2f, true, true, true, BaseColor.WHITE);
            rowSub.HorizontalAlignment = Image.ALIGN_CENTER;
            rowSub.BackgroundColor = nviGreen;
            rowSub.BorderColor = nviGreen;
            rowSub.Padding = 0;
            rowSub.PaddingTop = 2;
            rowSub.PaddingBottom = 0;
            rowSub.BorderWidthBottom = 0;

            BarGraphLabelNest.AddCell(ContentCell(" ", CellInfo.Empty));
            BarGraphLabelNest.AddCell(rowSub);
            BarGraphLabelNest.AddCell(ContentCell(" ", CellInfo.Empty));

            //empty row
            BarGraphLabelNest.AddCell(new PdfPCell(FCell(" ", MidText2 - 3, false, false, false, BaseColor.WHITE)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.NO_BORDER });
            BarGraphLabelNest.AddCell(new PdfPCell(FCell(" ", MidText2 - 3, false, false, false, BaseColor.WHITE)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.NO_BORDER });
            BarGraphLabelNest.AddCell(new PdfPCell(FCell(" ", MidText2 - 3, false, false, false, BaseColor.WHITE)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.NO_BORDER });


            rowSub = FCell("Old Costs", MidText2, false, true, false, nviDarkBlue);
            rowSub.Padding = 0;

            rowSub.PaddingTop = 2;
            rowSub.PaddingBottom = 5;
            rowSub.BorderWidth = 1.5f;
            rowSub.HorizontalAlignment = Image.ALIGN_CENTER;
            rowSub.BorderColor = nviDarkBlue;

            BarGraphLabelNest.AddCell(new PdfPCell(FCell(" ", MidText2 - 3, false, false, false, BaseColor.WHITE)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.NO_BORDER });
            BarGraphLabelNest.AddCell(rowSub);
            BarGraphLabelNest.AddCell(new PdfPCell(FCell(" ", MidText2 - 3, false, false, false, BaseColor.WHITE)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.NO_BORDER });

            BarGraphLabelNest.AddCell(new PdfPCell(FCell(" ", MidText2 - 3, false, false, false, BaseColor.WHITE)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.NO_BORDER });
            BarGraphLabelNest.AddCell(new PdfPCell(FCell(" ", MidText2 - 3, false, false, false, BaseColor.WHITE)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.NO_BORDER });
            BarGraphLabelNest.AddCell(new PdfPCell(FCell(" ", MidText2 - 3, false, false, false, BaseColor.WHITE)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.NO_BORDER });

            Finance_Row3_nest.AddCell(new PdfPCell(BarGraphLabelNest) { Border = Image.NO_BORDER });

            //TODO get real old costs new costs


            Image oldNewCostBarGraph = Image.GetInstance(GenerateBarGraph(640, 120, totalWithoutSolar, totalWithSolar));


            Finance_Row3_nest.AddCell(FCell(" ", MidText1, false, false, false, BaseColor.WHITE));

            PdfPTable graphnesttable = new PdfPTable(1);
            graphnesttable.AddCell(new PdfPCell(FCell("OLD COSTS vs NEW COST", MidText2 - 1, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingTop = 5 });
            graphnesttable.AddCell(new PdfPCell(oldNewCostBarGraph) { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_LEFT, Padding = 0, PaddingTop = 5, PaddingLeft = -41, Colspan = 1 });

            Finance_Row3_nest.AddCell(new PdfPCell(graphnesttable) { Padding = 0, Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER });
            Finance_Main_Table.AddCell(new PdfPCell(Finance_Row3_nest) { Border = Image.NO_BORDER });


            document.Add(Finance_Main_Table);
            #endregion


            document.SetPageSize(PageSize.A4);
            pdfwriter.CloseStream = false;
            document.Close();

            stream.Position = 0;
            stream.Flush();

            return stream.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="letter"></param>
        /// <param name="index"></param>
        /// <param name="subindex"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        public static byte[] generateOffer(string type, string letter, int index, int subindex, Project project)
        {
            SuperProject super = new SuperProject(project);
            MemoryStream stream = new MemoryStream(); //output stream
            Document document = new Document(); //pdf document to write
            PdfWriter pdfwriter = PdfWriter.GetInstance(document, stream);
            document.Open();

            Image bulletPoint = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/bullet square.png"));
            bulletPoint.ScalePercent(80);
            bulletPoint.Alignment = Image.ALIGN_CENTER;
            int subsub = 1;
            //Financed solutions  x, offer page (done) , addition terms x

            string fontpath = System.Web.HttpContext.Current.Server.MapPath("~/fonts/wingding.ttf");
            BaseFont baseW = BaseFont.CreateFont(fontpath, BaseFont.CP1252, BaseFont.EMBEDDED);

            Font wingding = new Font(baseW, MidText1, Font.NORMAL, nviDarkBlue);
            string title = "";
            string message = "";
            string optiontype = type == "SSA" ? "OPTION A" : "OPTION B";
            switch (type)
            {
                case "PBS": title = "PAY-BY-SOLAR"; message = "Pay-By-Solar"; break;
                case "SSA": title = "SOLAR SERVICES"; message = "Solar Services"; break;
            }

            #region Financed Solutions

            string currency = super.Country.Currency;

            Paragraph empty2 = new Paragraph(0f, " ", new Font(Font.FontFamily.HELVETICA, 5, Font.BOLD, BaseColor.WHITE));

            if (type == "SSA")
            {
                empty2.SpacingAfter = 1;
                document.Add(empty2);

                PdfPTable Fheader_table = new PdfPTable(new float[] { 540, 40, 70 });
                Fheader_table.TotalWidth = document.PageSize.Width - (document.RightMargin * 2) + 30;
                Fheader_table.LockedWidth = true;
                Fheader_table.PaddingTop = 0;
                Fheader_table.SpacingBefore = 20;
                //Top heading
                Fheader_table.AddCell(new PdfPCell(HeaderCell("FINANCED OPTIONS*", CellInfo.Header)) { PaddingBottom = 8 });
                Fheader_table.AddCell(new PdfPCell(HeaderCell(" ", CellInfo.Header)) { PaddingBottom = 8 });
                Fheader_table.AddCell(new PdfPCell(HeaderCell(" ", CellInfo.Header)) { PaddingBottom = 8 });
                //Fheader_table.AddCell(new PdfPCell(MyCell(letter, BigText, true, false, true)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = 8 });
                document.Add(Fheader_table);

                string spa = type == "SSA" ? "SOLAR SERVICES AGREEMENT" : "PAY-BY-SOLAR";
                //make index
                string option = string.Format("  OPTION {0}: {1} ({2})", "A", spa, type);

                #region Summary of Costs
                PdfPTable FBorderNest = new PdfPTable(1);
                FBorderNest.TotalWidth = document.PageSize.Width - (2 * document.LeftMargin + 20);
                FBorderNest.LockedWidth = true;

                PdfPCell FNestCell = new PdfPCell();
                FNestCell.Padding = 5;
                FNestCell.HorizontalAlignment = Image.ALIGN_CENTER;
                FNestCell.Border = Image.NO_BORDER;

                FNestCell.AddElement(new Paragraph("SolarAfrica accredited partners offer fully or partially financed solar solutions:", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)) { Leading = 2, MultipliedLeading = 1f, SpacingBefore = 15 });

                //FNestCell.AddElement(new Paragraph("SUMMARY OF COSTS:", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue)) { SpacingBefore = 10, SpacingAfter = -1 });
                //FNestCell.AddElement(new Paragraph("(Please see each accompanied quote for full details of terms and system specifications)", new Font(Font.FontFamily.HELVETICA, MidText2 - 1, Font.ITALIC, nviDarkBlue)) { SpacingAfter = 3 });
                int tWith = 430;

                PdfPTable FSummaryTable = new PdfPTable(new float[] { 390, 80, 80 });
                FSummaryTable.TotalWidth = 430;
                FSummaryTable.LockedWidth = true;
                FSummaryTable.SpacingBefore = 2;
                FSummaryTable.HorizontalAlignment = Image.ALIGN_LEFT;

                FSummaryTable.AddCell(new PdfPCell(FCell(" ", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("Option A SSA"), MidText2, true, false, true, nviLightBLue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviDarkBlue });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("Option B PBS"), MidText2, true, false, true, nviLightBLue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviDarkBlue });

                FSummaryTable.AddCell(new PdfPCell(FCell("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, PaddingBottom = 0, Colspan = 3 });
                //upfront payment
                FSummaryTable.AddCell(new PdfPCell(FCell("Upfront Payment:", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //Monthly Energy Payment
                FSummaryTable.AddCell(new PdfPCell(FCell("Monthly Energy Payment:", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //Maintenance Costs
                FSummaryTable.AddCell(new PdfPCell(FCell("Maintenance Costs:", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //Insurance Costs
                FSummaryTable.AddCell(new PdfPCell(FCell("Insurance Costs:", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //Monthly Battery Costs
                FSummaryTable.AddCell(new PdfPCell(FCell("Monthly Battery Costs:", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                ////TFR
                //FSummaryTable.AddCell(new PdfPCell(FCell("Variable KWh Tariff:", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                //FSummaryTable.AddCell(new PdfPCell(FCell(super.TFR.ToString("#,##0.###"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                //FSummaryTable.AddCell(new PdfPCell(FCell(super.TFR.ToString("#,##0.###"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //FP
                FSummaryTable.AddCell(new PdfPCell(FCell("Fixed Monthly Payment:", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell((super.FP / 12).ToString("#,##0."), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell((super.FP / 12).ToString("#,##0."), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                ////Term (months)
                //FSummaryTable.AddCell(new PdfPCell(FCell("Agreement Term:", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                //FSummaryTable.AddCell(new PdfPCell(FCell((super.Project.Costing.Term * 12).ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                //FSummaryTable.AddCell(new PdfPCell(FCell((super.Project.Costing.Term * 12).ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //empty row
                FSummaryTable.AddCell(new PdfPCell(ContentCell(" ", CellInfo.Empty)) { Padding = 0 });
                FSummaryTable.AddCell(new PdfPCell(ContentCell(" ", CellInfo.Empty)) { Padding = 0 });
                FSummaryTable.AddCell(new PdfPCell(ContentCell(" ", CellInfo.Empty)) { Padding = 0 });


                //Total Solar Costs all inclusive monthly           (Y*MW*TRF*1000)/12   + FP/12
                FSummaryTable.AddCell(new PdfPCell(FCell("TOTAL MONTHLY PAYMENT:", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell(((super.Project.Costing.Yield * super.prosize * super.TFR * 1000) / 12 + super.FP / 12).ToString("#,##0.##"), MidText2, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell(((super.Project.Costing.Yield * super.prosize * super.TFR * 1000) / 12 + super.FP / 12).ToString("#,##0.##"), MidText2, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //Unit Cost / Tariff
                FSummaryTable.AddCell(new PdfPCell(FCell("Unit Cost / Tariff:", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell("XX", MidText2, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("XX", MidText2, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //Monthly Savings
                FSummaryTable.AddCell(new PdfPCell(FCell("Monthly Savings:", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell("XX", MidText2, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("XX", MidText2, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //Actuall amount point
                //FSummaryTable.AddCell(new PdfPCell(FCell("* Actual Amount will depend on monthly performance of the Solar Plant.", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, PaddingBottom = 0, Colspan = 3 });

                PdfPTable FSumNest = new PdfPTable(1);
                FSumNest.TotalWidth = tWith + 3;
                FSumNest.LockedWidth = true;
                FSumNest.HorizontalAlignment = Image.ALIGN_LEFT;
                FSumNest.AddCell(new PdfPCell(FSummaryTable) { Padding = 3, PaddingBottom = 3, HorizontalAlignment = Image.ALIGN_LEFT });
                FNestCell.AddElement(FSumNest);

                FNestCell.AddElement(new Paragraph("*Subject to the terms and conditions.", new Font(Font.FontFamily.HELVETICA, MidText2 - 1, Font.ITALIC, nviDarkBlue)) { SpacingAfter = 3 });

                //--------------------------------------------

                if (type == "SSA")
                {
                    FSummaryTable = new PdfPTable(new float[] { 390 });
                    FSummaryTable.TotalWidth = 430;
                    FSummaryTable.LockedWidth = true;
                    FSummaryTable.SpacingBefore = 2;
                    FSummaryTable.HorizontalAlignment = Image.ALIGN_LEFT;

                    FNestCell.AddElement(new Paragraph("OPTION A - SOLAR SERVICES AGREEMENT", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue)) { SpacingBefore = 10 });
                    FSummaryTable.AddCell(new PdfPCell(FCell("SolarAfrica will contract with you to lease the solar system in this proposal to you, on operating lease basis", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSummaryTable.AddCell(new PdfPCell(FCell("MINIMUM CONTRACT PERIOD: 5 years (thereafter exit penalty applies, which reduces each year)", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSummaryTable.AddCell(new PdfPCell(FCell("PERFORMANCE GUARANTEE: Included", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSummaryTable.AddCell(new PdfPCell(FCell("BUY-OUT OPTION: Yes, after 5 years", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSummaryTable.AddCell(new PdfPCell(FCell("EARLY EXIT PENALTY: Yes", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSummaryTable.AddCell(new PdfPCell(FCell("Ownership of the solar system remains with SolarAfrica unless the system is purchased during the contract period.", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSummaryTable.AddCell(new PdfPCell(FCell("MONTHLY SAVING: Rxxxx", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSumNest = new PdfPTable(1);
                    FSumNest.TotalWidth = tWith + 3;
                    FSumNest.LockedWidth = true;
                    FSumNest.HorizontalAlignment = Image.ALIGN_LEFT;
                    FSumNest.AddCell(new PdfPCell(FSummaryTable) { Padding = 7, PaddingBottom = 3, HorizontalAlignment = Image.ALIGN_LEFT });
                    FNestCell.AddElement(FSumNest);
                }
                if (type == "PBS")
                {
                    FSummaryTable = new PdfPTable(new float[] { 390 });
                    FSummaryTable.TotalWidth = 430;
                    FSummaryTable.LockedWidth = true;
                    FSummaryTable.SpacingBefore = 2;
                    FSummaryTable.HorizontalAlignment = Image.ALIGN_LEFT;

                    FNestCell.AddElement(new Paragraph("OPTION B - PAY-BY-SOLAR LEASE AGREEMENT", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue)) { SpacingBefore = 10 });
                    FSummaryTable.AddCell(new PdfPCell(FCell("SolarAfrica will contract with to finance the solar system in this proposal under a finance lease.", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSummaryTable.AddCell(new PdfPCell(FCell("MINIMUM CONTRACT PERIOD: 5 years (thereafter exit penalty applies, which reduces each year)", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSummaryTable.AddCell(new PdfPCell(FCell("PERFORMANCE GUARANTEE: Included", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSummaryTable.AddCell(new PdfPCell(FCell("BUY-OUT OPTION: Yes, after 5 years", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSummaryTable.AddCell(new PdfPCell(FCell("CONTRACT EXIT PENALTY: Yes", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSummaryTable.AddCell(new PdfPCell(FCell("Ownership transfers once the last payment is made.", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSummaryTable.AddCell(new PdfPCell(FCell("MONTHLY SAVING: Rxxxx", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                    FSumNest = new PdfPTable(1);
                    FSumNest.TotalWidth = tWith + 3;
                    FSumNest.LockedWidth = true;
                    FSumNest.HorizontalAlignment = Image.ALIGN_LEFT;
                    FSumNest.AddCell(new PdfPCell(FSummaryTable) { Padding = 7, PaddingBottom = 3, HorizontalAlignment = Image.ALIGN_LEFT });
                    FNestCell.AddElement(FSumNest);
                }
                #endregion

                #region Frequently Asked Questions
                //Left solar 4 africa image, right lable, value 
                FNestCell.AddElement(new Paragraph("FREQUENTLY ASKED QUESTIONS:", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue)) { SpacingBefore = 5 });



                string _solarblkPath = System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/s4a Block.png");
                Image solarBlock = Image.GetInstance(_solarblkPath);
                solarBlock.ScalePercent(65);





                PdfPTable BottomImageTable = new PdfPTable(new float[] { solarBlock.ScaledWidth + 30, 500 });
                BottomImageTable.LockedWidth = true;
                BottomImageTable.TotalWidth = document.PageSize.Width - (2 * document.RightMargin + 20);
                BottomImageTable.SpacingBefore = 2;

                BottomImageTable.AddCell(new PdfPCell(solarBlock) { BorderColor = BaseColor.WHITE });



                Paragraph paragraph = new Paragraph();

                paragraph.Add(EquipmentPhrase("Who is the Contracting Party? ", true, MidText1 + 1, nviDarkBlue));
                paragraph.Add(EquipmentPhrase(" You contract directly with an NVI Energy country subsidiary. NVI Energy is the legal entity that founded and now manages the Solar4Africa Platform. Our platform financing partners will fund the installation in exchange for the payments received. NVI remains responsible for monitoring and management of each solar facility financed through Solar4Africa to ensure each plant is operating as intended.\n", false, MidText1 + 1, nviDarkBlue));

                paragraph.Add(EquipmentPhrase("\nWho does Installation and Maintenance? ", true, MidText1 + 1, nviDarkBlue));
                paragraph.Add(EquipmentPhrase(" You will contract with NVI Energy in order for us to finance the solar installation, NVI will contract with a Solar4Africa technical partner to install and maintain the solar system, to the requirements agreed in our Solar Services Agreement.   \n", false, MidText1 + 1, nviDarkBlue));

                paragraph.Add(EquipmentPhrase("\nCan the Agreement be cancelled? ", true, MidText1 + 1, nviDarkBlue));
                paragraph.Add(EquipmentPhrase(" From year 5 onwards, you may cancel the agreement by paying an early termination fee. Once cancelled, ownership will transfer to you. There is no further legal obligations between the parties once the Agreement is terminated.\n", false, MidText1 + 1, nviDarkBlue));

                paragraph.Add(EquipmentPhrase("\nWho is responsible for operations, maintenance and performance? ", true, MidText1 + 1, nviDarkBlue));
                paragraph.Add(EquipmentPhrase(" As the contracting party, NVI Energy is responsible for operations, maintenance and system performance. We contract with a technical partner who performs scheduled and unscheduled maintenance services at our cost. The systems performance is constantly monitored by us and as you only pay for energy produced, you are not paying for a system that doesn't produce energy.\n", false, MidText1 + 1, nviDarkBlue));


                BottomImageTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER, PaddingTop = -2 });


                FNestCell.AddElement(BottomImageTable);

                //FNestCell.AddElement(solarBlock);

                //FNestCell.AddElement(f1);
                //FNestCell.AddElement(f2);


                #endregion

                FBorderNest.AddCell(FNestCell);

                document.Add(FBorderNest);

                string _solarCircPath = System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/solar circle.png");
                Image solarCircle = Image.GetInstance(_solarCircPath);

                solarCircle.ScaleAbsolute(80, 80);
                solarCircle.SetAbsolutePosition(450, 622);
                solarCircle.Alignment = Image.TEXTWRAP;
                //pdfwriter.DirectContent.AddImage(solarCircle);
            }
            //at the end add all content to cell, add cell to bordernest, add bordernest to document.
            subsub++;
            #endregion

            document.NewPage();
            #region Offer Page

            #region Everything the the rectangle
            #region Top Titles

            PdfPTable Fheader_table2 = HeaderTable(new float[] { 50, 560 });
            Fheader_table2.PaddingTop = 0;
            Fheader_table2.SpacingBefore = 17;
            Fheader_table2.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
            Fheader_table2.AddCell(new PdfPCell(FCell(optiontype + " - " + type, BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
            document.Add(Fheader_table2);

            Paragraph em = new Paragraph(1, " ");
            document.Add(em);
            Paragraph CustomerQuote = new Paragraph("TERM SHEET", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue));
            CustomerQuote.Alignment = Image.ALIGN_CENTER;
            CustomerQuote.SpacingBefore = 20;
            CustomerQuote.SpacingAfter = -3;
            document.Add(CustomerQuote);

            Paragraph TitlePara = new Paragraph(title + " AGREEMENT (" + type + ")", new Font(Font.FontFamily.HELVETICA, BigText, Font.BOLD, nviDarkBlue));
            TitlePara.Alignment = Image.ALIGN_CENTER;

            document.Add(TitlePara);

            Paragraph quoteCredit = new Paragraph("< QUOTE SUBJECT TO CREDIT REVIEW >", new Font(Font.FontFamily.HELVETICA, MidText1 + 2, Font.NORMAL, nviDarkBlue));
            quoteCredit.Alignment = Image.ALIGN_CENTER;

            document.Add(quoteCredit);

            PdfPTable PageParentTable = new PdfPTable(1);

            int myPagePadding = 15;
            PageParentTable.TotalWidth = document.PageSize.Width - 2 * (document.LeftMargin + myPagePadding);
            PageParentTable.HorizontalAlignment = Image.ALIGN_CENTER;
            PdfPCell PageParentCell = new PdfPCell() { Border = Image.NO_BORDER, PaddingLeft = myPagePadding, PaddingRight = myPagePadding, HorizontalAlignment = Image.ALIGN_CENTER };

            ApplicationUser prepper = db.Users.Where(m => m.Id == project.CurrentUserID).FirstOrDefault();

            PdfPTable offerToTable = new PdfPTable(new float[] { 350, 300, 600 });
            offerToTable.TotalWidth = document.PageSize.Width - 2 * (document.LeftMargin + myPagePadding);
            offerToTable.LockedWidth = true;
            offerToTable.SpacingBefore = 10;

            offerToTable.AddCell(new PdfPCell(FCell("Offer To:", MidText1 + 1, true, true, false, nviDarkBlue)) { Border = Image.BOTTOM_BORDER, BorderColor = BaseColor.BLACK, BorderWidthBottom = 0.2f, PaddingBottom = -5 });
            offerToTable.AddCell(ContentCell("", CellInfo.Empty));
            offerToTable.AddCell(new PdfPCell(FCell("Prepared By:", MidText1 + 1, true, true, false, nviDarkBlue)) { Border = Image.BOTTOM_BORDER, BorderColor = BaseColor.BLACK, BorderWidthBottom = 0.2f, PaddingBottom = -5 });
            if (prepper.Company.Contains("nvi"))
            {
                offerToTable.AddCell(new PdfPCell(FCell(project.Details.CompanyName, MidText1 + 1, true, true, false, nviDarkBlue)) { Border = Image.NO_BORDER, PaddingTop = 8 });
                offerToTable.AddCell(ContentCell("", CellInfo.Empty));
                offerToTable.AddCell(new PdfPCell(FCell("NVI Energy Ltd", MidText1 + 1, true, true, false, nviDarkBlue)) { Border = Image.NO_BORDER, PaddingTop = 8 });
            }

            else
            {
                offerToTable.AddCell(new PdfPCell(FCell(project.Details.CompanyName, MidText1 + 1, true, true, false, nviDarkBlue)) { Border = Image.NO_BORDER, PaddingTop = 8 });
                offerToTable.AddCell(ContentCell("", CellInfo.Empty));
                offerToTable.AddCell(new PdfPCell(FCell(prepper.Company, MidText1 + 1, true, true, false, nviDarkBlue)) { Border = Image.NO_BORDER, PaddingTop = 8 });
            }
            offerToTable.AddCell(new PdfPCell(FCell(project.Details.P_City, MidText1 + 1, false, false, false, nviDarkBlue)) { Border = Image.NO_BORDER, Padding = 0 });
            offerToTable.AddCell(ContentCell("", CellInfo.Empty));
            offerToTable.AddCell(new PdfPCell(FCell(prepper.UserName, MidText1 + 1, false, false, false, nviDarkBlue)) { Border = Image.NO_BORDER, Padding = 0 });

            offerToTable.AddCell(new PdfPCell(FCell(project.Details.P_Site, MidText1 + 1, false, false, false, nviDarkBlue)) { Border = Image.NO_BORDER, Padding = 0 });
            offerToTable.AddCell(ContentCell("", CellInfo.Empty));
            offerToTable.AddCell(new PdfPCell(FCell(prepper.Email, MidText1 + 1, false, false, false, nviDarkBlue)) { Border = Image.NO_BORDER, Padding = 0 });

            PageParentCell.AddElement(offerToTable);
            #endregion

            #region Quoted Tariff & Other Charges

            PdfPTable TariffTable = new PdfPTable(new float[] { 520, 260, 220, 10, 390, 220, 200 });

            TariffTable.LockedWidth = true;
            TariffTable.TotalWidth = document.PageSize.Width - 2 * (document.LeftMargin + myPagePadding);
            TariffTable.SpacingBefore = 10;
            PdfPCell TariffHeader = new PdfPCell(FCell("YOUR QUOTED TARIFF & OTHER CHARGES", MidText1 + 2, true, false, true, BaseColor.WHITE)) { PaddingBottom = 7, PaddingTop = 2 };
            TariffHeader.Colspan = 7;
            TariffHeader.BackgroundColor = nviDarkBlue;

            TariffTable.AddCell(TariffHeader);

            TariffTable.AddCell(new PdfPCell(ContentCell("", CellInfo.Empty)) { Colspan = 7, Padding = 0 });//gap
            int rowBorderwidth = 4;
            if (type == "SSA")
            {
                TariffTable.AddCell(new PdfPCell(FCell("Tariff:", MidText1 + 1, true, false, false, nviDarkBlue))); //Tariff
                TariffTable.AddCell(new PdfPCell(FCell(super.TFR.ToString("#,##0."), MidText1 + 1, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2, PaddingTop = 0 });
                TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / kWh", MidText1 + 1, true, false, false, nviDarkBlue)));

            }

            else
            {
                TariffTable.AddCell(new PdfPCell(FCell("Monthly Lease Payment:", MidText1 + 1, true, false, false, nviDarkBlue))); //Tariff
                TariffTable.AddCell(new PdfPCell(FCell(super.TFR.ToString("#,##0."), MidText1 + 1, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2, PaddingTop = 0 });
                TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / kWh", MidText1 + 1, true, false, false, nviDarkBlue)));

            }

            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap


            TariffTable.AddCell(new PdfPCell(FCell("Initial Payment:*", MidText1 + 1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            TariffTable.AddCell(new PdfPCell(FCell(super.Project.real_UpfrontFee.ToString("#,##0.##"), MidText1 + 1, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth + 1, BorderColor = BaseColor.WHITE, PaddingBottom = 6, PaddingRight = 2, PaddingTop = 0 });
            TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency, MidText1 + 1, true, false, false, nviDarkBlue)));



            TariffTable.AddCell(new PdfPCell(FCell("Fixed Charges", MidText1 + 1, true, false, false, nviDarkBlue))); //Fixed Charges
            TariffTable.AddCell(new PdfPCell(FCell(super.FPMT.ToString("#,##0."), MidText1 + 1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / Mnth", MidText1 + 1, true, false, false, nviDarkBlue)));

            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap

            TariffTable.AddCell(new PdfPCell(new Phrase("Equivalent to:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.ITALIC, nviDarkBlue))) { Border = Image.NO_BORDER }); //Upfront payment requirement
            TariffTable.AddCell(new PdfPCell(FCell((super.Project.real_UpfrontFee / super.prosize / 1000000).ToString("#,##0.##"), MidText1 + 1, true, true, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, BorderColor = BaseColor.GRAY, PaddingBottom = 0 });
            TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / Wp", MidText1 + 1, true, false, false, nviDarkBlue)));




            TariffTable.AddCell(new PdfPCell(FCell("Energy Storage Charge", MidText1 + 1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            TariffTable.AddCell(new PdfPCell(FCell((super.FPMT / 12).ToString("#,##0."), MidText1 + 1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / Mnth", MidText1 + 1, true, false, false, nviDarkBlue)));
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap

            TariffTable.AddCell(new PdfPCell(FCell("* Initial Payment, payable upfront on signature of Agreement subject to standard conditions.", MidText2 - 1, false, false, false, nviDarkBlue)) { Colspan = 2 }); //Upfront payment requirement
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));


            TariffTable.AddCell(new PdfPCell(FCell("Maintenance Charge", MidText1 + 1, true, false, false, nviDarkBlue))); //Upfront payment requirement

            if (super.Project.OMYN)
            {
                //include OM
                TariffTable.AddCell(new PdfPCell(FCell("Included", MidText1 + 1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });

            }

            else
            {
                TariffTable.AddCell(new PdfPCell(FCell(super.OM.ToString("#,##0."), MidText1 + 1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });

            }

            TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / Mnth", MidText1 + 1, true, false, false, nviDarkBlue)));
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap

            TariffTable.AddCell(new PdfPCell(FCell("Estimated Insurance Charge", MidText1 + 1, true, false, false, nviDarkBlue))); //Upfront payment requirement

            if (super.Project.InsureYN)
            {
                //include OM
                TariffTable.AddCell(new PdfPCell(FCell("Included", MidText1 + 1, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });

            }

            else
            {
                TariffTable.AddCell(new PdfPCell(FCell(super.Insure.ToString("#,##0."), MidText1 + 1, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });

            }

            TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / Mnth", MidText1 + 1, true, false, false, nviDarkBlue)));
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap





            PageParentCell.AddElement(TariffTable);
            #endregion

            #region Project Indicators
            PdfPTable ProjectTable = new PdfPTable(new float[] { 520, 260, 220, 10, 390, 220, 200 });

            ProjectTable.LockedWidth = true;
            ProjectTable.TotalWidth = document.PageSize.Width - 2 * (document.LeftMargin + myPagePadding);
            ProjectTable.SpacingBefore = 10;
            PdfPCell ProjectHeader = FCell("YOUR PROJECT INDICATORS", MidText1, true, false, true, nviDarkBlue);
            ProjectHeader.Colspan = 7;
            ProjectHeader.BackgroundColor = nviLightBLue;
            ProjectHeader.PaddingBottom = 9;
            ProjectHeader.PaddingTop = 3;
            ProjectTable.AddCell(ProjectHeader);
            ProjectTable.AddCell(new PdfPCell(ContentCell("", CellInfo.Empty)) { Colspan = 7, Padding = 0 });//gap

            ProjectTable.AddCell(new PdfPCell(FCell("System Size", MidText1, true, false, false, nviDarkBlue))); //Tariff
            ProjectTable.AddCell(new PdfPCell(FCell((super.prosize * 1000).ToString("#,##0.##"), MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell("kWp", MidText1, true, false, false, nviDarkBlue)));

            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap


            ProjectTable.AddCell(new PdfPCell(FCell("Battery Size", MidText1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            ProjectTable.AddCell(new PdfPCell(FCell((super.BattCap / 1000).ToString("#,##0.00"), MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell("kWh", MidText1, true, false, false, nviDarkBlue)));



            ProjectTable.AddCell(new PdfPCell(FCell("Installation Type", MidText1, true, false, false, nviDarkBlue))); //Tariff
            ProjectTable.AddCell(new PdfPCell(FCell(super.Project.Costing.Type, MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell("kWp", MidText1, true, false, false, nviDarkBlue)));

            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap


            ProjectTable.AddCell(new PdfPCell(FCell("Bi-Directional Capacity", MidText1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            ProjectTable.AddCell(new PdfPCell(FCell((super.sunnyIslandCap / 1000).ToString("#,##0.00"), MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell("kWp", MidText1, true, false, false, nviDarkBlue)));




            ProjectTable.AddCell(new PdfPCell(FCell("Expected Energy Yield", MidText1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            ProjectTable.AddCell(new PdfPCell(FCell(super.Project.Costing.Yield.ToString("#,##0.##"), MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell("kWh / kWp", MidText1, true, false, false, nviDarkBlue)));
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap

            ProjectTable.AddCell(new PdfPCell(FCell("Trade Agreement Length", MidText1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            ProjectTable.AddCell(new PdfPCell(FCell((super.Project.Costing.Term * 12).ToString(), MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell("Months", MidText1, true, false, false, nviDarkBlue)));
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap


            ProjectTable.AddCell(new PdfPCell(FCell("Exchange Rate (US$)", MidText1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            ProjectTable.AddCell(new PdfPCell(FCell(super.Country.Currency_Per_Dollar.ToString(), MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell(super.Country.Currency + "/ US$", MidText1, true, false, false, nviDarkBlue)));
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap

            PageParentCell.AddElement(ProjectTable);
            #endregion

            #region General Conditions
            PdfPTable GeneralTable = new PdfPTable(1);
            Image bulletCirc = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/bullet circ.png"));
            bulletCirc.Alignment = Image.ALIGN_RIGHT;
            bulletCirc.ScalePercent(35);

            GeneralTable.LockedWidth = true;
            GeneralTable.TotalWidth = document.PageSize.Width - 2 * (document.LeftMargin + myPagePadding);
            GeneralTable.SpacingBefore = 10;
            PdfPCell GeneralHeader = FCell("General Conditions:", MidText1, true, false, true, nviDarkBlue);
            GeneralHeader.Colspan = 1;
            GeneralHeader.BackgroundColor = nviLightBLue;
            GeneralHeader.PaddingBottom = 3;
            GeneralHeader.PaddingTop = 1;
            GeneralTable.AddCell(GeneralHeader);

            #region Generate Table for list layout
            PdfPTable mtermsTable = new PdfPTable(new float[] { 30, 400 });
            mtermsTable.LockedWidth = true;
            mtermsTable.TotalWidth = document.PageSize.Width - (2 * document.LeftMargin);
            mtermsTable.SpacingBefore = 10;

            //¡   is wingding for white circle
            Font s = new Font(wingding);
            s.Size = MidText2 - 2;
            float sleading = 20;

            Font conditionFont = new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue);
            mtermsTable.AddCell(new PdfPCell(new Paragraph(sleading, "¡", s)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER, Padding = 0 });
            mtermsTable.AddCell(GenTermCell("This quote is an estimate only and is based on the assumptions shown herein. A site evaluation including analysis of shading and structural integrity of any planned roof surface (if applicable) must be undertakein. We also require a completed Application Form, including documents, agreements, notices and assignments that are reasonably requested by us to confirm all pricing. ", conditionFont));


            mtermsTable.AddCell(new PdfPCell(new Paragraph(sleading, "¡", s)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER, Padding = 0 });
            mtermsTable.AddCell(GenTermCell("The tariff and fixed payment quotes are subject to general contract conditions and are only binding on acceptance of a final offer and conclusion of an agreement. All charges escalate annually at CPI. ", conditionFont));

            mtermsTable.AddCell(new PdfPCell(new Paragraph("¡", s)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER, Padding = 0 });
            mtermsTable.AddCell(GenTermCell("The quoted pricing excludes any Works expressly agreed will be performed by the you and will be included in our agreement as ‘Client Works’. ", conditionFont));

            mtermsTable.AddCell(new PdfPCell(new Paragraph("¡", s)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER, Padding = 0 });
            mtermsTable.AddCell(GenTermCell("All tariffs or fixed pricing quoted is excl. of VAT and subject to exchange rate fluctuations. Final approved quotes are valid for 30 days and exchange rates valid for 7 days.", conditionFont));

            mtermsTable.AddCell(new PdfPCell(new Paragraph("¡", s)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER, Padding = 0 });
            mtermsTable.AddCell(GenTermCell("The operations & maintenance quote (O&M Services) is a fully inclusive offer, except for works that you have agreed will be performed by you (and listed in our agreement)", conditionFont));

            mtermsTable.AddCell(new PdfPCell() { Border = Image.NO_BORDER, Padding = 0 });
            PdfPCell mcell = new PdfPCell();

            //mtermsTable.AddCell(new PdfPCell(new Phrase("¡", s)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER, Padding = 2 });
            //mtermsTable.AddCell(new PdfPCell(EquipmentPhrase(" All interests held by NVI Energy, Solar4Africa or a financing party will cese on payment of the last outstanding amount under the agreement.", false, MidText1 , nviDarkBlue)) { Border = Image.NO_BORDER, Padding = 2 });



            PageParentCell.AddElement(GeneralTable);
            PageParentCell.AddElement(mtermsTable);

            Paragraph seeterms = new Paragraph("See Terms applicable to our " + title + " Agreement.", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
            seeterms.SpacingBefore = 6;
            seeterms.Alignment = Element.ALIGN_LEFT;
            seeterms.IndentationLeft = -50;
            PageParentCell.AddElement(seeterms);
            Chunk linebreak = new Chunk(new LineSeparator(0.5f, 126f, BaseColor.BLACK, Element.ALIGN_CENTER, 10));

            PageParentCell.AddElement(linebreak);
            
            //PdfPTable endTable = new PdfPTable(1);
            //endTable.LockedWidth = true;
            //endTable.TotalWidth = document.PageSize.Width - 2 * (document.LeftMargin + myPagePadding);
            //endTable.SpacingBefore = 8;
            //PdfPCell endItem = FCell("This is a non-binding indicative quotation based on information provided.", MidText1, false, false, true, nviDarkBlue);
            //endItem.HorizontalAlignment = Image.ALIGN_CENTER;
            //endItem.Colspan = 1;
            //endItem.Padding = 11; endItem.PaddingBottom = 13;
            //endTable.AddCell(endItem);

            //PageParentCell.AddElement(endTable);

            #endregion
            #endregion

            PageParentTable.AddCell(PageParentCell);

            document.Add(PageParentTable);

            //add solar circle image here
            //last so that directcontent writes over the text
            //solarCircle.SetAbsolutePosition(document.Right - 90, document.BottomMargin + 100);
            //solarCircle.Alignment = Image.TEXTWRAP;
            //solarCircle.ScaleAbsolute(85, 85);
            //pdfwriter.DirectContent.AddImage(solarCircle);
            #endregion


            document.NewPage();
            #region Economic Assesment

            #region Header
            document.Add(empty2);

            string titleCamel = type == "SSA" ? "Solar Services" : "Pay-By-Solar";

            PdfPTable Eheader_table = HeaderTable(new float[] { 50, 560 });
            Eheader_table.PaddingTop = 0;
            Eheader_table.SpacingBefore = 17;

            Eheader_table.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
            Eheader_table.AddCell(new PdfPCell(FCell("OPTION (" + type + "): CASH FLOW ASSESSMENT", BigText - 3, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
            document.Add(Eheader_table);

            Paragraph ESubheading = new Paragraph(EquipmentPhrase("The following data is presented for financial analysis of entering into a " + titleCamel + ":", false, MidText1, nviDarkBlue));
            ESubheading.SpacingBefore = 20;

            document.Add(ESubheading);

            ESubheading = new Paragraph(EquipmentPhrase("ENERGY COSTS AND CUMULATIVE SAVINGS:", true, MidText1, nviDarkBlue));
            ESubheading.SpacingBefore = 10;

            document.Add(ESubheading);

            #endregion

            #region Big Table

            PdfPTable bigTable = new PdfPTable(new float[] { 50, 120, 50, 100, 100, 100, 100, 100, 100 });
            bigTable.TotalWidth = document.PageSize.Width - (2 * document.RightMargin);
            bigTable.LockedWidth = true;

            bigTable.AddCell(new PdfPCell(ContentCell("", CellInfo.Empty)) { Colspan = 6 });
            bigTable.AddCell(new PdfPCell(FCell("(As Per D1)", MidText1, true, false, false, nviDarkBlue)) { PaddingBottom = 1 });
            bigTable.AddCell(new PdfPCell(ContentCell("", CellInfo.Empty)) { Colspan = 2 });


            #region Table Headers
            bigTable.AddCell(new PdfPCell(FCell("Year", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 5 });
            bigTable.AddCell(new PdfPCell(FCell("Solar Energy Production (KWh)", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, PaddingBottom = 5 });
            bigTable.AddCell(new PdfPCell(FCell("Tariff (" + super.Country.Currency + ")", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 5 });
            bigTable.AddCell(new PdfPCell(FCell("Annual Variable Cost", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 5 });
            bigTable.AddCell(new PdfPCell(FCell("Annual Fixed Cost", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 5 });
            bigTable.AddCell(new PdfPCell(FCell("Total Solar Costs", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 1 });
            bigTable.AddCell(new PdfPCell(FCell("Cost of Existing Energy (kWh)", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 1 });
            bigTable.AddCell(new PdfPCell(FCell("Energy Costs Avoided", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 1 });
            bigTable.AddCell(new PdfPCell(FCell("Cummulative Savings", MidText2, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 1 });

            #endregion

            #region Subheaders
            bigTable.AddCell(ContentCell("", CellInfo.Empty));
            bigTable.AddCell(new PdfPCell(FCell("A", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell("B", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell("= C", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell("D", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell("C + D", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell("E", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell("= A x E", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell(" ", MidText2 - 1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });

            #endregion



            #region Table Values
            for (int a = 0; a < 20; a++)
            {
                bigTable.AddCell(new PdfPCell(FCell((a + 1).ToString(), MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 4, PaddingRight = 6, PaddingBottom = 0.5f });
                bigTable.AddCell(new PdfPCell(FCell(super.SolarProduction20[a].ToString("#,##0."), MidText2, false, true, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = 0.5f, PaddingRight = 2 });
                bigTable.AddCell(new PdfPCell(FCell(super.Tarrif20[a].ToString("#,##0.##"), MidText2, false, true, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = 0, PaddingRight = 2 });
                bigTable.AddCell(new PdfPCell(FCell((super.AnnualVariable20[a]).ToString("#,##0."), MidText2, false, true, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = 0, PaddingRight = 2 });
                bigTable.AddCell(new PdfPCell(FCell(super.AnnualFixed20[a].ToString("#,##0."), MidText2, false, true, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = 0, PaddingRight = 2 });
                bigTable.AddCell(new PdfPCell(FCell(super.TotalSolar20[a].ToString("#,##0."), MidText2, false, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = -0, PaddingRight = 2, BackgroundColor = nviSemiGrey });
                bigTable.AddCell(new PdfPCell(FCell(super.ExistingEnergy20[a].ToString("#,##0.###"), MidText2, false, true, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = 0, PaddingRight = 2 });
                bigTable.AddCell(new PdfPCell(FCell(super.AvoidedEnergy20[a].ToString("#,##0."), MidText2, false, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = 0, PaddingRight = 2, BackgroundColor = nviSemiGrey });
                bigTable.AddCell(new PdfPCell(FCell(super.CumulativeSavings20[a].ToString("#,##0."), MidText2, false, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT });
            }
            #endregion



            #region Table Bottom

            //empty row
            bigTable.AddCell(new PdfPCell(ContentCell("", CellInfo.Empty)) { Colspan = 9 });


            bigTable.AddCell(new PdfPCell(FCell("Assumptions:", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Padding = 2, Colspan = 2 });
            bigTable.AddCell(new PdfPCell(ContentCell("", CellInfo.Empty)) { Colspan = 7 });

            //cpi
            bigTable.AddCell(new PdfPCell(FCell(" 1", MidText2 - 3, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Colspan = 1 });

            bigTable.AddCell(new PdfPCell(FCell("Annual Solar Energy Increase:", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Colspan = 3 });
            bigTable.AddCell(new PdfPCell(FCell(super.Country.CPI.ToString("0.00") + "%", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Colspan = 1 });
            bigTable.AddCell(new PdfPCell(FCell("Per Annum (CPI)", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Colspan = 4 });

            //Existing energy CPIu
            bigTable.AddCell(new PdfPCell(FCell(" 2", MidText2 - 3, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Colspan = 1 });

            bigTable.AddCell(new PdfPCell(FCell("Existing Energy Increase:", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Colspan = 3 });
            bigTable.AddCell(new PdfPCell(FCell((super.Country.UtilityInflation).ToString("0.00") + "%", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Colspan = 1 });
            bigTable.AddCell(new PdfPCell(FCell("(First Year)", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Colspan = 4 });
            
            //extra
            //bigTable.AddCell(new PdfPCell(FCell(" 3", MidText2 - 3, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Colspan = 1 });
            //bigTable.AddCell(new PdfPCell(FCell(titleCamel + " Agreement pricing is extended after the contract term at 30% below the then applicable contract rates.	", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Colspan = 8 });

            #endregion

            document.Add(bigTable);

            #endregion
            #endregion

            pdfwriter.CloseStream = false;
            document.Close();

            stream.Position = 0;
            stream.Flush();

            return stream.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string RemoveWhitespace(string content)
        {
            content.Replace(" ", "");
            return content;
        }

        #region Helper PDF cells
        public static PdfPCell SysSpecCell(string content, bool label)
        {
            int style = 0;
            style = label ? Font.BOLD : Font.NORMAL;
            PdfPCell mCell = new PdfPCell(new Phrase(content, new Font(Font.FontFamily.HELVETICA, MidText1 + 1, style, nviDarkBlue)));
            mCell.Padding = 6;
            mCell.PaddingLeft = 0;
            mCell.PaddingRight = 0;
            mCell.Border = Image.NO_BORDER;
            mCell.BackgroundColor = nviLightBLue;
            return mCell;
        }

        public static PdfPCell MyCell(string content, float FontSize, bool Bold, bool Border, bool Background)
        {
            int style = 0;

            if (Bold)
            {
                style = Font.BOLD;
            }

            else
            {
                style = Font.NORMAL;
            }
            PdfPCell mCell = new PdfPCell(new Phrase(content, new Font(Font.FontFamily.HELVETICA, FontSize, style, nviDarkBlue)));

            if (!Border)
            {
                mCell.Border = Image.NO_BORDER;
            }

            if (Background)
            {
                mCell.BackgroundColor = nviLightBLue;
            }

            mCell.PaddingBottom = 5f;

            return mCell;
        }

        public static PdfPCell FCell(string content, float FontSize, bool Bold, bool Border, bool Background, BaseColor color)
        {
            int style = 0;

            if (Bold)
            {
                style = Font.BOLD;
            }

            else
            {
                style = Font.NORMAL;
            }
            PdfPCell mCell = new PdfPCell(new Phrase(content, new Font(Font.FontFamily.HELVETICA, FontSize, style, color)) { Leading = 0 });

            if (!Border)
            {
                mCell.Border = Image.NO_BORDER;
            }

            if (Background)
            {
                mCell.BackgroundColor = nviLightBLue;
            }

            mCell.PaddingBottom = 1;
            return mCell;
        }

        public static PdfPCell ContentCell(string content, CellInfo cellInfo)
        {
            PdfPCell cell = new PdfPCell();

            switch (cellInfo)
            {

                case CellInfo.Header:
                    cell = new PdfPCell(new Phrase(content.ToUpper(), new Font(Font.FontFamily.HELVETICA, 18, Font.NORMAL, nviDarkBlue)));
                    cell.BackgroundColor = nviLightBLue;
                    //cell.PaddingLeft = 150f;
                    cell.HorizontalAlignment = Image.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Padding = 4f;
                    cell.PaddingBottom = 6f;
                    break;


                case CellInfo.Letter:
                    cell = new PdfPCell(new Phrase(content.ToUpper(), new Font(Font.FontFamily.HELVETICA, 18, Font.NORMAL, nviDarkBlue)));
                    cell.BorderColor = BaseColor.WHITE;
                    cell.BackgroundColor = nviLightBLue;
                    cell.HorizontalAlignment = Image.ALIGN_CENTER;
                    cell.PaddingBottom = 6f;

                    break;


                case CellInfo.Subheader:
                    cell = new PdfPCell(new Phrase(DottedHeader(content.ToUpper()), new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    cell.Border = Image.NO_BORDER;
                    cell.BackgroundColor = BaseColor.WHITE; break;



                case CellInfo.Subletter:
                    cell = new PdfPCell(new Phrase(content.ToUpper(), new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.NORMAL, nviDarkBlue)));
                    cell.BorderColor = BaseColor.WHITE;
                    cell.BackgroundColor = BaseColor.WHITE;
                    cell.HorizontalAlignment = Image.ALIGN_CENTER;
                    break;

                case CellInfo.Empty:
                    cell = new PdfPCell(new Phrase(" ")) { BorderColor = BaseColor.WHITE };
                    cell.Border = Image.NO_BORDER;
                    break;

                case CellInfo.Row:
                    cell = new PdfPCell(new Phrase(" ")) { BorderColor = BaseColor.WHITE };
                    cell.Colspan = 3;
                    cell.Border = Image.NO_BORDER;
                    break;
            }

            return cell;
        }

        public static string DottedHeader(string content)
        {
            //string myString = "";
            //int numberofLettersA = content.ToArray().Count();
            //int numberLettersLeftt = 46 - numberofLettersA;
            //StringBuilder sb = new StringBuilder();


            //if (IsOdd(numberofLettersA))
            //{
            //    for (int a = 0; a < Math.Round(numberLettersLeftt * 2.3); a++)
            //    {
            //        sb.Append('.');
            //    }
            //}

            //else
            //{
            //    for (int a = 0; a < Math.Round(1 + numberLettersLeftt * 2.3); a++)
            //    {
            //        sb.Append('.');
            //    }
            //}


            //myString = content + sb.ToString();
            return content;

        }

        public static  bool IsOdd(int value)
        {
            return value % 2 != 0;
        }

        public static  PdfPCell HeaderCell(string content, CellInfo cellInfo)
        {
            PdfPCell cell = new PdfPCell();
            switch (cellInfo)
            {
                case CellInfo.Header:
                    cell = new PdfPCell(new Phrase(content, new Font(Font.FontFamily.HELVETICA, BigText, Font.BOLD, nviDarkBlue)));
                    cell.HorizontalAlignment = Image.ALIGN_LEFT;
                    cell.Padding = 5f;
                    cell.BackgroundColor = nviLightBLue;
                    cell.Border = Image.NO_BORDER;
                    break;

                case CellInfo.Empty:
                    cell = new PdfPCell(new Phrase(" ")) { BorderColor = BaseColor.WHITE };
                    cell.Border = Image.NO_BORDER;
                    break;

                case CellInfo.Letter:
                    cell = new PdfPCell(new Phrase(content, new Font(Font.FontFamily.HELVETICA, BigText, Font.BOLD, nviDarkBlue)));
                    cell.HorizontalAlignment = Image.ALIGN_CENTER;
                    cell.Padding = 5f;
                    cell.BackgroundColor = nviLightBLue;
                    cell.Border = Image.NO_BORDER;
                    break;
            }

            return cell;
        }

        public static PdfPTable HeaderTable(float[] widths)
        {
            PdfPTable table = new PdfPTable(widths);
            //header, space, letter
            table = new PdfPTable(widths);
            table.SpacingBefore = -100f;
            table.TotalWidth = 525f;
            table.LockedWidth = true;
            table.HorizontalAlignment = Image.ALIGN_CENTER;

            return table;
        }

        public static PdfPCell EquipmentCell(Phrase phrase, EquipmentCellInfo cellinfo, int alignment)
        {

            PdfPCell mCell = new PdfPCell(phrase);

            switch (cellinfo)
            {
                case EquipmentCellInfo.Header: mCell.BackgroundColor = nviDarkBlue; break;
                case EquipmentCellInfo.Subheader: mCell.BackgroundColor = nviMidBLue; break;
                case EquipmentCellInfo.SubSubHeader: mCell.BackgroundColor = nviLightBLue; break;
                case EquipmentCellInfo.Item: break;
            }
            mCell.BorderColor = nviDarkBlue;
            mCell.HorizontalAlignment = alignment;
            mCell.PaddingBottom = 4;
            mCell.PaddingRight = 4;
            return mCell;

        }

        public static Phrase EquipmentPhrase(string content, bool bold, float fontSize, BaseColor color)
        {
            int style = bold ? Font.BOLD : Font.NORMAL;
            Phrase mPhrase = new Phrase(content, new Font(Font.FontFamily.HELVETICA, fontSize, style, color));
            mPhrase.SetLeading(4, 0.4f);
            return mPhrase;
        }

        public static PdfPCell GenTermCell(string content, Font font)
        {
            Phrase mPhrase = new Phrase(content, font);
            PdfPCell mcell = new PdfPCell(mPhrase);
            mcell.Border = Image.NO_BORDER;
            mcell.HorizontalAlignment = Image.ALIGN_LEFT;
            mcell.SetLeading(10, 0.4f);
            mcell.Padding = 0;
            mcell.PaddingTop = -6; mcell.PaddingBottom = 6;

            return mcell;
        }
        #endregion

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="PercSolar"></param>
        /// <param name="PercExisting"></param>
        /// <returns></returns>
        public static byte[] GeneratePieChart(int width, int height, int PercSolar, int PercExisting)
        {


            System.Web.Helpers.Chart myChart = new System.Web.Helpers.Chart(width: width, height: height, theme: GetTheme())
                     .AddSeries("Default", chartType: "Pie",

                       yValues: new int[] { PercSolar, PercExisting }, yFields: "Sales")


        .Write();



            return myChart.GetBytes("png");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="oldCosts"></param>
        /// <param name="newCosts"></param>
        /// <returns></returns>
        public static byte[] GenerateBarGraph(int width, int height, double[] oldCosts, double[] newCosts)
        {



            int[] years = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };




            MyColor nviDBLue = MyColor.FromArgb(60, 83, 111);
            MyColor nviGreen = MyColor.FromArgb(119, 161, 64);
            MyColor nviDarkGreen = MyColor.FromArgb(97, 130, 52);

            ChartArea ca = new System.Web.UI.DataVisualization.Charting.ChartArea("Default");
            var chart = new System.Web.UI.DataVisualization.Charting.Chart();
            chart.BackColor = MyColor.White;
            chart.BackGradientStyle = GradientStyle.TopBottom;
            chart.BackSecondaryColor = MyColor.White;
            chart.BorderColor = nviDBLue;
            chart.BorderlineDashStyle = ChartDashStyle.Solid;
            chart.BorderWidth = 0;
            chart.Palette = ChartColorPalette.None;
            chart.PaletteCustomColors = new MyColor[] { nviGreen, MyColor.White, };
            chart.Width = width;
            chart.Height = height;

            chart.ChartAreas.Add(new ChartArea("Default")
            {

                BackColor = MyColor.White,
                BackGradientStyle = GradientStyle.None,
                BackSecondaryColor = MyColor.White,
                BorderColor = nviDBLue,
                BorderDashStyle = ChartDashStyle.NotSet,
                ShadowColor = nviDBLue,
                Position = new System.Web.UI.DataVisualization.Charting.ElementPosition(0, 0, 100, 100),

                Area3DStyle = new ChartArea3DStyle
                {
                    LightStyle = LightStyle.Simplistic,
                    Enable3D = false,
                    Inclination = 5,
                    IsClustered = true,
                    IsRightAngleAxes = true,
                    Perspective = 5,
                    Rotation = 0,
                    WallWidth = 0
                }
            });

            chart.BorderSkin.SkinStyle = BorderSkinStyle.None;
            chart.ChartAreas[0].AxisX.IsMarginVisible = false;
            chart.ChartAreas[0].AxisX.IsStartedFromZero = false;
            chart.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            chart.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            chart.ChartAreas[0].AxisX.Maximum = 15.5;
            chart.ChartAreas[0].AxisX.Minimum = 0;
            chart.ChartAreas[0].AxisX.LabelAutoFitMaxFontSize = 5;
            chart.ChartAreas[0].AxisY.LabelAutoFitMaxFontSize = 5;
            chart.ChartAreas[0].AxisY.IsMarginVisible = false;
            chart.ChartAreas[0].Position = new ElementPosition(-1, 0, 100, 100);
            chart.ChartAreas[0].AxisX.LabelStyle.ForeColor = nviDBLue;
            chart.ChartAreas[0].AxisY.LabelStyle.ForeColor = nviDBLue;
            chart.ChartAreas[0].AxisX.LineColor = nviDBLue;
            chart.ChartAreas[0].AxisY.LineColor = nviDBLue;
            string[] seriesArray = new string[] { "New Costs", "Old Costs" };

            for (int a = 0; a < seriesArray.Count(); a++)
            {
                Series series = chart.Series.Add(seriesArray[a]);
                series.Font = new System.Drawing.Font("Arial", 5, System.Drawing.FontStyle.Regular);

                series.BorderWidth = 1;

                if (seriesArray[a] == "Old Costs")
                {
                    series.BorderColor = nviDBLue;

                    for (int b = 0; b < oldCosts.Length; b++)
                    {
                        series.Points.AddXY(years[b], oldCosts[b]);
                    }
                }

                else
                {

                    series.BorderColor = nviDarkGreen;

                    //new costs
                    for (int b = 0; b < newCosts.Length; b++)
                    {
                        series.Points.AddXY(years[b], newCosts[b]);

                    }
                }

            }



            MemoryStream stream = new MemoryStream();
            chart.SaveImage(stream, ChartImageFormat.Png);

            stream.Position = 0;
            return stream.GetBuffer();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetTheme()
        {

            MyColor nvigrey = MyColor.FromArgb(106, 115, 123);
            MyColor nviGreen = MyColor.FromArgb(119, 161, 64);
            MyColor nvilight = MyColor.FromArgb(227, 228, 230);
            string theme = "";
            ChartArea ca = new System.Web.UI.DataVisualization.Charting.ChartArea("Default");
            var chart = new System.Web.UI.DataVisualization.Charting.Chart();
            chart.BackColor = MyColor.White;
            chart.BackGradientStyle = GradientStyle.TopBottom;
            chart.BackSecondaryColor = MyColor.White;
            chart.BorderColor = nvigrey;
            chart.BorderlineDashStyle = ChartDashStyle.Solid;
            chart.BorderWidth = 2;
            chart.Palette = ChartColorPalette.None;
            chart.PaletteCustomColors = new MyColor[] { nviGreen, nvilight, };


            chart.ChartAreas.Add(new ChartArea("Default")
            {

                BackColor = MyColor.White,
                BackGradientStyle = GradientStyle.None,
                BackSecondaryColor = nvigrey,
                BorderColor = nvigrey,
                BorderDashStyle = ChartDashStyle.NotSet,
                ShadowColor = nvigrey,
                Area3DStyle = new ChartArea3DStyle
                {
                    LightStyle = LightStyle.Simplistic,
                    Enable3D = true,
                    Inclination = 30,
                    IsClustered = true,
                    IsRightAngleAxes = true,
                    Perspective = 5,
                    Rotation = -90,
                    WallWidth = 0
                }
            });
            chart.Legends.Add(new Legend("All")
            {
                BackColor = MyColor.Transparent,
                Font = new System.Drawing.Font("Helvitica", 4f, System.Drawing.FontStyle.Regular,
                   System.Drawing.GraphicsUnit.Point),
                IsTextAutoFit = false
            }
                );
            chart.BorderSkin.SkinStyle = BorderSkinStyle.None;

            var cs = chart.Serializer;
            cs.IsTemplateMode = true;
            //cs.Content = SerializationContents.Appearance;
            cs.Format = System.Web.UI.DataVisualization.Charting.SerializationFormat.Xml;
            var sb = new StringBuilder();
            using (XmlWriter xw = XmlWriter.Create(sb))
            {
                cs.Save(xw);
            }
            theme = sb.ToString().Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
            return theme;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="proposalInput"></param>
        /// <param name="document"></param>
        /// <param name="verionInfo"></param>
        /// <param name="pdfwriter"></param>
        public static void FinanceOnlyProposal(ProposalInputModel proposalInput, Document document, double verionInfo, PdfWriter pdfwriter)
        {
            PdfReader pdfreader;
            int offerCount = 0;

            #region Generate Cover Page
            if (proposalInput.alterCover != null && proposalInput.alterCover.ContentType.Contains("pdf"))
            {
                byte[] newCover = validPDFPage(proposalInput.alterCover);
                pdfreader = new PdfReader(newCover);
                PdfContentByte conByte = pdfwriter.DirectContent;
                conByte.AddTemplate(pdfwriter.GetImportedPage(pdfreader, 1), 1f, 0, 0, 1f, 0, 0);
                document.NewPage();
            }

            else
            {
                //space before will determine line height
                //leading is the line height per line
                //line indent is horizontal space before
                //background image last
                string solarimagPATH = "";
                //solarimagPATH = System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/Cover full.png");
                if (true)
                {
                    solarimagPATH = System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/Cover full financed.png");
                }

                Image cover_imgCenter = Image.GetInstance(solarimagPATH);
                cover_imgCenter.ScaleAbsolute(document.PageSize.Width - 2 * (document.LeftMargin - 16f), document.PageSize.Height - (181 + document.BottomMargin));
                cover_imgCenter.Alignment = Image.UNDERLYING;
                cover_imgCenter.SetAbsolutePosition(document.LeftMargin - 17, 167 + document.BottomMargin);
                document.Add(cover_imgCenter);

                //if the user has a pic to add to the quote
                if (proposalInput.preparer.Image != null && proposalInput.preparer.Image.Length > 0)
                {
                    try
                    {
                        Image cover_profileImage = Image.GetInstance(proposalInput.preparer.Image);
                        cover_profileImage.ScaleToFit(150f, 150f);
                        cover_profileImage.SetAbsolutePosition(document.Right - cover_profileImage.ScaledWidth, document.Bottom);
                        cover_profileImage.Alignment = Image.UNDERLYING;
                        document.Add(cover_profileImage);
                    }
                    catch (Exception)
                    {
                        //dont add image for some reason
                    }
                }

                //first back gray, then solar then main right
                PdfPTable conver_table = new PdfPTable(new float[] { 560 });

                Paragraph cover_Heading = new Paragraph(94, "FINANCED OFFER", new Font(Font.FontFamily.HELVETICA, 30f, Font.BOLD, nviDarkBlue));
                cover_Heading.Alignment = Image.ALIGN_TOP;
                cover_Heading.FirstLineIndent = 140f;
                document.Add(cover_Heading);

                cover_Heading = new Paragraph("SOLARAFRICA FINANCED SOLUTIONS", new Font(Font.FontFamily.HELVETICA, HeadText - 0.5f, Font.NORMAL, new BaseColor(40, 55, 74)));
                cover_Heading.Alignment = Image.ALIGN_TOP;
                cover_Heading.IndentationLeft = 158;
                document.Add(cover_Heading);

                BaseColor nviSuperDark = new BaseColor(40, 55, 74);

                Chunk cover_linebreak = new Chunk(new LineSeparator(1f, 106, BaseColor.GRAY, Image.ALIGN_CENTER, -480));
                document.Add(cover_linebreak);
                Paragraph cover_subheading = new Paragraph("FOR:", new Font(Font.FontFamily.HELVETICA, HeadText - 1, Font.BOLD, BaseColor.LIGHT_GRAY));
                cover_subheading.FirstLineIndent = 0;
                cover_subheading.SpacingBefore = 480f;
                cover_subheading.IndentationLeft = -15;
                document.Add(cover_subheading);

                Paragraph cover_companyName = new Paragraph(proposalInput.project.Details.CompanyName.ToUpper() + ".", new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD, nviDarkBlue));
                cover_companyName.FirstLineIndent = 0;
                cover_companyName.SpacingBefore = -6;
                cover_companyName.IndentationLeft = -15;
                document.Add(cover_companyName);

                Paragraph cover_companyNameSmall = new Paragraph(proposalInput.project.Details.CompanyName.ToUpper() + ".", new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD, nviDarkBlue));
                cover_companyNameSmall.FirstLineIndent = 0;
                cover_companyNameSmall.SpacingBefore = 2;
                cover_companyNameSmall.IndentationLeft = -15;
                document.Add(cover_companyNameSmall);

                string address = string.Format("{0},{1}.", proposalInput.project.Details.P_Road.ToUpper(), proposalInput.project.Details.P_Country.ToUpper());
                Paragraph cover_companyAddress = new Paragraph(address, new Font(Font.FontFamily.HELVETICA, 11, Font.ITALIC, BaseColor.LIGHT_GRAY));
                cover_companyAddress.FirstLineIndent = 0;
                cover_companyAddress.SpacingBefore = -3;
                cover_companyAddress.IndentationLeft = -15;
                document.Add(cover_companyAddress);

                Paragraph cover_dateLine = new Paragraph(DateTime.Now.ToString("dd MMMM yyyy"), new Font(Font.FontFamily.HELVETICA, 11, Font.ITALIC, nviDarkBlue));
                cover_dateLine.FirstLineIndent = 0;
                cover_dateLine.SpacingBefore = 2;
                cover_dateLine.IndentationLeft = -15;
                document.Add(cover_dateLine);

                Paragraph cover_versionLine = new Paragraph("Version :" + proposalInput.project.Revision.ToString("0.##"), new Font(Font.FontFamily.HELVETICA, 11, Font.BOLDITALIC, BaseColor.LIGHT_GRAY));
                cover_versionLine.FirstLineIndent = 0;
                cover_versionLine.SpacingBefore = -4;
                cover_versionLine.IndentationLeft = -15;
                document.Add(cover_versionLine);
            }

            #endregion
            ////---------------------------------------------------------End of Generate COVER PAGE----------------------------------------------------////

            #region Generate Contents Page
            //The cotent page
            //It has a left most tilted title - solar proposal
            //It has a main heading - content
            //it has sub headings Solar Proposal A , Technical Data B, Visual Overlays and Design C, Data sheets and yield analysis D, and supp e
            //Solar Proposal A
            //Overview A1
            //System Prop A2
            //Financial analysis A3
            //solar offers presented
            //ppa
            //pbs
            //generator
            document.NewPage();
            //load side image
            string content_sidePath = "";
            content_sidePath = System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/Side Image.png");

            Paragraph cParaNest = new Paragraph();
            cParaNest.IndentationLeft = -33;
            PdfPTable contentsPTable = new PdfPTable(new float[] { 140, 600 });
            contentsPTable.TotalWidth = document.PageSize.Width - 2 * document.RightMargin;
            contentsPTable.LockedWidth = true;

            PdfPCell leftImage = new PdfPCell(FCell("FINANCE PROPOSAL", BigText + 5, false, false, true, nviDarkBlue));
            leftImage.Rotation = 90;
            leftImage.PaddingLeft = 32;
            leftImage.PaddingTop = 208;
            leftImage.PaddingBottom = 270;
            leftImage.HorizontalAlignment = Image.ALIGN_CENTER;
            PdfPCell rightNest = new PdfPCell() { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER };
            rightNest.PaddingLeft = 120;
            rightNest.PaddingRight = -30;
            rightNest.PaddingTop = 123;

            //first back gray, then solar then main right
            Paragraph content_Heading = new Paragraph("CONTENTS", new Font(Font.FontFamily.HELVETICA, 25f, Font.BOLD, nviDarkBlue));
            content_Heading.Alignment = Image.ALIGN_LEFT;
            content_Heading.IndentationLeft = -20;
            rightNest.AddElement(content_Heading);

            //order of business - Item, empty, letter.............repeat
            int currentIndex = 1;
            string[] letters = new string[] { "A", "B", "C", "D", "E", "F", "G" };
            int letINdex = 0;
            string currentLetter = letters[letINdex];

            string[] HeaderIndexes = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09" };
            currentLetter = HeaderIndexes[letINdex];

            int subIndex = 1;
            int offerIndex = 3;

            PdfPTable content_table = new PdfPTable(new float[] { 253, 18, 27 });
            content_table.TotalWidth = 360;
            content_table.SpacingBefore = 32f;
            content_table.LockedWidth = true;
            content_table.HorizontalAlignment = Image.ALIGN_RIGHT;

            //if (proposalInput.IncludeGen || proposalInput.IncludePBS || proposalInput.IncludePPA || proposalInput.IncludeSDS)
            if(true)
            {
                letINdex++;
                currentLetter = letters[letINdex];
                currentLetter = HeaderIndexes[letINdex];

                offerIndex = currentIndex;

                PdfPCell content_cursive = new PdfPCell(new Phrase(" ", new Font(Font.FontFamily.HELVETICA, 10, Font.ITALIC, nviDarkBlue)));

                Phrase cover_companyName = new Phrase("SolarAfrica Financed Offers", new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, nviDarkBlue));
                content_table.AddCell(new PdfPCell(cover_companyName) { Border = Image.NO_BORDER, BackgroundColor = nviLightBLue });

                //content_table.AddCell(ContentCell("SolarAfrica Financed Offers", CellInfo.Subheader));
                content_table.AddCell(ContentCell(null, CellInfo.Empty));
                content_table.AddCell(ContentCell(null, CellInfo.Empty));
                //content_table.AddCell(new PdfPCell(new Phrase(" ")) { Padding = 1.5f, Colspan = 3, Border = Image.NO_BORDER });

                content_cursive.Border = Image.NO_BORDER;
                content_cursive.BackgroundColor = BaseColor.WHITE;
                content_table.AddCell(content_cursive);
                content_table.AddCell(ContentCell(null, CellInfo.Empty));
                content_table.AddCell(ContentCell(null, CellInfo.Empty));
            }

            #region Dynamic sub sections

            offerCount++;
            content_table.AddCell(ContentCell("SOLAR SERVICES OFFER (SSA)", CellInfo.Subheader));
            content_table.AddCell(ContentCell(null, CellInfo.Empty));
            content_table.AddCell(ContentCell(null, CellInfo.Empty));
            subIndex++;

            offerCount++;
            content_table.AddCell(ContentCell("SOLAR SERVICES OFFER (PBS)", CellInfo.Subheader));
            content_table.AddCell(ContentCell(null, CellInfo.Empty));
            content_table.AddCell(ContentCell(null, CellInfo.Empty));
            subIndex++;

            offerCount++;
            content_table.AddCell(ContentCell("TERMS & CONDITIONS", CellInfo.Subheader));
            content_table.AddCell(ContentCell(null, CellInfo.Empty));
            content_table.AddCell(ContentCell(null, CellInfo.Empty));
            subIndex++;

            content_table.AddCell(ContentCell(" ", CellInfo.Row)); //empy row

            #endregion

            rightNest.AddElement(content_table);
            contentsPTable.AddCell(leftImage);
            contentsPTable.AddCell(rightNest);
            cParaNest.Add(contentsPTable);
            document.Add(cParaNest);

            #endregion
            ////---------------------------------------------------------End of Generate CONTENTS PAGE----------------------------------------------------////

            letINdex = 0;
            currentIndex = 1;
            currentLetter = letters[letINdex];
            currentLetter = HeaderIndexes[letINdex];

            document.NewPage();
            Paragraph oPageContainer = new Paragraph(" ");
            document.Add(oPageContainer);

            PdfPTable overview_header_table = HeaderTable(new float[] { 50, 560 });
            overview_header_table.PaddingTop = 0;
            overview_header_table.SpacingBefore = 17;

            overview_header_table.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
            overview_header_table.AddCell(new PdfPCell(FCell("FINANCE OFFERS", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
            document.Add(overview_header_table);

            #region Generate Finance Offers A4-1 to 3          PBS   PPA   GEN

            subIndex = 1;

            //if (proposalInput.IncludePPA)
            if(true)
            {
                Project ppaProject = proposalInput.project;
                ppaProject.Costing.Plan = "Power Purchase Agreement";
                pdfreader = new PdfReader(FinaceOnlygenerateOffer(proposalInput, "SSA", currentLetter, currentIndex, subIndex, ppaProject));

                PdfContentByte cbPPA = pdfwriter.DirectContent;

                for (int a = 1; a <= pdfreader.NumberOfPages; a++)
                {
                    document.NewPage();
                    cbPPA.AddTemplate(pdfwriter.GetImportedPage(pdfreader, a), 1f, 0, 0, 1f, 0, 0);
                }
                subIndex++;
            }

            //if (proposalInput.IncludePBS)
            if (true)
            {
                document.NewPage();
                Project ppaProject = proposalInput.project;
                ppaProject.Costing.Plan = "Pay-By-Solar";
                pdfreader = new PdfReader(FinaceOnlygenerateOffer(proposalInput, "PBS", currentLetter, currentIndex, subIndex, ppaProject));

                PdfContentByte cbPPA = pdfwriter.DirectContent;

                for (int a = 1; a <= pdfreader.NumberOfPages; a++)
                {
                    document.NewPage();
                    cbPPA.AddTemplate(pdfwriter.GetImportedPage(pdfreader, a), 1f, 0, 0, 1f, 0, 0);
                }
                subIndex++;
            }

            #endregion
            ////---------------------------------------------------------End of Generate SECTION A----------------------------------------------------////

            #region Terms & Conditions
            ////Specific Terms
            document.NewPage();
            pdfreader = new PdfReader(SpecificTerms(proposalInput.Exclusions.Select(m => m.Name).ToList()));
            PdfContentByte cbGENERAL = pdfwriter.DirectContent;
            cbGENERAL = pdfwriter.DirectContent;
            cbGENERAL.AddTemplate(pdfwriter.GetImportedPage(pdfreader, 1), 1f, 0, 0, 1f, 0, 0);

            ////General Terms
            document.NewPage();
            pdfreader = new PdfReader(GeneralTerms(proposalInput.superProject.Country.Currency, proposalInput.superProject.Country.Currency_Per_Dollar));
            cbGENERAL = pdfwriter.DirectContent;
            cbGENERAL.AddTemplate(pdfwriter.GetImportedPage(pdfreader, 1), 1f, 0, 0, 1f, 0, 0);

            ////PBS Terms
            document.NewPage();
            pdfreader = new PdfReader(PBSTerms());
            cbGENERAL = pdfwriter.DirectContent;
            cbGENERAL.AddTemplate(pdfwriter.GetImportedPage(pdfreader, 1), 1f, 0, 0, 1f, 0, 0);
            #endregion
            ////---------------------------------------------------------End of Generate Terms & Condition----------------------------------------------------////

            letINdex++;
            currentLetter = letters[letINdex];
            currentLetter = HeaderIndexes[letINdex];
            currentIndex = 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="proposalInput"></param>
        /// <param name="type"></param>
        /// <param name="letter"></param>
        /// <param name="index"></param>
        /// <param name="subindex"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        public static byte[] FinaceOnlygenerateOffer(ProposalInputModel proposalInput ,string type, string letter, int index, int subindex, Project project)
        {
            SuperProject super = new SuperProject(project);
            MemoryStream stream = new MemoryStream(); //output stream
            Document document = new Document(); //pdf document to write
            PdfWriter pdfwriter = PdfWriter.GetInstance(document, stream);
            document.Open();

            Image bulletPoint = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/bullet square.png"));
            bulletPoint.ScalePercent(80);
            bulletPoint.Alignment = Image.ALIGN_CENTER;
            int subsub = 1;
            //Financed solutions  x, offer page (done) , addition terms x

            string fontpath = System.Web.HttpContext.Current.Server.MapPath("~/fonts/wingding.ttf");
            BaseFont baseW = BaseFont.CreateFont(fontpath, BaseFont.CP1252, BaseFont.EMBEDDED);

            Font wingding = new Font(baseW, MidText1, Font.NORMAL, nviDarkBlue);
            string title = "";
            string message = "";
            switch (type)
            {
                case "PBS": title = "PAY-BY-SOLAR"; message = "Pay-By-Solar"; break;
                case "SSA": title = "SOLAR SERVICES"; message = "Solar Services"; break;
            }

            #region Financed Solutions

            string currency = super.Country.Currency;
            #region Header
            Paragraph empty2 = new Paragraph(0f, " ", new Font(Font.FontFamily.HELVETICA, 5, Font.BOLD, BaseColor.WHITE));
            string spa = type == "SSA" ? "SOLAR SERVICES AGREEMENT" : "PAY-BY-SOLAR";
            string optiontype = type == "SSA" ? "OPTION A" : "OPTION B";
            //make index
            string option = string.Format("  OPTION {0}: {1} ({2})", "A", spa, type);

            Paragraph Fsubheader = new Paragraph(option, new Font(Font.FontFamily.HELVETICA, BigText - 3, Font.BOLD, nviDarkBlue)) { IndentationLeft = -10, SpacingBefore = 5 };
            ////Commented by prashant
            //document.Add(Fsubheader);

            Chunk cover_linebreak = new Chunk(new LineSeparator(1.1f, 106, BaseColor.GRAY, Image.ALIGN_CENTER, -17));

            #endregion

            if (type == "SSA")
            {
                empty2.SpacingAfter = 1;
                document.Add(empty2);

                PdfPTable Fheader_table = HeaderTable(new float[] { 50, 560 });
                Fheader_table.PaddingTop = 0;
                Fheader_table.SpacingBefore = 17;

                Fheader_table.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
                Fheader_table.AddCell(new PdfPCell(FCell("FINANCE OPTIONS*", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
                document.Add(Fheader_table);

                #region Summary of Costs
                PdfPTable FBorderNest = new PdfPTable(1);
                FBorderNest.TotalWidth = document.PageSize.Width - (2 * document.LeftMargin + 20);
                FBorderNest.LockedWidth = true;

                PdfPCell FNestCell = new PdfPCell();
                FNestCell.Padding = 5;
                FNestCell.HorizontalAlignment = Image.ALIGN_CENTER;
                FNestCell.Border = Image.NO_BORDER;

                //FNestCell.AddElement(new Paragraph("We are pleased to enclose a " + message + " offer (for the supply of energy only), through our \nSolar4Africa Platform.", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue)) { Leading = 2, MultipliedLeading = 1f, SpacingBefore = 15 });
                //FNestCell.AddElement(new Paragraph("Through our accredited partners, we offer fully financed or partially financed solar solutions.", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue)) { Leading = 2, MultipliedLeading = 1f, SpacingBefore = 15 });

                //FNestCell.AddElement(new Paragraph("Option A SSA:", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue)) { SpacingBefore = 10, SpacingAfter = -1 });

                FNestCell.AddElement(new Paragraph("Through our accredited partners, we offer fully financed or partially financed solar solutions.", new Font(Font.FontFamily.HELVETICA, MidText2 - 1, Font.ITALIC, nviDarkBlue)) { SpacingAfter = 3 });

                int tWith = 430;

                PdfPTable FSummaryTable = new PdfPTable(new float[] { 390, 80, 80 });
                FSummaryTable.TotalWidth = 430;
                FSummaryTable.LockedWidth = true;
                FSummaryTable.SpacingBefore = 2;
                FSummaryTable.HorizontalAlignment = Image.ALIGN_LEFT;

                FSummaryTable.AddCell(new PdfPCell(FCell(" ", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("Option A SSA"), MidText2, true, false, true, nviLightBLue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviDarkBlue });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("Option B PBS"), MidText2, true, false, true, nviLightBLue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviDarkBlue });

                FSummaryTable.AddCell(new PdfPCell(FCell("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, PaddingBottom = 0, Colspan = 3 });
                //upfront payment
                FSummaryTable.AddCell(new PdfPCell(FCell("Upfront Payment:", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.Up.ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //TFR
                FSummaryTable.AddCell(new PdfPCell(FCell("Variable KWh Tariff:", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.TFR.ToString("#,##0.###"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell(super.TFR.ToString("#,##0.###"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //FP
                FSummaryTable.AddCell(new PdfPCell(FCell("Additional Fixed Charge (Monthly):", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell((super.FP / 12).ToString("#,##0."), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell((super.FP / 12).ToString("#,##0."), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //Term (months)
                FSummaryTable.AddCell(new PdfPCell(FCell("Agreement Term:", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell((super.Project.Costing.Term * 12).ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell((super.Project.Costing.Term * 12).ToString("#,##0.##"), MidText2, false, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //empty row
                FSummaryTable.AddCell(new PdfPCell(ContentCell(" ", CellInfo.Empty)) { Padding = 0 });
                FSummaryTable.AddCell(new PdfPCell(ContentCell(" ", CellInfo.Empty)) { Padding = 0 });
                FSummaryTable.AddCell(new PdfPCell(ContentCell(" ", CellInfo.Empty)) { Padding = 0 });


                //Total Solar Costs all inclusive monthly           (Y*MW*TRF*1000)/12   + FP/12
                FSummaryTable.AddCell(new PdfPCell(FCell("Total Solar Costs (All Inclusive) - Monthly:", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f });
                FSummaryTable.AddCell(new PdfPCell(FCell(((super.Project.Costing.Yield * super.prosize * super.TFR * 1000) / 12 + super.FP / 12).ToString("#,##0.##"), MidText2, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell(((super.Project.Costing.Yield * super.prosize * super.TFR * 1000) / 12 + super.FP / 12).ToString("#,##0.##"), MidText2, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });

                //Actuall amount point
                FSummaryTable.AddCell(new PdfPCell(FCell("* Actual Amount will depend on monthly performance of the Solar Plant.", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, PaddingBottom = 0, Colspan = 3 });

                PdfPTable FSumNest = new PdfPTable(1);
                FSumNest.TotalWidth = tWith + 3;
                FSumNest.LockedWidth = true;
                FSumNest.HorizontalAlignment = Image.ALIGN_LEFT;
                FSumNest.AddCell(new PdfPCell(FSummaryTable) { Padding = 3, PaddingBottom = 3, HorizontalAlignment = Image.ALIGN_LEFT });
                FNestCell.AddElement(FSumNest);

                FNestCell.AddElement(new Paragraph("*Subject to the terms and conditions.", new Font(Font.FontFamily.HELVETICA, MidText2 - 1, Font.ITALIC, nviDarkBlue)) { SpacingAfter = 3 });
                //--------------------------------------------

                FSummaryTable = new PdfPTable(new float[] { 390 });
                FSummaryTable.TotalWidth = 430;
                FSummaryTable.LockedWidth = true;
                FSummaryTable.SpacingBefore = 2;
                FSummaryTable.HorizontalAlignment = Image.ALIGN_LEFT;

                FNestCell.AddElement(new Paragraph("OPTION A - SOLAR SERVICES AGREEMENT", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue)) { SpacingBefore = 10 });
                FSummaryTable.AddCell(new PdfPCell(FCell("SolarAfrica will contract with you to lease the solar system in this proposal to you, on operating lease basis", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("MINIMUM CONTRACT PERIOD: 5 years (thereafter exit penalty applies, which reduces each year)", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("PERFORMANCE GUARANTEE: Included", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("BUY-OUT OPTION: Yes, after 5 years", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("EARLY EXIT PENALTY: Yes", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("Ownership of the solar system remains with SolarAfrica unless the system is purchased during the contract period.", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("MONTHLY SAVING: Rxxxx", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSumNest = new PdfPTable(1);
                FSumNest.TotalWidth = tWith + 3;
                FSumNest.LockedWidth = true;
                FSumNest.HorizontalAlignment = Image.ALIGN_LEFT;
                FSumNest.AddCell(new PdfPCell(FSummaryTable) { Padding = 7, PaddingBottom = 3, HorizontalAlignment = Image.ALIGN_LEFT });
                FNestCell.AddElement(FSumNest);

                FSummaryTable = new PdfPTable(new float[] { 390 });
                FSummaryTable.TotalWidth = 430;
                FSummaryTable.LockedWidth = true;
                FSummaryTable.SpacingBefore = 2;
                FSummaryTable.HorizontalAlignment = Image.ALIGN_LEFT;

                FNestCell.AddElement(new Paragraph("OPTION B - PAY-BY-SOLAR LEASE AGREEMENT", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue)) { SpacingBefore = 10 });
                FSummaryTable.AddCell(new PdfPCell(FCell("SolarAfrica will contract with to finance the solar system in this proposal under a finance lease.", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("MINIMUM CONTRACT PERIOD: 5 years (thereafter exit penalty applies, which reduces each year)", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("PERFORMANCE GUARANTEE: Included", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("BUY-OUT OPTION: Yes, after 5 years", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("CONTRACT EXIT PENALTY: Yes", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("Ownership transfers once the last payment is made.", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSummaryTable.AddCell(new PdfPCell(FCell("MONTHLY SAVING: Rxxxx", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                FSumNest = new PdfPTable(1);
                FSumNest.TotalWidth = tWith + 3;
                FSumNest.LockedWidth = true;
                FSumNest.HorizontalAlignment = Image.ALIGN_LEFT;
                FSumNest.AddCell(new PdfPCell(FSummaryTable) { Padding = 7, PaddingBottom = 3, HorizontalAlignment = Image.ALIGN_LEFT });
                FNestCell.AddElement(FSumNest);
                //--------------------------------------------

                #endregion

                #region Frequently Asked Questions
                //Left solar 4 africa image, right lable, value 
                FNestCell.AddElement(new Paragraph("FREQUENTLY ASKED QUESTIONS:", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue)) { SpacingBefore = 5 });



                string _solarblkPath = System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/s4a Block.png");
                Image solarBlock = Image.GetInstance(_solarblkPath);
                solarBlock.ScalePercent(65);





                PdfPTable BottomImageTable = new PdfPTable(new float[] { solarBlock.ScaledWidth + 30, 500 });
                BottomImageTable.LockedWidth = true;
                BottomImageTable.TotalWidth = document.PageSize.Width - (2 * document.RightMargin + 20);
                BottomImageTable.SpacingBefore = 2;

                BottomImageTable.AddCell(new PdfPCell(solarBlock) { BorderColor = BaseColor.WHITE });



                Paragraph paragraph = new Paragraph();

                paragraph.Add(EquipmentPhrase("Who is the Contracting Party? ", true, MidText1 + 1, nviDarkBlue));
                paragraph.Add(EquipmentPhrase(" You contract directly with an NVI Energy country subsidiary. NVI Energy is the legal entity that founded and now manages the Solar4Africa Platform. Our platform financing partners will fund the installation in exchange for the payments received. NVI remains responsible for monitoring and management of each solar facility financed through Solar4Africa to ensure each plant is operating as intended.\n", false, MidText1 + 1, nviDarkBlue));

                paragraph.Add(EquipmentPhrase("\nWho does Installation and Maintenance? ", true, MidText1 + 1, nviDarkBlue));
                paragraph.Add(EquipmentPhrase(" You will contract with NVI Energy in order for us to finance the solar installation, NVI will contract with a Solar4Africa technical partner to install and maintain the solar system, to the requirements agreed in our Solar Services Agreement.   \n", false, MidText1 + 1, nviDarkBlue));

                paragraph.Add(EquipmentPhrase("\nCan the Agreement be cancelled? ", true, MidText1 + 1, nviDarkBlue));
                paragraph.Add(EquipmentPhrase(" From year 5 onwards, you may cancel the agreement by paying an early termination fee. Once cancelled, ownership will transfer to you. There is no further legal obligations between the parties once the Agreement is terminated.\n", false, MidText1 + 1, nviDarkBlue));

                paragraph.Add(EquipmentPhrase("\nWho is responsible for operations, maintenance and performance? ", true, MidText1 + 1, nviDarkBlue));
                paragraph.Add(EquipmentPhrase(" As the contracting party, NVI Energy is responsible for operations, maintenance and system performance. We contract with a technical partner who performs scheduled and unscheduled maintenance services at our cost. The systems performance is constantly monitored by us and as you only pay for energy produced, you are not paying for a system that doesn't produce energy.\n", false, MidText1 + 1, nviDarkBlue));


                BottomImageTable.AddCell(new PdfPCell(paragraph) { Border = Image.NO_BORDER, PaddingTop = -2 });


                FNestCell.AddElement(BottomImageTable);

                //FNestCell.AddElement(solarBlock);

                //FNestCell.AddElement(f1);
                //FNestCell.AddElement(f2);


                #endregion

                FBorderNest.AddCell(FNestCell);
                document.Add(FBorderNest);

                ////END the page with line
                //document.Add(cover_linebreak);

                //FSummaryTable = new PdfPTable(new float[] { 400, 400 });
                //FSummaryTable.TotalWidth = 800;
                //FSummaryTable.LockedWidth = true;
                //FSummaryTable.SpacingBefore = 2;
                //FSummaryTable.HorizontalAlignment = Image.ALIGN_LEFT;
                //FSummaryTable.AddCell(new PdfPCell(FCell(proposalInput.project.Details.Surname, MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                //FSummaryTable.AddCell(new PdfPCell(FCell("04", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 1.5f, BackgroundColor = nviLightBLue });
                //document.Add(FSummaryTable);
            }
            //string _solarCircPath = System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/solar circle.png");
            //Image solarCircle = Image.GetInstance(_solarCircPath);

            //solarCircle.ScaleAbsolute(80, 80);
            //solarCircle.SetAbsolutePosition(450, 622);
            //solarCircle.Alignment = Image.TEXTWRAP;
            //pdfwriter.DirectContent.AddImage(solarCircle);

            //at the end add all content to cell, add cell to bordernest, add bordernest to document.
            subsub++;
            #endregion

            document.NewPage();

            //...Commented...//
            #region Draw Rectangle
            //PdfContentByte rect_border = pdfwriter.DirectContent;
            //int heightS = 80;
            //rect_border.Rectangle(document.LeftMargin, document.BottomMargin + heightS, document.PageSize.Width - document.LeftMargin * 2, document.PageSize.Height - heightS - 2 * document.TopMargin);
            //rect_border.Stroke();
            #endregion
            //...Commented...//

            #region Everything the the rectangle

            #region Top Titles

            PdfPTable Fheader_table2 = HeaderTable(new float[] { 50, 560 });
            Fheader_table2.PaddingTop = 0;
            Fheader_table2.SpacingBefore = 17;

            Fheader_table2.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
            Fheader_table2.AddCell(new PdfPCell(FCell(optiontype + " - " + type, BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
            document.Add(Fheader_table2);

            Paragraph TitlePara = new Paragraph(optiontype + " - " + type, new Font(Font.FontFamily.HELVETICA, BigText, Font.BOLD, nviDarkBlue));
            //TitlePara.Alignment = Image.ALIGN_LEFT;
            //document.Add(TitlePara);
            ////Start the page with line
            //document.Add(cover_linebreak);

            Paragraph em = new Paragraph(1, " ");
            document.Add(em);
            Paragraph CustomerQuote = new Paragraph("TERM SHEET", new Font(Font.FontFamily.HELVETICA, MidText1 + 1, Font.BOLD, nviDarkBlue));
            CustomerQuote.Alignment = Image.ALIGN_CENTER;
            CustomerQuote.SpacingBefore = 20;
            CustomerQuote.SpacingAfter = -3;
            document.Add(CustomerQuote);

            TitlePara = new Paragraph(title + " AGREEMENT (" + type + ")", new Font(Font.FontFamily.HELVETICA, BigText, Font.BOLD, nviDarkBlue));
            TitlePara.Alignment = Image.ALIGN_CENTER;

            document.Add(TitlePara);

            Paragraph quoteCredit = new Paragraph("< QUOTE SUBJECT TO CREDIT REVIEW >", new Font(Font.FontFamily.HELVETICA, MidText1 + 2, Font.NORMAL, nviDarkBlue));
            quoteCredit.Alignment = Image.ALIGN_CENTER;

            document.Add(quoteCredit);

            PdfPTable PageParentTable = new PdfPTable(1);

            int myPagePadding = 15;
            PageParentTable.TotalWidth = document.PageSize.Width - 2 * (document.LeftMargin + myPagePadding);
            PageParentTable.HorizontalAlignment = Image.ALIGN_CENTER;
            PdfPCell PageParentCell = new PdfPCell() { Border = Image.NO_BORDER, PaddingLeft = myPagePadding, PaddingRight = myPagePadding, HorizontalAlignment = Image.ALIGN_CENTER };

            ApplicationUser prepper = db.Users.Where(m => m.Id == project.CurrentUserID).FirstOrDefault();

            PdfPTable offerToTable = new PdfPTable(new float[] { 350, 300, 600 });
            offerToTable.TotalWidth = document.PageSize.Width - 2 * (document.LeftMargin + myPagePadding);
            offerToTable.LockedWidth = true;
            offerToTable.SpacingBefore = 10;

            offerToTable.AddCell(new PdfPCell(FCell("Offer To:", MidText1 + 1, true, true, false, nviDarkBlue)) { Border = Image.BOTTOM_BORDER, BorderColor = BaseColor.BLACK, BorderWidthBottom = 0.2f, PaddingBottom = -5 });
            offerToTable.AddCell(ContentCell("", CellInfo.Empty));
            offerToTable.AddCell(new PdfPCell(FCell("Prepared By:", MidText1 + 1, true, true, false, nviDarkBlue)) { Border = Image.BOTTOM_BORDER, BorderColor = BaseColor.BLACK, BorderWidthBottom = 0.2f, PaddingBottom = -5 });

            if (prepper.Company.Contains("nvi"))
            {
                offerToTable.AddCell(new PdfPCell(FCell(project.Details.CompanyName, MidText1 + 1, true, true, false, nviDarkBlue)) {HorizontalAlignment = Image.ALIGN_LEFT ,Border = Image.NO_BORDER, PaddingTop = 8 });
                offerToTable.AddCell(ContentCell("", CellInfo.Empty));
                offerToTable.AddCell(new PdfPCell(FCell("NVI Energy Ltd", MidText1 + 1, true, true, false, nviDarkBlue)) { Border = Image.NO_BORDER, PaddingTop = 8 });
            }

            else
            {
                offerToTable.AddCell(new PdfPCell(FCell(project.Details.CompanyName, MidText1 + 1, true, true, false, nviDarkBlue)) {HorizontalAlignment = Image.ALIGN_LEFT, Border = Image.NO_BORDER, PaddingTop = 8 });
                offerToTable.AddCell(ContentCell("", CellInfo.Empty));
                offerToTable.AddCell(new PdfPCell(FCell(prepper.Company, MidText1 + 1, true, true, false, nviDarkBlue)) { Border = Image.NO_BORDER, PaddingTop = 8 });
            }


            offerToTable.AddCell(new PdfPCell(FCell(project.Details.P_City, MidText1 + 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Border = Image.NO_BORDER, Padding = 0 });
            offerToTable.AddCell(ContentCell("", CellInfo.Empty));
            offerToTable.AddCell(new PdfPCell(FCell(prepper.UserName, MidText1 + 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Border = Image.NO_BORDER, Padding = 0 });

            offerToTable.AddCell(new PdfPCell(FCell(project.Details.P_Site, MidText1 + 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Border = Image.NO_BORDER, Padding = 0 });
            offerToTable.AddCell(ContentCell("", CellInfo.Empty));
            offerToTable.AddCell(new PdfPCell(FCell(prepper.Email, MidText1 + 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Border = Image.NO_BORDER, Padding = 0 });

            PageParentCell.AddElement(offerToTable);

            #endregion

            #region Quoted Tariff & Other Charges

            PdfPTable TariffTable = new PdfPTable(new float[] { 520, 260, 220, 10, 390, 220, 200 });

            TariffTable.LockedWidth = true;
            TariffTable.TotalWidth = document.PageSize.Width - 2 * (document.LeftMargin + myPagePadding);
            TariffTable.SpacingBefore = 10;
            PdfPCell TariffHeader = new PdfPCell(FCell("YOUR QUOTED TARIFF & OTHER CHARGES", MidText1 + 2, true, false, true, BaseColor.WHITE)) { PaddingBottom = 7, PaddingTop = 2 };
            TariffHeader.Colspan = 7;
            TariffHeader.BackgroundColor = nviDarkBlue;

            TariffTable.AddCell(TariffHeader);

            TariffTable.AddCell(new PdfPCell(ContentCell("", CellInfo.Empty)) { Colspan = 7, Padding = 0 });//gap
            int rowBorderwidth = 4;
            if (type == "SSA")
            {
                TariffTable.AddCell(new PdfPCell(FCell("Tariff:", MidText1 + 1, true, false, false, nviDarkBlue))); //Tariff
                TariffTable.AddCell(new PdfPCell(FCell(super.TFR.ToString("#,##0."), MidText1 + 1, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2, PaddingTop = 0 });
                TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / kWh", MidText1 + 1, true, false, false, nviDarkBlue)));
            }
            else
            {
                TariffTable.AddCell(new PdfPCell(FCell("Tariff:", MidText1 + 1, true, false, false, nviDarkBlue))); //Tariff
                TariffTable.AddCell(new PdfPCell(FCell(super.TFR.ToString("#,##0."), MidText1 + 1, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2, PaddingTop = 0 });
                TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / kWh", MidText1 + 1, true, false, false, nviDarkBlue)));
            }

            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap

            TariffTable.AddCell(new PdfPCell(FCell("Initial Payment:*", MidText1 + 1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            TariffTable.AddCell(new PdfPCell(FCell(super.Project.real_UpfrontFee.ToString("#,##0.##"), MidText1 + 1, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth + 1, BorderColor = BaseColor.WHITE, PaddingBottom = 6, PaddingRight = 2, PaddingTop = 0 });
            TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency, MidText1 + 1, true, false, false, nviDarkBlue)));

            TariffTable.AddCell(new PdfPCell(FCell("Fixed Charges", MidText1 + 1, true, false, false, nviDarkBlue))); //Fixed Charges
            TariffTable.AddCell(new PdfPCell(FCell(super.FPMT.ToString("#,##0."), MidText1 + 1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / Mnth", MidText1 + 1, true, false, false, nviDarkBlue)));

            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap

            TariffTable.AddCell(new PdfPCell(new Phrase("Equivalent to:", new Font(Font.FontFamily.HELVETICA, MidText1, Font.ITALIC, nviDarkBlue))) { Border = Image.NO_BORDER }); //Upfront payment requirement
            TariffTable.AddCell(new PdfPCell(FCell((super.Project.real_UpfrontFee / super.prosize / 1000000).ToString("#,##0.##"), MidText1 + 1, true, true, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, BorderColor = BaseColor.GRAY, PaddingBottom = 0 });
            TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / Wp", MidText1 + 1, true, false, false, nviDarkBlue)));

            TariffTable.AddCell(new PdfPCell(FCell("Energy Storage Charge", MidText1 + 1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            TariffTable.AddCell(new PdfPCell(FCell((super.FPMT / 12).ToString("#,##0."), MidText1 + 1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / Mnth", MidText1 + 1, true, false, false, nviDarkBlue)));
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap

            TariffTable.AddCell(new PdfPCell(FCell("* Initial Payment, payable upfront on signature of Agreement subject to standard conditions.", MidText2 - 1, false, false, false, nviDarkBlue)) { Colspan = 2 }); //Upfront payment requirement
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));


            TariffTable.AddCell(new PdfPCell(FCell("Maintenance Charge", MidText1 + 1, true, false, false, nviDarkBlue))); //Upfront payment requirement

            if (super.Project.OMYN)
            {
                //include OM
                TariffTable.AddCell(new PdfPCell(FCell("Included", MidText1 + 1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            }
            else
            {
                TariffTable.AddCell(new PdfPCell(FCell(super.OM.ToString("#,##0."), MidText1 + 1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            }
            TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / Mnth", MidText1 + 1, true, false, false, nviDarkBlue)));
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap

            TariffTable.AddCell(new PdfPCell(FCell("Estimated Insurance Charge", MidText1 + 1, true, false, false, nviDarkBlue))); //Upfront payment requirement

            if (super.Project.InsureYN)
            {
                //include OM
                TariffTable.AddCell(new PdfPCell(FCell("Included", MidText1 + 1, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            }
            else
            {
                TariffTable.AddCell(new PdfPCell(FCell(super.Insure.ToString("#,##0."), MidText1 + 1, true, false, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            }
            TariffTable.AddCell(new PdfPCell(FCell(super.Country.Currency + " / Mnth", MidText1 + 1, true, false, false, nviDarkBlue)));
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            TariffTable.AddCell(ContentCell("", CellInfo.Empty));//gap

            PageParentCell.AddElement(TariffTable);
            #endregion

            #region Project Indicators
            PdfPTable ProjectTable = new PdfPTable(new float[] { 520, 260, 220, 10, 390, 220, 200 });

            ProjectTable.LockedWidth = true;
            ProjectTable.TotalWidth = document.PageSize.Width - 2 * (document.LeftMargin + myPagePadding);
            ProjectTable.SpacingBefore = 10;
            PdfPCell ProjectHeader = FCell("YOUR PROJECT INDICATORS", MidText1, true, false, true, nviDarkBlue);
            ProjectHeader.Colspan = 7;
            ProjectHeader.BackgroundColor = nviLightBLue;
            ProjectHeader.PaddingBottom = 9;
            ProjectHeader.PaddingTop = 3;
            ProjectTable.AddCell(ProjectHeader);
            ProjectTable.AddCell(new PdfPCell(ContentCell("", CellInfo.Empty)) { Colspan = 7, Padding = 0 });//gap

            ProjectTable.AddCell(new PdfPCell(FCell("System Size", MidText1, true, false, false, nviDarkBlue))); //Tariff
            ProjectTable.AddCell(new PdfPCell(FCell((super.prosize * 1000).ToString("#,##0.##"), MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell("kWp", MidText1, true, false, false, nviDarkBlue)));

            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap


            ProjectTable.AddCell(new PdfPCell(FCell("Battery Size", MidText1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            ProjectTable.AddCell(new PdfPCell(FCell((super.BattCap / 1000).ToString("#,##0.00"), MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell("kWh", MidText1, true, false, false, nviDarkBlue)));



            ProjectTable.AddCell(new PdfPCell(FCell("Installation Type", MidText1, true, false, false, nviDarkBlue))); //Tariff
            ProjectTable.AddCell(new PdfPCell(FCell(super.Project.Costing.Type, MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell("kWp", MidText1, true, false, false, nviDarkBlue)));

            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap


            ProjectTable.AddCell(new PdfPCell(FCell("Bi-Directional Capacity", MidText1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            ProjectTable.AddCell(new PdfPCell(FCell((super.sunnyIslandCap / 1000).ToString("#,##0.00"), MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell("kWp", MidText1, true, false, false, nviDarkBlue)));




            ProjectTable.AddCell(new PdfPCell(FCell("Expected Energy Yield", MidText1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            ProjectTable.AddCell(new PdfPCell(FCell(super.Project.Costing.Yield.ToString("#,##0.##"), MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell("kWh / kWp", MidText1, true, false, false, nviDarkBlue)));
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap

            ProjectTable.AddCell(new PdfPCell(FCell("Trade Agreement Length", MidText1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            ProjectTable.AddCell(new PdfPCell(FCell((super.Project.Costing.Term * 12).ToString(), MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell("Months", MidText1, true, false, false, nviDarkBlue)));
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap


            ProjectTable.AddCell(new PdfPCell(FCell("Exchange Rate (US$)", MidText1, true, false, false, nviDarkBlue))); //Upfront payment requirement
            ProjectTable.AddCell(new PdfPCell(FCell(super.Country.Currency_Per_Dollar.ToString(), MidText1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Border = Image.BOTTOM_BORDER, BorderWidth = rowBorderwidth, BorderColor = BaseColor.WHITE, PaddingBottom = 4, PaddingRight = 2 });
            ProjectTable.AddCell(new PdfPCell(FCell(super.Country.Currency + "/ US$", MidText1, true, false, false, nviDarkBlue)));
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap
            ProjectTable.AddCell(ContentCell("", CellInfo.Empty));//gap

            PageParentCell.AddElement(ProjectTable);
            #endregion

            #region General Conditions
            PdfPTable GeneralTable = new PdfPTable(1);
            Image bulletCirc = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/Images/bullet circ.png"));
            bulletCirc.Alignment = Image.ALIGN_RIGHT;
            bulletCirc.ScalePercent(35);

            GeneralTable.LockedWidth = true;
            GeneralTable.TotalWidth = document.PageSize.Width - 2 * (document.LeftMargin + myPagePadding);
            GeneralTable.SpacingBefore = 10;
            PdfPCell GeneralHeader = FCell("General Conditions:", MidText1, true, false, true, nviDarkBlue);
            GeneralHeader.Colspan = 1;
            GeneralHeader.BackgroundColor = nviLightBLue;
            GeneralHeader.PaddingBottom = 3;
            GeneralHeader.PaddingTop = 1;
            GeneralTable.AddCell(GeneralHeader);

            #region Generate Table for list layout
            PdfPTable mtermsTable = new PdfPTable(new float[] { 30, 400 });
            mtermsTable.LockedWidth = true;
            mtermsTable.TotalWidth = document.PageSize.Width - (2 * document.LeftMargin);
            mtermsTable.SpacingBefore = 10;

            //¡   is wingding for white circle
            Font s = new Font(wingding);
            s.Size = MidText2 - 2;
            float sleading = 20;

            Font conditionFont = new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue);
            mtermsTable.AddCell(new PdfPCell(new Paragraph(sleading, "¡", s)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER, Padding = 0 });
            mtermsTable.AddCell(GenTermCell("This quote is an estimate only and is based on the assumptions shown herein. A site evaluation including analysis of shading and structural integrity of any planned roof surface (if applicable) must be undertakein. We also require a completed Application Form, including documents, agreements, notices and assignments that are reasonably requested by us to confirm all pricing. ", conditionFont));

            mtermsTable.AddCell(new PdfPCell(new Paragraph(sleading, "¡", s)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER, Padding = 0 });
            mtermsTable.AddCell(GenTermCell("The tariff and fixed payment quotes are subject to general contract conditions and are only binding on acceptance of a final offer and conclusion of an agreement. All charges escalate annually at CPI. ", conditionFont));

            mtermsTable.AddCell(new PdfPCell(new Paragraph("¡", s)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER, Padding = 0 });
            mtermsTable.AddCell(GenTermCell("The quoted pricing excludes any Works expressly agreed will be performed by the you and will be included in our agreement as ‘Client Works’. ", conditionFont));

            mtermsTable.AddCell(new PdfPCell(new Paragraph("¡", s)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER, Padding = 0 });
            mtermsTable.AddCell(GenTermCell("All tariffs or fixed pricing quoted is excl. of VAT and subject to exchange rate fluctuations. Final approved quotes are valid for 30 days and exchange rates valid for 7 days.", conditionFont));

            mtermsTable.AddCell(new PdfPCell(new Paragraph("¡", s)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER, Padding = 0 });
            mtermsTable.AddCell(GenTermCell("The operations & maintenance quote (O&M Services) is a fully inclusive offer, except for works that you have agreed will be performed by you (and listed in our agreement)", conditionFont));

            mtermsTable.AddCell(new PdfPCell() { Border = Image.NO_BORDER, Padding = 0 });
            PdfPCell mcell = new PdfPCell();

            //mtermsTable.AddCell(new PdfPCell(new Phrase("¡", s)) { Border = Image.NO_BORDER, HorizontalAlignment = Image.ALIGN_CENTER, Padding = 2 });
            //mtermsTable.AddCell(new PdfPCell(EquipmentPhrase(" All interests held by NVI Energy, Solar4Africa or a financing party will cese on payment of the last outstanding amount under the agreement.", false, MidText1 , nviDarkBlue)) { Border = Image.NO_BORDER, Padding = 2 });

            PageParentCell.AddElement(GeneralTable);
            PageParentCell.AddElement(mtermsTable);

            Paragraph seeterms = new Paragraph("See Terms applicable to our " + title + " Agreement.", new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue));
            seeterms.SpacingBefore = 6;
            seeterms.Alignment = Element.ALIGN_LEFT;
            seeterms.IndentationLeft = -50;
            PageParentCell.AddElement(seeterms);
            Chunk linebreak = new Chunk(new LineSeparator(0.5f, 126f, BaseColor.BLACK, Element.ALIGN_CENTER, 10));

            //PageParentCell.AddElement(linebreak);
            //PdfPTable endTable = new PdfPTable(1);
            //endTable.LockedWidth = true;
            //endTable.TotalWidth = document.PageSize.Width - 2 * (document.LeftMargin + myPagePadding);
            //endTable.SpacingBefore = 8;
            //PdfPCell endItem = FCell("This is a non-binding indicative quotation based on information provided.", MidText1, false, false, true, nviDarkBlue);
            //endItem.HorizontalAlignment = Image.ALIGN_CENTER;
            //endItem.Colspan = 1;
            //endItem.Padding = 11; endItem.PaddingBottom = 13;
            //endTable.AddCell(endItem);

            //PageParentCell.AddElement(endTable);

            #endregion
            #endregion

            PageParentTable.AddCell(PageParentCell);
            document.Add(PageParentTable);

            ////END the page with line
            //document.Add(cover_linebreak);

            //add solar circle image here
            //last so that directcontent writes over the text
            //solarCircle.SetAbsolutePosition(document.Right - 90, document.BottomMargin + 100);
            //solarCircle.Alignment = Image.TEXTWRAP;
            //solarCircle.ScaleAbsolute(85, 85);

            //pdfwriter.DirectContent.AddImage(solarCircle);

            #endregion

            document.NewPage();
            #region Economic Assesment

            #region Header
            document.Add(empty2);

            PdfPTable Eheader_table = HeaderTable(new float[] { 50, 560 });
            Eheader_table.PaddingTop = 0;
            Eheader_table.SpacingBefore = 17;

            Eheader_table.AddCell(new PdfPCell(FCell(" ", BigText + 8, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6, HorizontalAlignment = Image.ALIGN_CENTER });
            Eheader_table.AddCell(new PdfPCell(FCell("ECONOMIC ASSESMENT: " + title.ToUpper() + " AGREEMENT", BigText - 3, true, false, true, nviDarkBlue)) { PaddingTop = 3, PaddingBottom = 6 });
            document.Add(Eheader_table);

            Paragraph ESubheading = new Paragraph(EquipmentPhrase("The following data is presented for financial analysis of entering into a :", false, MidText1, nviDarkBlue));
            ESubheading.SpacingBefore = 20;

            document.Add(ESubheading);

            ESubheading = new Paragraph(EquipmentPhrase("ENERGY PRODUCTION & CONSUMPTION:", true, MidText1, nviDarkBlue));
            ESubheading.SpacingBefore = 10;

            document.Add(ESubheading);

            #endregion

            #region Big Table

            PdfPTable bigTable = new PdfPTable(new float[] { 50, 120, 50, 100, 100, 100, 100, 100, 100 });
            bigTable.TotalWidth = document.PageSize.Width - (2 * document.RightMargin);
            bigTable.LockedWidth = true;

            bigTable.AddCell(new PdfPCell(ContentCell("", CellInfo.Empty)) { Colspan = 6 });
            bigTable.AddCell(new PdfPCell(FCell("(As Per D1)", MidText1, true, false, false, nviDarkBlue)) { PaddingBottom = 1 });
            bigTable.AddCell(new PdfPCell(ContentCell("", CellInfo.Empty)) { Colspan = 2 });


            #region Table Headers
            bigTable.AddCell(new PdfPCell(FCell("Year", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 5 });
            bigTable.AddCell(new PdfPCell(FCell("Solar Energy Production (KWh)", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, PaddingBottom = 5 });
            bigTable.AddCell(new PdfPCell(FCell("Tariff (" + super.Country.Currency + ")", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 5 });
            bigTable.AddCell(new PdfPCell(FCell("Annual Variable Cost", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 5 });
            bigTable.AddCell(new PdfPCell(FCell("Annual Fixed Cost", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 5 });
            bigTable.AddCell(new PdfPCell(FCell("Total Solar Costs", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 1 });
            bigTable.AddCell(new PdfPCell(FCell("Cost of Existing Energy (kWh)", MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 1 });
            bigTable.AddCell(new PdfPCell(FCell("Energy Costs Avoided", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Border = Image.BOTTOM_BORDER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 1 });
            bigTable.AddCell(new PdfPCell(FCell("Cummulative Savings", MidText2, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, BorderWidth = 0.2f, BorderColor = BaseColor.BLACK, PaddingBottom = 1 });

            #endregion

            #region Subheaders
            bigTable.AddCell(ContentCell("", CellInfo.Empty));
            bigTable.AddCell(new PdfPCell(FCell("A", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell("B", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell("= C", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell("D", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell("C + D", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell("E", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell("= A x E", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });
            bigTable.AddCell(new PdfPCell(FCell(" ", MidText2 - 1, true, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, PaddingBottom = -1 });

            #endregion

            #region Table Values
            for (int a = 0; a < 20; a++)
            {
                bigTable.AddCell(new PdfPCell(FCell((a + 1).ToString(), MidText2, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Padding = 4, PaddingRight = 6, PaddingBottom = 0.5f });
                bigTable.AddCell(new PdfPCell(FCell(super.SolarProduction20[a].ToString("#,##0."), MidText2, false, true, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = 0.5f, PaddingRight = 2 });
                bigTable.AddCell(new PdfPCell(FCell(super.Tarrif20[a].ToString("#,##0.##"), MidText2, false, true, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = 0, PaddingRight = 2 });
                bigTable.AddCell(new PdfPCell(FCell((super.AnnualVariable20[a]).ToString("#,##0."), MidText2, false, true, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = 0, PaddingRight = 2 });
                bigTable.AddCell(new PdfPCell(FCell(super.AnnualFixed20[a].ToString("#,##0."), MidText2, false, true, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = 0, PaddingRight = 2 });
                bigTable.AddCell(new PdfPCell(FCell(super.TotalSolar20[a].ToString("#,##0."), MidText2, false, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = -0, PaddingRight = 2, BackgroundColor = nviSemiGrey });
                bigTable.AddCell(new PdfPCell(FCell(super.ExistingEnergy20[a].ToString("#,##0.###"), MidText2, false, true, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = 0, PaddingRight = 2 });
                bigTable.AddCell(new PdfPCell(FCell(super.AvoidedEnergy20[a].ToString("#,##0."), MidText2, false, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, PaddingBottom = 0, PaddingRight = 2, BackgroundColor = nviSemiGrey });
                bigTable.AddCell(new PdfPCell(FCell(super.CumulativeSavings20[a].ToString("#,##0."), MidText2, false, true, true, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT });
            }
            #endregion

            #region Table Bottom

            //empty row
            bigTable.AddCell(new PdfPCell(ContentCell("", CellInfo.Empty)) { Colspan = 9 });

            bigTable.AddCell(new PdfPCell(FCell("Assumptions:", MidText2, true, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_CENTER, Padding = 2, Colspan = 2 });
            bigTable.AddCell(new PdfPCell(ContentCell("", CellInfo.Empty)) { Colspan = 7 });

            //cpi
            bigTable.AddCell(new PdfPCell(FCell(" 1", MidText2 - 3, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Colspan = 1 });

            bigTable.AddCell(new PdfPCell(FCell("Annual Solar Energy Increase:", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Colspan = 3 });
            bigTable.AddCell(new PdfPCell(FCell(super.Country.CPI.ToString("0.00") + "%", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Colspan = 1 });
            bigTable.AddCell(new PdfPCell(FCell("Per Annum (CPI)", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Colspan = 4 });

            //Existing energy CPIu
            bigTable.AddCell(new PdfPCell(FCell(" 2", MidText2 - 3, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Colspan = 1 });

            bigTable.AddCell(new PdfPCell(FCell("Existing Energy Increase:", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Colspan = 3 });
            bigTable.AddCell(new PdfPCell(FCell((super.Country.UtilityInflation).ToString("0.00") + "%", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Colspan = 1 });
            bigTable.AddCell(new PdfPCell(FCell("(First Year)", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Colspan = 4 });

            //extra
            //bigTable.AddCell(new PdfPCell(FCell(" 3", MidText2 - 3, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_RIGHT, Colspan = 1 });

            //bigTable.AddCell(new PdfPCell(FCell(" Agreement pricing is extended after the contract term at 30% below the then applicable contract rates.	", MidText2 - 1, false, false, false, nviDarkBlue)) { HorizontalAlignment = Image.ALIGN_LEFT, Colspan = 8 });

            #endregion

            document.Add(bigTable);
            ////END the page with line
            //document.Add(cover_linebreak);

            #endregion
            #endregion

            pdfwriter.CloseStream = false;
            document.Close();
            stream.Position = 0;
            stream.Flush();
            return stream.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="overview_table_1"></param>
        /// <param name="labeltext"></param>
        /// <param name="valuetext"></param>
        /// <returns></returns>
        public static PdfPTable GlobalCreatePdfTable(PdfPTable overview_table_1, string labeltext, string valuetext) 
        {
            PdfPCell overview_row_NP_label = new PdfPCell(new Phrase(labeltext, new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4 };
            PdfPCell overview_row_NP_value = new PdfPCell(new Phrase(valuetext, new Font(Font.FontFamily.HELVETICA, MidText1, Font.NORMAL, nviDarkBlue))) { PaddingTop = 0, PaddingBottom = 4, BackgroundColor = nviLightBLue };
            overview_row_NP_value.HorizontalAlignment = Image.ALIGN_RIGHT;
            overview_table_1.AddCell(overview_row_NP_label);
            overview_table_1.AddCell(overview_row_NP_value);
            return overview_table_1;
        }

        public enum EquipmentCellInfo { Header, Subheader, SubSubHeader, Item }
        public enum CellInfo { Header, Letter, Subheader, Subletter, Empty, Row }

    }
}