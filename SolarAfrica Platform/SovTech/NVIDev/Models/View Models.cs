﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NVIDev.Models
{

    #region Equipment View Models
    public class InverterViewModel
    {
        public int projectID { get; set; }

        public Inverters Inverter { get; set; }

        public List<Filler> Configurations { get; set; }
        public List<Filler> DCOverVoltageList { get; set; }
        public List<Filler> DCLoadDisconnectList { get; set; }

        public List<Filler> IOPortModulesList { get; set; }
        public List<Filler> CountrySpecificGridList { get; set; }
        public InverterViewModel()
        {
            Inverter = new Inverters();
            Configurations = new List<Filler>();
            DCLoadDisconnectList = new List<Filler>();
            DCOverVoltageList = new List<Filler>();
            IOPortModulesList = new List<Filler>();
            CountrySpecificGridList = new List<Filler>();
        }


        public void Populate(Inverters invert)
        {
            Inverter = invert;
            List<string> _Configurations = string.IsNullOrEmpty(invert.ConfigurationList) ? new List<string>() : invert.ConfigurationList.Split(',').ToList();
            List<string> _DCLoadDisconnectList = string.IsNullOrEmpty(invert.DCLoadDisconnectList) ? new List<string>() : invert.DCLoadDisconnectList.Split(',').ToList();
            List<string> _DCOverVoltageList = string.IsNullOrEmpty(invert.DCOverVoltageList) ? new List<string>() : invert.DCOverVoltageList.Split(',').ToList();
            List<string> _IOPortModulesList = string.IsNullOrEmpty(invert.IOPortModulesList) ? new List<string>() : invert.IOPortModulesList.Split(',').ToList();
            List<string> _CountrySpecificGridList = string.IsNullOrEmpty(invert.CountrySpecificGridList) ? new List<string>() : invert.CountrySpecificGridList.Split(',').ToList();

            for (int i = 0; i < _Configurations.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _Configurations[i]
                };

                Configurations.Add(filler);
            }
            for (int i = 0; i < _DCOverVoltageList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _DCOverVoltageList[i]
                };

                DCOverVoltageList.Add(filler);
            }

            for (int i = 0; i < _DCLoadDisconnectList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _DCLoadDisconnectList[i]
                };

                DCLoadDisconnectList.Add(filler);
            }

            for (int i = 0; i < _IOPortModulesList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _IOPortModulesList[i]
                };

                IOPortModulesList.Add(filler);
            }
            for (int i = 0; i < _CountrySpecificGridList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _CountrySpecificGridList[i]
                };

                CountrySpecificGridList.Add(filler);
            }
        }

        public Inverters ReversePopulate()
        {


            var _Configurations = from a in Configurations select a.Value;
            var _DCLoadDisconnectList = from b in DCLoadDisconnectList select b.Value;
            var _DCOverVoltageList = from c in DCOverVoltageList select c.Value;
            var _IOPortModulesList = from d in IOPortModulesList select d.Value;
            var _CountrySpecificGridList = from e in CountrySpecificGridList select e.Value;

            Inverter.ConfigurationList = string.Join(",", _Configurations.ToList());
            Inverter.DCLoadDisconnectList = string.Join(",", _DCLoadDisconnectList.ToList());
            Inverter.DCOverVoltageList = string.Join(",", _DCOverVoltageList.ToList());
            Inverter.IOPortModulesList = string.Join(",", _IOPortModulesList.ToList());
            Inverter.CountrySpecificGridList = string.Join(",", _CountrySpecificGridList.ToList());

            return Inverter;
        }

    }
    public class BiInverterViewModel
    {
        public int projectID { get; set; }

        public BiInverters BiInverter { get; set; }


        public List<Filler> CommunicationList { get; set; }
        public List<Filler> InterfaceList { get; set; }
        public List<Filler> ApplicationList { get; set; }


        public BiInverterViewModel()
        {
            BiInverter = new BiInverters();
            CommunicationList = new List<Filler>();
            InterfaceList = new List<Filler>();
            ApplicationList = new List<Filler>();

        }


        public void Populate(BiInverters Biinverter)
        {
            BiInverter = Biinverter;
            List<string> _CommunicationList = string.IsNullOrEmpty(Biinverter.CommunicationList) ? new List<string>() : Biinverter.CommunicationList.Split(',').ToList();
            List<string> _InterfaceList = string.IsNullOrEmpty(Biinverter.InterfaceList) ? new List<string>() : Biinverter.InterfaceList.Split(',').ToList();
            List<string> _ApplicationList = string.IsNullOrEmpty(Biinverter.ApplicationList) ? new List<string>() : Biinverter.ApplicationList.Split(',').ToList();

            for (int i = 0; i < _CommunicationList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _CommunicationList[i]
                };

                CommunicationList.Add(filler);
            }
            for (int i = 0; i < _InterfaceList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _InterfaceList[i]
                };

                InterfaceList.Add(filler);
            }

            for (int i = 0; i < _ApplicationList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _ApplicationList[i]
                };

                ApplicationList.Add(filler);
            }


        }

        public BiInverters ReversePopulate()
        {


            var _CommunicationList = from a in CommunicationList select a.Value;
            var _InterfaceList = from b in InterfaceList select b.Value;
            var _ApplicationList = from c in ApplicationList select c.Value;


            BiInverter.CommunicationList = string.Join(",", _CommunicationList.ToList());
            BiInverter.InterfaceList = string.Join(",", _InterfaceList.ToList());
            BiInverter.ApplicationList = string.Join(",", _ApplicationList.ToList());


            return BiInverter;
        }

    }
    public class Filler
    {
        public string Value { get; set; }
        public string ParentList { get; set; }
    }
    public class EquipmentSelectionViewModel
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public double Yield { get; set; }

        public string Type { get; set; }

        public double CurrencyPerDollar { get; set; }
        public double MountingCostInDollars { get; set; }
        public double MountingContainers20ft { get; set; }
        public List<InverterSub> InvertList { get; set; }

        public List<BiInverterSub> BiInverterList { get; set; }

        public List<Basic> PVModuleList { get; set; }
        public List<Basic> MountingStructureList { get; set; }
        public List<Basic> BatteryList { get; set; }
        public List<Basic> BalanceSystemList { get; set; }

        public EquipmentSelectionViewModel()
        {
            InvertList = new List<InverterSub>();
            BiInverterList = new List<BiInverterSub>();
            PVModuleList = new List<Basic>();
            MountingStructureList = new List<Basic>();
            BatteryList = new List<Basic>();
            BalanceSystemList = new List<Basic>();
        }

        public bool isEquipmentDone { get; set; }

        public void Populate(Project project, ApplicationDbContext context)
        {
            Id = project.Id;
            Code = project.Code;
            isEquipmentDone = project.isEquipmentDone();
            this.Yield = project.Costing.Yield;
            this.Type = project.Costing.Type;
            this.MountingContainers20ft = project.MountingContainersUsed20ft;
            this.MountingCostInDollars = project.MountingInDollars;
            List<string> _InvertIDS = string.IsNullOrEmpty(project.InverterIDS) ? new List<string>() : project.InverterIDS.Split(',').ToList();
            List<string> _InvertAmounts = string.IsNullOrEmpty(project.InverterAmounts) ? new List<string>() : project.InverterAmounts.Split(',').ToList();

            List<string> _BiInverterIds = string.IsNullOrEmpty(project.BiInverterIDS) ? new List<string>() : project.BiInverterIDS.Split(',').ToList();
            List<string> _BiInverterAmounts = string.IsNullOrEmpty(project.BiInverterAmounts) ? new List<string>() : project.BiInverterAmounts.Split(',').ToList();

            List<string> _PVids = string.IsNullOrEmpty(project.PVModuleIDS) ? new List<string>() : project.PVModuleIDS.Split(',').ToList();
            List<string> _PVAmounts = string.IsNullOrEmpty(project.PvModuleAmounts) ? new List<string>() : project.PvModuleAmounts.Split(',').ToList();

            List<string> _MountsIds = string.IsNullOrEmpty(project.MountingIDS) ? new List<string>() : project.MountingIDS.Split(',').ToList();
            List<string> _MountsAmount = string.IsNullOrEmpty(project.MountingAmounts) ? new List<string>() : project.MountingAmounts.Split(',').ToList();

            List<string> _BattsIDS = string.IsNullOrEmpty(project.BatteryIDS) ? new List<string>() : project.BatteryIDS.Split(',').ToList();
            List<string> _BattsAmounts = string.IsNullOrEmpty(project.BatteryAmounts) ? new List<string>() : project.BatteryAmounts.Split(',').ToList();

            List<string> _BalanceSystemIDs = string.IsNullOrEmpty(project.BalanceSystemIDS) ? new List<string>() : project.BalanceSystemIDS.Split(',').ToList();
            List<string> _BalanceSystemAmounts = string.IsNullOrEmpty(project.BalanceSystemAmounts) ? new List<string>() : project.BalanceSystemAmounts.Split(',').ToList();



            #region Populate Current Inverters

            for (int a = 0; a < _InvertIDS.Count; a++)
            {


                try
                {
                    string[] itemid = _InvertIDS[a].Split('!');
                    string[] package = _InvertAmounts[a].Split('!');
                    var equip = context.Inverters.Find(itemid[0]);

                    InverterSub isub = new InverterSub()
                    {
                        Id = itemid[0],
                        Configurations = itemid[1],
                        DCOverVoltage = itemid[2],
                        DCLoadDisconnect = itemid[3],
                        IOPortModules = itemid[4],
                        CountrySpecificGrid = itemid[5],
                        Amount = int.Parse(package[0]),
                        ParentList = "InvertList",
                        Key = equip.Key,
                        Watt = int.Parse(package[0]) * equip.UnitWP,
                        totalCost = int.Parse(package[0]) * equip.ItemCost * this.CurrencyPerDollar
                    };

                    InvertList.Add(isub);
                }

                catch (Exception)
                {
                    //InverterSub isub = new InverterSub();
                    //isub.ParentList = "InvertList";
                    //InvertList.Add(isub);
                }

            }
            #endregion
            #region Populate current BiDiverters
            for (int a = 0; a < _BiInverterIds.Count; a++)
            {

                try
                {
                    string[] itemid = _BiInverterIds[a].Split('!');
                    string[] package = _BiInverterAmounts[a].Split('!');
                    var equip = context.BiInverters.Find(itemid[0]);


                    BiInverterSub BIsub = new BiInverterSub()
                    {
                        Id = itemid[0],
                        Communication = itemid[1],
                        Application = itemid[2],
                        Interface = itemid[3],

                        Amount = int.Parse(package[0]),
                        ParentList = "BiInverterList",
                        Key = equip.Key,
                        Watt = int.Parse(package[0]) * equip.UnitWP,
                        totalCost = int.Parse(package[0]) * equip.ItemCost * this.CurrencyPerDollar
                    };


                    BiInverterList.Add(BIsub);
                }

                catch (Exception)
                {
                    //BiInverterSub BIsub = new BiInverterSub();
                    //BIsub.ParentList = "BiInverterList";
                    //BiInverterList.Add(BIsub);
                }

            }


            #endregion
            #region Populate Current PVModules
            for (int a = 0; a < _PVids.Count; a++)
            {

                try
                {
                    string itemid = _PVids[a];
                    string[] package = _PVAmounts[a].Split('!');
                    var equip = context.PVModules.Find(itemid);

                    Basic PvMod = new Basic()
                    {
                        Id = itemid,
                        ParentList = "PVModuleList",
                        Amount = int.Parse(package[0]),
                        Key = equip.Key,
                        Watt = int.Parse(package[0]) * equip.UnitWP,
                        totalCost = int.Parse(package[0]) * equip.ItemCost * this.CurrencyPerDollar

                    };

                    PVModuleList.Add(PvMod);
                }

                catch (Exception)
                {
                    //Basic PvMod = new Basic();
                    //PvMod.ParentList = "PVModuleList";
                    //PVModuleList.Add(PvMod);
                }

            }
            #endregion
            #region Populate Current Mounting Structures
            for (int a = 0; a < _MountsIds.Count; a++)
            {

                try
                {
                    string itemid = _MountsIds[a];
                    string amount = _MountsAmount[a];
                    var equip = context.MountingStructures.Find(itemid);

                    Basic Mount = new Basic()
                    {
                        Id = itemid,
                        ParentList = "MountingStructureList",

                        Amount = int.Parse(amount),
                        Key = equip.Key,
                        totalCost = int.Parse(amount) * equip.ItemCost * this.CurrencyPerDollar
                    };

                    MountingStructureList.Add(Mount);
                }

                catch (Exception)
                {
                    //    Basic Mount = new Basic();
                    //    Mount.ParentList = "MountingStructureList";
                    //    MountingStructureList.Add(Mount);
                    //
                }

            }
            #endregion
            #region Populate Current Batteries
            for (int a = 0; a < _BattsIDS.Count; a++)
            {

                try
                {
                    string itemid = _BattsIDS[a];
                    string[] package = _BattsAmounts[a].Split('!');
                    var equip = context.Batteries.Find(itemid);

                    Basic Battery = new Basic()
                    {
                        Id = itemid,
                        ParentList = "BatteryList",
                        Amount = int.Parse(package[0]),
                        Key = equip.Key,
                        Watt = int.Parse(package[0]) * equip.UnitAh_C10,
                        totalCost = int.Parse(package[0]) * equip.ItemCost * this.CurrencyPerDollar
                    };

                    BatteryList.Add(Battery);
                }

                catch (Exception)
                {
                    //Basic Battery = new Basic();
                    //Battery.ParentList = "BatteryList";
                    //BatteryList.Add(Battery);
                }

            }
            #endregion
            #region Populate Current BalanceSystems
            for (int a = 0; a < _BalanceSystemIDs.Count; a++)
            {

                try
                {
                    string[] itemid = _BalanceSystemIDs[a].Split('!');
                    string od = itemid[0];
                    string pare = itemid[1];
                    string[] package = _BalanceSystemAmounts[a].Split('!');
                    var equip = context.BalanceSystems.Find(itemid[0]);

                    Basic BalanceSystem = new Basic()
                    {
                        Id = od,
                        ParentList = pare,
                        Amount = int.Parse(package[0]),
                        Key = equip.Key,

                        totalCost = int.Parse(package[0]) * equip.ItemCost * this.CurrencyPerDollar
                    };

                    BalanceSystemList.Add(BalanceSystem);
                }

                catch (Exception)
                {
                    //Basic BalanceSystem = new Basic();
                    //BalanceSystem.ParentList = "BalanceSystemList";
                    //BalanceSystemList.Add(BalanceSystem);
                }

            }
            #endregion

        }


    }
    public class InverterSub : Basic
    {

        public string Configurations { get; set; }
        public string DCOverVoltage { get; set; }
        public string DCLoadDisconnect { get; set; }

        public string IOPortModules { get; set; }
        public string CountrySpecificGrid { get; set; }


    }
    public class BiInverterSub : Basic
    {

        public string Communication { get; set; }
        public string Interface { get; set; }
        public string Application { get; set; }



    }
    public class Basic
    {
        public string Id { get; set; }
        public string ParentList { get; set; }

        public string Name { get; set; }
        public string Key { get; set; }
        public double Watt { get; set; }
        public string Packaging { get; set; }

        [DefaultValue(1)]

        public int Amount { get; set; }

        public double totalCost { get; set; }

        public string UOM { get; set; }
        public string Description { get; set; }
    }

    #endregion

    public class FeeViewModel
    {

        [Key]
        public int Id { get; set; }
        public Project project { get; set; }

        public SuperProject superPro { get; set; }

        public Fees fees { get; set; }


        public double OnGoingFee_Admin { get; set; }
        public double Upfront { get; set; }

        /// formulae generated fee stuff




        // Preliminaries & General

        public double Prelim_Gen_FeeBase { get; set; }
        public double Prelim_Gen_CostDrive { get; set; }
        public double Prelim_Gen_EstCCY { get; set; }

        public double Prelim_Gen_TotalC { get; set; }


        //Procurement


        public double Procurement_FeeBase { get; set; }
        public double Procurement_CostDrive { get; set; }
        public double Procurement_EstCCY { get; set; }
        public double Procurement_TotalC { get; set; }

        //Working Capital


        public double Work_Cap_FeeBase { get; set; }
        public double Work_Cap_CostDrive { get; set; }
        public double Work_Cap_EstCCY { get; set; }
        public double Work_Cap_TotalC { get; set; }



        //Prepare Vetting Docs

        public double Prep_Vet_FeeBase { get; set; }
        public double Prep_Vet_CostDrive { get; set; }
        public double Prep_Vet_EstCCY { get; set; }
        public double Prep_Vet_TotalC { get; set; }


        //Technical Vetting UNIT

        public double Tech_Vet_FeeBase { get; set; }
        public double Tech_Vet_CostDrive { get; set; }
        public double Tech_Vet_EstCCY { get; set; }
        public double Tech_Vet_TotalC { get; set; }



        //Performance Gaurantee UNIT

        public double Performance_GaranT_FeeBase { get; set; }
        public double Performance_GaranT_CostDrive { get; set; }
        public double Performance_GaranT_EstCCY { get; set; }
        public double Performance_GaranT_TotalC { get; set; }
        //Project Management


        public double Project_Manage_FeeBase { get; set; }
        public double Project_Manage_CostDrive { get; set; }
        public double Project_Manage_EstCCY { get; set; }
        public double Project_Manage_TotalC { get; set; }



        //Commissioning


        public double Commissioning_FeeBase { get; set; }
        public double Commissioning_CostDrive { get; set; }
        public double Commissioning_EstCCY { get; set; }
        public double Commissioning_TotalC { get; set; }



        //Margin / Contingencies


        //
        public double Margin_Contin_CostDrive { get; set; }
        //
        public double Margin_Contin_TotalC { get; set; }

        public double Margin_Contin_realC { get; set; }







        #region Ongoing Costs
        //ALL IN DOLLARS     PPA PBS SDS 
        //        Monitoring Software

        public double Monitor_Software_FeeBase { get; set; }
        public double Monitor_Software_CostDrive { get; set; }
        public double Monitor_Software_EstCCY { get; set; }
        public double Monitor_Software_TotalC { get; set; }



        //        Administration

        public double Admin_FeeBase { get; set; }
        public double Admin_CostDrive { get; set; }
        public double Admin_EstCCY { get; set; }
        public double Admin_TotalC { get; set; }






        //        Asset management


        public double Asset_Manage_FeeBase { get; set; }
        public double Asset_Manage_CostDrive { get; set; }
        public double Asset_Manage_EstCCY { get; set; }
        public double Asset_Manage_TotalC { get; set; }




        //Duration

        public double Duration_FEECOSTB { get; set; }

        public double Duration_Est { get; set; }
        #endregion






        public FeeViewModel()
        {
            project = this.project;
            fees = this.fees;
        }
        public FeeViewModel(Project pro, Fees _fee)
        {
            var db = new ApplicationDbContext();
            project = db.Projects.Find(pro.Id);


            project.UseAdmin = pro.UseAdmin;
            project.UseAssetManagement = pro.UseAssetManagement;
            project.UseCommisioning = pro.UseCommisioning;
            project.UseMargin = pro.UseMargin;
            project.UseMonitorSoftware = pro.UseMonitorSoftware;
            project.UsePerformanceG = pro.UsePerformanceG;
            project.UsePrelim = pro.UsePrelim;
            project.UsePrepVet = pro.UsePrepVet;
            project.UseProcurement = pro.UseProcurement;
            project.UseProjectManage = pro.UseProjectManage;
            project.UseTechVet = pro.UseTechVet;
            project.UseWorkingCap = pro.UseWorkingCap;


            //Populate for first run
            if (!project.UseAdmin && !project.UseAssetManagement && !project.UseCommisioning && !project.UseMargin && !project.UseMonitorSoftware && !project.UsePerformanceG && !project.UsePrelim && !project.UsePrepVet && !project.UseProcurement && !project.UseProjectManage && !project.UseTechVet && !project.UseWorkingCap)
            {
                project.UseAdmin = true;
                project.UseAssetManagement = true;
                project.UseCommisioning = true;
                project.UseMargin = true;
                project.UseMonitorSoftware = true;
                project.UsePerformanceG = true;
                project.UsePrelim = true;
                project.UsePrepVet = true;
                project.UseProcurement = true;
                project.UseProjectManage = true;
                project.UseTechVet = true;
                project.UseWorkingCap = true;
                if (project.Costing.UseEPC)
                {
                    project.UseProcurement = false;
                    project.UseProjectManage = false;
                    project.UsePerformanceG = false;
                }
            }


            superPro = new SuperProject(project);
            fees = _fee;



        }

        public void Populate()  //Feebase then Cost driver % then est   then Total = Max(est * costdriver%
        {
            Prelim_Gen_FeeBase = (double)(superPro.A + superPro.BA); //CCY
            Procurement_FeeBase = (double)(superPro.A + superPro.BA); //CCY
            Prep_Vet_FeeBase = (double)(fees.Prep_Vet_FeeBase * (double)superPro.Exch); //CCY
            Tech_Vet_FeeBase = (double)(fees.Prep_Vet_FeeBase * (double)superPro.Exch); //CCY
            Project_Manage_FeeBase = (double)((superPro.A + superPro.BA + project.Costing.InstallFee + project.Costing.DesignFee)); //CCY
            Commissioning_FeeBase = (double)((superPro.A + superPro.BA + project.Costing.InstallFee + project.Costing.DesignFee)); //CCY
            Performance_GaranT_FeeBase = (double)((superPro.A + superPro.BA + project.Costing.InstallFee + project.Costing.DesignFee)); //CCY

            switch (project.Costing.Plan)
            {
                case "Pay-By-Solar":




                    Monitor_Software_FeeBase = (double)fees.Monitor_Software_PBS;
                    Admin_FeeBase = (double)fees.Admin_PBS;
                    Asset_Manage_FeeBase = (double)fees.Asset_Manage_PBS;
                    Work_Cap_FeeBase = (double)(superPro.A + superPro.BA + project.Costing.InstallFee + project.Costing.DesignFee - (superPro.Up * 1000)) / 2; //CCY

                    break;





                case "Power Purchase Agreement":



                    Monitor_Software_FeeBase = (double)fees.Monitor_Software_PPA;
                    Admin_FeeBase = (double)fees.Admin_PPA;
                    Asset_Manage_FeeBase = (double)fees.Asset_Manage_PPA;
                    Work_Cap_FeeBase = (double)(superPro.A + superPro.BA + project.Costing.InstallFee + project.Costing.DesignFee) / 2; //CCY



                    break;







                case "Solar Development Services":

                    Monitor_Software_FeeBase = (double)fees.Monitor_Software_SDS;
                    Admin_FeeBase = (double)fees.Admin_SDS;
                    Asset_Manage_FeeBase = (double)fees.Asset_Manage_SDS;
                    Work_Cap_FeeBase = (double)(superPro.A + superPro.BA + project.Costing.InstallFee + project.Costing.DesignFee) / 2; //CCY



                    break;
            }


            float size = (float)superPro.prosize;


            if (size <= 0.2 || size < 0.201)
            {
                Prelim_Gen_CostDrive = fees.Prelim_Gen_02;
                Procurement_CostDrive = fees.Procurement_02;
                Work_Cap_CostDrive = fees.Work_Cap_02;
                Prep_Vet_CostDrive = fees.Prep_Vet_02;
                Tech_Vet_CostDrive = fees.Tech_Vet_02;
                Performance_GaranT_CostDrive = fees.Performance_GaranT_02;
                Project_Manage_CostDrive = fees.Project_Manage_02;
                Commissioning_CostDrive = fees.Commissioning_02;
                Margin_Contin_CostDrive = fees.Margin_Contin_02;
                Monitor_Software_CostDrive = fees.Monitor_Software_02;
                Admin_CostDrive = fees.Admin_02;
                Asset_Manage_CostDrive = fees.Asset_Manage_02;
                Duration_FEECOSTB = fees.Duration_02;

            }

            else if (size <= 0.5 && size > 0.201)
            {
                Prelim_Gen_CostDrive = fees.Prelim_Gen_05;
                Procurement_CostDrive = fees.Procurement_05;
                Work_Cap_CostDrive = fees.Work_Cap_05;
                Prep_Vet_CostDrive = fees.Prep_Vet_05;
                Tech_Vet_CostDrive = fees.Tech_Vet_05;
                Performance_GaranT_CostDrive = fees.Performance_GaranT_05;
                Project_Manage_CostDrive = fees.Project_Manage_05;
                Commissioning_CostDrive = fees.Commissioning_05;
                Margin_Contin_CostDrive = fees.Margin_Contin_05;
                Monitor_Software_CostDrive = fees.Monitor_Software_05;
                Admin_CostDrive = fees.Admin_05;
                Asset_Manage_CostDrive = fees.Asset_Manage_05;
                Duration_FEECOSTB = fees.Duration_05;
            }

            else if (size <= 1 && size > 0.5)
            {
                Prelim_Gen_CostDrive = fees.Prelim_Gen_1;
                Procurement_CostDrive = fees.Procurement_1;
                Work_Cap_CostDrive = fees.Work_Cap_1;
                Prep_Vet_CostDrive = fees.Prep_Vet_1;
                Tech_Vet_CostDrive = fees.Tech_Vet_1;
                Performance_GaranT_CostDrive = fees.Performance_GaranT_1;
                Project_Manage_CostDrive = fees.Project_Manage_1;
                Commissioning_CostDrive = fees.Commissioning_1;
                Margin_Contin_CostDrive = fees.Margin_Contin_1;
                Monitor_Software_CostDrive = fees.Monitor_Software_1;
                Admin_CostDrive = fees.Admin_1;
                Asset_Manage_CostDrive = fees.Asset_Manage_1;
                Duration_FEECOSTB = fees.Duration_1;

            }

            else if (size <= 5 && size > 1)
            {
                Prelim_Gen_CostDrive = fees.Prelim_Gen_5;
                Procurement_CostDrive = fees.Procurement_5;
                Work_Cap_CostDrive = fees.Work_Cap_5;
                Prep_Vet_CostDrive = fees.Prep_Vet_5;
                Tech_Vet_CostDrive = fees.Tech_Vet_5;
                Performance_GaranT_CostDrive = fees.Performance_GaranT_5;
                Project_Manage_CostDrive = fees.Project_Manage_5;
                Commissioning_CostDrive = fees.Commissioning_5;
                Margin_Contin_CostDrive = fees.Margin_Contin_5;
                Monitor_Software_CostDrive = fees.Monitor_Software_5;
                Admin_CostDrive = fees.Admin_5;
                Asset_Manage_CostDrive = fees.Asset_Manage_5;
                Duration_FEECOSTB = fees.Duration_5;
            }


            else if (size > 5)
            {
                Prelim_Gen_CostDrive = fees.Prelim_Gen_51;
                Procurement_CostDrive = fees.Procurement_51;
                Work_Cap_CostDrive = fees.Work_Cap_51;
                Prep_Vet_CostDrive = fees.Prep_Vet_51;
                Tech_Vet_CostDrive = fees.Tech_Vet_51;
                Performance_GaranT_CostDrive = fees.Performance_GaranT_51;
                Project_Manage_CostDrive = fees.Project_Manage_51;
                Commissioning_CostDrive = fees.Commissioning_51;
                Margin_Contin_CostDrive = fees.Margin_Contin_51;
                Monitor_Software_CostDrive = fees.Monitor_Software_51;
                Admin_CostDrive = fees.Admin_51;
                Asset_Manage_CostDrive = fees.Asset_Manage_51;
                Duration_FEECOSTB = fees.Duration_51;
            }


            //work out est Costs


            Prelim_Gen_EstCCY = Prelim_Gen_FeeBase * (Prelim_Gen_CostDrive / 100);
            Procurement_EstCCY = Procurement_FeeBase * (Procurement_CostDrive / 100);
            Work_Cap_EstCCY = Work_Cap_FeeBase * (Work_Cap_CostDrive / 100) * (Duration_FEECOSTB / 12);
            Prep_Vet_EstCCY = Prep_Vet_FeeBase * Prep_Vet_CostDrive;
            Tech_Vet_EstCCY = Tech_Vet_FeeBase * Tech_Vet_CostDrive;
            Performance_GaranT_EstCCY = Performance_GaranT_FeeBase * (Performance_GaranT_CostDrive / 100) * (Duration_FEECOSTB / 12);
            Project_Manage_EstCCY = Project_Manage_FeeBase * (Project_Manage_CostDrive / 100);
            Commissioning_EstCCY = Commissioning_FeeBase * (Commissioning_CostDrive / 100);
            //margin NONE

            //Ongoing Costs


            Monitor_Software_EstCCY = Monitor_Software_FeeBase * Monitor_Software_CostDrive * (double)superPro.Exch;
            Admin_EstCCY = Admin_FeeBase * Admin_CostDrive * (double)superPro.Exch;
            Asset_Manage_EstCCY = Asset_Manage_FeeBase * Asset_Manage_CostDrive * (double)superPro.Exch;

            Duration_Est = Duration_FEECOSTB * Duration_FEECOSTB;





            //Work Out total
            if (project.Costing.Type == "Diesel-Hybrid")
            {
                Prelim_Gen_TotalC = Math.Max(Prelim_Gen_EstCCY, fees.Prelim_Gen_Min * superPro.Exch) * fees.Prelim_Gen_DHybrid;
                Procurement_TotalC = Math.Max(Procurement_EstCCY, fees.Procurement_Min * superPro.Exch) * fees.Procurement_DHybrid;
                Work_Cap_TotalC = Work_Cap_EstCCY * fees.Work_Cap_DHybrid;
                Prep_Vet_TotalC = Prep_Vet_EstCCY * fees.Prep_Vet_DHybrid;
                Tech_Vet_TotalC = Tech_Vet_EstCCY * fees.Tech_Vet_DHybrid;
                Performance_GaranT_TotalC = Performance_GaranT_EstCCY * fees.Performance_GaranT_DHybrid;
                Project_Manage_TotalC = Math.Max(Project_Manage_EstCCY, fees.Project_Manage_Min * superPro.Exch) * fees.Project_Manage_DHybrid;
                Commissioning_TotalC = Math.Max(Commissioning_EstCCY, fees.Commissioning_Min * superPro.Exch) * fees.Commissioning_DHybrid;
                //margin NONE

                //Ongoing Costs


                Monitor_Software_TotalC = Math.Max(Monitor_Software_EstCCY, fees.Monitor_Software_Min * superPro.Exch) * fees.Monitor_Software_DHybrid;
                Admin_TotalC = Admin_EstCCY * Admin_CostDrive / 100 * fees.Admin_DHybrid;
                Asset_Manage_TotalC = Asset_Manage_EstCCY * fees.Asset_Manage_DHybrid;

            }


            else
            {

                Prelim_Gen_TotalC = Math.Max(Prelim_Gen_EstCCY, fees.Prelim_Gen_Min * superPro.Exch);
                Procurement_TotalC = Math.Max(Procurement_EstCCY, fees.Procurement_Min * superPro.Exch);
                Work_Cap_TotalC = Work_Cap_EstCCY;
                Prep_Vet_TotalC = Prep_Vet_EstCCY;
                Tech_Vet_TotalC = Tech_Vet_EstCCY;
                Performance_GaranT_TotalC = Performance_GaranT_EstCCY;
                Project_Manage_TotalC = Math.Max(Project_Manage_EstCCY, fees.Project_Manage_Min * superPro.Exch);
                Commissioning_TotalC = Math.Max(Commissioning_EstCCY, fees.Commissioning_Min * superPro.Exch);
                //margin NONE

                //Ongoing Costs


                Monitor_Software_TotalC = Math.Max(Monitor_Software_EstCCY, fees.Monitor_Software_Min * superPro.Exch);
                Admin_TotalC = Admin_EstCCY;
                Asset_Manage_TotalC = Asset_Manage_EstCCY;

            }
            Margin_Contin_realC = Margin_Contin_CostDrive / 100 * Upfront;

            Margin_Contin_TotalC = (Prelim_Gen_TotalC + Procurement_TotalC + Work_Cap_TotalC + Prep_Vet_TotalC + Tech_Vet_TotalC + Performance_GaranT_TotalC + Project_Manage_TotalC + Commissioning_TotalC) * Margin_Contin_CostDrive / 100;


        }


        double calUpfront()
        {
            double rVal = 0;


            rVal += project.UsePrelim ? Prelim_Gen_TotalC : 0;
            rVal += project.UseProcurement ? Procurement_TotalC : 0;
            rVal += project.UseWorkingCap ? Work_Cap_TotalC : 0;
            rVal += project.UsePrepVet ? Prep_Vet_TotalC : 0;
            rVal += project.UseTechVet ? Tech_Vet_TotalC : 0;
            rVal += project.UsePerformanceG ? Performance_GaranT_TotalC : 0;
            rVal += project.UseProjectManage ? Project_Manage_TotalC : 0;
            rVal += project.UseCommisioning ? Commissioning_TotalC : 0;
            rVal += Margin_Contin_TotalC;


            return rVal;
        }

        double calOngoing()
        {
            double rVal = 0;
            rVal += project.UseMonitorSoftware ? Monitor_Software_TotalC : 0;
            rVal += project.UseAdmin ? Admin_TotalC : 0;
            rVal += project.UseAssetManagement ? Asset_Manage_TotalC : 0;
            return rVal;
        }


        public void Submission()
        {
            Upfront = (double)calUpfront();
            Upfront = double.IsNaN(Upfront) ? 0.0 : Upfront;

            OnGoingFee_Admin = (double)calOngoing();
            OnGoingFee_Admin = double.IsNaN(OnGoingFee_Admin) ? 0.0 : OnGoingFee_Admin;
        }


    }


    #region Role View Models
    public class RoleViewModel
    {

        public List<RoleModel> Roles { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public RoleViewModel()
        {
            Roles = new List<RoleModel>();

        }

        public RoleViewModel(List<string> userRoles, string[] allRoles)
        {

            Roles = new List<RoleModel>();
            RoleModel roleToAdd = new RoleModel();
            foreach (var role in allRoles)
            {
                if (userRoles.Contains(role))
                {
                    roleToAdd = new RoleModel() { Role = role, includeMe = true };
                    Roles.Add(roleToAdd);
                }

                else
                {
                    roleToAdd = new RoleModel() { Role = role, includeMe = false };
                    Roles.Add(roleToAdd);
                }
            }
        }
    }

    public class RoleModel
    {
        public string Role { get; set; }
        public bool includeMe { get; set; }
        public RoleModel()
        {

        }
    }
    #endregion

    #region Other Helper View Models
    public class TransferViewModel
    {
        public string CurrentUserID { get; set; }
        public string NewUserName { get; set; }

        public int ProjectID { get; set; }
        public string ProjectName { get; set; }

        public List<string> AllCompanyUsers { get; set; }
        public TransferViewModel()
        {
            AllCompanyUsers = new List<string>();
        }


    }


    public class CompanyProjView
    {
        public int id { get; set; }
        public string Client { get; set; }
        public string Type { get; set; }
        public string CurrentUser { get; set; }

        public string Country { get; set; }
        public string Status { get; set; }
    }

    public class CompanyUserView
    {
        public string id { get; set; }

        public string Username { get; set; }
        public string FullName { get; set; }

        public string Roles { get; set; }
    }
    #endregion


    public class QuotingViewModel
    {
        public int id { get; set; }

        public string Code { get; set; }
        public List<QuotingCost> Costs { get; set; }

        public double Duties { get; set; }
        public double TotalSolarSystemPrice { get; set; }
        public QuotingViewModel()
        {
            Costs = new List<QuotingCost>();

        }

        public bool isQuotingDone { get; set; }

        public double InstallationCosts { get; set; }
        public double DesignCosts { get; set; }

        public double TransportCosts { get; set; }
        public double OMWorks { get; set; }

        public void Populate(SuperProject super)
        {

            InstallationCosts = super.Project.Costing.InstallFee;
            DesignCosts = super.Project.Costing.DesignFee;
            OMWorks = super.Project.Costing.OMWorks;
            TransportCosts = super.TotalTrans;

            string[] _QuotingCostNames = super.Project.QuotingCostNames.Split(',');
            string[] _QuotingCostMains = super.Project.QuotingCostMains.Split(',');
            string[] _QuotingCostSuppliers = super.Project.QuotingCostSuppliers.Split(',');
            string[] _QuotingCostCosts = super.Project.QuotingCostCosts.Split(',');
            string[] _QuotingCostMarkups = super.Project.QuotingCostMarkups.Split(',');
            string[] _QuotingCostSellPrices = super.Project.QuotingCostSellPrices.Split(',');
            string[] _QuotingCostWPMarks = super.Project.QuotingCostWPMarks.Split(',');
            string[] _QuotingCostFixeds = super.Project.QuotingCostFixeds.Split(',');
            id = super.Project.Id;
            isQuotingDone = super.Project.isQuotingDone();
            TotalSolarSystemPrice = super.CalculateBA() + super.CalculateBA();
            Duties = super.TotalDutiesA + super.TotalDutiesBa;
            Code = super.Project.Code;
            Costs = new List<QuotingCost>();
            CurrentWP = TotalSolarSystemPrice/(super.prosize * 1000000.00);
            TargetWP = TotalSolarSystemPrice / (super.prosize *1000000.00);
            MW = super.prosize;
            for (int a = 0; a < _QuotingCostNames.Count(); a++)
            {
                QuotingCost item = new QuotingCost();
                item.Name = _QuotingCostNames[a];
                item.Main = bool.Parse(_QuotingCostMains[a]);
                item.Supplier = _QuotingCostSuppliers[a];
                item.Cost = double.Parse(_QuotingCostCosts[a]);
                item.Markup = double.Parse(_QuotingCostMarkups[a]);
                item.SellPrice = double.Parse(_QuotingCostSellPrices[a]);
                item.WPMark = _QuotingCostWPMarks[a];
                item.Fixed = bool.Parse(_QuotingCostFixeds[a]);


                Costs.Add(item);
            }

            


        }

        public double CurrentTotalSystemCosts()
        {
            double total = Costs.Select(m => m.SellPrice).Sum();
            TotalSolarSystemPrice = total;
            return total;
        }

        public double MW { get; set; }

        public double CurrentWP { get; set; }
        public double TargetWP { get; set; }

        public double DifferenceFactor { get; set; }
      

    }
    public class QuotingCost
    {

        public string Name { get; set; }
        public bool Main { get; set; }

        public string Supplier { get; set; }
        public double Cost { get; set; }
        public double Markup { get; set; }
        public double SellPrice { get; set; }
        public string WPMark { get; set; }

        public bool Fixed { get; set; }

        public bool Assigned { get; set; }

        public bool Removeable { get; set; }
        public string UserID { get; set; }

        public QuotingCost()
        {
            WPMark = "Markup";
            Assigned = false;
            Fixed = false;
            SellPrice = 0.00;
            Cost = 0.00;
            Markup = 0.00; //%
            Supplier = "NA";
            Main = false;
            Removeable = false;
        }


        public void Populate(CostItem item)
        {
            WPMark = item.WPMark;
            Assigned = true;
            Cost = item.Cost;
            Markup = item.Markup;
            SellPrice = Cost * (1 + Markup / 100); //markup %
            Main = false;
            Removeable = true;
            Supplier = "NA";
            UserID = item.UserID;
            Name = item.Name;

        }

        public void CalculateSellPrice()
        {
            SellPrice = Cost * (1 + Markup / 100); //markup %
        }

        public string FormattedSellprice
        {
            get
            {
                return this.SellPrice.ToString("#,##0.00");
            }
        }
    }

    
}