﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NVIDev.Models
{
    public class Audit
    {
        [Key]
        public long Id {get;set;}
        public string UserID { get; set; }
        public string Company { get; set; }

        public string Content { get; set; }
        public DateTime Time { get; set; }
        public Audit ()
        {
            Time = DateTime.Now;
        }


        public Audit(string userID, string company, string content)
        {
            UserID = userID;
            Content = content;
            Time = DateTime.Now;
            Company = company;
        }

        public Audit(Audit other)
        {
            Id = other.Id;
            UserID = other.UserID;
            Content = other.Content;
            Time = other.Time;
            Company = other.Company;
        }

       
    }
}