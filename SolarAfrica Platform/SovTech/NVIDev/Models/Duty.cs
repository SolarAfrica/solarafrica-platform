﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace NVIDev.Models
{
    public class Duty
    {
        
        public int id { get; set; } //number id
        [Required]
        public string Name { get; set; }
        public string Countries { get; set; } //this will store a deliminated list of country names seperated by !
        public bool AllCountries { get; set; } //if this tax applies to all countries
        [Required]

        public double Rate { get; set; } //the rate of this duty/tax
        public string VATRates { get; set; } //this will be pulled from the selected country on runtime... seperated by !
        public string CustomsValue { get; set; } //This is the specified custom value : FOB, CIF or CIF + Vat (some cases)

        //FOB is the standard price of the equipment
        //CIF = FOB + Insurance(Marine) + Transport
        //Marine Insurance is null if project of country and country of origin are the same
     
    }

    public class DutyViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<DutyItem> UseCountries { get; set; }
        public List<DutyItem> PossibleCountries { get; set; }

        public List<DutyItem> UseEquipment { get; set; }
        public List<DutyItem> PossibleEquipment { get; set; }
        public bool AllCountries { get; set; }
        public bool AllEquipment { get; set; }

        [Required]
        public double Rate { get; set; }
        public string CustomsValue { get; set; }

        public DutyViewModel()
        {
            UseCountries = new List<DutyItem>();
            PossibleCountries = new List<DutyItem>();
            UseEquipment = new List<DutyItem>();
            PossibleEquipment = new List<DutyItem>();
        }


        public void Populate(Duty model, ApplicationDbContext db)
        {
            List<string> myCountries = model.Countries.Split('!').ToList();
            var myEquipment = db.Catergories.Where(m => m.Taxes.Contains(model.Name));
            foreach(var country in myCountries)
            {
                DutyItem count = new DutyItem()
                {
                    Name = country,
                    UseMe = true
                };

                UseCountries.Add(count);

            }
            foreach(var item in myEquipment)
            {
                DutyItem category = new DutyItem()
                {
                    Name = item.Name,
                    UseMe = true
                };
                UseEquipment.Add(category);
            }

            AllCountries = model.AllCountries;
            Name = model.Name;
            Rate = model.Rate;
            Id = model.id;
            CustomsValue = model.CustomsValue;
        }
    }

    public class DutyItem
    {
        public string Name { get; set; }
        public bool UseMe { get; set; }
        public string Extra { get; set; }
    }


    public class Category
    {
        //each equipment item falls under a main equipment type, then into a category
        public int id { get; set; }
        [Required]
        public string Name { get; set; } //the name of the category e.g thin light panels
        [Required]
        public string Type { get; set; } //the major equipment type e.g pv panels
        public string Taxes { get; set; } //this will store a deliminated list of taxes applied to this category seperated by !
    }

    public class CategoryViewModel
    {
        public int id { get; set; }
        [Required]
        public string Name { get; set; } //the name of the category e.g thin light panels
         [Required]

        public string Type { get; set; } //the major equipment type e.g pv panels

        public List<DutyItem> UseTaxes { get; set; } //this will store a deliminated list of taxes applied to this category seperated by !
        public List<DutyItem> PossibleTaxes { get; set; } //All possible taxes

        public CategoryViewModel()
        {
            UseTaxes = new List<DutyItem>();
            PossibleTaxes = new List<DutyItem>();
        }
    }


    public class Order
    {
        public List<Container> Containers { get; set; }
        public Order()
        {
            Containers = new List<Container>();
        }
    }


    public enum EquipmentType { PVModule,Mounting,Inverter,Bi_Inverter,BOS,Battery}
    public class EquipmentItem
    {
        public string Name { get; set; }
        public string Extra { get; set; }
        public string CountryOfOrigin { get; set; }
        public int Amount { get; set; }

        public EquipmentType Type { get; set; }
        public double ItemCost { get; set; }

        public double RealCharge { get; set; }
        public int PalletPer20ft { get; set; }

        public double palletSpaceUsed { get; set; }
        public int ModulesPerPallet { get; set; }
        public double ContainerPercentage { get; set; }

        public double NeededPallets { get; set; }
      public  void CalculateSpaceUsed()
        {
            //container is 1
            palletSpaceUsed = 1.00 / ModulesPerPallet; // 5 per pallet, each takes 0.2 of a pallet
            NeededPallets = (Amount * 1.00 / ModulesPerPallet * 1.00); //get these values to double for accuracy
          

        }

        public void Populate(EquipmentItem item)
      {
              Name = item.Name;
          Extra = item.Extra;
        CountryOfOrigin = item.CountryOfOrigin;
      Amount = item.Amount;

      ItemCost = item.ItemCost;

      RealCharge = item.RealCharge;
       PalletPer20ft = item.PalletPer20ft;
        ModulesPerPallet = item.ModulesPerPallet;
       ContainerPercentage = item.ContainerPercentage;
      }
    }

    //20 ft and 40 ft containers
    //Each container gets filled by equipment that takes up a certain 

  public  enum ContainerType { ft40, ft20 }
    public class Container : IDisposable
    {

        public ContainerType Size { get; set; }
        public string CountryOfOrigin { get; set; }
        public List<Pallet> Pallets { get; set; }
        public double PercentageFilled { get; set; } //1 being a full container , need acceptable limits
        public bool MaxedOut { get; set; }

        public double FreighCost { get; set; }
        public Container ()
        {
            PercentageFilled = 0;
            MaxedOut = false;
            Pallets = new List<Pallet>();
        }

        public void CheckCapacity()
        {

            if(PercentageFilled < 1 && PercentageFilled >= 0)
            {
                MaxedOut = false;
            }
            else
            {
                MaxedOut = true;
            }
        }

        public void Dispose()
        {
            Pallets.Clear();
            Pallets = null;
           
        }

       

        
    }

    public class Pallet
    {
        public   List<EquipmentItem> Equipment { get; set; }

        public double PercentageFilled { get; set; }

        public string Name { get; set; }

        public int PalletPer20ft { get; set; }

        public double ContainerSpaceUsed { get; set; }
        public string CountryOfOrigin { get; set; }
        public bool MaxedOut { get; set; }
        public Pallet()
        {
            Equipment = new List<EquipmentItem>();
        }

        public void CheckCapacity()
        {
            if (PercentageFilled < 1 && PercentageFilled >= 0)
            {
                MaxedOut = false;
            }
            else
            {
                MaxedOut = true;
            }
        }

        public void CalculateSpaceUsed()
        {
            ContainerSpaceUsed = 1.00 / (PalletPer20ft * 1.00); //what percentage will the pallet fill a 20ft
        }
    }

 public class DutyApplied
 {
     public string Name {get;set;}
     public string Text {get;set;}
     public double Amount {get;set;}

     public DutyApplied()
     {

     }
 }


}