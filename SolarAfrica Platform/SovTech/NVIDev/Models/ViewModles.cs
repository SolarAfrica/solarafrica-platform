﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace NVIDev.Models
{
    public class InverterViewModel
    {
        public int projectID { get; set; }

        public Inverters Inverter { get; set; }

        public List<Filler> Configurations { get; set; }
        public List<Filler> DCOverVoltageList { get; set; }
        public List<Filler> DCLoadDisconnectList { get; set; }

        public List<Filler> IOPortModulesList { get; set; }
        public List<Filler> CountrySpecificGridList { get; set; }
        public InverterViewModel()
        {
            Inverter = new Inverters();
            Configurations = new List<Filler>();
            DCLoadDisconnectList = new List<Filler>();
            DCOverVoltageList = new List<Filler>();
            IOPortModulesList = new List<Filler>();
            CountrySpecificGridList = new List<Filler>();
        }


        public void Populate(Inverters invert)
        {
            Inverter = invert;
            List<string> _Configurations = string.IsNullOrEmpty(invert.ConfigurationList) ? new List<string>() : invert.ConfigurationList.Split(',').ToList();
            List<string> _DCLoadDisconnectList = string.IsNullOrEmpty(invert.DCLoadDisconnectList) ? new List<string>() : invert.DCLoadDisconnectList.Split(',').ToList();
            List<string> _DCOverVoltageList = string.IsNullOrEmpty(invert.DCOverVoltageList) ? new List<string>() : invert.DCOverVoltageList.Split(',').ToList();
            List<string> _IOPortModulesList = string.IsNullOrEmpty(invert.IOPortModulesList) ? new List<string>() : invert.IOPortModulesList.Split(',').ToList();
            List<string> _CountrySpecificGridList = string.IsNullOrEmpty(invert.CountrySpecificGridList) ? new List<string>() : invert.CountrySpecificGridList.Split(',').ToList();

            for (int i = 0; i < _Configurations.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _Configurations[i]
                };

                Configurations.Add(filler);
            }
            for (int i = 0; i < _DCOverVoltageList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _DCOverVoltageList[i]
                };

                DCOverVoltageList.Add(filler);
            }

            for (int i = 0; i < _DCLoadDisconnectList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _DCLoadDisconnectList[i]
                };

                DCLoadDisconnectList.Add(filler);
            }

            for (int i = 0; i < _IOPortModulesList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _IOPortModulesList[i]
                };

                IOPortModulesList.Add(filler);
            }
            for (int i = 0; i < _CountrySpecificGridList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _CountrySpecificGridList[i]
                };

                CountrySpecificGridList.Add(filler);
            }
        }

        public Inverters ReversePopulate()
        {


            var _Configurations = from a in Configurations select a.Value;
            var _DCLoadDisconnectList = from b in DCLoadDisconnectList select b.Value;
            var _DCOverVoltageList = from c in DCOverVoltageList select c.Value;
            var _IOPortModulesList = from d in IOPortModulesList select d.Value;
            var _CountrySpecificGridList = from e in CountrySpecificGridList select e.Value;

            Inverter.ConfigurationList = string.Join(",", _Configurations.ToList());
            Inverter.DCLoadDisconnectList = string.Join(",", _DCLoadDisconnectList.ToList());
            Inverter.DCOverVoltageList = string.Join(",", _DCOverVoltageList.ToList());
            Inverter.IOPortModulesList = string.Join(",", _IOPortModulesList.ToList());
            Inverter.CountrySpecificGridList = string.Join(",", _CountrySpecificGridList.ToList());

            return Inverter;
        }

    }
    public class BiInverterViewModel
    {
        public int projectID { get; set; }

        public BiInverters BiInverter { get; set; }


        public List<Filler> CommunicationList { get; set; }
        public List<Filler> InterfaceList { get; set; }
        public List<Filler> ApplicationList { get; set; }


        public BiInverterViewModel()
        {
            BiInverter = new BiInverters();
            CommunicationList = new List<Filler>();
            InterfaceList = new List<Filler>();
            ApplicationList = new List<Filler>();

        }


        public void Populate(BiInverters Biinverter)
        {
            BiInverter = Biinverter;
            List<string> _CommunicationList = string.IsNullOrEmpty(Biinverter.CommunicationList) ? new List<string>() : Biinverter.CommunicationList.Split(',').ToList();
            List<string> _InterfaceList = string.IsNullOrEmpty(Biinverter.InterfaceList) ? new List<string>() : Biinverter.InterfaceList.Split(',').ToList();
            List<string> _ApplicationList = string.IsNullOrEmpty(Biinverter.ApplicationList) ? new List<string>() : Biinverter.ApplicationList.Split(',').ToList();

            for (int i = 0; i < _CommunicationList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _CommunicationList[i]
                };

                CommunicationList.Add(filler);
            }
            for (int i = 0; i < _InterfaceList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _InterfaceList[i]
                };

                InterfaceList.Add(filler);
            }

            for (int i = 0; i < _ApplicationList.Count; i++)
            {
                Filler filler = new Filler()
                {
                    Value = _ApplicationList[i]
                };

                ApplicationList.Add(filler);
            }


        }

        public BiInverters ReversePopulate()
        {


            var _CommunicationList = from a in CommunicationList select a.Value;
            var _InterfaceList = from b in InterfaceList select b.Value;
            var _ApplicationList = from c in ApplicationList select c.Value;


            BiInverter.CommunicationList = string.Join(",", _CommunicationList.ToList());
            BiInverter.InterfaceList = string.Join(",", _InterfaceList.ToList());
            BiInverter.ApplicationList = string.Join(",", _ApplicationList.ToList());


            return BiInverter;
        }

    }
    public class Filler
    {
        public string Value { get; set; }
        public string ParentList { get; set; }
    }
    public class EquipmentSelectionViewModel
    {
        public int Id { get; set; }



        public List<InverterSub> InvertList { get; set; }

        public List<BiInverterSub> BiInverterList { get; set; }

        public List<Basic> PVModuleList { get; set; }
        public List<Basic> MountingStructureList { get; set; }
        public List<Basic> BatteryList { get; set; }
        public List<Basic> BalanceSystemList { get; set; }

        public EquipmentSelectionViewModel()
        {
            InvertList = new List<InverterSub>();
            BiInverterList = new List<BiInverterSub>();
            PVModuleList = new List<Basic>();
            MountingStructureList = new List<Basic>();
            BatteryList = new List<Basic>();
            BalanceSystemList = new List<Basic>();
        }
        public void Populate(Project project)
        {
            Id = project.Id;
            List<string> _InvertIDS = string.IsNullOrEmpty(project.InverterIDS) ? new List<string>() : project.InverterIDS.Split(',').ToList();
            List<string> _InvertAmounts = string.IsNullOrEmpty(project.InverterAmounts) ? new List<string>() : project.InverterAmounts.Split(',').ToList();

            List<string> _BiInverterIds = string.IsNullOrEmpty(project.BiInverterIDS) ? new List<string>() : project.BiInverterIDS.Split(',').ToList();
            List<string> _BiInverterAmounts = string.IsNullOrEmpty(project.BiInverterAmounts) ? new List<string>() : project.BiInverterAmounts.Split(',').ToList();

            List<string> _PVids = string.IsNullOrEmpty(project.PVModuleIDS) ? new List<string>() : project.PVModuleIDS.Split(',').ToList();
            List<string> _PVAmounts = string.IsNullOrEmpty(project.PvModuleAmounts) ? new List<string>() : project.PvModuleAmounts.Split(',').ToList();

            List<string> _MountsIds = string.IsNullOrEmpty(project.MountingIDS) ? new List<string>() : project.MountingIDS.Split(',').ToList();
            List<string> _MountsAmount = string.IsNullOrEmpty(project.MountingAmounts) ? new List<string>() : project.MountingAmounts.Split(',').ToList();

            List<string> _BattsIDS = string.IsNullOrEmpty(project.BatteryIDS) ? new List<string>() : project.BatteryIDS.Split(',').ToList();
            List<string> _BattsAmounts = string.IsNullOrEmpty(project.BatteryAmounts) ? new List<string>() : project.BatteryAmounts.Split(',').ToList();

            List<string> _BalanceSystemIDs = string.IsNullOrEmpty(project.BalanceSystemIDS) ? new List<string>() : project.BalanceSystemIDS.Split(',').ToList();
            List<string> _BalanceSystemAmounts = string.IsNullOrEmpty(project.BalanceSystemAmounts) ? new List<string>() : project.BalanceSystemAmounts.Split(',').ToList();

            #region Populate Current Inverters

            for (int a = 0; a < _InvertIDS.Count; a++)
            {
                string[] itemid = _InvertIDS[a].Split('!');
                string[] package = _InvertAmounts[a].Split('!');
                try
                {
                    InverterSub isub = new InverterSub()
                    {
                        Id = itemid[0],
                        Configurations = itemid[1],
                        DCOverVoltage = itemid[2],
                        DCLoadDisconnect = itemid[3],
                        IOPortModules = itemid[4],
                        CountrySpecificGrid = itemid[5],
                        Packaging = package[0],
                        Amount = int.Parse(package[1]),
                        ParentList = "InvertList"
                    };

                    InvertList.Add(isub);
                }

                catch (Exception)
                {
                    InverterSub isub = new InverterSub();
                    isub.ParentList = "InvertList";
                    InvertList.Add(isub);
                }

            }
            #endregion
            #region Populate current BiDiverters
            for (int a = 0; a < _BiInverterIds.Count; a++)
            {
                string[] itemid = _BiInverterIds[a].Split('!');
                string[] package = _BiInverterAmounts[a].Split('!');
                try
                {
                    BiInverterSub BIsub = new BiInverterSub()
                    {
                        Id = itemid[0],
                        Communication = itemid[1],
                        Application = itemid[2],
                        Interface = itemid[3],

                        Packaging = package[0],
                        Amount = int.Parse(package[1]),
                        ParentList = "BiInverterList"

                    };


                    BiInverterList.Add(BIsub);
                }

                catch (Exception)
                {
                    BiInverterSub BIsub = new BiInverterSub();
                    BIsub.ParentList = "BiInverterList";
                    BiInverterList.Add(BIsub);
                }

            }


            #endregion
            #region Populate Current PVModules
            for (int a = 0; a < _PVids.Count; a++)
            {
                string itemid = _PVids[a];
                string[] package = _PVAmounts[a].Split('!');
                try
                {
                    Basic PvMod = new Basic()
                    {
                        Id = itemid,
                        ParentList = "PVModuleList",
                        Packaging = package[0],
                        Amount = int.Parse(package[1])

                    };

                    PVModuleList.Add(PvMod);
                }

                catch (Exception)
                {
                    Basic PvMod = new Basic();
                    PvMod.ParentList = "PVModuleList";
                    PVModuleList.Add(PvMod);
                }

            }
            #endregion
            #region Populate Current Mounting Structures
            for (int a = 0; a < _MountsIds.Count; a++)
            {
                string itemid = _MountsIds[a];
                string amount = _MountsAmount[a];
                try
                {
                    Basic Mount = new Basic()
                    {
                        Id = itemid,
                        ParentList = "MountingStructureList",

                        Amount = int.Parse(amount)

                    };

                    MountingStructureList.Add(Mount);
                }

                catch (Exception)
                {
                    Basic Mount = new Basic();
                    Mount.ParentList = "MountingStructureList";
                    MountingStructureList.Add(Mount);
                }

            }
            #endregion
            #region Populate Current Batteries
            for (int a = 0; a < _BattsIDS.Count; a++)
            {
                string itemid = _BattsIDS[a];
                string[] package = _BattsAmounts[a].Split('!');
                try
                {
                    Basic Battery = new Basic()
                    {
                        Id = itemid,
                        ParentList = "BatteryList",
                        Packaging = package[0],
                        Amount = int.Parse(package[1])

                    };

                    BatteryList.Add(Battery);
                }

                catch (Exception)
                {
                    Basic Battery = new Basic();
                    Battery.ParentList = "BatteryList";
                    BatteryList.Add(Battery);
                }

            }
            #endregion
            #region Populate Current BalanceSystems
            for (int a = 0; a < _BalanceSystemIDs.Count; a++)
            {
                string[] itemid = _BalanceSystemIDs[a].Split('!');
                string od = itemid[0];
                string pare = itemid[1];
                string[] package = _BalanceSystemAmounts[a].Split('!');
                try
                {
                    Basic BalanceSystem = new Basic()
                    {
                        Id = od,
                        ParentList = pare,
                        Packaging = package[0],
                        Amount = int.Parse(package[1])

                    };

                    BalanceSystemList.Add(BalanceSystem);
                }

                catch (Exception)
                {
                    Basic BalanceSystem = new Basic();
                    BalanceSystem.ParentList = "BalanceSystemList";
                    BalanceSystemList.Add(BalanceSystem);
                }

            }
            #endregion

        }


    }
    public class InverterSub : Basic
    {

        public string Configurations { get; set; }
        public string DCOverVoltage { get; set; }
        public string DCLoadDisconnect { get; set; }

        public string IOPortModules { get; set; }
        public string CountrySpecificGrid { get; set; }


    }
    public class BiInverterSub : Basic
    {

        public string Communication { get; set; }
        public string Interface { get; set; }
        public string Application { get; set; }



    }
    public class Basic
    {
        public string Id { get; set; }
        public string ParentList { get; set; }
        public string Key { get; set; }
        public int Watt { get; set; }
        public string Packaging { get; set; }

        [DefaultValue(1)]

        public int Amount { get; set; }
    }
}