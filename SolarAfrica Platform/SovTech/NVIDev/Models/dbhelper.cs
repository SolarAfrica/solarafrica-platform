﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace NVIDev.Models
{
    public class DBHelper
    {
        public ApplicationDbContext db = new ApplicationDbContext();

        public DBHelper()
        {

        }


        #region Projects
   
        public List<Project> GetUserProjects(string userID)
        {
            var PList = db.Projects.Where(m => m.CurrentUserID == userID).ToList();

            List<Project> mProjects = new List<Project>();
            mProjects.AddRange(PList);

            return mProjects;
        }

        public Project GetProject(int id)
        {
            return db.Projects.Find(id);
        }

        public List<Audit> GetProjectAudits(int id)
        {
            var projectsTOFind = GetProject(id);

            List<Audit> maudits = new List<Audit>();
            var proAudits = db.Audits.Where(m => m.Content.Contains(projectsTOFind.Details.ProjectName));
            maudits.AddRange(proAudits);

            return maudits;
        }



        public bool CreateProject(Project project, string UserID)
        {
            project.CreatorID = UserID;
            project.CurrentUserID = UserID;
            project.xn1 = 0.125;
            project.xn2 = 0.125;
            project.xn3 = 0.125;
            project.xn4 = 0.125;
            project.xn5 = 0.125;
            project.xn6 = 0.125;
            project.xn7 = 0.125;
            project.xn8 = 0.125;
            project.Costing = new Costing();
            project.Costing.Tarrif = 0.1;
            project.Costing.Type = "Grid-Tied";
            project.Costing.Yield = 0;
            project.Costing.Term = 3;

            project.Initialize();
            try
            {
                db.Projects.Add(project);
                project.Code = project.Details.Abbreviation.ToUpper() + "-" + project.Id.ToString();
                db.SaveChanges();
                return true;
            }

            catch
            {
                db.Projects.Remove(project);
                return false;
            }
        }


        public bool EditProjectDetails(Project project)
        {

            try
            {
                bool success = UpdateDetails(project);
                if (success)
                {
                    return true;
                }
            }

            catch
            {
                //something went wrong
            }

            return false;
        }


        public bool EditProjectCosting(Project project)
        {
            try
            {
                UpdateCosting(project);
                return true;
            }

            catch
            {
                return false;
            }
        }

        public bool EditProjectQuoting(QuotingViewModel model)
        {
            try
            {
                var projectToUpdate = PopulatedQuoteItems(model);

                //projectToUpdate.Costing.InstallFee = project.Costing.InstallFee;
                //projectToUpdate.Costing.DesignFee = project.Costing.DesignFee;
                //projectToUpdate.Costing.OMWorks = project.Costing.OMWorks;
                //projectToUpdate.Costing.UseEPC = project.Costing.UseEPC;
                //projectToUpdate.Costing.EPCPrice = project.Costing.EPCPrice;
                db.Entry(projectToUpdate).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }

            catch
            {
                return false;
            }

        }


        public List<QuotingCost> GetUserCostItems(string userID)
        {
            List<QuotingCost> mcosts = new List<QuotingCost>();

            var costItems = db.CostItems.Where(m => m.UserID == userID).ToList();

            if(costItems != null && costItems.Count > 0)
            {
                foreach (var item in costItems)
                {
                    QuotingCost cost = new QuotingCost();
                    cost.Populate(item);

                    mcosts.Add(cost);
                }

            }


            return mcosts;
        }

        public string TryInsertCostItem(QuotingCost item)
        {

            var costItem = db.CostItems.Where(m => m.Name == item.Name && m.UserID == item.UserID).FirstOrDefault();

            if(costItem == null)
            {
                //does not exist, add it
                try
                {
                    CostItem newItem = new CostItem();
                    newItem.Populate(item);

                    db.CostItems.Add(newItem);
                    db.SaveChanges();
                    return "Succesful";
                }

                catch
                {
                    return "Failed";
                }
              
            }

            else
            {
                return "Already Exists";
            }

        }

        public Project PopulatedQuoteItems(QuotingViewModel model)
        {
            var projectToUpdate = db.Projects.Find(model.id);

            projectToUpdate.Costing.InstallFee = model.InstallationCosts;
            projectToUpdate.Costing.DesignFee = model.DesignCosts;
            projectToUpdate.Costing.OMWorks = model.OMWorks;
            string[] _QuotingCostNames = new string[model.Costs.Count];
            string[] _QuotingCostMains = new string[model.Costs.Count];
            string[] _QuotingCostSuppliers = new string[model.Costs.Count];
            string[] _QuotingCostCosts = new string[model.Costs.Count];
            string[] _QuotingCostMarkups = new string[model.Costs.Count];
            string[] _QuotingCostSellPrices = new string[model.Costs.Count];
            string[] _QuotingCostWPMarks = new string[model.Costs.Count];
            string[] _QuotingCostFixeds = new string[model.Costs.Count];






            for (int a = 0; a < model.Costs.Count; a++)
            {
                _QuotingCostNames[a] = model.Costs[a].Name;
                _QuotingCostMains[a] = model.Costs[a].Main.ToString();

                if(_QuotingCostMains[a] =="false")
                {
                    _QuotingCostSuppliers[a] = "NA";

                }

                else
                {
                    _QuotingCostSuppliers[a] = model.Costs[a].Supplier;

                }
                _QuotingCostCosts[a] = model.Costs[a].Cost.ToString();
                _QuotingCostMarkups[a] = model.Costs[a].Markup.ToString(); 
                _QuotingCostSellPrices[a] = (model.Costs[a].Cost * (1 + model.Costs[a].Markup / 100)).ToString();
                _QuotingCostWPMarks[a] = model.Costs[a].WPMark;
                _QuotingCostFixeds[a] = model.Costs[a].Fixed.ToString();

            }


            projectToUpdate.QuotingCostNames = string.Join(",", _QuotingCostNames);
            projectToUpdate.QuotingCostMains = string.Join(",", _QuotingCostMains);
            projectToUpdate.QuotingCostSuppliers = string.Join(",", _QuotingCostSuppliers);
            projectToUpdate.QuotingCostCosts = string.Join(",", _QuotingCostCosts);
            projectToUpdate.QuotingCostMarkups = string.Join(",", _QuotingCostMarkups);
            projectToUpdate.QuotingCostSellPrices = string.Join(",", _QuotingCostSellPrices);
            projectToUpdate.QuotingCostWPMarks = string.Join(",", _QuotingCostWPMarks);
            projectToUpdate.QuotingCostFixeds = string.Join(",", _QuotingCostFixeds);

            return projectToUpdate;
        }

        public bool EditProjectFinance(Project project)
        {
            try
            {
                var projectToUpdate = GetProject(project.Id);

                projectToUpdate.Costing.Term = project.Costing.Term;
                projectToUpdate.Costing.Plan = project.Costing.Plan;


                projectToUpdate.real_UpfrontClient = project.real_UpfrontClient;
                projectToUpdate.real_FixedAnualCharge = project.real_FixedAnualCharge;
                projectToUpdate.InsureYN = project.InsureYN;
                projectToUpdate.OMYN = project.OMYN;


                db.Entry(projectToUpdate).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }


            catch
            {
                return false;
            }
        }

        public string UpdateAllProjectCodes()
        {

            List<Project> pros = db.Projects.ToList();

            int numToChange = pros.Count;
            int changed = 0;
            foreach (var pro in pros)
            {
                try
                {


                    if (!string.IsNullOrWhiteSpace(pro.Details.Abbreviation) && !pro.Details.Abbreviation.Contains("-"))
                    {
                        pro.Code = pro.Details.Abbreviation + "-" + pro.Id.ToString();

                        db.Entry(pro).State = EntityState.Modified;
                        db.SaveChanges();

                        changed++;
                    }



                }

                catch (DbEntityValidationException ex)
                {


                    db.Entry(pro).State = EntityState.Unchanged;
                    foreach (var eve in ex.EntityValidationErrors)
                    {
                        string a = eve.Entry.GetType().Name;
                        string b = eve.Entry.State.ToString();


                        foreach (var aeo in eve.ValidationErrors)
                        {
                            string c = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                    aeo.PropertyName, aeo.ErrorMessage);
                        }
                    }


                }

            }

            string mess = "";
            if (changed <= 0)
            {
                mess = "No Changes";
            }

            if (changed > 0 && changed < numToChange)
            {
                mess = "Changed Some";
            }

            if (changed == numToChange)
            {
                mess = "Changed All";
            }


            return mess;

        }


        public bool EditProjectEquipment(EquipmentSelectionViewModel Model)
        {
            try
            {


                EquipmentSelectionViewModel _model = new EquipmentSelectionViewModel();
                _model.Id = Model.Id;

                _model.BalanceSystemList = (Model.BalanceSystemList != null) ? Model.BalanceSystemList : new List<Basic>();
                _model.BiInverterList = (Model.BiInverterList != null) ? Model.BiInverterList : new List<BiInverterSub>();
                _model.InvertList = (Model.InvertList != null) ? Model.InvertList : new List<InverterSub>();
                _model.BatteryList = (Model.BatteryList != null) ? Model.BatteryList : new List<Basic>();
                _model.PVModuleList = (Model.PVModuleList != null) ? Model.PVModuleList : new List<Basic>();
                _model.MountingStructureList = (Model.MountingStructureList != null) ? Model.MountingStructureList : new List<Basic>();
                _model.Yield = Model.Yield;
                _model.Type = Model.Type;



                Project pro = db.Projects.Find(_model.Id);

                pro.MountingInDollars = Model.MountingCostInDollars;
                pro.MountingContainersUsed20ft = Model.MountingContainers20ft;
                List<string> invIds = new List<string>();
                List<string> inAmounts = new List<string>();
                for (int a = 0; a < _model.InvertList.Count; a++)
                {

                    if (!string.IsNullOrEmpty(_model.InvertList[a].Id) && _model.InvertList[a].Amount > 0)
                    {
                        InverterSub i = _model.InvertList[a];
                        string[] idAr = new string[] { i.Id, i.Configurations, i.DCOverVoltage, i.DCLoadDisconnect, i.IOPortModules, i.CountrySpecificGrid };
                        string id = string.Join("!", idAr);

                        string[] Packamon = new string[] { i.Amount.ToString() };
                        string amon = string.Join("!", Packamon);

                        invIds.Add(id);
                        inAmounts.Add(amon);
                    }



                }

                pro.InverterIDS = string.Join(",", invIds);
                pro.InverterAmounts = string.Join(",", inAmounts);



                List<string> BiinvIds = new List<string>();
                List<string> BiinAmounts = new List<string>();
                for (int a = 0; a < _model.BiInverterList.Count; a++)
                {

                    if (!string.IsNullOrEmpty(_model.BiInverterList[a].Id) && _model.BiInverterList[a].Amount > 0)
                    {
                        BiInverterSub i = _model.BiInverterList[a];
                        string[] idAr = new string[] { i.Id, i.Communication, i.Application, i.Interface };
                        string id = string.Join("!", idAr);

                        string[] Packamon = new string[] { i.Amount.ToString() };
                        string amon = string.Join("!", Packamon);

                        BiinvIds.Add(id);
                        BiinAmounts.Add(amon);
                    }



                }

                pro.BiInverterIDS = string.Join(",", BiinvIds);
                pro.BiInverterAmounts = string.Join(",", BiinAmounts);

                List<string> MountIds = new List<string>();
                List<string> MountAmounts = new List<string>();
                for (int a = 0; a < _model.MountingStructureList.Count; a++)
                {

                    if (!string.IsNullOrEmpty(_model.MountingStructureList[a].Id) && _model.MountingStructureList[a].Amount > 0)
                    {
                        Basic i = _model.MountingStructureList[a];

                        string id = i.Id;

                        string amon = i.Amount.ToString();

                        MountIds.Add(id);
                        MountAmounts.Add(amon);
                    }



                }

                pro.MountingIDS = string.Join(",", MountIds);
                pro.MountingAmounts = string.Join(",", MountAmounts);

                List<string> PVIds = new List<string>();
                List<string> PVAmounts = new List<string>();
                for (int a = 0; a < _model.PVModuleList.Count; a++)
                {

                    if (!string.IsNullOrEmpty(_model.PVModuleList[a].Id) && _model.PVModuleList[a].Amount > 0)
                    {
                        Basic i = _model.PVModuleList[a];

                        string id = i.Id;

                        string[] Packamon = new string[] { i.Amount.ToString() };
                        string amon = string.Join("!", Packamon);

                        PVIds.Add(id);
                        PVAmounts.Add(amon);
                    }



                }

                pro.PVModuleIDS = string.Join(",", PVIds);
                pro.PvModuleAmounts = string.Join(",", PVAmounts);

                List<string> BattIds = new List<string>();
                List<string> BattAmounts = new List<string>();
                for (int a = 0; a < _model.BatteryList.Count; a++)
                {

                    if (!string.IsNullOrEmpty(_model.BatteryList[a].Id) && _model.BatteryList[a].Amount > 0)
                    {
                        Basic i = _model.BatteryList[a];

                        string id = i.Id;

                        string[] Packamon = new string[] { i.Amount.ToString() };
                        string amon = string.Join("!", Packamon);

                        BattIds.Add(id);
                        BattAmounts.Add(amon);
                    }



                }

                pro.BatteryIDS = string.Join(",", BattIds);
                pro.BatteryAmounts = string.Join(",", BattAmounts);


                List<string> BalanceIds = new List<string>();
                List<string> BalanceAmounts = new List<string>();
                for (int a = 0; a < _model.BalanceSystemList.Count; a++)
                {

                    if (!string.IsNullOrEmpty(_model.BalanceSystemList[a].Id) && _model.BalanceSystemList[a].Amount > 0)
                    {
                        Basic i = _model.BalanceSystemList[a];

                        string[] idAr = new string[] { i.Id, i.ParentList };
                        string id = string.Join("!", idAr);

                        string[] Packamon = new string[] { i.Amount.ToString() };
                        string amon = string.Join("!", Packamon);

                        BalanceIds.Add(id);
                        BalanceAmounts.Add(amon);
                    }



                }
                pro.BalanceSystemIDS = string.Join(",", BalanceIds);
                pro.BalanceSystemAmounts = string.Join(",", BalanceAmounts);
                pro.Costing.Yield = Model.Yield;
                pro.Costing.Type = Model.Type;
                var fee = db.Fees.FirstOrDefault();
                var Feemodel = new FeeViewModel(pro, fee);
                Feemodel.Populate();

                Feemodel.Submission();
                pro.AdminXII = Feemodel.OnGoingFee_Admin;
                pro.UpfrontXI = Feemodel.Upfront;

                SuperProject sm = Feemodel.superPro;

                pro.Revision += 0.010;
                pro.Revision = Math.Round(pro.Revision, 2);
                pro.in_UpfrontClient = double.IsNaN(sm.UpInitial) ? 0.0 : sm.UpInitial;
                pro.in_SolarCapexFee = double.IsNaN(sm.A) ? 0.0 : sm.A;
                pro.Costing.OMRate = sm.OMRate;
                db.Entry(pro).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }




        public bool DeleteProject(Project project)
        {
            try
            {
                db.Entry(project).State = EntityState.Deleted;
                db.SaveChanges();
                return true;
            }

            catch
            {
                db.Entry(project).State = EntityState.Unchanged;
                return false;
            }

        }

        public bool TransferProject(Project project, string newUser)
        {
            try
            {
                project.CurrentUserID = db.Users.Where(m => m.UserName == newUser).Select(m => m.Id).First();
                db.SaveChanges();
                return true;
            }

            catch
            {
                return false;
            }
        }


        public QuotingCost GetQuoteCost(string name, string userID)
        {
            QuotingCost model = new QuotingCost();
            model.Name = name;
            var costItem = GetCostItemByNameID(name,userID);

            if(costItem!= null)
            {
                model.Populate(costItem);
            }


            return model;
        }
        public CostItem GetCostItemByNameID(string name, string userID)
        {
            var costItem = db.CostItems.Where(m => m.Name == name && m.UserID == userID).FirstOrDefault();

            return costItem;
        }

        Project UpdateCosting(Project project)
        {
            var a = db.Projects.Find(project.Id);
            try
            {
                if (project.Costing.ApplyForAllMonths)
                {
                    project.Costing.ClientDemand2 = project.Costing.ClientDemand1;
                    project.Costing.ClientDemand3 = project.Costing.ClientDemand1;
                    project.Costing.ClientDemand4 = project.Costing.ClientDemand1;
                    project.Costing.ClientDemand5 = project.Costing.ClientDemand1;
                    project.Costing.ClientDemand6 = project.Costing.ClientDemand1;
                    project.Costing.ClientDemand7 = project.Costing.ClientDemand1;
                    project.Costing.ClientDemand8 = project.Costing.ClientDemand1;
                    project.Costing.ClientDemand9 = project.Costing.ClientDemand1;
                    project.Costing.ClientDemand10 = project.Costing.ClientDemand1;
                    project.Costing.ClientDemand11 = project.Costing.ClientDemand1;
                    project.Costing.ClientDemand12 = project.Costing.ClientDemand1;

                }

                project.Costing.Demand = project.Costing.ClientDemand1 + project.Costing.ClientDemand2 + project.Costing.ClientDemand3 + project.Costing.ClientDemand4 + project.Costing.ClientDemand5 + project.Costing.ClientDemand6 + project.Costing.ClientDemand7 + project.Costing.ClientDemand8 + project.Costing.ClientDemand9 + project.Costing.ClientDemand10 + project.Costing.ClientDemand11 + project.Costing.ClientDemand12;

                a.real_FixedAnualCharge = project.real_FixedAnualCharge;
                a.Costing = project.Costing;

                a.Costing.ApplyForAllMonths = project.Costing.ApplyForAllMonths;
                a.Code = a.Details.Abbreviation + a.Id.ToString();
                a.Revision += 0.01;
                a.OMYN = project.OMYN;
                a.Costing.OMWorks = project.Costing.OMWorks;
                a.Costing.DesignFee = project.Costing.DesignFee;
                a.Costing.InstallFee = project.Costing.InstallFee;


                a.InsureYN = project.InsureYN;
                a.Valid = project.isEnergyDone();
                a.Costing.Plan = project.Costing.Plan;
                //Repopulate Fees
                var fee = db.Fees.FirstOrDefault();
                var Feemodel = new FeeViewModel(a, fee);
                Feemodel.Populate();

                Feemodel.Submission();
                a.AdminXII = Feemodel.OnGoingFee_Admin;
                a.UpfrontXI = Feemodel.Upfront;
                a.Costing.OMRate = Feemodel.superPro.OMRate;
                var country = db.Countries.Find(a.Details.P_Country);
                switch (a.Costing.Type)
                {
                    case "Grid-Tied": a.In_Ri = country.Grid_Tied_Ri; break;
                    case "Diesel-Hybrid": a.In_Ri = country.DieselH_Ri; break;
                    case "Battery-Hybrid": a.In_Ri = country.BatteryH_Ri; break;

                }

                if (a.UtilityInflation1 == 0) //set first time utility inflation
                {
                    a.UtilityInflation1 = country.UtilityInflation1;
                    a.UtilityInflation2 = country.UtilityInflation2;
                    a.UtilityInflation3 = country.UtilityInflation3;
                    a.UtilityInflation4 = country.UtilityInflation4;
                    a.UtilityInflation5 = country.UtilityInflation5;
                    a.UtilityInflation6 = country.UtilityInflation6;
                    a.UtilityInflation7 = country.UtilityInflation7;
                    a.UtilityInflation8 = country.UtilityInflation8;
                    a.UtilityInflation9 = country.UtilityInflation9;
                    a.UtilityInflation10 = country.UtilityInflation10;
                    a.UtilityInflation11 = country.UtilityInflation11;
                    a.UtilityInflation12 = country.UtilityInflation12;
                    a.UtilityInflation13 = country.UtilityInflation13;
                    a.UtilityInflation14 = country.UtilityInflation14;
                    a.UtilityInflation15 = country.UtilityInflation15;
                    a.UtilityInflation16 = country.UtilityInflation16;
                    a.UtilityInflation17 = country.UtilityInflation17;
                    a.UtilityInflation18 = country.UtilityInflation18;
                    a.UtilityInflation19 = country.UtilityInflation19;
                    a.UtilityInflation20 = country.UtilityInflation20;

                }
                db.Entry(a).State = EntityState.Modified;
                db.SaveChanges();
                return a;
            }
            catch (Exception) 
            {
                throw;
            }            
        }


        public bool UpdateDetails(Project project)
        {
            var a = db.Projects.Find(project.Id);
            project.Code = project.Details.Abbreviation + project.Id.ToString();
            a.Details = project.Details;
            a.Code = a.Details.Abbreviation + a.Id.ToString();
            a.Revision += 0.01;
            SuperProject sp = new SuperProject(a);
            a.Costing.OMRate = sp.OMRate;

            try
            {
                db.Entry(a).State = EntityState.Modified;

                db.SaveChanges();
                return true;
            }

            catch (Exception)
            {

                return false;
                //something broke
            }

        }


        public bool ReviewProject(FeeViewModel model)
        {
            Fees fee = CurrentFees();

            try
            {
                model = new FeeViewModel(model.project, fee);
                model.Populate();

                model.Submission();
                Project pro = db.Projects.Find(model.project.Id);

                pro.UseAdmin = model.project.UseAdmin;
                pro.UseAssetManagement = model.project.UseAssetManagement;
                pro.UseCommisioning = model.project.UseCommisioning;
                pro.UseMargin = model.project.UseMargin;
                pro.UseMonitorSoftware = model.project.UseMonitorSoftware;
                pro.UsePerformanceG = model.project.UsePerformanceG;
                pro.UsePrelim = model.project.UsePrelim;
                pro.UsePrepVet = model.project.UsePrepVet;
                pro.UseProcurement = model.project.UseProcurement;
                pro.UseProjectManage = model.project.UseProjectManage;
                pro.UseTechVet = model.project.UseTechVet;
                pro.UseWorkingCap = model.project.UseWorkingCap;

                //EPC Check 

                pro.in_UpfrontClient = model.superPro.UpInitial;
                pro.in_UpfrontFee = model.Upfront;
                pro.in_AdminFee = model.OnGoingFee_Admin;
                pro.AdminXII = model.OnGoingFee_Admin;
                pro.UpfrontXI = model.Upfront;
                db.Entry(pro).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }

            catch
            {
                return false;
            }
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public int[] Terms()
        {
            return new int[] { 3, 5, 8, 10, 12, 15, 20 };
        }

        public string[] ProjectPlans()
        {
            return new string[] { "Pay-By-Solar", "Power Purchase Agreement", "Solar Development Services" };
        }

        public string[] ProjectTypes()
        {
            return new string[] { "Grid-Tied", "Diesel-Hybrid", "Battery-Hybrid" };
        }

        public string[] EnergySources()
        {
            return new string[] { "Grid-Tied", "Diesel", "Both" };
        }

        public string ProjectNumbers(string userID)
        {
            string pronumbers = db.Projects.Where(d => d.CurrentUserID == userID).Count().ToString();
            return pronumbers;
        }

        public FeeViewModel UpdatedFees(Project project)
        {
            NVIDev.Models.Fees fee = db.Fees.FirstOrDefault();
            FeeViewModel model = new FeeViewModel(project, fee);
            model.Populate();

            model.Submission();

            return model;
        }


        public Fees CurrentFees()
        {
            return db.Fees.FirstOrDefault();
        }


        public double CurrencyPerDollar(string country)
        {
            return db.Countries.Find(country).Currency_Per_Dollar;
        }

        public string[] EquipmentTypes()
        {
            return new string[] { "Inverters", "Bi-Inverter", "Mounting Structure", "Balance Of System", "PV Panel", "Battery" };

        }

        #endregion

        #region Admin

        public List<ApplicationUser> GetAllUsers()
        {
            return db.Users.ToList();
        }

        public string[] UserRoles()
        {
            return new string[] { "NVI Admin", "NVI User", "TP Admin", "TP User" };
        }

        public string[] FinancedApprovedStatus()
        {
            return new string[] { "Approved", "In Process", "Reject" };
        }


        public DbRawSqlQuery<Vw_User> GetUserList()
        {
            DbRawSqlQuery<Vw_User> UserList = db.Database.SqlQuery<Vw_User>("SELECT  [UserId],[Email],[PasswordHash],[PhoneNumber],[UserName],[Company],[Image],[Road],[City],[Country],[Salutation],[FirstName],[LastName],[UserType],[Designation],[CreatedOn],[RoleId],[RoleName]  FROM  [Vw_Users]  Where [RoleName]!= 'NVIAdmin' ");
            return UserList;
        }

        public void GetList()
        {
            var raw= db.Users.
                Select(x=> new  {
                x.Roles,
                x.Id,
                x.Salutation,
                x.FirstName,
                x.LastName,
                x.Email,
                x.PasswordHash,
                x.PhoneNumber,
                x.UserName,
                x.UserType,
                x.Designation,
                x.Company,
                x.Image, 
               x.Address_Street,
               x.Address_City,
               x.Address_Country,
               x.CreatedOn               
                });
        }

        #endregion


        #region Users

        public string GetUserFullName(string id)
        {
            return db.Users.Find(id).UserName;
        }

        public ApplicationUser GetUser(string id)
        {
            return db.Users.Find(id);
        }


        public int AddAudit(Audit audit)
        {
            try
            {
                db.Audits.Add(audit);
                db.SaveChanges();
                return 1;
            }

            catch
            {
                db.Audits.Remove(audit);
                return 0;
            }
        }
        public List<string> GetAllCompanyUserFullNames(string company)
        {
            var uList = db.Users.Where(m => m.Company == company).Select(a => a.UserName).ToList();

            List<string> names = new List<string>();
            names.AddRange(uList);
            return names;
        }


        public List<Audit> UserAudits(string userID)
        {
            List<Audit> mAudits = new List<Audit>();


            mAudits.AddRange(db.Audits.Where(m => m.UserID == userID));

            return mAudits;
        }



        #endregion


        #region Data



        public List<string> PersonTitles()
        {
            List<string> titles = new List<string>();
            titles.AddRange(new string[] { "Mr", "Mrs", "Ms", "Miss", "Dr", "Prf" });
            return titles;
        }


        #region Countries
        public List<Country> GetAllCountries()
        {
            var countries = db.Countries.ToList();

            List<Country> mCountries = new List<Country>();
            mCountries.AddRange(countries);
            return mCountries;
        }

        public List<string> GetAllCountryNames()
        {
            var countries = GetAllCountries();

            List<string> mnames = new List<string>();
            mnames.AddRange(countries.Select(m => m.Id));

            return mnames;
        }
        #endregion

        #region Currencies
        public string GetCurrency(string countryID)
        {
            return db.Countries.Find(countryID).Currency;
        }

        public Currency GetCurrencyC(string id)
        {
            return db.Currencies.Find(id);
        }


        public List<string> GetAllCurrenciesNames()
        {
            var currencys = db.Currencies.Select(m => m.id);
            List<string> allCurrencies = new List<string>();
            allCurrencies.AddRange(currencys);

            if (!allCurrencies.Contains("USD") && !allCurrencies.Contains("US$"))
            {
                allCurrencies.Add("USD");
            }
            return allCurrencies;
        }

        public List<Currency> GetAllCurrencies()
        {
            var currencys = db.Currencies.ToList();
            List<Currency> allCurrencies = new List<Currency>();
            allCurrencies.AddRange(currencys);
            return allCurrencies;
        }


        public bool CreatCurrency(Currency model)
        {
            try
            {
                db.Currencies.Add(model);

                db.SaveChanges();
                return true;
            }

            catch
            {
                return false;
            }
        }

        public bool EditCurrency(Currency model)
        {
            try
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();


                List<Country> countriesUsingIt = db.Countries.Where(m => m.Currency == model.id).ToList() ;

                if(countriesUsingIt != null)
                {
                    if (  countriesUsingIt.Count > 0)
                    {
                        foreach(var count in countriesUsingIt)
                        {
                            count.Currency_Per_Dollar = model.ValueInDollars;

                            db.Entry(count).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
                return true;
            }

            catch
            {
                db.Entry(model).State = EntityState.Unchanged;
                return false;
            }
        }

        public bool ChangeAllCountryCurrencies(Currency model)
        {
            try
            {

                var countriesUsingCCY = db.Countries.Where(m => m.Currency == model.id);

                foreach (Country cntry in countriesUsingCCY)
                {
                    cntry.Currency_Per_Dollar = model.ValueInDollars;

                    db.Entry(cntry).State = EntityState.Modified;
                }

                db.SaveChanges();
                return true;
            }

            catch
            {
                return false;
            }
        }

        public bool DeleteCurrency(string id)
        {

            try
            {
                var currencyToDelete = db.Currencies.Find(id);
                db.Entry(currencyToDelete).State = EntityState.Deleted;
                db.SaveChanges();
                return true;
            }

            catch
            {
                return false;
            }
        }

        #endregion

        #region Country Of Origin COOS

        public List<string> GetAllCOOSNames()
        {
            var coos = db.CountryOfOrigins.ToList().Select(m => m.Name);
            List<string> mcoos = new List<string>();
            mcoos.AddRange(coos);

            if (mcoos.Count <= 0)
            {
                mcoos.Add("NONE");
            }

            return mcoos;
        }


        public List<CountryOfOrigin> GetAllCoos()
        {
            var coos = db.CountryOfOrigins.ToList();
            List<CountryOfOrigin> mcoos = new List<CountryOfOrigin>();
            mcoos.AddRange(coos);



            return mcoos;
        }

        public bool CreateCOO(CountryOfOrigin model)
        {
            try
            {
                db.CountryOfOrigins.Add(model);
                db.SaveChanges();
                return true;
            }

            catch
            {
                db.CountryOfOrigins.Remove(model);
                return false;
            }
        }


        public bool DeleteCOO(int id)
        {
            try
            {
                var cooToDelete = db.CountryOfOrigins.Find(id);
                db.Entry(cooToDelete).State = EntityState.Deleted;
                db.SaveChanges();
                return true;
            }

            catch
            {

                return false;
            }
        }

        public CountryOfOrigin GetCOO(int id)
        {
            return db.CountryOfOrigins.Find(id);
        }
        #endregion

        #region Ports
        public List<Port> GetAllPorts()
        {
            var ports = db.Ports.ToList();

            List<Port> mports = new List<Port>();
            mports.AddRange(ports);

            return mports;
        }

        public List<string> GetAllPortNames()
        {
            var mports = GetAllPorts();

            List<string> mNames = new List<string>();

            mNames.AddRange(mports.Select(m => m.Name));
            return mNames;
        }

        public List<Port> GetAllPorts(string countryID)
        {
            var ports = db.Ports.ToList();

            List<Port> mports = new List<Port>(ports.Where(m=> m.Country == countryID));
            mports.AddRange(ports);

            return mports;
        }

        public List<string> GetAllPortNames(string countryID)
        {
            var mports = GetAllPorts();

            List<string> mNames = new List<string>();

            mNames.AddRange(mports.Where(m => m.Country == countryID).Select(m => m.Name));
            return mNames;
        }

        #endregion


        #endregion


        #region Equipment

        public string [] Suppliers(string userid)
        {
            var userdeets = GetUser(userid);

            if(!userdeets.Company.Contains("NVI"))
            {
                return new string[] { "SolarAfrica", userdeets.Company };
            }

            else
            {
                return new string[] { "SolarAfrica" };
            }

        }

        public string[] Packages()
        {
            return new string[] { "Unit", "Pallet", "20ft Container", "40ft Container" };
        }

        public List<string> BalanceCategories()
        {
            List<string> bosCategories = new List<string>();

            foreach (var cat in db.BalanceSystems.Select(m => m.Type).ToList())
            {
                if (!bosCategories.Contains(cat))
                {
                    bosCategories.Add(cat);
                }
            }

            return bosCategories;
        }



        public List<Inverters> GetAllInverters()
        {
            return db.Inverters.ToList();
        }

        public List<BiInverters> GetAllBiInverters()
        {
            return db.BiInverters.ToList();
        }

        public List<PVModules> GetAllPVModules()
        {
            return db.PVModules.ToList();
        }








        public Inverters GetInverter(string ID)
        {
            return db.Inverters.Find(ID);
        }

        public BiInverters GetBiInverter(string id)
        {
            return db.BiInverters.Find(id);
        }


        public PVModules GetPVModule(string id)
        {
            return db.PVModules.Find(id);
        }


        public bool DeleteItem(string id)
        {

            try
            {


                string[] idtype = id.Split('!');

                string IDD = idtype[0];
                string type = idtype[1];

                switch (type)
                {
                    case "Inverter": db.Inverters.Remove(db.Inverters.Find(IDD)); break;
                    case "BiInverter": db.BiInverters.Remove(db.BiInverters.Find(IDD)); break;
                    case "Mountingstructure": db.MountingStructures.Remove(db.MountingStructures.Find(IDD)); break;
                    case "Battery": db.Batteries.Remove(db.Batteries.Find(IDD)); break;
                    case "BalanceSystem": db.BalanceSystems.Remove(db.BalanceSystems.Find(IDD)); break;
                    case "PVModule": db.PVModules.Remove(db.PVModules.Find(IDD)); break;
                }

                db.SaveChanges();
                return true;
            }


            catch
            {
                return false;
            }
        }



        #region Mounting

        public List<MountingStructures> GetAllMounting()
        {
            return db.MountingStructures.ToList();
        }

        public MountingStructures GetMounting(string id)
        {
            return db.MountingStructures.Find(id);
        }

        public bool CreateMounting(MountingStructures model)
        {

            try
            {
                model.Validate();
                model.Id = model.Id.Replace(" ", "_");

                db.MountingStructures.Add(model);
                db.SaveChanges();
                return true;
            }

            catch
            {
                db.MountingStructures.Remove(model);
                return false;

            }

        }


        public bool EditMounting(MountingStructures model)
        {
            try
            {
                model.Validate();


                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }

            catch
            {
                db.Entry(model).State = EntityState.Unchanged;

                return false;
            }
        }

        #endregion

        #region Batteries
        public Batteries GetBattery(string id)
        {
            return db.Batteries.Find(id);
        }
        public List<Batteries> GetAllBatteries()
        {
            return db.Batteries.ToList();
        }


        public bool CreateBattery(Batteries model)
        {
            try
            {
                model.Validate();
                model.Id = model.Id.Replace(" ", "_");

                db.Batteries.Add(model);

                db.SaveChanges();
                return true;
            }

            catch
            {
                db.Batteries.Remove(model);

                return false;
            }
        }


        public bool EditBattery(Batteries model)
        {
            try
            {
                model.Validate();


                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }

            catch
            {
                db.Entry(model).State = EntityState.Unchanged;

                return false;
            }
        }
        #endregion

        #region Balance Of System

        public List<BalanceSystem> GetAllBalanceSystems()
        {
            return db.BalanceSystems.ToList();
        }

        public BalanceSystem GetBalanceSystem(string id)
        {
            return db.BalanceSystems.Find(id);
        }

        public bool CreateBalanceSystem(BalanceSystem model)
        {
            try
            {
                model.Validate();
                model.Id = model.Id.Replace(" ", "_");

                db.BalanceSystems.Add(model);
                db.SaveChanges();
                return true;
            }

            catch
            {
                db.BalanceSystems.Remove(model);
                return false;
            }
        }


        public bool EditBalanceSystem(BalanceSystem model)
        {
            try
            {
                model.Validate();


                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                return true;
            }

            catch
            {
                return false;
            }
        }
        #endregion
        #endregion
    }
}