﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVIDev.Models
{
    ///Work on this!!
    public class SuperProject
    {
        public ApplicationDbContext VarDB = new ApplicationDbContext();
        public int Id { get; set; }

        public double Ni { get; set; } //Formula	2	Nominal Rate of Return		
        public double NR { get; set; } //	Formula	3	Net Revenue required		
        public double Admin { get; set; } //	Formula	6	This is the portion of XII that is charged for administration (formula = X1II X user defined %) - the default is 100%		
        public double TPF { get; set; } //	Formula	7	Technical Partner Factor		
        public double A { get; set; }//	Sum	8	Capex Costs		
        public double XII { get; set; } //formula	9	ongoing fees (in USD)		
        public double XI { get; set; }	//formula	10	upfront fees (in USD)		
        public double BF { get; set; } //	Formula	11	Base Fee per project from which all other NVI fees are derived. This should be an NVI "Static Data" input only adjustable (and viewable) by NVI		
        public double BA { get; set; } //	Formula	12	Total Battery Capital costs for that project (BattC X total battery capacity designed)		
        public double C { get; set; } //.	Formula	13	Total Capital at risk		
        public double TS { get; set; } //	Formula	14	NPV of all tax shield payments.		
        public double MV { get; set; }//	Formula	15	Market Vale		
        public double TxV { get; set; } //	Formula	16	Tax Value of the System in the books of the client		
        public double ExC { get; set; }//	Formula	17	External Capital Requirements (Before upfront)		
        public double FPMT { get; set; } //	Formula	18	Fixed Monthly Payment due by Customer		
        public double Inv { get; set; } // 	Formula	19	Gross Invoice Price (Total incl. Batteries and before UP)		
        public double Insure { get; set; } //	Formula	20	Insurance Cost, which is calculated as a % of (A+ BA +Inst+Design). % Set by User Input		
        public double TF { get; set; } //formula	21	Tax factor applied to the tariff 		
        public double Up { get; set; } //Formula	21	The upfront requirement		
        public double Df { get; set; } //	Formula	22	Degredation Factor to adjust Real Revenues for degredation effect		
        public double DRF { get; set; } //Formula	23	The development fee components paid out as Series B Preference Shares		
        public double NRAT { get; set; } //	Formula	24	The Tax adjustment number		
        public double R { get; set; } //Formula	25	The Revenu number required to meet Ri		
        public double TFR { get; set; } //Formula	26	The Total Tariff rate applicable for the project		
        public double Margin { get; set; } //	Formula	27	The margin calculated between NVI costs and NVI invoice price at Country Level		
        public double MPV { get; set; } //	Formula	28	PV of margin payments		
        public double Rrev { get; set; } //	Formula	29	Real Annual Revenue required to generate Ri given Invest = PV		
        public double AM { get; set; } //	Formula	30	Annual Margin Payment (Series B) Payments From N years onwards over n 		
        public double FCF { get; set; } //	Formula	[Not currently Used]	Refers to a formula that can be usd to calculate the Real CF before Tax Shield for any year		
        public double TPr { get; set; } //	Formula	[Not used]	sum of technical partner revenue		
        public bool RR { get; set; } //Formula	[still to define]	If flagged as "Yes" - then the project requires review by NVI before it can be used to generate a proposal (the attributes will be outside pre-defined levels)		
        public double Exch { get; set; } //	External 		Exchange Rate between the currency of Tariff and equipment currencies		
        public double RCF { get; set; } //	Formula		Annual real Before Tax Cash flows for any year		
        public double Invest { get; set; } //	Formula		The total investment made by Investor	

        public double InsTotal { get; set; }
        public double CRF { get; set; } //Capital Recovery Factor
        public double ARF { get; set; } //Admin Recovery Factor
        public double IRF { get; set; } //Installation Recovery Factor

        public double OMRate { get; set; }
        public double OM { get; set; }
        public double t { get; set; }
        public double TFRDev { get; set; }
        public double ExistingPower { get; set; }

        public double UpInitial { get; set; }
        public double TotalEquipmentCostInDollars { get; set; }
        public Project Project { get; set; }

        public Country Country { get; set; }

        public NVIStatic NVIStatic { get; set; }
        public ProSpec Prospec { get; set; }


        public double prosize { get; set; }


        public double RI_Adj { get; set; }

        public double MU { get; set; }
        public double CMU { get; set; }
        public double sunnyIslandCap { get; set; }

        public double BattCap { get; set; }

        public double InverterWatt { get; set; }

        public double FP { get; set; }

        public List<DutyApplied> AppliedDuties = new List<DutyApplied>();
        //Equipment Stuff
        public List<InverterSub> _inverters = new List<InverterSub>();


        public List<BiInverterSub> _biInverters = new List<BiInverterSub>();



        public List<Basic> _mountingStructures = new List<Basic>();


        public List<Basic> _batteries = new List<Basic>();



        public List<Basic> _balanceSystems = new List<Basic>();


        public List<Basic> _pvmodules = new List<Basic>();




    








        /// <summary>
        /// NOTE all the percentages are doubles that are later devided by 100 in each formulae
        /// E.g Country.Ri can be 19.5
        /// In the formulae Country.Ri is divided by/100 in each case
        /// </summary>





        public SuperProject(Project project)
        {
            this.Project = project;
            this.NVIStatic = VarDB.NVIStatic.FirstOrDefault();
            this.Prospec = VarDB.Prospec.Find(project.Costing.Type);
            this.Country = VarDB.Countries.Find(project.Details.P_Country);

            Exch = this.Country.Currency_Per_Dollar;
            switch (project.Costing.Type)
            {
                case "Grid-Tied": Country.Ri = Country.Grid_Tied_Ri; break;
                case "Diesel-Hybrid": Country.Ri = Country.DieselH_Ri; break;
                case "Battery-Hybrid": Country.Ri = Country.BatteryH_Ri; break;
            }

            MU = (Project.Costing.UseEPC) ? 0 : Country.MU;
            CMU = (Project.Costing.UseEPC) ? 0 : Country.CMU;
            RI_Adj = Country.Ri + project.RIAdjustment;
            EquipCalc(this.Project);
            CalculateTotalAndShipping();
            //CalculateEquipment(this.Project);
            Process();

            Energy20();

        }

        public void Process()
        {


            switch (Project.Costing.Plan)
            {
                case "Pay-By-Solar": t = Country.TaxPBS / 100; break;
                case "Power Purchase Agreement": t = Country.TaxPPA / 100; break;
            }
            switch (Project.Costing.Source)
            {
                //existing power costs in inputted currency
                case "Grid-Tied": ExistingPower = Project.Costing.Tarrif; break; //1
                case "Diesel": ExistingPower = Project.Costing.DieselPrice / Project.Costing.GeneratorEfficiency / 100; break;
                case "Both": ExistingPower = ((Project.Costing.DieselPrice / Project.Costing.GeneratorEfficiency / 100) * Project.Costing.DieselPercent / 100) + ((Project.Costing.Tarrif) * (1 - Project.Costing.DieselPercent / 100)); break;
            }

            ////////////////////////////////////////////////////////////////////////////// ^_^ Fix all nulls



            //Calculate BA
            for (int a = 0; a < _batteries.Count; a++)
            {
                var ba = _batteries[a].totalCost;
                BA += ba;  //Battery Total Cost in dollars

                var btc = _batteries[a].Watt;
                BattCap += btc;
            }

            BattCap /= 1000; //now in  kilowatts

            for (int a = 0; a < _biInverters.Count; a++)
            {
                var sunny = _biInverters[a].Watt;
                sunnyIslandCap += sunny;
            }

            sunnyIslandCap /= 1000;
            for (int a = 0; a < _pvmodules.Count; a++)
            {
                var pvsi = _pvmodules[a].Watt;
                prosize += pvsi;  //Battery Total Cost in dollars
            }

            prosize /= 1000000; // watts -> kilowats (/1000) -> mwm(/1000)

            // ^_^


            ////////////////////////////////////////////////////////////////////////////// // ^_^

            //BF = (prosize < 0.5) ? prosize* 400 * Exch :( 225 * prosize + 87.5 ) * Exch; //Base fee in new currency

            ////////////////////////////////////////////////////////////////////////////// ^_^


            Ni = (1 + RI_Adj / 100) * (1 + Country.CPI / 100) - 1; //2


            ///////////////////////////////////////////////////////////// Ongoing Payments (NVI) DOLLAR  ^_^





            /////////////////////////////////////////////////////////// Admin Fee in NEW CURRENCY
            Admin = Project.AdminXII * (1 + Project.FAdmin / 100); //100%

            //OM rate

            Project.Costing.OMWorks /= 1000;
            OM = (Project.OMYN) ? Project.Costing.OMWorks : 0;

            if (Project.Costing.Yield <= 0 || prosize <= 0)
            {
                OMRate = 0.0;
            }
            else
            {
                OMRate = OM / (Project.Costing.Yield * prosize);


            }



            //A must be the total capex - batteries
            A = CalculateA() ;
            BA = CalculateBA();
            /////////////////////////////////////////////////////////////////////////////////////// XI Upfront

            XI = Project.UpfrontXI * (1 + Project.UpAdj / 100);


            /////////////////////////////////////////////////////////// Technical Partner Factor ^_^

            double _tpf = PMT(Country.Ri / 100 + NVIStatic.TPAF, 0, 100, Project.Costing.Term * 12);
            TPF = (_tpf * 12) / (Project.Costing.Yield * prosize);



            ///////////////////////////////////////////////////////////FPMT  Fixed Monthly payment batteries in NEW CURRENCY
            var _fpmtN = (Project.Costing.Plan == "Pay-By-Solar") ? Math.Min(NVIStatic.BattP, Project.Costing.Term) : NVIStatic.BattP;
            var _FPMTPMT = (BA * (1 + MU / 100) * t) / _fpmtN;
            var _FPMT = BA + NPV(Ni, _fpmtN, _FPMTPMT);
            var _FPMT_Two = PMT((RI_Adj / 100) / 12, 0, _FPMT, _fpmtN);

            FPMT = (_FPMT_Two / (1 - t));

            /////////////////////////////////////////////////////////// Capital ^_^


            // REPLACE A with total equipment - BA
            //REPLACE BA with total batteires
            double totalEquip = TotalEquipmentCostInDollars;
            //A = (Project.Costing.UseEPC) ? (Project.Costing.EPCPrice - BA) : (totalEquip - BA / Exch) * (1 + Project.adj_SolarCapexFee / 100) * Exch; //****ADD equipment selection totals here NEW CURRENCY
            //A /= 1000; // A 000'ccy


            A /= 1000;

            Project.Costing.DesignFee /= 1000;
            Project.Costing.InstallFee /= 1000;
            BA /= 1000;
            XI /= 1000;
            Admin /= 1000;
            Project.AdminXII /= 1000;
            Project.Costing.EPCPrice /= 1000;


            //^_^ A * 1 + MU + inst + design

            MV = (((1 + MU / 100) * A) + Project.Costing.InstallFee + Project.Costing.DesignFee) * (1 + CMU / 100); // NEW CURRENCY

            // ^_^   Total External Capital  NEW CURRENCY
            ExC = A + XI;



            ///////////////////////////////////////////// Installation Recovery Factor

            IRF = (Project.Costing.DesignFee + Project.Costing.InstallFee);

            NR = CRF + ARF + IRF;



            ///////////////////////////////////////// Upfront payment UP NEW CURRENCY

            switch (Project.Costing.Plan)
            {
                case "Power Purchase Agreement": Up = 0 + Project.UpAdj; break;
                case "Pay-By-Solar":
                    if (Project.Costing.UseEPC == false)
                    {
                        Up = Math.Max((2 * 1000 * prosize), MV);

                    }

                    else
                    {
                        Up = (Project.Costing.EPCPrice);


                    }

                    Up *= NVIStatic.PBSPerc / 100;

                    break;

                case "Solar Development Services":
                    Up = (A + BA + Project.Costing.InstallFee + Project.Costing.DesignFee + XI);
                    break;
            }
            UpInitial = Up * 1000;
            Up += Project.UpAdj / 1000; //Add the adjustment

            //upfront required in NEW currency


            ///////////////////////////////////////////////////////////////////// Tax Value NEW CURRENCY

            TxV = (MV - Up);
            //value x1 to x8... sum must equall 100%.. set in static and defined at project level
            //to work out tax shield ***

            if (Project.Costing.Plan == "Pay-By-Solar")
            {
                var _tsPMT = (TxV / Project.Costing.Term) * t;

                var _TS = NPV(Ni, Project.Costing.Term, _tsPMT);

                TS = _TS;

            }

            else if (Project.Costing.Plan == "Power Purchase Agreement")
            {

                for (int a = 1; a <= Math.Min(Project.Costing.Term, 8); a++)
                {
                    double xn = 0.00;
                    switch (a)
                    {
                        case 1: xn = Project.xn1; break;
                        case 2: xn = Project.xn2; break;
                        case 3: xn = Project.xn3; break;
                        case 4: xn = Project.xn4; break;
                        case 5: xn = Project.xn5; break;
                        case 6: xn = Project.xn6; break;
                        case 7: xn = Project.xn7; break;
                        case 8: xn = Project.xn8; break;

                    }

                    var xpow = Math.Pow((1 + Ni), a);
                    double cfn = (xn * TxV * t) / Math.Pow((1 + Ni), a);

                    TS += cfn;
                }

            }

            //////////////////////////////////////////////////////////////////////////
            TS *= -1;
            C = ExC - Up - TS; /// This needs to be checked NEW CURRENCY




            /////////////////////////////////////////////////////////////////////////////tax factor ^_^ (should be positive

            double _tx1 = 0;

            for (int i = 1; i <= Project.Costing.Term; i++)
            {
                //////need t value ( 1 - t)
                _tx1 += ((1 * Math.Pow((1 + Country.CPI / 100), i) * (1 - t)) * (1 - Country.DivTX / 100)) / Math.Pow((1 + Ni), i);
            }

            TF = PMT((RI_Adj / 100) / 12, 0, _tx1, Project.Costing.Term * 12) * 12;







            ///////////////////////////////////////////////////////////Degredation Factor Df ^_^

            double _dxNPV = 0;
            double x = 1;
            for (int i = 1; i <= Project.Costing.Term; i++)
            {
                if (i == 1)
                {
                    x = 1;
                }



                else
                {
                    var a = x;
                    x = (a) - NVIStatic.Deg / 100;
                }

                var B = (x / Math.Pow((1.00 + RI_Adj / 100), i));
                _dxNPV += B;





            }
            var _dxPMT = PMT(RI_Adj / 100, 0, _dxNPV, Project.Costing.Term);
            Df = _dxPMT;
            Df = Math.Round(Df, 2);

            ////////////////////////////////////////////////////////////////////////////////// Gross Invoice Price NEW currency





            Inv = (MV + (BA * (1 + MU / 100)));



            /////////////////////////////////////////////////////////////////////////// Insurance Per Annum

            InsTotal = (A + BA + Project.Costing.InstallFee + Project.Costing.DesignFee);
            InsTotal *= NVIStatic.InsR / 100;

            Insure = (this.Project.InsureYN) ? InsTotal : 0;




            ////////////////////////////////////////////////////////////// Development Fee Recovery (Series B) DRF ^_^

            double _DRFMarkup = Inv - (A + Project.Costing.InstallFee + Project.Costing.DesignFee + XI + Up + BA) / Exch;
            double _DRFPV = (_DRFMarkup / Project.Costing.Term) * ((1 - Math.Pow((1 + RI_Adj / 100), -Project.Costing.Term)) / RI_Adj / 100);
            double _DRFPMT = PMT(RI_Adj / 100, 0, _DRFPV, Project.Costing.Term);
            DRF = (_DRFPMT / (1 - Country.DivTX / 100));

            ///////////////////////////////////////////////////////////// Net Revenue After Tax NRAT ^_^


            ///////////////////////////////////////////////////////////// Revenue R ^_^ NEW CURRENCY

            double _R_A = NR * Exch + NRAT;
            double _R_B = Admin + Insure + OM;
            R = ((_R_A + _R_B) / Df);


            Invest = -(A + Project.Costing.InstallFee + XI + Project.Costing.DesignFee - Up);

            ////////////////////////////////////////////////////////////////////// Rrev

            if (prosize == 0.0)
            {
                prosize = 1;
            }

            //var rrevB = -PMT(RI_Adj/100, 0, -rrevA, Project.Costing.Term);
            TFRDev = (Admin + OM + Insure) / (prosize * Project.Costing.Yield);
            FP = (Project.Costing.Plan == "Solar Development Services") ? Project.real_FixedAnualCharge / 1000.00 + (TFRDev * prosize * Project.Costing.Yield) : Project.real_FixedAnualCharge / 1000.00;
            double iRen = 1.00 / 12.00;
            double rrevIo = Math.Pow((1 + RI_Adj / 100.00), (iRen));
            double rrevi = Math.Pow(1 + RI_Adj / 100.00, iRen) - 1;

            double rrev1 = -PMT(rrevi, 0, Invest, Project.Costing.Term * 12.00) * 12.00;
            double rrev2 = (rrev1 / TF) + Insure + Admin + OM;
            double rrev3 = -PMT(rrevi, 0, TS, Project.Costing.Term * 12.00) * 12.00;

            Rrev = rrev2 - rrev3;
            Rrev -= FP;
            //Rrev = rrevB + Insure + Admin + OM - Project.real_FixedAnualCharge/1000;  //OM is inputted in user currency already ADD - FP


            ///////////////////////////////////////////////////////////Total Tarrif TFR  ***

            TFR = Rrev / (Convert.ToDouble(Project.Costing.Yield) * prosize * Df * 1.00);

            if (Project.Costing.Plan == "Solar Development Services")
            {
                TFR = TFRDev;
            }


            if (double.IsInfinity(TFR))
            {
                TFR = 0;
            }
            /////////////////////////////////////////////////////////// Margin NEW CURRENCY

            Margin = ((A + BA) * MU / 100) - XI;


            /////////////////////////////////////////////////////////// annual series B payments NEW CURRENCY


            //try
            //{
            //    var marA = Margin * Math.Pow((1 + 18 / 100), 3);
            //    var div3 = (Project.Costing.Term - 3);
            //    if (div3 == 0)
            //    {
            //        div3 = 1;
            //    }

            //  AM = marA / div3;
            //}

            //catch(DivideByZeroException)
            //{
            //    var marA = Margin * Math.Pow((1 + 18 / 100), 3);
            //    AM = marA / 1;
            //}
            /////////////////////////////////////////////////////////// Net Present Value Margin Payments MPV NEW CURRENCY


            //double _MPV_NPV = NPV(Ni, Project.Costing.Term, AM);

            //for(int a = 1; a < 4; a++)
            //{
            //    _MPV_NPV -= AM / Math.Pow((1 + Ni), a);
            //}

            //MPV = _MPV_NPV;



            //multiply back to ccy
            A *= 1000.00; //++++ A 000'ccy
            Project.Costing.DesignFee *= 1000.00;
            Project.Costing.InstallFee *= 1000.00;
            BA *= 1000;
            XI *= 1000;
            Project.AdminXII *= 1000.00;
            Project.Costing.OMWorks *= 1000.00;
            Admin *= 1000;
            Project.Costing.EPCPrice *= 1000.00;

            FP *= 1000.00;
        }


        public double[] Tarrif20 { get; set; }
        public double[] SolarProduction20 { get; set; }
        public double[] AnnualVariable20 { get; set; }
        public double[] AnnualFixed20 { get; set; }
        public double[] TotalSolar20 { get; set; }
        public double[] ExistingEnergy20 { get; set; }
        public double[] AvoidedEnergy20 { get; set; }

        public double[] CumulativeSavings20 { get; set; }


        public void Energy20()
        {

            Tarrif20 = new double[20];
            SolarProduction20 = new double[20];
            AnnualVariable20 = new double[20];
            AnnualFixed20 = new double[20];
            TotalSolar20 = new double[20];
            ExistingEnergy20 = new double[20];
            AvoidedEnergy20 = new double[20];
            CumulativeSavings20 = new double[20];

            //set each initial value in the array

            SolarProduction20[0] = prosize * Project.Costing.Yield;
            Tarrif20[0] = TFR;
            AnnualVariable20[0] = SolarProduction20[0] * Tarrif20[0] * 1000.00;
            AnnualFixed20[0] = FP;
            TotalSolar20[0] = AnnualVariable20[0] + AnnualFixed20[0];
            ExistingEnergy20[0] = ExistingPower;
            AvoidedEnergy20[0] = SolarProduction20[0] * ExistingEnergy20[0] * 1000;
            CumulativeSavings20[0] = AvoidedEnergy20[0] - TotalSolar20[0] + 0; //0 for previous row
            //existing = Demand /1000 - solar generated ..prosize
            for (int a = 1; a < 20; a++)
            {
                SolarProduction20[a] = SolarProduction20[a - 1] * (1 - NVIStatic.Deg / 100);
                if (a == Project.Costing.Term + 1)
                {
                    //if the tarrif is calculated after the end of term, add in the discount
                    Tarrif20[a] = Tarrif20[a - 1] * (1 + Country.CPI / 100) * 30 / 100;
                    AnnualFixed20[a] = AnnualFixed20[a - 1] * (1 + Country.CPI / 100) * 30 / 100;
                }
                else
                {
                    Tarrif20[a] = Tarrif20[a - 1] * (1 + Country.CPI / 100);
                    AnnualFixed20[a] = AnnualFixed20[a - 1] * (1 + Country.CPI / 100);
                }

                AnnualVariable20[a] = SolarProduction20[a] * Tarrif20[a] * 1000.00;

                TotalSolar20[a] = AnnualVariable20[a] + AnnualFixed20[a];


                ExistingEnergy20[a] = ExistingEnergy20[a - 1] * (1 + Country.UtilityInflation / 100); //CPI utitlity 
                AvoidedEnergy20[a] = SolarProduction20[a] * ExistingEnergy20[a] * 1000.00;
                CumulativeSavings20[a] = AvoidedEnergy20[a] - TotalSolar20[a] + CumulativeSavings20[a - 1]; //0 for previous row
            }

        }





        //Equipment individual items are packaged into pallets, pallets are then packed into 20Ft or 40Ft containers
        //Equipment are shipped in 40 ft and 20ft from their country of origins to the port of entry of the project
        //The equipment is then transported to the site from the port along road/ charged per kilometer / container?? TODO check this


        //The equipment from each company can be packed together in 20ft and 40ft
        //The solution must be the most ergonomical
        //for instance 1 40ft is better than 1.5 20ft containers
        //round up all 20 ft containers.
        //Devided the total 20fts by 2, equavilanet in 40 ft
        //the remainder can either be rounded up to another 40ft or if < 0.5 a 20ft
        //The order will then be a conglomorate of 20ft + 40ft containers from a specific country, forming part of the Total BOQ of assets

        //Get equipment details + country of origin + taxes per subcategory
        //Apply tax details (FOB or CIF or CIF + VAT), if country of origin is destination country Marine insurance + transport = 0 <.> CIF = FOB
        //Apply VAT if neccessary
        //Pack all equipment of specific country of origin into 20ft containers
        //Devided 20ft containers = equivalent in 40ft
        //Deal with the remainder into another 40ft or 20ft    if < 0.5 add 20ft else 40ft
        //Summaries shipment details per country of origin, work out costs
        //Create a total order summary , add all shipping costs
        //Calculate total delivery cost from Port to site, charged in km / module or 20ft or 40ft 

        #region Shipping and Costing Variables

        public double TotalFOB = 0.0;  //all these values will be calculate first in USD then converterd into CCY
        public double TotalInsr = 0.0;
        public double TotalFrt = 0.0;
        public double TotalDutiesA = 0.0;
        public double TotalDutiesBa = 0.0;
        public double TotalTrans = 0.0;
        public double TotalTransBA = 0.0;
        public double TotalEq_A = 0.0; //all equipment without batteries
        public double TotalEq_BA = 0.0;//battery total
        public double TotalCIFVAT = 0.0;
        public int number_20fts = 0;
        public int number_40fts = 0;

        public double InverterPerc = 0.0;
        public double BiInvertPerc = 0.0;
        public double ModulesPerc = 0.0;
        public double FramingPerc = 0.0;
        public double BatteryPerc = 0.0;
        public double BalancePerc = 0.0;


        public double InverterFOB = 0.0;
        public double BiInvertFOB = 0.0;
        public double ModulesFOB = 0.0;
        public double FramingFOB = 0.0;
        public double BatteryFOB = 0.0;
        public double BalanceFOB = 0.0;

        public double InverterDuties = 0.0;
        public double BiInvertDuties = 0.0;
        public double ModulesDuties = 0.0;
        public double FramingDuties = 0.0;
        public double BatteryDuties = 0.0;
        public double BalanceDuties = 0.0;


        public double InverterInsr = 0.0;
        public double BiInvertInsr = 0.0;
        public double ModulesInsr = 0.0;
        public double FramingInsr = 0.0;
        public double BatteryInsr = 0.0;
        public double BalanceInsr = 0.0;


        public double InverterFrt = 0.0;
        public double BiInvertFrt = 0.0;
        public double ModulesFrt = 0.0;
        public double FramingFrt = 0.0;
        public double BatteryFrt = 0.0;
        public double BalanceFrt = 0.0;


        public double InverterCIF = 0.0;
        public double BiInvertCIF = 0.0;
        public double ModulesCIF = 0.0;
        public double FramingCIF = 0.0;
        public double BatteryCIF = 0.0;
        public double BalanceCIF = 0.0;


        public double InverterCIF_Vat = 0.0;
        public double BiInvertCIF_Vat = 0.0;
        public double ModulesCIF_Vat = 0.0;
        public double FramingCIF_Vat = 0.0;
        public double BatteryCIF_Vat = 0.0;
        public double BalanceCIF_Vat = 0.0;


        public List<Duty> allDuties = new List<Duty>();
        public List<Duty> dutiesToBeApplied = new List<Duty>();
        public List<Category> categoriesUsed = new List<Category>();

        #endregion
        public void CalculateTotalAndShipping()
        {
            TotalFOB = TotalEquipmentCostInDollars; //convert to ccy end/
            double VATRate = Country.VATRate / 100.00;
            Order_Details shippingInfo = new Order_Details();
            shippingInfo.CalculateEquipment(Project);

            double insrPerc = NVIStatic.MarineInsure / 100.00;

            TotalInsr = insrPerc * TotalFOB;
            TotalFrt = shippingInfo.order.Containers.Select(m => m.FreighCost).Sum(); //sum of all container costs 20ft and 40ft in USD


            #region Land Transport Costs
            double transport_20ft = Country.CCY_Per_20ftKM * Exch; //get value in dollars
            double transport_40ft = Country.CCY_Per_40ftKM * Exch;

            int numberOf20fts = shippingInfo.order.Containers.Where(m => m.Size == ContainerType.ft20).Count();
            int numberOf40fts = shippingInfo.order.Containers.Where(m => m.Size == ContainerType.ft40).Count();

            //TEMP sort mounting into 40fts or 20ft                        1,3              2,5
            int mounting20 = 0;
            double mounting40 = 0.00;

            mounting20 = Convert.ToInt32(Math.Round(Project.MountingContainersUsed20ft));

            mounting40 = mounting20 / 2; //1,5
            //rem = 5

            double rem = Math.IEEERemainder(mounting20, 2.00);
            mounting40 -= rem;
            mounting20 = 0;
            if (rem <= 0.5)
            {
                //add a 40ft
                mounting40 += 1;
            }

            else
            {
                //add a 20ft
                mounting20 += 1;
            }

            number_20fts = numberOf20fts;
            number_40fts = numberOf40fts;
            number_20fts += mounting20;
            number_40fts += Convert.ToInt32(mounting40);
            TotalTrans = (Project.Details.Distance * ((transport_20ft * numberOf20fts) + (transport_40ft * numberOf40fts))); //total inland transport in USD
            #endregion


            #region Sorting individual % , FOB , CIF and CIF + VAT
            //calculate individual percentages %

            InverterPerc = (InverterFOB / TotalFOB); //  0.015    = 1.5 %
            BiInvertPerc = (BiInvertFOB / TotalFOB);
            ModulesPerc = (ModulesFOB / TotalFOB);
            FramingPerc = (FramingFOB / TotalFOB);
            BalancePerc = (BalanceFOB / TotalFOB);
            BatteryPerc = (BatteryFOB / TotalFOB);

            //proportional insurance
            InverterInsr = TotalInsr * InverterPerc;
            BiInvertInsr = TotalInsr * BiInvertPerc;
            ModulesInsr = TotalInsr * ModulesPerc;
            FramingInsr = TotalInsr * FramingPerc;
            BatteryInsr = TotalInsr * BatteryPerc;
            BalanceInsr = TotalInsr * BalancePerc;
            TotalTransBA = TotalTrans * BatteryPerc;
            //proportional freight costs

            InverterFrt = TotalFrt * InverterPerc;
            BiInvertFrt = TotalFrt * BiInvertPerc;
            ModulesFrt = TotalFrt * ModulesPerc;
            FramingFrt = TotalFrt * FramingPerc;
            BatteryFrt = TotalFrt * BatteryPerc;
            BalanceFrt = TotalFrt * BalancePerc;

            //CIF Costs

            InverterCIF = InverterFOB + InverterFrt + InverterInsr;
            BiInvertCIF = BiInvertFOB + BiInvertFrt + BiInvertInsr;
            ModulesCIF = ModulesFOB + ModulesFrt + ModulesInsr;
            FramingCIF = FramingFOB + FramingFrt + FramingInsr;
            BatteryCIF = BatteryFOB + BatteryFrt + BatteryInsr;
            BalanceCIF = BalanceFOB + BalanceFrt + BalanceInsr;


            //CIF + VAT
            InverterCIF_Vat = InverterCIF * (1 + VATRate);
            BiInvertCIF_Vat = BiInvertCIF * (1 + VATRate);
            ModulesCIF_Vat = ModulesCIF * (1 + VATRate);
            FramingCIF_Vat = FramingCIF * (1 + VATRate);
            BatteryCIF_Vat = BatteryCIF * (1 + VATRate);
            BalanceCIF_Vat = BalanceCIF * (1 + VATRate);


            TotalCIFVAT = InverterCIF_Vat + BiInvertCIF_Vat + ModulesCIF_Vat + FramingCIF_Vat + BatteryCIF_Vat + BalanceCIF_Vat;


            #endregion


            #region Calculate Duties and Taxes per category


            //find all relevant duties. These can be found by category and if the duty applies to all countries || this country, apply it.

            allDuties = VarDB.Duties.ToList(); //all duties
            dutiesToBeApplied.AddRange(allDuties); // give the duties to be applied all duties, then remove not necessary ones


            //each equipment item has a category that determines what taxes apply to it
            //these categories are listed
            //Each category has 1 or  more taxes that apply
            //i need to get the duties per category, the ones that apply to this country

            // RDL (Modules)!RDL (Kenya)             category has 2 taxes applied

            //IDEA !! Cycle through categories, apply taxes, store results.. PREVENTS OVER-LAP
            foreach (Category category in categoriesUsed)
            {

                List<Duty> catsDuties = new List<Duty>();
                List<string> dutiesused = category.Taxes.Split('!').ToList();

                if (dutiesused.Count > 0)
                {
                    dutiesToBeApplied = dutiesToBeApplied.Where(m => m.AllCountries == true || m.Countries.Contains(Country.Id)).ToList(); //all duties applied to this country

                    catsDuties = dutiesToBeApplied.Where(m => dutiesused.Contains(m.Name)).ToList();


                }


                foreach (Duty dutyToApply in catsDuties)
                {
                    double TargetVALUE = 0.0;
                    double TargetFOB = 0.0;
                    double TargetCIF = 0.0;
                    double TargetCIF_VAT = 0.0;
                    double EndValue = 0.0;
                    string valueName = "FOB";
                    //apply the duty, changes values, record the adding 

                    //add it to the right category
                    //add it to that categories FOB or CIF or CIF + VAT

                    //InverterFrt
                    //BiInvertFrt
                    //ModulesFrt 
                    //FramingFrt 
                    //BatteryFrt 
                    //BalanceFrt 

                    switch (category.Name)
                    {
                        case "Inverters": TargetFOB = InverterFOB; TargetCIF = InverterCIF; TargetCIF_VAT = InverterCIF_Vat; break;
                        case "Mounting Structure": TargetFOB = FramingFOB; TargetCIF = FramingCIF; TargetCIF_VAT = FramingCIF_Vat; break;
                        case "Modules": TargetFOB = ModulesFOB; TargetCIF = ModulesCIF; TargetCIF_VAT = ModulesCIF_Vat; break;
                        case "Bi-Inverter": TargetFOB = BiInvertFOB; TargetCIF = BiInvertCIF; TargetCIF_VAT = BiInvertCIF_Vat; break;
                        case "Battery": TargetFOB = BatteryFOB; TargetCIF = BatteryCIF; TargetCIF_VAT = BatteryCIF_Vat; break;
                        case "Balance Of System": TargetFOB = BalanceFOB; TargetCIF = BalanceCIF; TargetCIF_VAT = BalanceCIF_Vat; break;
                    }

                    switch (dutyToApply.CustomsValue)
                    {
                        case "FOB": TargetVALUE = TargetFOB; break;
                        case "CIF": TargetVALUE = TargetCIF; break;
                        case "CIF + VAT": TargetVALUE = TargetCIF_VAT; break;
                    }

                    valueName = dutyToApply.CustomsValue;

                    EndValue = TargetVALUE * (dutyToApply.Rate / 100.00);



                    //seperate duties into categories
                    switch (category.Name)
                    {
                        case "Inverters": InverterDuties += EndValue; break;
                        case "Mounting Structure": FramingDuties += EndValue; break;
                        case "Modules": ModulesDuties += EndValue; break;
                        case "Bi-Inverter": BiInvertDuties += EndValue; break;
                        case "Battery": BatteryDuties += EndValue; break;
                        case "Balance Of System": BalanceDuties += EndValue; break;
                    }


                    DutyApplied duty_done = new DutyApplied()
                    {
                        Amount = EndValue / Exch,
                        Name = dutyToApply.Name,
                        Text = string.Format("{0} at {1} on {2} for {3}", dutyToApply.Name, dutyToApply.Rate.ToString() + "%", valueName, category.Name)
                    };

                    AppliedDuties.Add(duty_done);


                }

                TotalDutiesA = InverterDuties + FramingDuties + ModulesDuties + BiInvertDuties + BalanceDuties + BatteryDuties;
                TotalDutiesBa = BatteryDuties;

                TotalEq_BA = BatteryCIF_Vat + TotalDutiesBa + (TotalTrans * BatteryPerc);
                TotalEq_A = (TotalDutiesA + TotalTrans + TotalCIFVAT) - TotalEq_BA;

                TotalDutiesA -= BatteryDuties;




            }

            #endregion

            #region Convert All To CCY
            //Convert All to CCY
            TotalFOB /= Exch;
            TotalInsr /= Exch;
            TotalFrt /= Exch;
            TotalDutiesA /= Exch; ; //already in ccy
            TotalDutiesBa /= Exch; ;
            TotalTrans /= Exch;
            TotalTransBA /= Exch;
            TotalEq_A /= Exch;
            TotalEq_BA /= Exch;
            TotalCIFVAT /= Exch;
            #endregion



        }

        public void EquipCalc(Project pro)
        {
            TotalEquipmentCostInDollars = 0;

            #region Get inverters
            List<string> invIDs = (!string.IsNullOrEmpty(pro.InverterIDS)) ? pro.InverterIDS.Split(',').ToList() : new List<string>();
            List<string> invamounts = (!string.IsNullOrEmpty(pro.InverterAmounts)) ? pro.InverterAmounts.Split(',').ToList() : new List<string>();

            try
            {
                for (int a = 0; a < invIDs.Count; a++)
                {
                    string id = invIDs[a].Split('!')[0];
                    string config = invIDs[a].Split('!')[1];
                    string dcVol = invIDs[a].Split('!')[2];
                    string dcLoad = invIDs[a].Split('!')[3];
                    string io = invIDs[a].Split('!')[4];
                    string country = invIDs[a].Split('!')[5];

                    string Amount = invamounts[a].Split('!')[0];

                    int multiplier = 0;
                    Inverters inv = VarDB.Inverters.Find(id);
                    multiplier = 1;

                    Category cat = VarDB.Catergories.Where(m => m.Name == inv.SubCategory).First();
                    if (!categoriesUsed.Contains(cat))
                    {
                        categoriesUsed.Add(cat);
                    }
                    double exchangeToDollars = VarDB.Currencies.Find(inv.CCY).ValueInDollars;
                    InverterSub inver = new InverterSub()
                    {
                        Amount = int.Parse(Amount),
                        Id = id,
                        Configurations = config,
                        DCLoadDisconnect = dcLoad,
                        DCOverVoltage = dcVol,
                        IOPortModules = io,
                        CountrySpecificGrid = country,
                        totalCost = (double)(Convert.ToInt32(Amount) * multiplier * inv.ItemCost) * exchangeToDollars,
                        Description = inv.Description,
                        UOM = inv.UOM
                    };

                    InverterFOB += inver.totalCost;
                    TotalEquipmentCostInDollars += inver.totalCost;
                    InverterWatt += inver.Watt;
                    _inverters.Add(inver);
                }
            }
            catch (Exception)
            {
                //
            }

            #endregion

            #region Get Bi inverters
            List<string> BIinvIDs = (!string.IsNullOrEmpty(pro.BiInverterIDS)) ? pro.BiInverterIDS.Split(',').ToList() : new List<string>();
            List<string> biinvamounts = (!string.IsNullOrEmpty(pro.BiInverterAmounts)) ? pro.BiInverterAmounts.Split(',').ToList() : new List<string>();

            try
            {
                for (int a = 0; a < BIinvIDs.Count; a++)
                {
                    string id = BIinvIDs[a].Split('!')[0];
                    string coms = BIinvIDs[a].Split('!')[1];
                    string apps = BIinvIDs[a].Split('!')[2];
                    string inters = BIinvIDs[a].Split('!')[3];


                    string Amount = biinvamounts[a].Split('!')[0];

                    int multiplier = 0;
                    BiInverters inv = VarDB.BiInverters.Find(id);

                    double exchangeToDollars = VarDB.Currencies.Find(inv.CCY).ValueInDollars;
                    multiplier = 1;
                    Category cat = VarDB.Catergories.Where(m => m.Name == inv.SubCategory).First();
                    if (!categoriesUsed.Contains(cat))
                    {
                        categoriesUsed.Add(cat);
                    }
                    BiInverterSub inver = new BiInverterSub()
                    {
                        Amount = int.Parse(Amount),
                        Id = id,
                        Communication = coms,
                        Application = apps,
                        Interface = inters,
                        Watt = inv.UnitWP * multiplier * Convert.ToInt32(Amount),
                        totalCost = Convert.ToInt32(Amount) * multiplier * inv.ItemCost * exchangeToDollars,
                        Description = inv.Description,
                        UOM = inv.UOM

                    };

                    BiInvertFOB += inver.totalCost;
                    TotalEquipmentCostInDollars += inver.totalCost;

                    _biInverters.Add(inver);
                }
            }
            catch (Exception)
            {
                //
            }


            #endregion

            #region Get Mounting

            //List<string> mountIDS = (!string.IsNullOrEmpty(pro.MountingIDS)) ? pro.MountingIDS.Split(',').ToList() : new List<string>();
            //List<string> mountAmounts = (!string.IsNullOrEmpty(pro.MountingAmounts)) ? pro.MountingAmounts.Split(',').ToList() : new List<string>();

            //try
            //{
            //    for (int a = 0; a < mountIDS.Count; a++)
            //    {
            //        string id = mountIDS[a];

            //        string Amount = mountAmounts[a].Split('!')[0];


            //        MountingStructures inv = VarDB.MountingStructures.Find(id);
            //        double exchangeToDollars = VarDB.Currencies.Find(inv.CCY).ValueInDollars;
            //        Category cat = VarDB.Catergories.Where(m => m.Name == inv.SubCategory).First();
            //        if (!categoriesUsed.Contains(cat))
            //        {
            //            categoriesUsed.Add(cat);
            //        }
            //        Basic inver = new Basic()
            //        {
            //            Amount = int.Parse(Amount),
            //            Id = id,


            //            totalCost = Convert.ToInt32(Amount) * inv.ItemCost * exchangeToDollars
            //        };

            //        FramingFOB += inver.totalCost;
            //        TotalEquipmentCostInDollars += inver.totalCost;

            //        _mountingStructures.Add(inver);
            //    }
            //}
            //catch (Exception)
            //{
            //    //
            //}


            //TODO TEMP MOUNTING
            FramingFOB += Project.MountingInDollars;
            TotalEquipmentCostInDollars += Project.MountingInDollars;

            #endregion

            #region Get PV Modules


            List<string> pvModsIDs = (!string.IsNullOrEmpty(pro.PVModuleIDS)) ? pro.PVModuleIDS.Split(',').ToList() : new List<string>();
            List<string> pvAmounts = (!string.IsNullOrEmpty(pro.PvModuleAmounts)) ? pro.PvModuleAmounts.Split(',').ToList() : new List<string>();

            try
            {
                for (int a = 0; a < pvModsIDs.Count; a++)
                {
                    string id = pvModsIDs[a];


                    string Amount = pvAmounts[a].Split('!')[0];

                    int multiplier = 0;


                    PVModules inv = VarDB.PVModules.Find(id);
                    double exchangeToDollars = VarDB.Currencies.Find(inv.CCY).ValueInDollars;
                    multiplier = 1;
                    Category cat = VarDB.Catergories.Where(m => m.Name == inv.SubCategory).First();
                    if (!categoriesUsed.Contains(cat))
                    {
                        categoriesUsed.Add(cat);
                    }
                    Basic inver = new Basic()
                    {
                        Amount = int.Parse(Amount),
                        Id = id,

                        Watt = inv.UnitWP * int.Parse(Amount) * multiplier,
                        totalCost = Convert.ToInt32(Amount) * multiplier * inv.ItemCost * exchangeToDollars
                        ,
                        Description = inv.Description,
                        UOM = inv.UOM

                    };

                    ModulesFOB += inver.totalCost;
                    TotalEquipmentCostInDollars += inver.totalCost;

                    _pvmodules.Add(inver);
                }
            }
            catch (Exception)
            {
                //
            }

            #endregion

            #region Get Batteries
            List<string> batIds = (!string.IsNullOrEmpty(pro.BatteryIDS)) ? pro.BatteryIDS.Split(',').ToList() : new List<string>();
            List<string> batamounts = (!string.IsNullOrEmpty(pro.BatteryAmounts)) ? pro.BatteryAmounts.Split(',').ToList() : new List<string>();

            try
            {
                for (int a = 0; a < batIds.Count; a++)
                {
                    string id = batIds[a];


                    string Amount = batamounts[a].Split('!')[0];

                    int multiplier = 0;


                    Batteries inv = VarDB.Batteries.Find(id);
                    multiplier = 1;
                    double exchangeToDollars = VarDB.Currencies.Find(inv.CCY).ValueInDollars;
                    Category cat = VarDB.Catergories.Where(m => m.Name == inv.SubCategory).First();
                    if (!categoriesUsed.Contains(cat))
                    {
                        categoriesUsed.Add(cat);
                    }
                    Basic inver = new Basic()
                    {
                        Amount = int.Parse(Amount),
                        Id = id,

                        Watt = inv.UnitAh_C10 * multiplier * Convert.ToInt32(Amount),
                        totalCost = Convert.ToInt32(Amount) * multiplier * inv.ItemCost * exchangeToDollars,
                        Description = inv.Description,
                        UOM = inv.UOM

                    };

                    BatteryFOB += inver.totalCost;
                    TotalEquipmentCostInDollars += inver.totalCost;

                    _batteries.Add(inver);
                }
            }
            catch (Exception)
            {
                //
            }

            #endregion

            #region Get Balance of system

            List<string> balanceIds = (!string.IsNullOrEmpty(pro.BalanceSystemIDS)) ? pro.BalanceSystemIDS.Split(',').ToList() : new List<string>();
            List<string> balanceAmounts = (!string.IsNullOrEmpty(pro.BalanceSystemAmounts)) ? pro.BalanceSystemAmounts.Split(',').ToList() : new List<string>();

            try
            {
                for (int a = 0; a < balanceIds.Count; a++)
                {
                    string id = balanceIds[a].Split('!')[0];
                    string type = balanceIds[a].Split('!')[1];


                    string Amount = balanceAmounts[a].Split('!')[0];

                    int multiplier = 0;


                    BalanceSystem inv = VarDB.BalanceSystems.Find(id);
                    multiplier = 1;
                    double exchangeToDollars = VarDB.Currencies.Find(inv.CCY).ValueInDollars;
                    Category cat = VarDB.Catergories.Where(m => m.Name == inv.SubCategory).First();
                    if (!categoriesUsed.Contains(cat))
                    {
                        categoriesUsed.Add(cat);
                    }
                    Basic inver = new Basic()
                    {
                        Amount = int.Parse(Amount),
                        Id = id,

                        ParentList = inv.Type,
                        totalCost = Convert.ToInt32(Amount) * multiplier * inv.ItemCost * exchangeToDollars,
                        Description = inv.Description
                       ,
                        UOM = inv.UOM

                    };

                    BalanceFOB += inver.totalCost;
                    TotalEquipmentCostInDollars += inver.totalCost;


                    _balanceSystems.Add(inver);
                }
            }
            catch (Exception)
            {
                //
            }
            #endregion
        }

        public Project PopulateCosts(Project pro)
        {

        
            if (string.IsNullOrWhiteSpace(pro.QuotingCostNames))
            {
                //no information, populate first run
                pro.Initialize();
            }

            //"Solar Modules,Inverters,Framing,Bi-Directional Inverters,Batteries,Balance of System,Installation & Labour Costs,Design Costs";
            QuotingCost cost = pro.GetCost("Solar Modules");
            cost.Cost = ModulesFOB;
            pro.UpdateLineItem(cost);



            cost = pro.GetCost("Inverters");
            cost.Cost = InverterCIF_Vat;
            pro.UpdateLineItem(cost);

            cost = pro.GetCost("Framing");
            cost.Cost = FramingCIF_Vat;
            pro.UpdateLineItem(cost);


            cost = pro.GetCost("Bi-Directional Inverters");
            cost.Cost = BiInvertCIF_Vat;
            pro.UpdateLineItem(cost);

            cost = pro.GetCost("Batteries");
            cost.Cost = BatteryCIF_Vat;
            pro.UpdateLineItem(cost);

            cost = pro.GetCost("Balance of System");
            cost.Cost = BalanceCIF_Vat;
            pro.UpdateLineItem(cost);


       

            cost = pro.GetCost("Solar Modules");
            cost.Cost = ModulesCIF_Vat;
            pro.UpdateLineItem(cost);

       
            return pro;

        }



        public double NPV(double i, double n, double pmt)
        {
            double rets = 0;

            for (int a = 1; a <= n; a++)
            {
                rets += pmt / Math.Pow((1 + i), a);
            }
            return rets;
        }

        public double PMT(double i, double FV, double PV, double n)
        {


            double result = (PV * i * Math.Pow((1 + i), n)) / (Math.Pow((1 + i), n) - 1);
            return result;
        }

        //Equipment costing flow
        //Calculate number of containers (20ft and 40ft) per country
        //Calculate the transport cost for shipping
        //calculate the transport cost for land
        //Split the total tranport costs between the units in the container , increase their cost
        //Apply duties and taxes to either the



        public double CalculateA()
        {
            double a = 0;

            //get all equipment without BA
            //get all transport without BA
            //Add all transport
            try
            {
                double modules = ModulesCIF_Vat * (1 + Project.GetCost("Solar Modules").Markup / 100);
                double inverters = ModulesCIF_Vat * (1 + Project.GetCost("Inverters").Markup / 100);
                double biinverters = ModulesCIF_Vat * (1 + Project.GetCost("Bi-Directional Inverters").Markup / 100);
                double balance = ModulesCIF_Vat * (1 + Project.GetCost("Balance of System").Markup / 100);

                double totalEquipment = modules + inverters + biinverters + balance + (Project.MountingInDollars * 1 / Country.Currency_Per_Dollar);

                a = TotalDutiesA + totalEquipment + TotalTrans - TotalTransBA;
            }

            catch
            {

            }
     
            //get all duties


            return a;
        }

        public double CalculateBA()
        {
            double ba = 0;


            try
            {
                double batteries = ModulesCIF_Vat * (1 + Project.GetCost("Batteries").Markup / 100);

                ba = batteries + TotalDutiesBa + TotalTransBA;
            }

            catch
            {

            }
           

            return ba;
        }

    }


 
    public class PaybackCalc
    {

        public double[] Row1 { get; set; }
        public double[] Row2 { get; set; }
        public PaybackCalc(double price, double[] payments)
        {
            //price to negative over a thousand


        }
    }
}