﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVIDev.Models
{
    public class QuoteModel
    {
        public string ProjectType { get; set; }
        public double Tariff { get; set; } //currency per KWH

        public double OMRate { get; set; }
        public double OMWork { get; set; }
        public double TPRebate { get; set; }
        public double UpfrontPay { get; set; }
        public double StorageCharge { get; set; }

        public double FixedMonthlyCharges { get; set; }
        public double EstInsurance { get; set; }
        ////////////////////////////////////////////
     public   bool inclOM { get; set; }
     public bool inIn { get; set; }
        //Project Indicators

        public double SystemSize { get; set; }
        public string InstallType { get; set; }
        public double ExpEnergyYield { get; set; }
        public int Term { get; set; }
        public double ExchangeRate { get; set; }

        public double  InstallationQuote { get; set; }
        public double  DesignQuote { get; set; }
        public int  BatterySize { get; set; }
        public int  SunnyIslandCap { get; set; }
        public string Country { get; set; }

        public double TPCosts { get; set; }
    }
}