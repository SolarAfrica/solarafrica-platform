﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NVIDev.Models
{
    public class Project
    {
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Code { get; set; }

        [DefaultValue(0)]
        public double AAdjustment { get; set; } //NVI can edit
        public double FAdjustment { get; set; }
        public double RIAdjustment { get; set; }
        public double UpAdj { get; set; } //upfront user fee %
        public double FUP { get; set; } //ongoing %  Fee
        public double FAdmin { get; set; } //ongoing Admin Fee XI and XII for each Project
        public bool RR { get; set; }
        public string CurrentUserID { get; set; }

        public string CreatorID { get; set; }
        public double Revision { get; set; }

        public double In_Ri { get; set; }
        public double AdminXII { get; set; }
        public double UpfrontXI { get; set; }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////

        [DefaultValue(true)]
        public bool UsePrelim { get; set; }
        [DefaultValue(true)]

        public bool UseProcurement { get; set; }
        [DefaultValue(true)]

        public bool UseWorkingCap { get; set; }
        [DefaultValue(true)]

        public bool UsePrepVet { get; set; }
        [DefaultValue(true)]

        public bool UseTechVet { get; set; }
        [DefaultValue(true)]

        public bool UsePerformanceG { get; set; }
        [DefaultValue(true)]

        public bool UseProjectManage { get; set; }
        [DefaultValue(true)]

        public bool UseCommisioning { get; set; }
        [DefaultValue(true)]

        public bool UseMargin { get; set; }
        [DefaultValue(true)]

        public bool UseMonitorSoftware { get; set; }
        [DefaultValue(true)]

        public bool UseAdmin { get; set; }
        [DefaultValue(true)]

        public bool UseAssetManagement { get; set; }



        //Items that are adjusted
        public double in_SolarCapexFee { get; set; }
        public double adj_SolarCapexFee { get; set; }

        public double real_SolarCapexFee { get; set; }

        public double in_UpfrontFee { get; set; }
        public double real_UpfrontFee { get; set; }

        public double in_AdminFee { get; set; }
        public double real_AdminFee { get; set; }

        public double in_UpfrontClient { get; set; }
        public double real_UpfrontClient { get; set; }


        public double real_FixedAnualCharge { get; set; }

        public double real_RI { get; set; }
        ///////////////////////////////////////////
        public Details Details { get; set; }
        public Costing Costing { get; set; }

        public bool Valid { get; set; }


        public bool InsureYN { get; set; }
        public bool OMYN { get; set; }


        //Equipment IDS
        public string InverterIDS { get; set; }
        public string InverterAmounts { get; set; }
        public string BiInverterIDS { get; set; }
        public string BiInverterAmounts { get; set; }
        public string MountingIDS { get; set; }
        public string MountingAmounts { get; set; }
        public string PVModuleIDS { get; set; }
        public string PvModuleAmounts { get; set; }
        public string BatteryIDS { get; set; }
        public string BatteryAmounts { get; set; }
        public string BalanceSystemIDS { get; set; }
        public string BalanceSystemAmounts { get; set; }

        //temporary fix NVI mounting

        public double MountingInDollars { get; set; } //mounting costs

        public double MountingContainersUsed20ft { get; set; } //number of containers



        //quoting section things   all deliminated by , 

        public string QuotingCostNames { get; set; }
        public string QuotingCostMains { get; set; }
        public string QuotingCostSuppliers { get; set; }
        public string QuotingCostCosts { get; set; }
        public string QuotingCostMarkups { get; set; }
        public string QuotingCostSellPrices { get; set; }
        public string QuotingCostWPMarks { get; set; }
        public string QuotingCostFixeds { get; set; }


        [DefaultValue(0.125)]
        public double xn1 { get; set; }
        [DefaultValue(0.125)]
        public double xn2 { get; set; }
        [DefaultValue(0.125)]
        public double xn3 { get; set; }
        [DefaultValue(0.125)]
        public double xn4 { get; set; }
        [DefaultValue(0.125)]
        public double xn5 { get; set; }
        [DefaultValue(0.125)]
        public double xn6 { get; set; }
        [DefaultValue(0.125)]
        public double xn7 { get; set; }
        [DefaultValue(0.125)]
        public double xn8 { get; set; }
        public Project()
        {
            Details = new Details();
            Costing = new Costing();
        }


        public void UpdateLineItem(QuotingCost cost)
        {

            int index = 0;


            List<string> _QuotingCostNames = QuotingCostNames.Split(',').ToList();
            List<string> _QuotingCostMains = QuotingCostMains.Split(',').ToList();
            List<string> _QuotingCostSuppliers = QuotingCostSuppliers.Split(',').ToList();
            List<string> _QuotingCostCosts = QuotingCostCosts.Split(',').ToList();
            List<string> _QuotingCostMarkups = QuotingCostMarkups.Split(',').ToList();
            List<string> _QuotingCostSellPrices = QuotingCostSellPrices.Split(',').ToList();
            List<string> _QuotingCostWPMarks = QuotingCostWPMarks.Split(',').ToList();
            List<string> _QuotingCostFixeds = QuotingCostFixeds.Split(',').ToList();


            index = _QuotingCostNames.IndexOf(cost.Name);

            _QuotingCostMains[index] = cost.Main.ToString();
            _QuotingCostSuppliers[index] = cost.Supplier;
            _QuotingCostCosts[index] = cost.Cost.ToString();
            _QuotingCostMarkups[index] = cost.Markup.ToString();
            _QuotingCostSellPrices[index] = (cost.Cost * (1 + cost.Markup / 100)).ToString();
            _QuotingCostWPMarks[index] = cost.WPMark;
            _QuotingCostFixeds[index] = cost.Fixed.ToString();



            QuotingCostMains = string.Join(",", _QuotingCostMains);
            QuotingCostSuppliers = string.Join(",", _QuotingCostSuppliers);
            QuotingCostCosts = string.Join(",", _QuotingCostCosts);
            QuotingCostMarkups = string.Join(",", _QuotingCostMarkups);
            QuotingCostSellPrices = string.Join(",", _QuotingCostSellPrices);
            QuotingCostWPMarks = string.Join(",", _QuotingCostWPMarks);
            QuotingCostFixeds = string.Join(",", _QuotingCostFixeds);



        }

        public QuotingCost GetCost(string name)
        {
            try
            {
                QuotingCost model = new QuotingCost();
                int index = 0;


                List<string> _QuotingCostNames = QuotingCostNames.Split(',').ToList();
                List<string> _QuotingCostMains = QuotingCostMains.Split(',').ToList();
                List<string> _QuotingCostSuppliers = QuotingCostSuppliers.Split(',').ToList();
                List<string> _QuotingCostCosts = QuotingCostCosts.Split(',').ToList();
                List<string> _QuotingCostMarkups = QuotingCostMarkups.Split(',').ToList();
                List<string> _QuotingCostSellPrices = QuotingCostSellPrices.Split(',').ToList();
                List<string> _QuotingCostWPMarks = QuotingCostWPMarks.Split(',').ToList();
                List<string> _QuotingCostFixeds = QuotingCostFixeds.Split(',').ToList();


                index = _QuotingCostNames.IndexOf(name);



                model.Name = name;
                model.Main = bool.Parse(_QuotingCostMains[index]);
                model.Supplier = model.Main == true ? _QuotingCostSuppliers[index] : "NA";
                model.Cost = double.Parse(_QuotingCostCosts[index]);
                model.Markup = double.Parse(_QuotingCostMarkups[index]);
                model.SellPrice = (model.Cost * model.Markup / 100);
                model.WPMark = _QuotingCostWPMarks[index];
                model.Fixed = bool.Parse(_QuotingCostFixeds[index]);
                return model;
            }

            catch
            {
                return null;
            }

        }

        public bool RemoveCost(string name)
        {

            int index =-1;
            List<string> _QuotingCostNames = QuotingCostNames.Split(',').ToList();
            List<string> _QuotingCostMains = QuotingCostMains.Split(',').ToList();
            List<string> _QuotingCostSuppliers = QuotingCostSuppliers.Split(',').ToList();
            List<string> _QuotingCostCosts = QuotingCostCosts.Split(',').ToList();
            List<string> _QuotingCostMarkups = QuotingCostMarkups.Split(',').ToList();
            List<string> _QuotingCostSellPrices = QuotingCostSellPrices.Split(',').ToList();
            List<string> _QuotingCostWPMarks = QuotingCostWPMarks.Split(',').ToList();
            List<string> _QuotingCostFixeds = QuotingCostFixeds.Split(',').ToList();


            for(int a = 0; a< _QuotingCostNames.Count; a++)
            {
                if(_QuotingCostNames[a] == name)
                {
                    index = a; break;
                }
            }

            if(index < 0)
            {
                return false; //does not exist
            }

            _QuotingCostNames.Remove(_QuotingCostNames[index]);
            _QuotingCostMains.Remove(_QuotingCostMains[index]);
            _QuotingCostSuppliers.Remove(_QuotingCostSuppliers[index]);
            _QuotingCostCosts.Remove(_QuotingCostCosts[index]);
            _QuotingCostMarkups.Remove(_QuotingCostMarkups[index]);
            _QuotingCostSellPrices.Remove(_QuotingCostSellPrices[index]);
            _QuotingCostWPMarks.Remove(_QuotingCostWPMarks[index]);
        _QuotingCostFixeds.Remove(_QuotingCostFixeds[index]);


            QuotingCostNames = string.Join(",", _QuotingCostNames);
            QuotingCostMains = string.Join(",", _QuotingCostMains);
            QuotingCostSuppliers = string.Join(",", _QuotingCostSuppliers);
            QuotingCostCosts = string.Join(",", _QuotingCostCosts);
            QuotingCostMarkups = string.Join(",", _QuotingCostMarkups);
            QuotingCostSellPrices = string.Join(",", _QuotingCostSellPrices);
            QuotingCostWPMarks = string.Join(",", _QuotingCostWPMarks);
            QuotingCostFixeds = string.Join(",", _QuotingCostFixeds);



            return true;
        }



        public void Initialize()
        {
            //6 initial fields
            QuotingCostNames = "Solar Modules,Inverters,Framing,Bi-Directional Inverters,Batteries,Balance of System";
            QuotingCostMains = "true,true,true,true,true,true,false,false,false";
            QuotingCostSuppliers = "SolarAfrica,SolarAfrica,SolarAfrica,SolarAfrica,SolarAfrica,SolarAfrica";
            QuotingCostCosts = "0.00,0.00,0.00,0.00,0.00,0.00";
            QuotingCostMarkups = "0.00,0.00,0.00,0.00,0.00,0.00";
            QuotingCostSellPrices = "0.00,0.00,0.00,0.00,0.00,0.00";
            QuotingCostWPMarks = "Markup,Markup,Markup,Markup,Markup,Markup";
            QuotingCostFixeds = "false,false,false,false,false,false";

        }

        public double UtilityInflation1 { get; set; }
        public double UtilityInflation2 { get; set; }
        public double UtilityInflation3 { get; set; }
        public double UtilityInflation4 { get; set; }
        public double UtilityInflation5 { get; set; }
        public double UtilityInflation6 { get; set; }
        public double UtilityInflation7 { get; set; }
        public double UtilityInflation8 { get; set; }
        public double UtilityInflation9 { get; set; }
        public double UtilityInflation10 { get; set; }
        public double UtilityInflation11 { get; set; }
        public double UtilityInflation12 { get; set; }
        public double UtilityInflation13 { get; set; }
        public double UtilityInflation14 { get; set; }
        public double UtilityInflation15 { get; set; }
        public double UtilityInflation16 { get; set; }
        public double UtilityInflation17 { get; set; }
        public double UtilityInflation18 { get; set; }
        public double UtilityInflation19 { get; set; }
        public double UtilityInflation20 { get; set; }

        public bool isDetailsDone()
        {
            if (string.IsNullOrWhiteSpace(Details.Abbreviation) || string.IsNullOrWhiteSpace(Details.ProjectName) ||   string.IsNullOrWhiteSpace(Details.P_City)   || string.IsNullOrWhiteSpace(Details.P_Site) || Details.Distance <= 0)
            {
                return false;
            }

            return true;
        }
        public bool isEnergyDone()
        {
            if (Costing.ClientDemand1 == 0)
            {
                Valid = false;
                return false;
            }

            Valid = true;
            return true;
        }


        public bool isEquipmentDone()
        {
            if (string.IsNullOrWhiteSpace(PvModuleAmounts))
            {
                Valid = false;
                return false;
            }

            else
            {
                return true;
            }


        }

        public bool isQuotingDone()
        {
            if (Costing == null)
            {
                Valid = false;
                return false;
            }

            else
            {
                return true;
            }
        }


        public bool isReviewDone()
        {

            if (UpfrontXI <= 0 || real_AdminFee <= 0 || real_SolarCapexFee <= 0)
            {
                Valid = false;
                return false;
            }

            else
            {
                return true;
            }

        }
        public bool isFinanceDone()
        {

            if (Costing.Term <= 0 || Costing == null || string.IsNullOrWhiteSpace(Costing.Plan))
            {
                Valid = false;
                return false;
            }

            else
            {
                return true;
            }

        }


        #region Population
        public void Populate(Project other)
        {

            this.Details = new Details();
            this.Costing = new Costing();

            this.Details.Populate(other);
            this.Costing.Populate(other);




            #region Project
            Id = other.Id;
            Code = other.Code;
            AAdjustment = other.AAdjustment;
            FAdjustment = other.FAdjustment;
            RIAdjustment = other.RIAdjustment;
            UpAdj = other.UpAdj;
            FUP = other.FUP;
            FAdmin = other.FAdmin;
            RR = other.RR;
            CurrentUserID = other.CurrentUserID;
            CreatorID = other.CreatorID;
            Revision = other.Revision;
            In_Ri = other.In_Ri;
            AdminXII = other.AdminXII;
            UpfrontXI = other.UpfrontXI;
            UsePrelim = other.UsePrelim;
            UseProcurement = other.UseProcurement;
            UseWorkingCap = other.UseProjectManage;
            UsePrepVet = other.UsePrepVet;
            UseTechVet = other.UseTechVet;
            UsePerformanceG = other.UsePerformanceG;
            UseProjectManage = other.UseProjectManage;
            UseCommisioning = other.UseCommisioning;
            UseMargin = other.UseMargin;
            UseMonitorSoftware = other.UseMonitorSoftware;
            UseAdmin = other.UseAdmin;
            UseAssetManagement = other.UseAssetManagement;
            in_SolarCapexFee = other.in_SolarCapexFee;
            adj_SolarCapexFee = other.adj_SolarCapexFee;
            real_SolarCapexFee = other.real_SolarCapexFee;
            in_UpfrontFee = other.in_UpfrontFee;
            real_UpfrontFee = other.real_UpfrontFee;
            in_AdminFee = other.in_AdminFee;
            real_AdminFee = other.real_AdminFee;
            in_UpfrontClient = other.real_UpfrontClient;
            real_UpfrontClient = other.real_UpfrontClient;
            real_FixedAnualCharge = other.real_FixedAnualCharge;
            real_RI = other.real_RI;



            Valid = other.Valid;
            InsureYN = other.InsureYN;
            OMYN = other.OMYN;
            InverterIDS = other.InverterIDS;
            InverterAmounts = other.InverterAmounts;
            BiInverterIDS = other.BiInverterIDS;
            BiInverterAmounts = other.BiInverterAmounts;
            MountingIDS = other.MountingIDS;
            MountingAmounts = other.MountingAmounts;
            PVModuleIDS = other.PVModuleIDS;
            PvModuleAmounts = other.PvModuleAmounts;
            BatteryIDS = other.BatteryIDS;
            BatteryAmounts = other.BatteryAmounts;
            BalanceSystemIDS = other.BalanceSystemIDS;
            BalanceSystemAmounts = other.BalanceSystemAmounts;
            MountingInDollars = other.MountingInDollars;
            MountingContainersUsed20ft = other.MountingContainersUsed20ft;
            xn1 = other.xn1;
            xn2 = other.xn2;
            xn3 = other.xn3;
            xn4 = other.xn4;
            xn5 = other.xn5;
            xn6 = other.xn6;
            xn7 = other.xn7;
            xn8 = other.xn8;
            UtilityInflation1 = other.UtilityInflation1;
            UtilityInflation2 = other.UtilityInflation2;
            UtilityInflation3 = other.UtilityInflation3;
            UtilityInflation4 = other.UtilityInflation4;
            UtilityInflation5 = other.UtilityInflation5;
            UtilityInflation6 = other.UtilityInflation6;
            UtilityInflation7 = other.UtilityInflation7;
            UtilityInflation8 = other.UtilityInflation8;
            UtilityInflation9 = other.UtilityInflation9;
            UtilityInflation10 = other.UtilityInflation10;
            UtilityInflation11 = other.UtilityInflation11;
            UtilityInflation12 = other.UtilityInflation12;
            UtilityInflation13 = other.UtilityInflation13;
            UtilityInflation14 = other.UtilityInflation14;
            UtilityInflation15 = other.UtilityInflation15;
            UtilityInflation16 = other.UtilityInflation16;
            UtilityInflation17 = other.UtilityInflation17;
            UtilityInflation18 = other.UtilityInflation18;
            UtilityInflation19 = other.UtilityInflation19;
            UtilityInflation20 = other.UtilityInflation20;

            #endregion


        }


        public void Fees(FeeViewModel model)
        {
            this.UseAdmin = model.project.UseAdmin;
            this.UseAssetManagement = model.project.UseAssetManagement;
            this.UseCommisioning = model.project.UseCommisioning;
            this.UseMargin = model.project.UseMargin;
            this.UseMonitorSoftware = model.project.UseMonitorSoftware;
            this.UsePerformanceG = model.project.UsePerformanceG;
            this.UsePrelim = model.project.UsePrelim;
            this.UsePrepVet = model.project.UsePrepVet;
            this.UseProcurement = model.project.UseProcurement;
            this.UseProjectManage = model.project.UseProjectManage;
            this.UseTechVet = model.project.UseTechVet;
            this.UseWorkingCap = model.project.UseWorkingCap;


            this.in_UpfrontClient = model.superPro.UpInitial;
            this.in_UpfrontFee = model.Upfront;
            this.in_AdminFee = model.OnGoingFee_Admin;
            this.AdminXII = model.OnGoingFee_Admin;
            this.UpfrontXI = model.Upfront;
        }

        #endregion

    }

    public class Details
    {
        [Required(ErrorMessage = "Required")]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "Required")]
        public string ProjectName { get; set; }

        [Required(ErrorMessage = "Required")]
        //port of equipment delivery

        public string PortOfEntry { get; set; }

        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Must be no longer than 3 letters.")]

        public string Abbreviation { get; set; }





        //Contact Details (Person)      
        [Required(ErrorMessage = "Required")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Required")]
        public string Initial { get; set; }
        [Required(ErrorMessage = "Required")]

        public string Surname { get; set; }
        [Required(ErrorMessage = "Required")]
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "Required")]
        public string Telephone { get; set; }



        //Project Details
        [Required(ErrorMessage = "Required")]
        public string P_Site { get; set; }
        [Required(ErrorMessage = "Required")]
        public string P_Country { get; set; }
        [Required(ErrorMessage = "Required")]
        public string P_City { get; set; }
        [Required(ErrorMessage = "Required")]
        public string P_Road { get; set; }

        public string GPSCoords { get; set; } //gps location for google maps API

        [Required(ErrorMessage = "Please calculate the distance")]

        public double Distance { get; set; }


        public void Populate(Project other)
        {

            CompanyName = other.Details.CompanyName;
            ProjectName = other.Details.CompanyName;
            PortOfEntry = other.Details.PortOfEntry;
            Abbreviation = other.Details.Abbreviation;
            Title = other.Details.Title;
            Initial = other.Details.Initial;
            Surname = other.Details.Surname;
            EmailAddress = other.Details.EmailAddress;
            Telephone = other.Details.Telephone;
            P_Site = other.Details.P_Site;
            P_Country = other.Details.P_Country;
            P_City = other.Details.P_City;
            P_Road = other.Details.P_Road;
            GPSCoords = other.Details.GPSCoords;
            Distance = other.Details.Distance;
        }



    }

    public class Costing
    {
        public string Plan { get; set; }
        public string Type { get; set; }



        public string Country { get; set; }

        public string Source { get; set; }
        public double Yield { get; set; }
        public int Term { get; set; } //n


        public double ElectricUsagePYear { get; set; }
        public double GeneratorEfficiency { get; set; }
        public double Structure { get; set; }

        public double Demand { get; set; } //Clients Demand
        /////////////////

        public double DieselPrice { get; set; }
        public double DieselConsumer { get; set; } //litres
        public double DieselPercent { get; set; } //what percent of diesel does it use to make total output

        //Technical Partner Stuff

        public bool Generic { get; set; }
        public bool UseEPC { get; set; }

        public double EPCPrice { get; set; }

        //might replace this with a class
        public string TechnicalPartner { get; set; }

        public double InstallFee { get; set; }
        public double DesignFee { get; set; }
        public double OMWorks { get; set; }
        public double TPShare { get; set; }
        public bool TaxFlow { get; set; }

        public double Rebate { get; set; }

        //

        [Range(0, double.MaxValue, ErrorMessage = "The value must be greater than 0")]

        public double Tarrif { get; set; }
        public double BaseRate { get; set; }
        public double TPRate { get; set; }
        public double OMRate { get; set; }
        public double StorageCost { get; set; } //month



        /////// Project Stats

        public double IRRProject { get; set; } //Percentage
        public double Contingency { get; set; } //Percentage Positive means

        //
        public double ExchangeRate { get; set; }
        public string Currenct { get; set; }
        public double Ri { get; set; }

        public Costing()
        {
            TaxFlow = true;
        }
        public bool ApplyForAllMonths { get; set; } //if yes, all  months get the value of
        //client demand for each month
        public double ClientDemand1 { get; set; }
        public double ClientDemand2 { get; set; }
        public double ClientDemand3 { get; set; }
        public double ClientDemand4 { get; set; }
        public double ClientDemand5 { get; set; }
        public double ClientDemand6 { get; set; }
        public double ClientDemand7 { get; set; }
        public double ClientDemand8 { get; set; }
        public double ClientDemand9 { get; set; }
        public double ClientDemand10 { get; set; }
        public double ClientDemand11 { get; set; }
        public double ClientDemand12 { get; set; }



        public void Populate(Project other)
        {
            Plan = other.Costing.Plan;
            Type = other.Costing.Type;
            Country = other.Costing.Country;
            Source = other.Costing.Source;
            Yield = other.Costing.Yield;
            Term = other.Costing.Term;
            ElectricUsagePYear = other.Costing.ElectricUsagePYear;
            GeneratorEfficiency = other.Costing.GeneratorEfficiency;
            Structure = other.Costing.Structure;
            Demand = other.Costing.Demand;
            DieselPrice = other.Costing.DieselPrice;
            DieselConsumer = other.Costing.DieselConsumer;
            DieselPercent = other.Costing.DieselPercent;
            Generic = other.Costing.Generic;
            UseEPC = other.Costing.UseEPC;
            EPCPrice = other.Costing.EPCPrice;
            TechnicalPartner = other.Costing.TechnicalPartner;
            InstallFee = other.Costing.InstallFee;
            DesignFee = other.Costing.DesignFee;
            OMWorks = other.Costing.OMWorks;
            TPShare = other.Costing.TPShare;
            TaxFlow = other.Costing.TaxFlow;
            Rebate = other.Costing.Rebate;
            Tarrif = other.Costing.Tarrif;
            BaseRate = other.Costing.BaseRate;
            TPRate = other.Costing.TPRate;
            OMRate = other.Costing.OMRate;
            StorageCost = other.Costing.StorageCost;
            IRRProject = other.Costing.IRRProject;
            Contingency = other.Costing.Contingency;
            ExchangeRate = other.Costing.ExchangeRate;
            Currenct = other.Costing.Currenct;
            Ri = other.Costing.Ri;
            ApplyForAllMonths = other.Costing.ApplyForAllMonths;
            ClientDemand1 = other.Costing.ClientDemand1;
            ClientDemand2 = other.Costing.ClientDemand2;
            ClientDemand3 = other.Costing.ClientDemand3;
            ClientDemand4 = other.Costing.ClientDemand4;
            ClientDemand5 = other.Costing.ClientDemand5;
            ClientDemand6 = other.Costing.ClientDemand6;
            ClientDemand7 = other.Costing.ClientDemand7;
            ClientDemand8 = other.Costing.ClientDemand8;
            ClientDemand9 = other.Costing.ClientDemand9;
            ClientDemand10 = other.Costing.ClientDemand10;
            ClientDemand11 = other.Costing.ClientDemand11;
            ClientDemand12 = other.Costing.ClientDemand12;

        }




    }
}