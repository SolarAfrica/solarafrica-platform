﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Web.ModelBinding;
using System.Security.Principal;
using System;
using System.Web;

namespace NVIDev.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser 
    {
        public string Salutation { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "User Type")]
        public string UserType { get; set; }

        [Display(Name = "Company Name")]
        public string Company { get; set; }
        [Display(Name = "Designation")]
        public string Designation { get; set; }

        public byte[] Image { get; set; } //Use as profile picture/quote pic/proposal pic

        [Display(Name = "Road")]
        public string Address_Street { get; set; }
        [Display(Name = "City")]
        public string Address_City { get; set; }
        [Display(Name = "Country")]
        public string Address_Country { get; set; }

        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }
        public string IsFinancedApproved { get; set; }
         
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);  
 
            // Add custom user claims here
            return userIdentity;
        }

        public void Populate(ManageDetails model)
        {
            this.Salutation = model.Salutation;
            this.FirstName = model.FirstName;
            this.LastName = model.LastName;
            this.Designation = model.Designation;
            this.PhoneNumber = model.MobileNumber;
            this.Email = model.Email;
            this.UserType = model.UserType;
            this.Address_Country = model.Address_Country;
            this.Address_City = model.Address_City;
            this.Address_Street = model.Address_Street;
            this.Image = model.Image;
            this.Company = model.Company;
             
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<NVIDev.Models.Project> Projects { get; set; }
        public System.Data.Entity.DbSet<NVIDev.Models.Country> Countries { get; set; }
        public System.Data.Entity.DbSet<NVIDev.Models.NVIStatic> NVIStatic { get; set; }
        public System.Data.Entity.DbSet<NVIDev.Models.ProSpec> Prospec { get; set; }
        public System.Data.Entity.DbSet<NVIDev.Models.Batteries> Batteries { get; set; }
        public System.Data.Entity.DbSet<NVIDev.Models.Inverters> Inverters { get; set; }
        public System.Data.Entity.DbSet<NVIDev.Models.BiInverters> BiInverters { get; set; }
        public System.Data.Entity.DbSet<NVIDev.Models.BalanceSystem> BalanceSystems { get; set; }
        public System.Data.Entity.DbSet<NVIDev.Models.MountingStructures> MountingStructures { get; set; }
        public System.Data.Entity.DbSet<NVIDev.Models.PVModules> PVModules { get; set; }

        public System.Data.Entity.DbSet<NVIDev.Models.Fees> Fees { get; set; }

        public System.Data.Entity.DbSet<NVIDev.Models.Duty> Duties { get; set; }

        public System.Data.Entity.DbSet<NVIDev.Models.Category> Catergories { get; set; }

        public System.Data.Entity.DbSet<NVIDev.Models.CountryOfOrigin> CountryOfOrigins { get; set; }

        public System.Data.Entity.DbSet<NVIDev.Models.Port> Ports { get; set; }

        public System.Data.Entity.DbSet<NVIDev.Models.Currency> Currencies { get; set; }

        public System.Data.Entity.DbSet<NVIDev.Models.Audit> Audits { get; set; }
        public System.Data.Entity.DbSet<NVIDev.Models.CostItem> CostItems { get; set; }
        public System.Data.Entity.DbSet<NVIDev.Models.Company> Company { get; set; }
        public System.Data.Entity.DbSet<NVIDev.Models.ApprovalDocument> ApprovalDocument { get; set; }
        public System.Data.Entity.DbSet<NVIDev.Models.UserApprovalDocumentModel> UserApprovalDocument { get; set; }

    }
}