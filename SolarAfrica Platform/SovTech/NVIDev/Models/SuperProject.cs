﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVIDev.Models
{
    public class SuperProject
    {
        //        [Key]
        public int Id { get; set; }

        private ApplicationDbContext VarDB = new ApplicationDbContext();
        public double Ni { get; set; } //Formula	2	Nominal Rate of Return		
        public double NR { get; set; } //	Formula	3	Net Revenue required		
        public double Admin { get; set; } //	Formula	6	This is the portion of XII that is charged for administration (formula = X1II X user defined %) - the default is 100%		
        public double TPF { get; set; } //	Formula	7	Technical Partner Factor		
        public double A { get; set; }//	Sum	8	Capex Costs		
        public double XII { get; set; } //formula	9	ongoing fees (in USD)		
        public double XI { get; set; }	//formula	10	upfront fees (in USD)		
        public double BF { get; set; } //	Formula	11	Base Fee per project from which all other NVI fees are derived. This should be an NVI "Static Data" input only adjustable (and viewable) by NVI		
        public double BA { get; set; } //	Formula	12	Total Battery Capital costs for that project (BattC X total battery capacity designed)		
        public double C { get; set; } //.	Formula	13	Total Capital at risk		
        public double TS { get; set; } //	Formula	14	NPV of all tax shield payments.		
        public double MV { get; set; }//	Formula	15	Market Vale		
        public double TxV { get; set; } //	Formula	16	Tax Value of the System in the books of the client		
        public double ExC { get; set; }//	Formula	17	External Capital Requirements (Before upfront)		
        public double FPMT { get; set; } //	Formula	18	Fixed Monthly Payment due by Customer		
        public double Inv { get; set; } // 	Formula	19	Gross Invoice Price (Total incl. Batteries and before UP)		
        public double Insure { get; set; } //	Formula	20	Insurance Cost, which is calculated as a % of (A+ BA +Inst+Design). % Set by User Input		
        public double TF { get; set; } //formula	21	Tax factor applied to the tariff 		
        public double Up { get; set; } //Formula	21	The upfront requirement		
        public double Df { get; set; } //	Formula	22	Degredation Factor to adjust Real Revenues for degredation effect		
        public double DRF { get; set; } //Formula	23	The development fee components paid out as Series B Preference Shares		
        public double NRAT { get; set; } //	Formula	24	The Tax adjustment number		
        public double R { get; set; } //Formula	25	The Revenu number required to meet Ri		
        public double TFR { get; set; } //Formula	26	The Total Tariff rate applicable for the project		
        public double Margin { get; set; } //	Formula	27	The margin calculated between NVI costs and NVI invoice price at Country Level		
        public double MPV { get; set; } //	Formula	28	PV of margin payments		
        public double Rrev { get; set; } //	Formula	29	Real Annual Revenue required to generate Ri given Invest = PV		
        public double AM { get; set; } //	Formula	30	Annual Margin Payment (Series B) Payments From N years onwards over n 		
        public double FCF { get; set; } //	Formula	[Not currently Used]	Refers to a formula that can be usd to calculate the Real CF before Tax Shield for any year		
        public double TPr { get; set; } //	Formula	[Not used]	sum of technical partner revenue		
        public bool RR { get; set; } //Formula	[still to define]	If flagged as "Yes" - then the project requires review by NVI before it can be used to generate a proposal (the attributes will be outside pre-defined levels)		
        public double Exch { get; set; } //	External 		Exchange Rate between the currency of Tariff and equipment currencies		
        public double RCF { get; set; } //	Formula		Annual real Before Tax Cash flows for any year		
        public double Invest { get; set; } //	Formula		The total investment made by Investor	

        public double CRF { get; set; } //Capital Recovery Factor
        public double ARF { get; set; } //Admin Recovery Factor
        public double IRF { get; set; } //Installation Recovery Factor

        public double t { get; set; }

        public double ExistingPower { get; set; }
        public Project Project { get; set; }

        public Country Country { get; set; }

        public NVIStatic NVIStatic { get; set; }
        public ProSpec Prospec { get; set; }



        /// <summary>
        /// NOTE all the percentages are doubles that are later devided by 100 in each formulae
        /// E.g Country.Ri can be 19.5
        /// In the formulae Country.Ri is divided by/100 in each case
        /// </summary>


        public SuperProject(Project project)
        {
            this.Project = project;
            this.NVIStatic = VarDB.NVIStatic.FirstOrDefault();
            this.Prospec = VarDB.Prospec.Find(project.Costing.Type);
            this.Country = VarDB.Countries.Find(project.Details.P_Country);

            switch (project.Costing.Type)
            {
                case "Grid-Tied": Country.Ri = Country.Grid_Tied_Ri; break;
                case "Diesel-Hybrid": Country.Ri = Country.DieselH_Ri; break;
                case "Battery-Hybrid": Country.Ri = Country.BatteryH_Ri; break;
            }


            Process();
            Process();
        }

        public void Process()
        {

            switch (Project.Costing.Plan)
            {
                case "PBS": t = Country.TaxPBS / 100; break;
                case "PPA": t = Country.TaxPPA / 100; break;
            }
            switch (Project.Costing.Source)
            {
                case "Grid-Tied": ExistingPower = Project.Costing.Tarrif; break; //1
                case "Diesel": ExistingPower = Project.Costing.DieselPrice / Project.Costing.GeneratorEfficiency; break;
                case "Both": ExistingPower = ((Project.Costing.DieselPrice / Project.Costing.GeneratorEfficiency) * Project.Costing.DieselPercent / 100) + ((Project.Costing.Tarrif) * (1 - Project.Costing.DieselPercent / 100)); break;
            }

            //////////////////////////////////////////////////////////////////////////////
            BA = Project.Costing.BatterySize * NVIStatic.BatC;

            //////////////////////////////////////////////////////////////////////////////

            BF = (Project.Costing.Size < 0.5) ? Project.Costing.Size * 400 * Country.Currency_Per_Dollar : 225 * Project.Costing.Size + 87.5;

            ////////////////////////////////////////////////////////////////////////////


            Ni = (1 + Country.Ri / 100) * (1 + Country.CPI / 100) - 1; //2


            ///////////////////////////////////////////////////////////// Ongoing Payments (NVI)
            var _xii = BF * (1 + Project.FUP) / (5 * Prospec.FPT);
            if (Project.Costing.Plan == "Dev Services")
            {
                _xii = PMT(Ni, 0, BF, Project.Costing.Term);
            }

            else { }

            _xii = _xii + Prospec.MinimumFee;
            _xii = _xii * (1 + Project.FAdjustment);  //Change this to minimum fee per  project increasing fees set by country

            XII = _xii;


            /////////////////////////////////////////////////////////// Admin Fee
            Admin = XII * 100 / 100; //100%


            /////////////////////////////////////////////////////////////////////////////////////// XI Upfront

            var _xi = BF * Project.FUP / 100 * Prospec.FPT;
            if (Project.Costing.Plan == "Dev Services")
            {
                _xi = 0;
            }

            else { }

            _xi = _xi + Prospec.MinimumFee;
            XI = _xi * (1 + Project.FAdjustment);


            /////////////////////////////////////////////////////////// Technical Partner Fee
            double _tpf = PMT(Country.Ri / 100 + NVIStatic.TPAF, 0, 100, Project.Costing.Term * 12);
            TPF = (_tpf * 12) / (Project.Costing.Yield * Project.Costing.Size);

            ///////////////////////////////////////////////////////////FPMT



            /////////////////////////////////////////////////////////// Capital

            int numberOFSunnyIsland = 10; //Change to number of sunny island inputs
            double totalEquip = 1000;
            A = ((numberOFSunnyIsland * NVIStatic.Sic) + totalEquip) * (1 + Project.AAdjustment); //****ADD equipment selection totals here
            MV = A * (1 + NVIStatic.MU / 100) + (Project.Costing.InstallFee + Project.Costing.DesignFee);
            TxV = (MV - Up);

            ExC = A + XI;

            if (Project.Costing.Plan == "PBS")
            {
                //to work out tax shield ***
                var __PMT = (TxV / Project.Costing.Term) * t;
                var __PV = __PMT * ((1 - Math.Pow((1 + Ni), -Project.Costing.Term)) / Ni);
                //Calculate TS = NPV   PV = PMT * ( (1-Mathf.Pow((1 + Ni),-n))/ Ni)
            }

            else if (Project.Costing.Plan == "PPA")
            {
                double _NPV = 0;
                // NPV = sum of 
                for (int i = 1; i < 9; i++)
                {
                    var X = 100 / 8;
                    var __CFN = (X * TxV) / Math.Pow((1 + Ni), Project.Costing.Term);
                    _NPV += __CFN;
                }

                //X1 tp X 8........sum must be 100% so X1 = 0.025
            }


            ///////////////////////////////////////////// Installation Recovery Factor

            IRF = Project.Costing.DesignFee + Project.Costing.InstallFee;

            NR = CRF + ARF + IRF;



            ///////////////////////////////////////// Upfront payment UP

            switch (Project.Costing.Plan)
            {
                case "PPA": Up = 0 + Project.FAdjustment; break;
                case "PBS": if (Project.Costing.UseEPC == false)
                    {
                        Up = (2 * Project.Costing.Size * NVIStatic.PBSPerc) + Project.AAdjustment;

                    }

                    else
                    {
                        Up = (ExistingPower * NVIStatic.PBSPerc) + Project.AAdjustment;


                    }

                    break;

                case "Dev Services": Up = C + BA + Project.Costing.InstallFee + Project.Costing.DesignFee + XI + Project.AAdjustment;
                    break;
            }

            C = ExC - Up - TS; /// This needs to be checked

            ///////////////////////////////////////////////////////////////////// Tax Value




            /////////////////////////////////////////////////////////////////////////////tax factor

            double _tx1 = double.NaN;

            for (int i = 0; i < Project.Costing.Term; i++)
            {
                //////need t value ( 1 - t)
                _tx1 += ((1 * Math.Pow((1 + Country.CPI / 100), Project.Costing.Term) * (1 - t)) * (1 - Country.DivTX / 100)) / Math.Pow((1 + Ni), Project.Costing.Term);
            }

            TF = PMT((Country.Ri / 100) / 12, 0, _tx1, Project.Costing.Term * 12);







            ///////////////////////////////////////////////////////////Degredation Factor Df (NEEDS FIXING)


            double _dxNPV = 0;
            for (int i = 1; i <= 20; i++)
            {
                double x = 1;
                if (i == 1)
                {
                    x = 1;
                }

                else if (i == 2)
                {
                    x = 1 - 0.025;
                }

                else if (i >= 3)
                {
                    x = (i - 1) - NVIStatic.Deg / 100;
                }
                _dxNPV += (x / Math.Pow((1 + Country.Ri / 100), Project.Costing.Term));
            }




            Inv = (MV + (BA * (1 + NVIStatic.MU / 100)));
            Insure = (A + BA + Project.Costing.InstallFee + Project.Costing.DesignFee);






            ////////////////////////////////////////////////////////////// Development Fee Recovery (Series B) DRF

            double _DRFMarkup = Inv - (A + Project.Costing.InstallFee + Project.Costing.DesignFee + XI + Up + BA);
            double _DRFPV = (_DRFMarkup / Project.Costing.Term) * ((1 - Math.Pow((1 + Country.Ri), -Project.Costing.Term)) / Country.Ri);
            double _DRFPMT = PMT(Country.Ri / 100, 0, _DRFPV, Project.Costing.Term);
            DRF = (_DRFPMT / (1 - Country.DivTX / 100));

            ///////////////////////////////////////////////////////////// Net Revenue After Tax NRAT

            NRAT = NR / (1 - TF);


            ///////////////////////////////////////////////////////////// Revenue R

            double _R_A = NR + NRAT;
            double _R_B = Admin + Insure + Project.Costing.OMRate;
            R = (_R_A + _R_B) / Df;

            ////////////////////////////////////////////////////////////////////// Rrev

            var rrevA = (-Invest + MPV + TS) / (1 - t);

            var rrevB = PMT(Country.Ri, 0, -rrevA, Project.Costing.Term);

            Rrev = rrevB + Insure + Admin + Project.Costing.OMRate;
            ///////////////////////////////////////////////////////////Total Tarrif TFR

            TFR = Rrev / (Project.Costing.Yield * Project.Costing.Size * Df);

            /////////////////////////////////////////////////////////// Margin 

            Margin = ((A + BA) * NVIStatic.MU / 100) - XI;


            ///////////////////////////////////////////////////////////

            var marA = Margin * Math.Pow((1 + 18 / 100), 3);
            AM = marA / (Project.Costing.Term - 3);

            /////////////////////////////////////////////////////////// Net Present Value Margin Payments MPV


            double _MPV_PV = AM * ((1 - Math.Pow((1 + Ni), Project.Costing.Term)) / Ni);
            //MPV = 




        }



        public double PMT(double i, double FV, double PV, double n)
        {
            double result = (PV - (FV) / Math.Pow((1 + i), n)) / ((1 - (1 / Math.Pow((1 + i), n)) / i));
            return result;
        }

    }
}