﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NVIDev.Models
{
    #region Equipment Data Models
    public class Inverters
    {
        [Key]
        [Required]
        public string Id { get; set; }

        public string Key { get; set; } //this will be the number of the products on their books

        [Required]
        public string SubCategory { get; set; }

        [Required]
        public string Manufacturer { get; set; }

        public string Description { get; set; } //No commas please

        public string Dimensions { get; set; }

        public string UOM { get; set; }
        [Required]
        public string CountryOfOrigin { get; set; } //this is where the equipment is delivered from

        [Required]
        public double UnitWP { get; set; }
        [Required]
        public double ItemCost { get; set; }
        [Required]
        public double UnitCost_WP { get; set; }


        [DefaultValue(0)]
        public int LeadTime { get; set; }

        [Required]
        public string CCY { get; set; }
        //Configuration


        public string ConfigurationList { get; set; }
        public string DCOverVoltageList { get; set; }
        public string DCLoadDisconnectList { get; set; }

        public string IOPortModulesList { get; set; }
        public string CountrySpecificGridList { get; set; }



        [Range(1,1000,ErrorMessage="Cannnot Be Less Than 1")]
        [Required]
        public int ModulesPerPallet { get; set; }

        [Range(1, 1000, ErrorMessage = "Cannnot Be Less Than 1")]
        [Required]
        public int PalletsPer20ft { get; set; }





        public string IncoTerms { get; set; }




        public EquipmentItem ToEquipmentItem()
        {
            EquipmentItem equipmentItem = new EquipmentItem()
            {
                Name = Id,
                Extra = "Config , DC Over , DC Load , IO Port , Country Spec",
                ModulesPerPallet = ModulesPerPallet,
                CountryOfOrigin = CountryOfOrigin,
                PalletPer20ft = PalletsPer20ft,
                ItemCost = ItemCost,
                RealCharge = 0.00,
                Amount = 0


            };
            return equipmentItem;
        }

        public void Validate()
        {
            try
            {
                UnitCost_WP = ItemCost / UnitWP;



            }
            catch (Exception)
            {

            }
        }


    }

    public class PVModules
    {
        [Key]
        [Required]
        public string Id { get; set; }
        public string Key { get; set; } //this will be the number of the products on their books
        [Required]
        public string SubCategory { get; set; }
        [Required]
        public string Manufacturer { get; set; }

        public string Description { get; set; } //No commas please

        public string Dimensions { get; set; }

        public string UOM { get; set; }
        [Required]
        public string CountryOfOrigin { get; set; } //this is where the equipment is delivered from
        [Required]
        public double UnitWP { get; set; }
        [Required]
        public double ItemCost { get; set; }

        public double UnitCost_WP { get; set; }


        [DefaultValue(0)]
        public int LeadTime { get; set; }


        [Required]
       public string CCY { get; set; } //the currency the equipment is costed in, will be converterd to dollars
        //Configuration[Required]
        [Range(1, 1000, ErrorMessage = "Cannnot Be Less Than 1")]
        [Required]
        public int ModulesPerPallet { get; set; }
        [Range(1, 1000, ErrorMessage = "Cannnot Be Less Than 1")]
        [Required]
        public int PalletsPer20ft { get; set; }




        public string IncoTerms { get; set; }



        //Logistics




        public EquipmentItem ToEquipmentItem()
        {
            EquipmentItem equipmentItem = new EquipmentItem()
            {
                Name = Id,
                Extra = "PV Modules",
                ModulesPerPallet = ModulesPerPallet,
                CountryOfOrigin = CountryOfOrigin,
                PalletPer20ft = PalletsPer20ft,
                ItemCost = ItemCost,
                RealCharge = 0.00,
                Amount = 0


            };
            return equipmentItem;
        }
        public void Validate()
        {
            try
            {
                UnitCost_WP = ItemCost / UnitWP;


            }

            catch (Exception)
            {

            }

        }
    }

    public class BiInverters
    {
        [Key]
        [Required]
        public string Id { get; set; }

        public string Key { get; set; } //this will be the number of the products on their books
        [Required]
        public string SubCategory { get; set; }
        [Required]
        public string Manufacturer { get; set; }

        public string Description { get; set; } //No commas please

        public string Dimensions { get; set; }

        public string UOM { get; set; }
        [Required]
        public string CountryOfOrigin { get; set; } //this is where the equipment is delivered from
        [Required]
        public double UnitWP { get; set; }
        [Required]
        public double ItemCost { get; set; }

        public double UnitCost_WP { get; set; }


        [DefaultValue(0)]
        public int LeadTime { get; set; }
        [Required]
        public string CCY { get; set; }

        //Configuration

        public string CommunicationList { get; set; }
        public string InterfaceList { get; set; }
        public string ApplicationList { get; set; }


        //Configuration


        [Range(1, 1000, ErrorMessage = "Cannnot Be Less Than 1")]
        [Required]
        public int ModulesPerPallet { get; set; }

        [Range(1, 1000, ErrorMessage = "Cannnot Be Less Than 1")]
        [Required]
        public int PalletsPer20ft { get; set; }



        public string IncoTerms { get; set; }




        public EquipmentItem ToEquipmentItem()
        {
            EquipmentItem equipmentItem = new EquipmentItem()
            {
                Name = Id,
                Extra = "Coms + Interface + Apps",
                ModulesPerPallet = ModulesPerPallet,
                CountryOfOrigin = CountryOfOrigin,
                PalletPer20ft = PalletsPer20ft,
                ItemCost = ItemCost,
                RealCharge = 0.00,
                Amount = 0


            };
            return equipmentItem;
        }

        public void Validate()
        {
            try
            {
                UnitCost_WP = ItemCost / UnitWP;


            }

            catch (Exception)
            { }

        }
    }

    public class MountingStructures
    {
        [Key]
        [Required]
        public string Id { get; set; }

        public string Key { get; set; } //this will be the number of the products on their books
        [Required]
        public string Type { get; set; } //Roof-mounted etc
        [Required]
        public string SubCategory { get; set; }
        [Required]
        public string Manufacturer { get; set; }


        [Required]
        public string CountryOfOrigin { get; set; } //this is where the equipment is delivered from
        [Required]
        public double UnitWP { get; set; }
        [Required]
        public double ItemCost { get; set; }

        public double UnitCost_WP { get; set; }



        [DefaultValue(0)]
        public int LeadTime { get; set; }
        [Required]
        public string CCY { get; set; }

        [Range(1, 1000, ErrorMessage = "Cannnot Be Less Than 1")]

        [Required]
        public int ModulesPerPallet { get; set; }
        [Range(1, 1000, ErrorMessage = "Cannnot Be Less Than 1")]
        [Required]
        public int PalletsPer20ft { get; set; }




        public string IncoTerms { get; set; }






        public EquipmentItem ToEquipmentItem()
        {
            EquipmentItem equipmentItem = new EquipmentItem()
            {
                Name = Id,
                Extra = "Mount",
                ModulesPerPallet = ModulesPerPallet,
                CountryOfOrigin = CountryOfOrigin,
                PalletPer20ft = PalletsPer20ft,
                ItemCost = ItemCost,
                RealCharge = 0.00,
                Amount = 0


            };
            return equipmentItem;
        }
        public void Validate()
        {
            UnitCost_WP = ItemCost / UnitWP;


        }
    }

    public class Batteries
    {
        [Key]
        [Required]
        public string Id { get; set; }

        public string Key { get; set; } //this will be the number of the products on their books
        [Required]
        public string SubCategory { get; set; }
        [Required]

        public string Manufacturer { get; set; }

        public string Description { get; set; } //No commas please

        public string Dimensions { get; set; }

        public string UOM { get; set; }
        [Required]
        public string CountryOfOrigin { get; set; } //this is where the equipment is delivered from
        [Required]
        public double UnitAh_C10 { get; set; }
        [Required]
        public double ItemCost { get; set; }

        public double UnitCost_WP { get; set; }




        [DefaultValue(0)]
        public int LeadTime { get; set; }

        [Required]
        public string CCY { get; set; }

        [Range(1, 1000, ErrorMessage = "Cannnot Be Less Than 1")]
        [Required]
        public int ModulesPerPallet { get; set; }
        [Range(1, 1000, ErrorMessage = "Cannnot Be Less Than 1")]
        [Required]
        public int PalletsPer20ft { get; set; }




        public string IncoTerms { get; set; }






        public void Validate()
        {
            UnitCost_WP = ItemCost / UnitAh_C10;

        }
        public EquipmentItem ToEquipmentItem()
        {
            EquipmentItem equipmentItem = new EquipmentItem()
            {
                Name = Id,
                Extra = "Battery",
                ModulesPerPallet = ModulesPerPallet,
                CountryOfOrigin = CountryOfOrigin,
                PalletPer20ft = PalletsPer20ft,
                ItemCost = ItemCost,
                RealCharge = 0.00,
                Amount = 0


            };
            return equipmentItem;
        }
    }

    public class BalanceSystem
    {
        [Key]
        [Required]
        public string Id { get; set; }

        public string Key { get; set; } //this will be the number of the products on their books
        [Required]
        public string Type { get; set; }
        [Required]
        public string SubCategory { get; set; }
        [Required]
        public string Manufacturer { get; set; }

        public string Description { get; set; } //No commas please

        public string Dimensions { get; set; }

        public string UOM { get; set; }
        [Required]
        public string CountryOfOrigin { get; set; } //this is where the equipment is delivered from
        [Required]
        public int Units { get; set; }
        [Required]
        public double ItemCost { get; set; }

        public double UnitCost { get; set; }



        [DefaultValue(0)]
        public int LeadTime { get; set; }
        [Required]
        public string CCY { get; set; }

        [Range(1, 1000, ErrorMessage = "Cannnot Be Less Than 1")]
        [Required]
        public int ModulesPerPallet { get; set; }
        [Range(1, 1000, ErrorMessage = "Cannnot Be Less Than 1")]
        [Required]
        public int PalletsPer20ft { get; set; }


        public string IncoTerms { get; set; }



        //Logistics





        public void Validate()
        {
            try
            {
                UnitCost = ItemCost / Units;


            }

            catch (Exception)
            {

            }

        }

        public EquipmentItem ToEquipmentItem()
        {
            EquipmentItem equipmentItem = new EquipmentItem()
            {
                Name = Id,
                Extra = "Balance Of System",
                ModulesPerPallet = ModulesPerPallet,
                CountryOfOrigin = CountryOfOrigin,
                PalletPer20ft = PalletsPer20ft,
                ItemCost = ItemCost,
                RealCharge = 0.00,
                Amount = 0
                

            };
            return equipmentItem;
        }
    }

    #endregion
    public class EquipmentListviewModel
    {
        public List<Inverters> Inverters { get; set; }
        public List<BiInverters> BiInverters { get; set; }
        public List<MountingStructures> MountingStructures { get; set; }

        public List<PVModules> PVModules { get; set; }
        public List<Batteries> Batteries { get; set; }

        public List<BalanceSystem> BalanceSystems { get; set; }

        public EquipmentListviewModel()
        {
            Inverters = new List<Inverters>();
            BiInverters = new List<BiInverters>();
            MountingStructures = new List<MountingStructures>();
            PVModules = new List<PVModules>();
            Batteries = new List<Batteries>();
            BalanceSystems = new List<BalanceSystem>();
        }



    }

    public class CountryOfOrigin
    {
        public int id { get; set; }

        [RegularExpression("^[a-zA-Z ]+$")] //letters only
        [Remote("CheckForDuplication", "Validation")]
        public string Name { get; set; }
    }

    public class Currency
    {
        [Required]
        [RegularExpression("^[a-zA-Z ]+$")]
        [Remote("CheckForDuplicationCurrency", "Validation")]
       public string id { get; set; } //EUR = EURO
        [Required]
       public double ValueInDollars { get; set; }
    }

}