﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NVIDev.Models
{
    public class NVIStatic
    {


        //NVI static data that only they have access to
        public int Id { get; set; }


        public DateTime RevisionDate { get; set; }
        [Required]
        public double BattP { get; set; } //NVI static data input - consistent for all projects. Default input of 6 years. This may be overided for each project.
        [Required]
        public double TPAF { get; set; } //Additional Factor to be added to Technical Partner Ni to calculate the TPF
        [Required]
        public double BatC { get; set; } //Battery Cost
        [Required]
        public double CPIu { get; set; }//CPI rate to be applied to the cost of existing source of energy
        [Required]

        public double Deg { get; set; } //Degredation
        [Required]

        public double CPIg { get; set; } //General CPI Rate
    
        [Required]

        public double BatNi { get; set; } //Storage Capacity Per battery
        [Required]

        public double PBSPerc { get; set; }
        [Required]

        public double Sic { get; set; } //Sunny Island Cost

        [Required]
        [Display(Name="Insurance Rate")]
        public double InsR { get; set; }

        [Required]
        [Display(Name = "Marine Insurance")]
        public double MarineInsure { get; set; }

    }


    public class Country
    {
        public string Id { get; set; }
        [Required]
        public string Currency { get; set; }
        [Required]
        public double Currency_Per_Dollar { get; set; }


        [Required]
        public double CCY_Per_20ftKM { get; set; } //amount per 20ft in CCY per kilometre

        [Required]
        public double CCY_Per_40ftKM { get; set; } //amount per 40ft in CCY per kilometre
        [Required]
        public double VATRate { get; set; }
        [Required]
        public double CF { get; set; } //Carbon Factor (dependent on Country (if grid tied) or if Off-grid then will use defacto diesel factor.
        [Required]
        public double CPI { get; set; } //Consumer Price Index , Inflation

        [Required]
        public double CPIn { get; set; } //Consumer Price Index n


        public double Ri { get; set; }
        [Required]
        public double DivTX { get; set; } //Dividend Witholding tax selected off table dependent on CountryV

        [Required]
        public double DividendTax { get; set; }
        [Required]
        public double S4AD { get; set; } //Increase In Our Tarrif
        [Required]
        public double UtilityInflation { get; set; }

        [Required] //wear and tear over 5 years
        public double WearTear1 { get; set; }
        [Required]
        public double WearTear2 { get; set; }
        [Required]
        public double WearTear3 { get; set; }
        [Required]
        public double WearTear4 { get; set; }
        [Required]
        public double WearTear5 { get; set; }

        [Required]
        public double Grid_Tied_Ri { get; set; }
        [Required]
        public double DieselH_Ri { get; set; }
        [Required]
        public double BatteryH_Ri { get; set; }

        [Required]
        public double TaxPPA { get; set; }

        [Required]
        public double TaxPBS { get; set; }


        [Required]
        public double MU { get; set; } //Mauritius mark up

        [Required]
        public double CMU { get; set; } //In country Markup

        [Required]
        public double DieselPrice { get; set; }


        //Utility inflation for 20 years
        [Required]
        public double UtilityInflation1 { get; set; }
        [Required]
        public double UtilityInflation2 { get; set; }
        [Required]
        public double UtilityInflation3 { get; set; }
        [Required]
        public double UtilityInflation4 { get; set; }
        [Required]
        public double UtilityInflation5 { get; set; }
        [Required]
        public double UtilityInflation6 { get; set; }
        [Required]
        public double UtilityInflation7 { get; set; }
        [Required]
        public double UtilityInflation8 { get; set; }
        [Required]
        public double UtilityInflation9 { get; set; }
        [Required]
        public double UtilityInflation10 { get; set; }
        [Required]
        public double UtilityInflation11 { get; set; }
        [Required]
        public double UtilityInflation12 { get; set; }
        [Required]
        public double UtilityInflation13 { get; set; }
        [Required]
        public double UtilityInflation14 { get; set; }
        [Required]
        public double UtilityInflation15 { get; set; }
        [Required]
        public double UtilityInflation16 { get; set; }
        [Required]
        public double UtilityInflation17 { get; set; }
        [Required]
        public double UtilityInflation18 { get; set; }
        [Required]
        public double UtilityInflation19 { get; set; }
        [Required]
        public double UtilityInflation20 { get; set; }
        [Required]
        public string CountryCode { get; set; }
         [Required]
        public string CountryAbbrev { get; set; }  
 
    }


    public class Port
    {
        public int id { get; set; }

        [Required]
        [RegularExpression("^[0-9A-Za-z ]+$")]
        public string Name { get; set; }
        [Required]
        public string GPS { get; set; }
        [Required]
        public string Country { get; set; } //Parent Country
        public string COOs { get; set; } //Delimited '!' list of countries of origin e.g germany,china
        public string DeliveryCosts20 { get; set; } //Deliminated '!' list of port delivery costs linked to the COO 20ft
        public string DeliveryCosts40 { get; set; } //Deliminated '!' list of port delivery costs linked to the COO 40ft

    }

    public class PortViewModel
    {
        public int id { get; set; }

        [Required]
        [RegularExpression("^[0-9A-Za-z ]+$")]
        public string Name { get; set; }

        [Required]
        public string GPS { get; set; }
        [Required]
        public string Country { get; set; } //populate parent country via dropdown

        public List<Country2Port> Coo2Ports { get; set; }

        public PortViewModel()
        {
            Coo2Ports = new List<Country2Port>();
        }


        public void Populate(List<string> allCoos)
        {
                        Coo2Ports = new List<Country2Port>();

                        foreach (string country in allCoos)
                        {


                            Country2Port coport = new Country2Port()
                            {
                                Country = country,
                                Amount20 = 0.00,
                                Amount40 = 0.00
                            };

                            Coo2Ports.Add(coport);
                        }

        }
        public void Populate(Port port, List<string> allCoos) 
        {
            // get a list of all COOs
            id = port.id;
            Name = port.Name;
            Country = port.Country;
            GPS = port.GPS;
            Coo2Ports = new List<Country2Port>();

            List<string> currentCountries = port.COOs.Split('!').ToList();
            List<string> currentValues20 = port.DeliveryCosts20.Split('!').ToList();
           List<string> currentValues40 = port.DeliveryCosts40.Split('!').ToList();


            foreach(string country in allCoos)
            {
                if(currentCountries.Contains(country))
                {
                    int index = currentCountries.IndexOf(country);

                    Country2Port coPort = new Country2Port()
                    {
                        Country = currentCountries[index],
                        Amount20 = double.Parse(currentValues20[index]),
                        Amount40 =double.Parse(currentValues40[index])
                    };

                    Coo2Ports.Add(coPort);
                }

                else
                {
                    Country2Port coport = new Country2Port()
                    {
                        Country = country,
                        Amount20 = 0.00,
                        Amount40 = 0.00
                    };
                    Coo2Ports.Add(coport);

                }
            }
            //get a list of all countries of origin
            //populate them.. 
            //each port of entry must have a value for every export country
           
           
        }
    }

    public class Country2Port
    {
      
        public string Country { get; set; }
        public double Amount20 { get; set; }
        public double Amount40 { get; set; }
    }

    public class ProSpec
    {
        public string Id { get; set; }
        [Required]
        public double FPT { get; set; }
        [Required]
        public double BOQ_Asset_101 { get; set; }
        [Required]
        public double BOQ_Asset_390 { get; set; }
        [Required]
        public double BOQ_Asset_500 { get; set; }

        [Required]
        public double MinimumFee { get; set; }
    }



    public class Fees
    {
        [Key]
       public int Id { get; set; }

        #region Upfront Costs

        //02   05    1   5    5+



           // Preliminaries & General

        public double Prelim_Gen_02 { get; set; }
        public double Prelim_Gen_05 { get; set; }
        public double Prelim_Gen_1 { get; set; }
        public double Prelim_Gen_5 { get; set; }
        public double Prelim_Gen_51 { get; set; }
        public double Prelim_Gen_Min { get; set; }
        public double Prelim_Gen_DHybrid { get; set; }



            //Procurement


        public double Procurement_02 { get; set; }
        public double Procurement_05 { get; set; }
        public double Procurement_1 { get; set; }
        public double Procurement_5 { get; set; }
        public double Procurement_51 { get; set; }
        public double Procurement_Min { get; set; }

        public double Procurement_DHybrid { get; set; }

            //Working Capital


        public double Work_Cap_02 { get; set; }
        public double Work_Cap_05 { get; set; }
        public double Work_Cap_1 { get; set; }
        public double Work_Cap_5 { get; set; }
        public double Work_Cap_51 { get; set; }
        public double Work_Cap_Min { get; set; }
        public double Work_Cap_DHybrid { get; set; }


            //Prepare Vetting Docs

        public double Prep_Vet_02 { get; set; }
        public double Prep_Vet_05 { get; set; }
        public double Prep_Vet_1 { get; set; }
        public double Prep_Vet_5 { get; set; }
        public double Prep_Vet_51 { get; set; }

        public double Prep_Vet_FeeBase { get; set; }

        public double Prep_Vet_Min { get; set; }

        public double Prep_Vet_DHybrid { get; set; }


            //Technical Vetting UNIT

        public double Tech_Vet_02 { get; set; }
        public double Tech_Vet_05 { get; set; }
        public double Tech_Vet_1 { get; set; }
        public double Tech_Vet_5 { get; set; }
        public double Tech_Vet_51 { get; set; }
        public double Tech_Vet_FeeBase { get; set; }

        public double Tech_Vet_Min { get; set; }

        public double Tech_Vet_DHybrid { get; set; }


            //Performance Gaurantee UNIT

        public double Performance_GaranT_02 { get; set; }
        public double Performance_GaranT_05 { get; set; }
        public double Performance_GaranT_1 { get; set; }
        public double Performance_GaranT_5 { get; set; }
        public double Performance_GaranT_51 { get; set; }
        public double Performance_GaranT_Min { get; set; }

        public double Performance_GaranT_DHybrid { get; set; }


            //Project Management


        public double Project_Manage_02 { get; set; }
        public double Project_Manage_05 { get; set; }
        public double Project_Manage_1 { get; set; }
        public double Project_Manage_5 { get; set; }
        public double Project_Manage_51 { get; set; }
        public double Project_Manage_Min { get; set; }

        public double Project_Manage_DHybrid { get; set; }


            //Commissioning


        public double Commissioning_02 { get; set; }
        public double Commissioning_05 { get; set; }
        public double Commissioning_1 { get; set; }
        public double Commissioning_5 { get; set; }
        public double Commissioning_51 { get; set; }
        public double Commissioning_Min { get; set; }

        public double Commissioning_DHybrid { get; set; }


            //Margin / Contingencies


        public double Margin_Contin_02 { get; set; }
        public double Margin_Contin_05 { get; set; }
        public double Margin_Contin_1 { get; set; }
        public double Margin_Contin_5 { get; set; }
        public double Margin_Contin_51 { get; set; }
        public double Margin_Contin_Min { get; set; }

        public double Margin_Contin_DHybrid { get; set; }




        #endregion


        #region Ongoing Costs
        //ALL IN DOLLARS     PPA PBS SDS 
//        Monitoring Software

        public double Monitor_Software_02 { get; set; }
        public double Monitor_Software_05 { get; set; }
        public double Monitor_Software_1 { get; set; }
        public double Monitor_Software_5 { get; set; }
        public double Monitor_Software_51 { get; set; }

        public double Monitor_Software_PBS { get; set; }
        public double Monitor_Software_PPA { get; set; }
        public double Monitor_Software_SDS { get; set; }
        public double Monitor_Software_Min { get; set; }


        public double Monitor_Software_DHybrid { get; set; }


//        Administration

        public double Admin_02 { get; set; }
        public double Admin_05 { get; set; }
        public double Admin_1 { get; set; }
        public double Admin_5 { get; set; }
        public double Admin_51 { get; set; }

        public double Admin_PBS { get; set; }
        public double Admin_PPA { get; set; }
        public double Admin_SDS { get; set; }
        public double Admin_Min { get; set; }

        public double Admin_DHybrid { get; set; }


//        Asset management


        public double Asset_Manage_02 { get; set; }
        public double Asset_Manage_05 { get; set; }
        public double Asset_Manage_1 { get; set; }
        public double Asset_Manage_5 { get; set; }
        public double Asset_Manage_51 { get; set; }

        public double Asset_Manage_PBS { get; set; }
        public double Asset_Manage_PPA { get; set; }
        public double Asset_Manage_SDS { get; set; }
        public double Asset_Manage_Min { get; set; }

        public double Asset_Manage_DHybrid { get; set; }




        //Project Duration    May change this with Term (n)
        public double Duration_02 { get; set; }
        public double Duration_05 { get; set; }
        public double Duration_1 { get; set; }
        public double Duration_5 { get; set; }
        public double Duration_51 { get; set; }
        #endregion



    }


    public class CostItem
    {
        public int id { get; set; }

        public string UserID { get; set; } //line items are specific to users
        public string Name { get; set; }

        public double Cost { get; set; }
        public double Markup { get; set; }
        public double SellPrice { get; set; }
        public string WPMark { get; set; }

        public bool Fixed { get; set; }


        public void Populate(QuotingCost model)
        {
            UserID = model.UserID;
             Name = model.Name;

            Cost = model.Cost;
            Markup = model.Markup;
            SellPrice = Cost * (1 + Markup / 100);
            WPMark = model.WPMark;

            Fixed = model.Fixed;

        }

        public void CalculateSellPrice()
        {
            SellPrice = Cost * (1 + Markup / 100);
        }
    }
}