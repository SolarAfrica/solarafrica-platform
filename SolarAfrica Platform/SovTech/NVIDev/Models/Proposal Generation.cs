﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NVIDev.Models
{
    public class ProposalInputModel
    {
        public int Id { get; set; }
        public Project project { get; set; }
        public SuperProject superProject { get; set; }

        public string Type { get; set; } //Sales - Direct , Sales + Financed - Direct, Financed - Direct, Financed - Indirect
        public ApplicationUser preparer { get; set; } //person doing the proposal

        public int ProjectID { get; set; }
        public string preparerID { get; set; }

        public PSection TechData { get; set; }
        public PSection VisualData { get; set; }
        public PSection DataSheets { get; set; }
        public PSection SupplemData { get; set; }

        [ValidateFileAtrribute(ErrorMessage = "Only PDFs May Be Uploaded")]
        public HttpPostedFileBase alterCover { get; set; } //the cover attachment to the section

        [ValidateFileAtrribute(ErrorMessage = "Only PDFs May Be Uploaded")]
        public HttpPostedFileBase siteLayout { get; set; } //the cover attachment to the section
        public double Capex { get; set; }
        public double Duties { get; set; }
        public double Transport { get; set; }

        public double Margin { get; set; }
        public double AddMargin { get; set; }

        public double TotalSellPrice { get; set; }
        public double DevelopmentFee { get; set; }
        public double SubTotal { get; set; }
        public List<Exclusion> Exclusions { get; set; }

        public bool IncludePPA { get; set; }
        public bool IncludePBS { get; set; }
        public bool IncludeSDS { get; set; }

        public bool IncludeGen { get; set; } //future generator rental
        public ProposalInputModel()
        {

            Exclusions = new List<Exclusion>();
        }

        public void Calculate()
        {

            if (superProject != null)
            {
                Capex = superProject.TotalEquipmentCostInDollars;
                Duties = superProject.TotalDutiesA + superProject.TotalDutiesBa;
                Transport = superProject.TotalTrans + superProject.TotalTransBA;
                DevelopmentFee = superProject.Project.Costing.DesignFee;
                SubTotal = Capex + Duties + Transport;

                AddMargin = SubTotal * superProject.MU / 100;
                Margin = superProject.Margin / 100;
                TotalSellPrice = SubTotal + Margin;
            }

        }

        [ValidateFileAtrribute(ErrorMessage = "Only Image May Be Uploaded")]
        public HttpPostedFileBase tpLogo { get; set; } //the cover attachment to the section
        public string CommercialOffer { get; set; }
        public bool IncludePrice { get; set; }
        public string AboutCompany { get; set; }
        public string ProposalType { get; set; }
        [Required(ErrorMessage = "*required")]
        public string CoverPageHeader { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class ValidateFileAtrribute : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {

            if (value == null)
            {
                return ValidationResult.Success;
            }

            var file = value as HttpPostedFileBase;


            if (file.ContentType.Contains("pdf"))
            {

                if (file.ContentLength <= 0)
                {
                    return new ValidationResult("This file is not a valid PDF!");
                }

            }

            else
            {
                return new ValidationResult("This file is not a PDF!");
            }

            return ValidationResult.Success;
        }
    }
    public class PSection
    {
        public string Name { get; set; }
        public bool IncludeMe { get; set; }
        public string Letter { get; set; }
        public List<PSubSection> SubSections { get; set; }

        public PSection()
        {
            IncludeMe = false;
            SubSections = new List<PSubSection>();
        }
    }

    public class PSubSection
    {
        public string Name { get; set; }

        public string LetterCode { get; set; }
        public string ParentName { get; set; }

        public bool IncludeMe { get; set; }
        public bool Standard { get; set; }

        [ValidateFileAtrribute(ErrorMessage = "Only PDFs May Be Uploaded")]

        public HttpPostedFileBase PDF { get; set; } //the pdf attachment to the section
        public PSubSection()
        {
            Standard = true;
        }



    }

    public class Exclusion
    {
        public string Name { get; set; }
    }




}