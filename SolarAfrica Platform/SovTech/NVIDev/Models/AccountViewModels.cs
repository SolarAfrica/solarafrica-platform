﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using System.Web.Mvc;

namespace NVIDev.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Username")]

        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Display(Name = "Salutation")]
        public string Salutation { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "*required")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,15}$", ErrorMessage = "The {0} must be at least 8 characters long. Passwords must have at least one non letter or digit character. Passwords must have at least one lowercase ('a'-'z'). Passwords must have at least one uppercase ('A'-'Z').")]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Email")]
        [EmailAddress]
        [Remote("doesUserNameExist", "Account", HttpMethod = "POST", ErrorMessage = "Email id already exists. Please enter a different email id.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "Designation")]
        public string Designation { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "User Type")]
        public string UserType { get; set; } //NVI Or TP Or Investor


        [Required(ErrorMessage = "*required")]
        [Display(Name = "Company Name")]
        [Remote("doesCompanyNameExist", "Account", HttpMethod = "POST", ErrorMessage = "Company already exists. Please enter a different company name.")]
        public string CompanyName { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "Mobile No")]
        [DataType(DataType.PhoneNumber)]
        public string MobileNumber { get; set; }

        //[Required(ErrorMessage = "*required")]
        public string CountryCode { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Country")]
        public string Country { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "City")]
        public string City { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "Road")]
        public string Road { get; set; }

        //[Required(ErrorMessage = "*required")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase ImageUpload { get; set; }




        //properties of companyAdd model
        [Key]
        public decimal CompanyID { get; set; }

        //[Required(ErrorMessage = "*required")]
        //[Display(Name = "Company name")]
        //[Remote("doesCompanyExist", "Account", HttpMethod = "POST", ErrorMessage = "Company Name already exists. Please enter a different Company Name.")]
        //public string CompanyName { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Company Address")]
        public string CompanyAddress { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Company Tel")]
        public string CompanyTel { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Company WebSite")]
        public string CompanyWebSiteName { get; set; }

        [Required(ErrorMessage = "*required")]
        public string CompanyWebSiteDomain { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "Company Registration No")]
        public string CompanyRegistrationNo { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Country")]
        public string CompanyCountry { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "City")]
        public string CompanyCity { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "Min Size KWP")]
        public string MinSizeKWP { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "Max Size KWP")]
        public string MaxSizeKWP { get; set; }

        //[Required(ErrorMessage = "*required")]
        [Display(Name = "Company Icon")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase CompanyLogo { get; set; }

        [Display(Name = "Accept Term")]
        public bool IsAcceptTerm { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }

    [Table("Vw_User")]
    public class Vw_User
    {
        public string UserId { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
        public string Designation { get; set; }
        public string UserType { get; set; } //NVI Or TP Or Investor 
        public string Company { get; set; }
        public string PhoneNumber { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Road { get; set; }
        public byte[] Image { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public class RegistrationModel
    {

        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Designation { get; set; }
        public string UserType { get; set; } //NVI Or TP Or Investor 
        public string CompanyName { get; set; }
        public string Email { get; set; }
        //public string Password { get; set; } 
        //public string ConfirmPassword { get; set; } 
        public string Country { get; set; }
        public string City { get; set; }
        public string MobileNo { get; set; }
        public string Role { get; set; }
        public string Status { get; set; }
        public string InitiatedOn { get; set; }
        public string View { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }   
}
