﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Web;
 

namespace NVIDev.Models
{
    public class IndexViewModel
    {
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }


    public class ManageDetails
    {

        [Display(Name = "Salutation")]
        public string Salutation { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "Designation")]
        public string Designation { get; set; }

        [Required(ErrorMessage = "*required")]
        [Display(Name = "User Type")]
        public string UserType { get; set; } //NVI Or TP Or Investor

        public string Company { get; set; }
        public byte[] Image { get; set; } //Use as profile picture/quote pic/proposal pic

        [Display(Name ="Street")]
        public string Address_Street { get; set; }
        [Display(Name = "City")]
        public string Address_City { get; set; }
        [Display(Name = "Country")]
        public string Address_Country { get; set; }        

        [Phone]
        [Display(Name = "Mobile No.")]
        public string MobileNumber { get; set; }

        //[Display(Name ="Full Name")]
        //public string Contact_Fullname { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase ImageUpload { get; set; }

        public void Populate (ApplicationUser model)
        {
            this.Salutation = model.Salutation;
            this.FirstName = model.FirstName;
            this.LastName = model.LastName;
            this.Designation = model.Designation;
            this.MobileNumber = model.PhoneNumber;
            this.Email = model.Email;
            this.UserType = model.UserType;
            this.Address_Country = model.Address_Country;
            this.Address_City = model.Address_City;
            this.Address_Street = model.Address_Street;
            this.Image = model.Image;
            this.Company = model.Company;
        }
    }
}