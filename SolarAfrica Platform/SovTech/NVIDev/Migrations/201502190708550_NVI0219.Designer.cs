// <auto-generated />
namespace NVIDev.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class NVI0219 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(NVI0219));
        
        string IMigrationMetadata.Id
        {
            get { return "201502190708550_NVI0219"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
