namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Audits01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Audits", "Company", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Audits", "Company");
        }
    }
}
