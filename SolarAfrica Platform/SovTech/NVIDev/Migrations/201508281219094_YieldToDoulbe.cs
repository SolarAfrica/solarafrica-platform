namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class YieldToDoulbe : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Projects", "Costing_Yield", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Projects", "Costing_Yield", c => c.Int(nullable: false));
        }
    }
}
