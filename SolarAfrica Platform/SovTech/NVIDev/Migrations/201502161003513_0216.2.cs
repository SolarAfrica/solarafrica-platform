namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _02162 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Projects", "Costing_Size");
            DropColumn("dbo.Projects", "Costing_BatterySize");
            DropColumn("dbo.Projects", "Costing_BatteryType");
            DropColumn("dbo.Projects", "Costing_Num_Batteries");
            DropColumn("dbo.Projects", "Costing_SunnyIslandCap");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Projects", "Costing_SunnyIslandCap", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_Num_Batteries", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_BatteryType", c => c.String());
            AddColumn("dbo.Projects", "Costing_BatterySize", c => c.Int(nullable: false));
            AddColumn("dbo.Projects", "Costing_Size", c => c.Double(nullable: false));
        }
    }
}
