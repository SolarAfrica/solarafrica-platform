namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PalletPer20Ft : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BalanceSystems", "PalletsPer20ft", c => c.Int(nullable: false));
            AddColumn("dbo.Batteries", "PalletsPer20ft", c => c.Int(nullable: false));
            AddColumn("dbo.BiInverters", "PalletsPer20ft", c => c.Int(nullable: false));
            AddColumn("dbo.Inverters", "PalletsPer20ft", c => c.Int(nullable: false));
            AddColumn("dbo.MountingStructures", "ModulesPerPallet", c => c.Int(nullable: false));
            AddColumn("dbo.MountingStructures", "PalletsPer20ft", c => c.Int(nullable: false));
            AddColumn("dbo.PVModules", "PalletsPer20ft", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PVModules", "PalletsPer20ft");
            DropColumn("dbo.MountingStructures", "PalletsPer20ft");
            DropColumn("dbo.MountingStructures", "ModulesPerPallet");
            DropColumn("dbo.Inverters", "PalletsPer20ft");
            DropColumn("dbo.BiInverters", "PalletsPer20ft");
            DropColumn("dbo.Batteries", "PalletsPer20ft");
            DropColumn("dbo.BalanceSystems", "PalletsPer20ft");
        }
    }
}
