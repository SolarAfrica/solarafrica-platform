namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DynamicDieselPrice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Countries", "DieselPrice", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Countries", "DieselPrice");
        }
    }
}
