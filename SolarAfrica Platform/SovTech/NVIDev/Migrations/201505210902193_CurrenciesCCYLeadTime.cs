namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CurrenciesCCYLeadTime : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        ValueInDollars = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.BalanceSystems", "LeadTime", c => c.Int(nullable: false));
            AddColumn("dbo.BalanceSystems", "CCY", c => c.String(nullable: false));
            AddColumn("dbo.Batteries", "LeadTime", c => c.Int(nullable: false));
            AddColumn("dbo.Batteries", "CCY", c => c.String(nullable: false));
            AddColumn("dbo.BiInverters", "LeadTime", c => c.Int(nullable: false));
            AddColumn("dbo.BiInverters", "CCY", c => c.String(nullable: false));
            AddColumn("dbo.Inverters", "LeadTime", c => c.Int(nullable: false));
            AddColumn("dbo.Inverters", "CCY", c => c.String(nullable: false));
            AddColumn("dbo.MountingStructures", "LeadTime", c => c.Int(nullable: false));
            AddColumn("dbo.MountingStructures", "CCY", c => c.String(nullable: false));
            AddColumn("dbo.PVModules", "LeadTime", c => c.Int(nullable: false));
            AddColumn("dbo.PVModules", "CCY", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PVModules", "CCY");
            DropColumn("dbo.PVModules", "LeadTime");
            DropColumn("dbo.MountingStructures", "CCY");
            DropColumn("dbo.MountingStructures", "LeadTime");
            DropColumn("dbo.Inverters", "CCY");
            DropColumn("dbo.Inverters", "LeadTime");
            DropColumn("dbo.BiInverters", "CCY");
            DropColumn("dbo.BiInverters", "LeadTime");
            DropColumn("dbo.Batteries", "CCY");
            DropColumn("dbo.Batteries", "LeadTime");
            DropColumn("dbo.BalanceSystems", "CCY");
            DropColumn("dbo.BalanceSystems", "LeadTime");
            DropTable("dbo.Currencies");
        }
    }
}
