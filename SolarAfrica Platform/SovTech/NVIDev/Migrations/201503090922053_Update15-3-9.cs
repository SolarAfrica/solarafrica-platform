namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update1539 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "InsureYN", c => c.Boolean(nullable: false));
            AddColumn("dbo.Projects", "OMYN", c => c.Boolean(nullable: false));
            AddColumn("dbo.Projects", "xn1", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "xn2", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "xn3", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "xn4", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "xn5", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "xn6", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "xn7", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "xn8", c => c.Double(nullable: false));
            DropColumn("dbo.Projects", "in_FixedAnualCharge");
            DropColumn("dbo.Projects", "adj_FixedAnualCharge");
            DropColumn("dbo.Projects", "Costing_TariffYN");
            DropColumn("dbo.Projects", "Costing_OMYN");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Projects", "Costing_OMYN", c => c.Boolean(nullable: false));
            AddColumn("dbo.Projects", "Costing_TariffYN", c => c.Boolean(nullable: false));
            AddColumn("dbo.Projects", "adj_FixedAnualCharge", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "in_FixedAnualCharge", c => c.Double(nullable: false));
            DropColumn("dbo.Projects", "xn8");
            DropColumn("dbo.Projects", "xn7");
            DropColumn("dbo.Projects", "xn6");
            DropColumn("dbo.Projects", "xn5");
            DropColumn("dbo.Projects", "xn4");
            DropColumn("dbo.Projects", "xn3");
            DropColumn("dbo.Projects", "xn2");
            DropColumn("dbo.Projects", "xn1");
            DropColumn("dbo.Projects", "OMYN");
            DropColumn("dbo.Projects", "InsureYN");
        }
    }
}
