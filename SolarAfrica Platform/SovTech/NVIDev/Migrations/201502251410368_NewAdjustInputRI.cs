namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewAdjustInputRI : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "In_Ri", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "In_Ri");
        }
    }
}
