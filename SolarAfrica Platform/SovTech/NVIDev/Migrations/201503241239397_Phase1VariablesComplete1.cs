namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Phase1VariablesComplete1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "UtilityInflation1", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation2", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation3", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation4", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation5", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation6", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation7", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation8", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation9", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation10", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation11", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation12", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation13", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation14", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation15", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation16", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation17", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation18", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation19", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "UtilityInflation20", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "UtilityInflation20");
            DropColumn("dbo.Projects", "UtilityInflation19");
            DropColumn("dbo.Projects", "UtilityInflation18");
            DropColumn("dbo.Projects", "UtilityInflation17");
            DropColumn("dbo.Projects", "UtilityInflation16");
            DropColumn("dbo.Projects", "UtilityInflation15");
            DropColumn("dbo.Projects", "UtilityInflation14");
            DropColumn("dbo.Projects", "UtilityInflation13");
            DropColumn("dbo.Projects", "UtilityInflation12");
            DropColumn("dbo.Projects", "UtilityInflation11");
            DropColumn("dbo.Projects", "UtilityInflation10");
            DropColumn("dbo.Projects", "UtilityInflation9");
            DropColumn("dbo.Projects", "UtilityInflation8");
            DropColumn("dbo.Projects", "UtilityInflation7");
            DropColumn("dbo.Projects", "UtilityInflation6");
            DropColumn("dbo.Projects", "UtilityInflation5");
            DropColumn("dbo.Projects", "UtilityInflation4");
            DropColumn("dbo.Projects", "UtilityInflation3");
            DropColumn("dbo.Projects", "UtilityInflation2");
            DropColumn("dbo.Projects", "UtilityInflation1");
        }
    }
}
