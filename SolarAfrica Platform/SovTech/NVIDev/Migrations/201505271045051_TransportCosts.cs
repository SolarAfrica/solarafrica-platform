namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TransportCosts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Countries", "CCY_Per_20ftKM", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "CCY_Per_40ftKM", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Countries", "CCY_Per_40ftKM");
            DropColumn("dbo.Countries", "CCY_Per_20ftKM");
        }
    }
}
