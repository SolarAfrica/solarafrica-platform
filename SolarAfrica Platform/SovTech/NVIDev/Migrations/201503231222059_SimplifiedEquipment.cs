namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SimplifiedEquipment : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.BalanceSystems", "Log_Value");
            DropColumn("dbo.BalanceSystems", "Log_ItemCost");
            DropColumn("dbo.BalanceSystems", "Log_UnitCost");
            DropColumn("dbo.BalanceSystems", "IDF");
            DropColumn("dbo.BalanceSystems", "RDL");
            DropColumn("dbo.BalanceSystems", "Duties");
            DropColumn("dbo.BalanceSystems", "Duties_Value");
            DropColumn("dbo.BalanceSystems", "Duties_ItemCost");
            DropColumn("dbo.BalanceSystems", "Duties_UnitCost");
            DropColumn("dbo.BalanceSystems", "TotalCost");
            DropColumn("dbo.BalanceSystems", "T_UnitCosts");
            DropColumn("dbo.Batteries", "Log_Value");
            DropColumn("dbo.Batteries", "Log_ItemCost");
            DropColumn("dbo.Batteries", "Log_UnitCost");
            DropColumn("dbo.Batteries", "IDF");
            DropColumn("dbo.Batteries", "RDL");
            DropColumn("dbo.Batteries", "Duties");
            DropColumn("dbo.Batteries", "Duties_Value");
            DropColumn("dbo.Batteries", "Duties_ItemCost");
            DropColumn("dbo.Batteries", "Duties_UnitCost");
            DropColumn("dbo.Batteries", "TotalCost");
            DropColumn("dbo.Batteries", "T_UnitCosts");
            DropColumn("dbo.BiInverters", "Log_Value");
            DropColumn("dbo.BiInverters", "Log_ItemCost");
            DropColumn("dbo.BiInverters", "Log_UnitCost");
            DropColumn("dbo.BiInverters", "IDF");
            DropColumn("dbo.BiInverters", "RDL");
            DropColumn("dbo.BiInverters", "Duties");
            DropColumn("dbo.BiInverters", "Duties_Value");
            DropColumn("dbo.BiInverters", "Duties_ItemCost");
            DropColumn("dbo.BiInverters", "Duties_UnitCost");
            DropColumn("dbo.BiInverters", "TotalCost");
            DropColumn("dbo.BiInverters", "T_UnitCosts");
            DropColumn("dbo.Inverters", "Log_Value");
            DropColumn("dbo.Inverters", "Log_ItemCost");
            DropColumn("dbo.Inverters", "Log_UnitCost");
            DropColumn("dbo.Inverters", "IDF");
            DropColumn("dbo.Inverters", "RDL");
            DropColumn("dbo.Inverters", "Duties");
            DropColumn("dbo.Inverters", "Duties_Value");
            DropColumn("dbo.Inverters", "Duties_ItemCost");
            DropColumn("dbo.Inverters", "Duties_UnitCost");
            DropColumn("dbo.Inverters", "TotalCost");
            DropColumn("dbo.Inverters", "T_UnitCosts");
            DropColumn("dbo.MountingStructures", "Log_Value");
            DropColumn("dbo.MountingStructures", "Log_ItemCost");
            DropColumn("dbo.MountingStructures", "Log_UnitCost");
            DropColumn("dbo.MountingStructures", "IDF");
            DropColumn("dbo.MountingStructures", "RDL");
            DropColumn("dbo.MountingStructures", "Duties");
            DropColumn("dbo.MountingStructures", "Duties_Value");
            DropColumn("dbo.MountingStructures", "Duties_ItemCost");
            DropColumn("dbo.MountingStructures", "Duties_UnitCost");
            DropColumn("dbo.MountingStructures", "TotalCost");
            DropColumn("dbo.MountingStructures", "T_UnitCosts");
            DropColumn("dbo.PVModules", "Log_Value");
            DropColumn("dbo.PVModules", "Log_ItemCost");
            DropColumn("dbo.PVModules", "Log_UnitCost");
            DropColumn("dbo.PVModules", "IDF");
            DropColumn("dbo.PVModules", "RDL");
            DropColumn("dbo.PVModules", "Duties");
            DropColumn("dbo.PVModules", "Duties_Value");
            DropColumn("dbo.PVModules", "Duties_ItemCost");
            DropColumn("dbo.PVModules", "Duties_UnitCost");
            DropColumn("dbo.PVModules", "TotalCost");
            DropColumn("dbo.PVModules", "T_UnitCosts");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PVModules", "T_UnitCosts", c => c.Double(nullable: false));
            AddColumn("dbo.PVModules", "TotalCost", c => c.Double(nullable: false));
            AddColumn("dbo.PVModules", "Duties_UnitCost", c => c.Double(nullable: false));
            AddColumn("dbo.PVModules", "Duties_ItemCost", c => c.Double(nullable: false));
            AddColumn("dbo.PVModules", "Duties_Value", c => c.Double(nullable: false));
            AddColumn("dbo.PVModules", "Duties", c => c.Double(nullable: false));
            AddColumn("dbo.PVModules", "RDL", c => c.Double(nullable: false));
            AddColumn("dbo.PVModules", "IDF", c => c.Double(nullable: false));
            AddColumn("dbo.PVModules", "Log_UnitCost", c => c.Double(nullable: false));
            AddColumn("dbo.PVModules", "Log_ItemCost", c => c.Double(nullable: false));
            AddColumn("dbo.PVModules", "Log_Value", c => c.Double(nullable: false));
            AddColumn("dbo.MountingStructures", "T_UnitCosts", c => c.Double(nullable: false));
            AddColumn("dbo.MountingStructures", "TotalCost", c => c.Double(nullable: false));
            AddColumn("dbo.MountingStructures", "Duties_UnitCost", c => c.Double(nullable: false));
            AddColumn("dbo.MountingStructures", "Duties_ItemCost", c => c.Double(nullable: false));
            AddColumn("dbo.MountingStructures", "Duties_Value", c => c.Double(nullable: false));
            AddColumn("dbo.MountingStructures", "Duties", c => c.Double(nullable: false));
            AddColumn("dbo.MountingStructures", "RDL", c => c.Double(nullable: false));
            AddColumn("dbo.MountingStructures", "IDF", c => c.Double(nullable: false));
            AddColumn("dbo.MountingStructures", "Log_UnitCost", c => c.Double(nullable: false));
            AddColumn("dbo.MountingStructures", "Log_ItemCost", c => c.Double(nullable: false));
            AddColumn("dbo.MountingStructures", "Log_Value", c => c.Double(nullable: false));
            AddColumn("dbo.Inverters", "T_UnitCosts", c => c.Double(nullable: false));
            AddColumn("dbo.Inverters", "TotalCost", c => c.Double(nullable: false));
            AddColumn("dbo.Inverters", "Duties_UnitCost", c => c.Double(nullable: false));
            AddColumn("dbo.Inverters", "Duties_ItemCost", c => c.Double(nullable: false));
            AddColumn("dbo.Inverters", "Duties_Value", c => c.Double(nullable: false));
            AddColumn("dbo.Inverters", "Duties", c => c.Double(nullable: false));
            AddColumn("dbo.Inverters", "RDL", c => c.Double(nullable: false));
            AddColumn("dbo.Inverters", "IDF", c => c.Double(nullable: false));
            AddColumn("dbo.Inverters", "Log_UnitCost", c => c.Double(nullable: false));
            AddColumn("dbo.Inverters", "Log_ItemCost", c => c.Double(nullable: false));
            AddColumn("dbo.Inverters", "Log_Value", c => c.Double(nullable: false));
            AddColumn("dbo.BiInverters", "T_UnitCosts", c => c.Double(nullable: false));
            AddColumn("dbo.BiInverters", "TotalCost", c => c.Double(nullable: false));
            AddColumn("dbo.BiInverters", "Duties_UnitCost", c => c.Double(nullable: false));
            AddColumn("dbo.BiInverters", "Duties_ItemCost", c => c.Double(nullable: false));
            AddColumn("dbo.BiInverters", "Duties_Value", c => c.Double(nullable: false));
            AddColumn("dbo.BiInverters", "Duties", c => c.Double(nullable: false));
            AddColumn("dbo.BiInverters", "RDL", c => c.Double(nullable: false));
            AddColumn("dbo.BiInverters", "IDF", c => c.Double(nullable: false));
            AddColumn("dbo.BiInverters", "Log_UnitCost", c => c.Int(nullable: false));
            AddColumn("dbo.BiInverters", "Log_ItemCost", c => c.Int(nullable: false));
            AddColumn("dbo.BiInverters", "Log_Value", c => c.Int(nullable: false));
            AddColumn("dbo.Batteries", "T_UnitCosts", c => c.Double(nullable: false));
            AddColumn("dbo.Batteries", "TotalCost", c => c.Double(nullable: false));
            AddColumn("dbo.Batteries", "Duties_UnitCost", c => c.Double(nullable: false));
            AddColumn("dbo.Batteries", "Duties_ItemCost", c => c.Double(nullable: false));
            AddColumn("dbo.Batteries", "Duties_Value", c => c.Double(nullable: false));
            AddColumn("dbo.Batteries", "Duties", c => c.Double(nullable: false));
            AddColumn("dbo.Batteries", "RDL", c => c.Double(nullable: false));
            AddColumn("dbo.Batteries", "IDF", c => c.Double(nullable: false));
            AddColumn("dbo.Batteries", "Log_UnitCost", c => c.Double(nullable: false));
            AddColumn("dbo.Batteries", "Log_ItemCost", c => c.Double(nullable: false));
            AddColumn("dbo.Batteries", "Log_Value", c => c.Double(nullable: false));
            AddColumn("dbo.BalanceSystems", "T_UnitCosts", c => c.Double(nullable: false));
            AddColumn("dbo.BalanceSystems", "TotalCost", c => c.Double(nullable: false));
            AddColumn("dbo.BalanceSystems", "Duties_UnitCost", c => c.Double(nullable: false));
            AddColumn("dbo.BalanceSystems", "Duties_ItemCost", c => c.Double(nullable: false));
            AddColumn("dbo.BalanceSystems", "Duties_Value", c => c.Double(nullable: false));
            AddColumn("dbo.BalanceSystems", "Duties", c => c.Double(nullable: false));
            AddColumn("dbo.BalanceSystems", "RDL", c => c.Double(nullable: false));
            AddColumn("dbo.BalanceSystems", "IDF", c => c.Double(nullable: false));
            AddColumn("dbo.BalanceSystems", "Log_UnitCost", c => c.Double(nullable: false));
            AddColumn("dbo.BalanceSystems", "Log_ItemCost", c => c.Double(nullable: false));
            AddColumn("dbo.BalanceSystems", "Log_Value", c => c.Double(nullable: false));
        }
    }
}
