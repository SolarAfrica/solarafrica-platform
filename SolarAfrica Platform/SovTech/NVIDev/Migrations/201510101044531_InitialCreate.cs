namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApprovalDocuments",
                c => new
                    {
                        ApprovalDocumentId = c.Long(nullable: false, identity: true),
                        ApprovalDocumentMenu = c.String(),
                        ApprovalDocumentSubMenu = c.String(),
                        IsActive = c.Boolean(),
                        CreatedBy = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ApprovalDocumentId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        CompanyID = c.Long(nullable: false, identity: true),
                        CompanyName = c.String(),
                        CompanyAddress = c.String(),
                        PostalCode = c.String(),
                        CompanyTel = c.String(),
                        CompanyWebSite = c.String(),
                        CompanyRegistrationNo = c.String(),
                        Country = c.String(),
                        City = c.String(),
                        MinSizeKWP = c.String(),
                        MaxSizeKWP = c.String(),
                        CompanyLogo = c.Binary(),
                        IsAcceptTerm = c.Boolean(),
                        IsActive = c.Boolean(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.CompanyID);
            
            CreateTable(
                "dbo.UserApprovalDocument",
                c => new
                    {
                        UserDocumentId = c.Long(nullable: false, identity: true),
                        CompanyID = c.String(),
                        UserID = c.String(),
                        DocumentID = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.UserDocumentId);
            
            AddColumn("dbo.AspNetUsers", "Salutation", c => c.String());
            AddColumn("dbo.AspNetUsers", "FirstName", c => c.String());
            AddColumn("dbo.AspNetUsers", "LastName", c => c.String());
            AddColumn("dbo.AspNetUsers", "UserType", c => c.String());
            AddColumn("dbo.AspNetUsers", "Designation", c => c.String());
            AddColumn("dbo.AspNetUsers", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "IsFinancedApproved", c => c.String());
            DropColumn("dbo.AspNetUsers", "Contact_Fullname");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Contact_Fullname", c => c.String());
            DropColumn("dbo.AspNetUsers", "IsFinancedApproved");
            DropColumn("dbo.AspNetUsers", "CreatedOn");
            DropColumn("dbo.AspNetUsers", "Designation");
            DropColumn("dbo.AspNetUsers", "UserType");
            DropColumn("dbo.AspNetUsers", "LastName");
            DropColumn("dbo.AspNetUsers", "FirstName");
            DropColumn("dbo.AspNetUsers", "Salutation");
            DropTable("dbo.UserApprovalDocument");
            DropTable("dbo.Companies");
            DropTable("dbo.ApprovalDocuments");
        }
    }
}
