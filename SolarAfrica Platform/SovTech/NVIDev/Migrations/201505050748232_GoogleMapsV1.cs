namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GoogleMapsV1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "Details_PortOfEntry", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "Details_PortOfEntry");
        }
    }
}
