namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CountryOfOrigin : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BalanceSystems", "CountryOfOrigin", c => c.String(nullable: false));
            AddColumn("dbo.Batteries", "CountryOfOrigin", c => c.String(nullable: false));
            AddColumn("dbo.BiInverters", "CountryOfOrigin", c => c.String(nullable: false));
            AddColumn("dbo.Inverters", "CountryOfOrigin", c => c.String(nullable: false));
            AddColumn("dbo.MountingStructures", "CountryOfOrigin", c => c.String(nullable: false));
            AddColumn("dbo.PVModules", "CountryOfOrigin", c => c.String(nullable: false));
            DropColumn("dbo.BalanceSystems", "ModulesPer20FT");
            DropColumn("dbo.BalanceSystems", "ModulesPer40FT");
            DropColumn("dbo.BalanceSystems", "QTY");
            DropColumn("dbo.Batteries", "ModulesPer20FT");
            DropColumn("dbo.Batteries", "ModulesPer40FT");
            DropColumn("dbo.Batteries", "QTY");
            DropColumn("dbo.BiInverters", "ModulesPer20FT");
            DropColumn("dbo.BiInverters", "ModulesPer40FT");
            DropColumn("dbo.BiInverters", "QTY");
            DropColumn("dbo.BiInverters", "Conf_Value");
            DropColumn("dbo.Inverters", "ModulesPer20FT");
            DropColumn("dbo.Inverters", "ModulesPer40FT");
            DropColumn("dbo.Inverters", "QTY");
            DropColumn("dbo.Inverters", "Conf_Value");
            DropColumn("dbo.MountingStructures", "QTY");
            DropColumn("dbo.PVModules", "ModulesPer20FT");
            DropColumn("dbo.PVModules", "ModulesPer40FT");
            DropColumn("dbo.PVModules", "QTY");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PVModules", "QTY", c => c.Int(nullable: false));
            AddColumn("dbo.PVModules", "ModulesPer40FT", c => c.Int(nullable: false));
            AddColumn("dbo.PVModules", "ModulesPer20FT", c => c.Int(nullable: false));
            AddColumn("dbo.MountingStructures", "QTY", c => c.Int(nullable: false));
            AddColumn("dbo.Inverters", "Conf_Value", c => c.Double(nullable: false));
            AddColumn("dbo.Inverters", "QTY", c => c.Int(nullable: false));
            AddColumn("dbo.Inverters", "ModulesPer40FT", c => c.Int(nullable: false));
            AddColumn("dbo.Inverters", "ModulesPer20FT", c => c.Int(nullable: false));
            AddColumn("dbo.BiInverters", "Conf_Value", c => c.Double(nullable: false));
            AddColumn("dbo.BiInverters", "QTY", c => c.Int(nullable: false));
            AddColumn("dbo.BiInverters", "ModulesPer40FT", c => c.Int(nullable: false));
            AddColumn("dbo.BiInverters", "ModulesPer20FT", c => c.Int(nullable: false));
            AddColumn("dbo.Batteries", "QTY", c => c.Int(nullable: false));
            AddColumn("dbo.Batteries", "ModulesPer40FT", c => c.Int(nullable: false));
            AddColumn("dbo.Batteries", "ModulesPer20FT", c => c.Int(nullable: false));
            AddColumn("dbo.BalanceSystems", "QTY", c => c.Int(nullable: false));
            AddColumn("dbo.BalanceSystems", "ModulesPer40FT", c => c.Int(nullable: false));
            AddColumn("dbo.BalanceSystems", "ModulesPer20FT", c => c.Int(nullable: false));
            DropColumn("dbo.PVModules", "CountryOfOrigin");
            DropColumn("dbo.MountingStructures", "CountryOfOrigin");
            DropColumn("dbo.Inverters", "CountryOfOrigin");
            DropColumn("dbo.BiInverters", "CountryOfOrigin");
            DropColumn("dbo.Batteries", "CountryOfOrigin");
            DropColumn("dbo.BalanceSystems", "CountryOfOrigin");
        }
    }
}
