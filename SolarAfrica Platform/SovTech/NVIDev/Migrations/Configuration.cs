namespace NVIDev.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    using NVIDev.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Diagnostics;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<NVIDev.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(NVIDev.Models.ApplicationDbContext context)
        {
            //ApplicationUser Admin = new Models.ApplicationUser();
            //ApplicationUserManager manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            //await manager.CreateAsync(Admin, "@NVI1admin2");

            context.Configuration.LazyLoadingEnabled = true;
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            IdentityRole[] rolesToAdd = newRoles(new string[] { "NVIAdmin", "NVIUser", "TPAdmin", "TPUser", "Basic", "User" });

            foreach (var role in rolesToAdd)
            {
                try
                {
                    if (!roleManager.RoleExists(role.Name))
                    {
                        roleManager.Create(role);
                    }
                }
                catch
                {
                    //role exists
                }
            }
            context.SaveChanges();
            //IdentityRole basic = new IdentityRole();
            //basic.Id = "Basic";
            //basic.Name = "Basic";

            //IdentityRole usea = new IdentityRole();
            //usea.Id = "User";
            //usea.Name = "User";
            ////roleManager.Create(admin);
            //roleManager.Create(basic);
            //roleManager.Create(usea);
        }

        public IdentityRole[] newRoles(string[] roles)
        {
            IdentityRole[] rolesToAdd = new IdentityRole[roles.Count()];
            for (int a = 0; a < roles.Count(); a++)
            {
                IdentityRole rle = new IdentityRole();
                rle.Id = roles[a];
                rle.Name = roles[a];
                rolesToAdd[a] = rle;
            }
            return rolesToAdd;
        }
    }
}
