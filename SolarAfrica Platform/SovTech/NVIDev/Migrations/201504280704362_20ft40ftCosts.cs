namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20ft40ftCosts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ports", "DeliveryCosts20", c => c.String());
            AddColumn("dbo.Ports", "DeliveryCosts40", c => c.String());
            DropColumn("dbo.Ports", "DeliveryCosts");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Ports", "DeliveryCosts", c => c.String());
            DropColumn("dbo.Ports", "DeliveryCosts40");
            DropColumn("dbo.Ports", "DeliveryCosts20");
        }
    }
}
