namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mountingTemporary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "MountingInDollars", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "MountingContainersUsed20ft", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "MountingContainersUsed20ft");
            DropColumn("dbo.Projects", "MountingInDollars");
        }
    }
}
