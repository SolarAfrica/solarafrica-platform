namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GPSDistance : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "Details_Distance", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "Details_Distance");
        }
    }
}
