namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Ports : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ports",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        GPS = c.String(nullable: false),
                        Country = c.String(nullable: false),
                        COOs = c.String(),
                        DeliveryCosts = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Ports");
        }
    }
}
