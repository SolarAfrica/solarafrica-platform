namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveC_Userproperties : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Projects", "Details_C_Country");
            DropColumn("dbo.Projects", "Details_C_City");
            DropColumn("dbo.Projects", "Details_C_Road");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Projects", "Details_C_Road", c => c.String(nullable: false));
            AddColumn("dbo.Projects", "Details_C_City", c => c.String(nullable: false));
            AddColumn("dbo.Projects", "Details_C_Country", c => c.String(nullable: false));
        }
    }
}
