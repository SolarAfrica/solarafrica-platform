namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class QuotingSectionNew : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CostItems",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        UserID = c.String(),
                        Name = c.String(),
                        Cost = c.Double(nullable: false),
                        Markup = c.Double(nullable: false),
                        SellPrice = c.Double(nullable: false),
                        WPMark = c.String(),
                        Fixed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.Projects", "QuotingCostNames", c => c.String());
            AddColumn("dbo.Projects", "QuotingCostMains", c => c.String());
            AddColumn("dbo.Projects", "QuotingCostSuppliers", c => c.String());
            AddColumn("dbo.Projects", "QuotingCostCosts", c => c.String());
            AddColumn("dbo.Projects", "QuotingCostMarkups", c => c.String());
            AddColumn("dbo.Projects", "QuotingCostSellPrices", c => c.String());
            AddColumn("dbo.Projects", "QuotingCostWPMarks", c => c.String());
            AddColumn("dbo.Projects", "QuotingCostFixeds", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "QuotingCostFixeds");
            DropColumn("dbo.Projects", "QuotingCostWPMarks");
            DropColumn("dbo.Projects", "QuotingCostSellPrices");
            DropColumn("dbo.Projects", "QuotingCostMarkups");
            DropColumn("dbo.Projects", "QuotingCostCosts");
            DropColumn("dbo.Projects", "QuotingCostSuppliers");
            DropColumn("dbo.Projects", "QuotingCostMains");
            DropColumn("dbo.Projects", "QuotingCostNames");
            DropTable("dbo.CostItems");
        }
    }
}
