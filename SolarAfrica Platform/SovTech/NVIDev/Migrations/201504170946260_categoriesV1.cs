namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class categoriesV1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Type = c.String(nullable: false),
                        Taxes = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.BalanceSystems", "SubCategory", c => c.String(nullable: false));
            AddColumn("dbo.Batteries", "SubCategory", c => c.String(nullable: false));
            AddColumn("dbo.BiInverters", "SubCategory", c => c.String(nullable: false));
            AddColumn("dbo.Inverters", "SubCategory", c => c.String(nullable: false));
            AddColumn("dbo.MountingStructures", "SubCategory", c => c.String(nullable: false));
            AddColumn("dbo.PVModules", "SubCategory", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PVModules", "SubCategory");
            DropColumn("dbo.MountingStructures", "SubCategory");
            DropColumn("dbo.Inverters", "SubCategory");
            DropColumn("dbo.BiInverters", "SubCategory");
            DropColumn("dbo.Batteries", "SubCategory");
            DropColumn("dbo.BalanceSystems", "SubCategory");
            DropTable("dbo.Categories");
        }
    }
}
