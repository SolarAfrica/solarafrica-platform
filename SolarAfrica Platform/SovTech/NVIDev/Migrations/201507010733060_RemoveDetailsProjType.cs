namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveDetailsProjType : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Projects", "Details_ProjType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Projects", "Details_ProjType", c => c.String());
        }
    }
}
