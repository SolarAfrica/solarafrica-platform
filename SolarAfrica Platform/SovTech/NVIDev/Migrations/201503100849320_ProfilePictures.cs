namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProfilePictures : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Company", c => c.String());
            AddColumn("dbo.AspNetUsers", "Image", c => c.Binary());
            AddColumn("dbo.AspNetUsers", "Address_Street", c => c.String());
            AddColumn("dbo.AspNetUsers", "Address_City", c => c.String());
            AddColumn("dbo.AspNetUsers", "Address_Country", c => c.String());
            AddColumn("dbo.AspNetUsers", "Contact_Fullname", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Contact_Fullname");
            DropColumn("dbo.AspNetUsers", "Address_Country");
            DropColumn("dbo.AspNetUsers", "Address_City");
            DropColumn("dbo.AspNetUsers", "Address_Street");
            DropColumn("dbo.AspNetUsers", "Image");
            DropColumn("dbo.AspNetUsers", "Company");
        }
    }
}
