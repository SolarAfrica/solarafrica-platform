namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NVIAdjustments0217 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "in_SolarCapexFee", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "adj_SolarCapexFee", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "real_SolarCapexFee", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "in_UpfrontFee", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "real_UpfrontFee", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "in_AdminFee", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "real_AdminFee", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "in_UpfrontClient", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "real_UpfrontClient", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "in_FixedAnualCharge", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "adj_FixedAnualCharge", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "real_FixedAnualCharge", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "real_RI", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "real_RI");
            DropColumn("dbo.Projects", "real_FixedAnualCharge");
            DropColumn("dbo.Projects", "adj_FixedAnualCharge");
            DropColumn("dbo.Projects", "in_FixedAnualCharge");
            DropColumn("dbo.Projects", "real_UpfrontClient");
            DropColumn("dbo.Projects", "in_UpfrontClient");
            DropColumn("dbo.Projects", "real_AdminFee");
            DropColumn("dbo.Projects", "in_AdminFee");
            DropColumn("dbo.Projects", "real_UpfrontFee");
            DropColumn("dbo.Projects", "in_UpfrontFee");
            DropColumn("dbo.Projects", "real_SolarCapexFee");
            DropColumn("dbo.Projects", "adj_SolarCapexFee");
            DropColumn("dbo.Projects", "in_SolarCapexFee");
        }
    }
}
