namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Phase1VariablesComplete : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Countries", "CPIn", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "WearTear1", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "WearTear2", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "WearTear3", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "WearTear4", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "WearTear5", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation1", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation2", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation3", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation4", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation5", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation6", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation7", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation8", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation9", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation10", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation11", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation12", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation13", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation14", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation15", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation16", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation17", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation18", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation19", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "UtilityInflation20", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_ApplyForAllMonths", c => c.Boolean(nullable: false));
            AddColumn("dbo.Projects", "Costing_ClientDemand1", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_ClientDemand2", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_ClientDemand3", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_ClientDemand4", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_ClientDemand5", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_ClientDemand6", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_ClientDemand7", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_ClientDemand8", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_ClientDemand9", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_ClientDemand10", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_ClientDemand11", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_ClientDemand12", c => c.Double(nullable: false));
            DropColumn("dbo.Countries", "WearTear");
            DropColumn("dbo.NVIStatics", "CPIn");
        }
        
        public override void Down()
        {
            AddColumn("dbo.NVIStatics", "CPIn", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "WearTear", c => c.Double(nullable: false));
            DropColumn("dbo.Projects", "Costing_ClientDemand12");
            DropColumn("dbo.Projects", "Costing_ClientDemand11");
            DropColumn("dbo.Projects", "Costing_ClientDemand10");
            DropColumn("dbo.Projects", "Costing_ClientDemand9");
            DropColumn("dbo.Projects", "Costing_ClientDemand8");
            DropColumn("dbo.Projects", "Costing_ClientDemand7");
            DropColumn("dbo.Projects", "Costing_ClientDemand6");
            DropColumn("dbo.Projects", "Costing_ClientDemand5");
            DropColumn("dbo.Projects", "Costing_ClientDemand4");
            DropColumn("dbo.Projects", "Costing_ClientDemand3");
            DropColumn("dbo.Projects", "Costing_ClientDemand2");
            DropColumn("dbo.Projects", "Costing_ClientDemand1");
            DropColumn("dbo.Projects", "Costing_ApplyForAllMonths");
            DropColumn("dbo.Countries", "UtilityInflation20");
            DropColumn("dbo.Countries", "UtilityInflation19");
            DropColumn("dbo.Countries", "UtilityInflation18");
            DropColumn("dbo.Countries", "UtilityInflation17");
            DropColumn("dbo.Countries", "UtilityInflation16");
            DropColumn("dbo.Countries", "UtilityInflation15");
            DropColumn("dbo.Countries", "UtilityInflation14");
            DropColumn("dbo.Countries", "UtilityInflation13");
            DropColumn("dbo.Countries", "UtilityInflation12");
            DropColumn("dbo.Countries", "UtilityInflation11");
            DropColumn("dbo.Countries", "UtilityInflation10");
            DropColumn("dbo.Countries", "UtilityInflation9");
            DropColumn("dbo.Countries", "UtilityInflation8");
            DropColumn("dbo.Countries", "UtilityInflation7");
            DropColumn("dbo.Countries", "UtilityInflation6");
            DropColumn("dbo.Countries", "UtilityInflation5");
            DropColumn("dbo.Countries", "UtilityInflation4");
            DropColumn("dbo.Countries", "UtilityInflation3");
            DropColumn("dbo.Countries", "UtilityInflation2");
            DropColumn("dbo.Countries", "UtilityInflation1");
            DropColumn("dbo.Countries", "WearTear5");
            DropColumn("dbo.Countries", "WearTear4");
            DropColumn("dbo.Countries", "WearTear3");
            DropColumn("dbo.Countries", "WearTear2");
            DropColumn("dbo.Countries", "WearTear1");
            DropColumn("dbo.Countries", "CPIn");
        }
    }
}
