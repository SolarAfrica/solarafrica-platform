namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BalanceSystems",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Key = c.String(),
                        Type = c.String(nullable: false),
                        Manufacturer = c.String(nullable: false),
                        Description = c.String(),
                        Dimensions = c.String(),
                        UOM = c.String(),
                        Units = c.Int(nullable: false),
                        ItemCost = c.Int(nullable: false),
                        UnitCost = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        ModulesPerPallet = c.Int(nullable: false),
                        ModulesPer20FT = c.Int(nullable: false),
                        ModulesPer40FT = c.Int(nullable: false),
                        QTY = c.Int(nullable: false),
                        Conf_Value = c.Int(nullable: false),
                        IncoTerms = c.String(),
                        Log_Value = c.Int(nullable: false),
                        Log_ItemCost = c.Int(nullable: false),
                        Log_UnitCost = c.Int(nullable: false),
                        IDF = c.Int(nullable: false),
                        RDL = c.Int(nullable: false),
                        Duties = c.Int(nullable: false),
                        Duties_Value = c.Int(nullable: false),
                        Duties_ItemCost = c.Int(nullable: false),
                        Duties_UnitCost = c.Int(nullable: false),
                        TotalCost = c.Int(nullable: false),
                        T_UnitCosts = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Batteries",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Key = c.String(),
                        Manufacturer = c.String(nullable: false),
                        Description = c.String(),
                        Dimensions = c.String(),
                        UOM = c.String(),
                        UnitAh_C10 = c.Int(nullable: false),
                        ItemCost = c.Int(nullable: false),
                        UnitCost_WP = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        ModulesPerPallet = c.Int(nullable: false),
                        ModulesPer20FT = c.Int(nullable: false),
                        ModulesPer40FT = c.Int(nullable: false),
                        QTY = c.Int(nullable: false),
                        Conf_Value = c.Int(nullable: false),
                        IncoTerms = c.String(),
                        Log_Value = c.Int(nullable: false),
                        Log_ItemCost = c.Int(nullable: false),
                        Log_UnitCost = c.Int(nullable: false),
                        IDF = c.Int(nullable: false),
                        RDL = c.Int(nullable: false),
                        Duties = c.Int(nullable: false),
                        Duties_Value = c.Int(nullable: false),
                        Duties_ItemCost = c.Int(nullable: false),
                        Duties_UnitCost = c.Int(nullable: false),
                        TotalCost = c.Int(nullable: false),
                        T_UnitCosts = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BiInverters",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Key = c.String(),
                        Manufacturer = c.String(nullable: false),
                        Description = c.String(),
                        Dimensions = c.String(),
                        UOM = c.String(),
                        UnitWP = c.Int(nullable: false),
                        ItemCost = c.Int(nullable: false),
                        UnitCost_WP = c.Int(nullable: false),
                        CommunicationList = c.String(),
                        InterfaceList = c.String(),
                        ApplicationList = c.String(),
                        Price = c.Int(nullable: false),
                        ModulesPerPallet = c.Int(nullable: false),
                        ModulesPer20FT = c.Int(nullable: false),
                        ModulesPer40FT = c.Int(nullable: false),
                        QTY = c.Int(nullable: false),
                        Conf_Value = c.Int(nullable: false),
                        IncoTerms = c.String(),
                        Log_Value = c.Int(nullable: false),
                        Log_ItemCost = c.Int(nullable: false),
                        Log_UnitCost = c.Int(nullable: false),
                        IDF = c.Int(nullable: false),
                        RDL = c.Int(nullable: false),
                        Duties = c.Int(nullable: false),
                        Duties_Value = c.Int(nullable: false),
                        Duties_ItemCost = c.Int(nullable: false),
                        Duties_UnitCost = c.Int(nullable: false),
                        TotalCost = c.Int(nullable: false),
                        T_UnitCosts = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Currency = c.String(nullable: false),
                        Currency_Per_Dollar = c.Double(nullable: false),
                        CF = c.Double(nullable: false),
                        CPI = c.Double(nullable: false),
                        Ri = c.Double(nullable: false),
                        DivTX = c.Double(nullable: false),
                        DividendTax = c.Double(nullable: false),
                        S4AD = c.Double(nullable: false),
                        UtilityInflation = c.Double(nullable: false),
                        WearTear = c.Double(nullable: false),
                        Grid_Tied_Ri = c.Double(nullable: false),
                        DieselH_Ri = c.Double(nullable: false),
                        BatteryH_Ri = c.Double(nullable: false),
                        TaxPPA = c.Double(nullable: false),
                        TaxPBS = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Inverters",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Key = c.String(),
                        Manufacturer = c.String(nullable: false),
                        Description = c.String(),
                        Dimensions = c.String(),
                        UOM = c.String(),
                        UnitWP = c.Int(nullable: false),
                        ItemCost = c.Int(nullable: false),
                        UnitCost_WP = c.Int(nullable: false),
                        ConfigurationList = c.String(),
                        DCOverVoltageList = c.String(),
                        DCLoadDisconnectList = c.String(),
                        IOPortModulesList = c.String(),
                        CountrySpecificGridList = c.String(),
                        Price = c.Int(nullable: false),
                        ModulesPerPallet = c.Int(nullable: false),
                        ModulesPer20FT = c.Int(nullable: false),
                        ModulesPer40FT = c.Int(nullable: false),
                        QTY = c.Int(nullable: false),
                        Conf_Value = c.Int(nullable: false),
                        IncoTerms = c.String(nullable: false),
                        Log_Value = c.Int(nullable: false),
                        Log_ItemCost = c.Int(nullable: false),
                        Log_UnitCost = c.Int(nullable: false),
                        IDF = c.Int(nullable: false),
                        RDL = c.Int(nullable: false),
                        Duties = c.Int(nullable: false),
                        Duties_Value = c.Int(nullable: false),
                        Duties_ItemCost = c.Double(nullable: false),
                        Duties_UnitCost = c.Double(nullable: false),
                        TotalCost = c.Int(nullable: false),
                        T_UnitCosts = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MountingStructures",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Key = c.String(),
                        Type = c.String(nullable: false),
                        Manufacturer = c.String(nullable: false),
                        UnitWP = c.Int(nullable: false),
                        ItemCost = c.Int(nullable: false),
                        UnitCost_WP = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        QTY = c.Int(nullable: false),
                        Conf_Value = c.Int(nullable: false),
                        IncoTerms = c.String(),
                        Log_Value = c.Int(nullable: false),
                        Log_ItemCost = c.Int(nullable: false),
                        Log_UnitCost = c.Int(nullable: false),
                        IDF = c.Int(nullable: false),
                        RDL = c.Int(nullable: false),
                        Duties = c.Int(nullable: false),
                        Duties_Value = c.Int(nullable: false),
                        Duties_ItemCost = c.Int(nullable: false),
                        Duties_UnitCost = c.Int(nullable: false),
                        TotalCost = c.Int(nullable: false),
                        T_UnitCosts = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NVIStatics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RevisionDate = c.DateTime(nullable: false),
                        CPIn = c.Double(nullable: false),
                        BattP = c.Double(nullable: false),
                        TPAF = c.Double(nullable: false),
                        BatC = c.Double(nullable: false),
                        CPIu = c.Double(nullable: false),
                        Deg = c.Double(nullable: false),
                        CPIg = c.Double(nullable: false),
                        MU = c.Double(nullable: false),
                        BatNi = c.Double(nullable: false),
                        PBSPerc = c.Double(nullable: false),
                        Sic = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        AAdjustment = c.Double(nullable: false),
                        FAdjustment = c.Double(nullable: false),
                        UpAdj = c.Double(nullable: false),
                        FUP = c.Double(nullable: false),
                        FAdmin = c.Double(nullable: false),
                        RR = c.Boolean(nullable: false),
                        UserID = c.String(),
                        Revision = c.Double(nullable: false),
                        Details_CompanyName = c.String(nullable: false),
                        Details_ProjectName = c.String(nullable: false),
                        Details_Abbreviation = c.String(nullable: false, maxLength: 3),
                        Details_ProjType = c.String(),
                        Details_C_Country = c.String(nullable: false),
                        Details_C_City = c.String(nullable: false),
                        Details_C_Road = c.String(nullable: false),
                        Details_Title = c.String(nullable: false),
                        Details_Initial = c.String(nullable: false),
                        Details_Surname = c.String(nullable: false),
                        Details_EmailAddress = c.String(nullable: false),
                        Details_Telephone = c.String(nullable: false),
                        Details_P_Site = c.String(nullable: false),
                        Details_P_Country = c.String(nullable: false),
                        Details_P_City = c.String(nullable: false),
                        Details_P_Road = c.String(nullable: false),
                        Costing_Plan = c.String(),
                        Costing_Type = c.String(),
                        Costing_Country = c.String(),
                        Costing_Source = c.String(),
                        Costing_Yield = c.Int(nullable: false),
                        Costing_Term = c.Int(nullable: false),
                        Costing_Size = c.Double(nullable: false),
                        Costing_BatterySize = c.Int(nullable: false),
                        Costing_BatteryType = c.String(),
                        Costing_Num_Batteries = c.Double(nullable: false),
                        Costing_SunnyIslandCap = c.Double(nullable: false),
                        Costing_ElectricUsagePYear = c.Double(nullable: false),
                        Costing_GeneratorEfficiency = c.Double(nullable: false),
                        Costing_Structure = c.Double(nullable: false),
                        Costing_Demand = c.Double(nullable: false),
                        Costing_DieselPrice = c.Double(nullable: false),
                        Costing_DieselConsumer = c.Double(nullable: false),
                        Costing_DieselPercent = c.Double(nullable: false),
                        Costing_Generic = c.Boolean(nullable: false),
                        Costing_UseEPC = c.Boolean(nullable: false),
                        Costing_EPCPrice = c.Double(nullable: false),
                        Costing_TechnicalPartner = c.String(),
                        Costing_InstallFee = c.Double(nullable: false),
                        Costing_DesignFee = c.Double(nullable: false),
                        Costing_OMWorks = c.Double(nullable: false),
                        Costing_TPShare = c.Double(nullable: false),
                        Costing_TaxFlow = c.Boolean(nullable: false),
                        Costing_Rebate = c.Double(nullable: false),
                        Costing_Tarrif = c.Double(nullable: false),
                        Costing_BaseRate = c.Double(nullable: false),
                        Costing_TPRate = c.Double(nullable: false),
                        Costing_OMRate = c.Double(nullable: false),
                        Costing_StorageCost = c.Double(nullable: false),
                        Costing_IRRProject = c.Double(nullable: false),
                        Costing_Contingency = c.Double(nullable: false),
                        Costing_ExchangeRate = c.Double(nullable: false),
                        Costing_Currenct = c.String(),
                        Costing_Ri = c.Double(nullable: false),
                        Valid = c.Boolean(nullable: false),
                        InverterIDS = c.String(),
                        InverterAmounts = c.String(),
                        BiInverterIDS = c.String(),
                        BiInverterAmounts = c.String(),
                        MountingIDS = c.String(),
                        MountingAmounts = c.String(),
                        PVModuleIDS = c.String(),
                        PvModuleAmounts = c.String(),
                        BatteryIDS = c.String(),
                        BatteryAmounts = c.String(),
                        BalanceSystemIDS = c.String(),
                        BalanceSystemAmounts = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProSpecs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FPT = c.Double(nullable: false),
                        BOQ_Asset_101 = c.Double(nullable: false),
                        BOQ_Asset_390 = c.Double(nullable: false),
                        BOQ_Asset_500 = c.Double(nullable: false),
                        MinimumFee = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PVModules",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Key = c.String(),
                        Manufacturer = c.String(nullable: false),
                        Description = c.String(),
                        Dimensions = c.String(),
                        UOM = c.String(),
                        UnitWP = c.Int(nullable: false),
                        ItemCost = c.Int(nullable: false),
                        UnitCost_WP = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        ModulesPerPallet = c.Int(nullable: false),
                        ModulesPer20FT = c.Int(nullable: false),
                        ModulesPer40FT = c.Int(nullable: false),
                        QTY = c.Int(nullable: false),
                        Conf_Value = c.Int(nullable: false),
                        IncoTerms = c.String(),
                        Log_Value = c.Int(nullable: false),
                        Log_ItemCost = c.Int(nullable: false),
                        Log_UnitCost = c.Int(nullable: false),
                        IDF = c.Int(nullable: false),
                        RDL = c.Int(nullable: false),
                        Duties = c.Int(nullable: false),
                        Duties_Value = c.Int(nullable: false),
                        Duties_ItemCost = c.Int(nullable: false),
                        Duties_UnitCost = c.Int(nullable: false),
                        TotalCost = c.Int(nullable: false),
                        T_UnitCosts = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.PVModules");
            DropTable("dbo.ProSpecs");
            DropTable("dbo.Projects");
            DropTable("dbo.NVIStatics");
            DropTable("dbo.MountingStructures");
            DropTable("dbo.Inverters");
            DropTable("dbo.Countries");
            DropTable("dbo.BiInverters");
            DropTable("dbo.Batteries");
            DropTable("dbo.BalanceSystems");
        }
    }
}
