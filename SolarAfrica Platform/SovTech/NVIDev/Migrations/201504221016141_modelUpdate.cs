namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modelUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NVIStatics", "MarineInsure", c => c.Double(nullable: false));
            DropColumn("dbo.BalanceSystems", "Price");
            DropColumn("dbo.Batteries", "Price");
            DropColumn("dbo.BiInverters", "Price");
            DropColumn("dbo.Inverters", "Price");
            DropColumn("dbo.MountingStructures", "Price");
            DropColumn("dbo.PVModules", "Price");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PVModules", "Price", c => c.Double(nullable: false));
            AddColumn("dbo.MountingStructures", "Price", c => c.Double(nullable: false));
            AddColumn("dbo.Inverters", "Price", c => c.Double(nullable: false));
            AddColumn("dbo.BiInverters", "Price", c => c.Int(nullable: false));
            AddColumn("dbo.Batteries", "Price", c => c.Double(nullable: false));
            AddColumn("dbo.BalanceSystems", "Price", c => c.Double(nullable: false));
            DropColumn("dbo.NVIStatics", "MarineInsure");
        }
    }
}
