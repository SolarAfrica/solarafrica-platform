namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserTransfers : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.Projects", "UserID", "CurrentUserID");
            //AddColumn("dbo.Projects", "CurrentUserID", c => c.String());
            AddColumn("dbo.Projects", "CreatorID", c => c.String());
        }
        
        public override void Down()
        {
            RenameColumn("dbo.Projects", "CurrentUserID", "UserID");

            //AddColumn("dbo.Projects", "UserID", c => c.String());
            DropColumn("dbo.Projects", "CreatorID");
        }
    }
}
