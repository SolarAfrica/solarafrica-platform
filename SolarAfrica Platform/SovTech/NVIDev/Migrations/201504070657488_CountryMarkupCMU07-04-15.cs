namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CountryMarkupCMU070415 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Countries", "MU", c => c.Double(nullable: false));
            AddColumn("dbo.Countries", "CMU", c => c.Double(nullable: false));
            DropColumn("dbo.NVIStatics", "MU");
        }
        
        public override void Down()
        {
            AddColumn("dbo.NVIStatics", "MU", c => c.Double(nullable: false));
            DropColumn("dbo.Countries", "CMU");
            DropColumn("dbo.Countries", "MU");
        }
    }
}
