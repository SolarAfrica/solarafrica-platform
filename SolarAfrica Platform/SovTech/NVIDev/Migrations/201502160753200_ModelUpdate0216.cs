namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelUpdate0216 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "RIAdjustment", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Costing_TariffYN", c => c.Boolean(nullable: false));
            AddColumn("dbo.Projects", "Costing_OMYN", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "Costing_OMYN");
            DropColumn("dbo.Projects", "Costing_TariffYN");
            DropColumn("dbo.Projects", "RIAdjustment");
        }
    }
}
