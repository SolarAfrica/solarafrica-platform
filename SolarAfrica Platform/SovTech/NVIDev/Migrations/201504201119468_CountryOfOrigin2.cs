namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CountryOfOrigin2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CountryOfOrigins",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CountryOfOrigins");
        }
    }
}
