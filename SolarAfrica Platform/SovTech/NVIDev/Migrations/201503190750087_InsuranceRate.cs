namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InsuranceRate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NVIStatics", "InsR", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NVIStatics", "InsR");
        }
    }
}
