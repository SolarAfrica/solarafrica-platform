namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DutiesV116042015 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Duties",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Countries = c.String(),
                        AllCountries = c.Boolean(nullable: false),
                        Rate = c.Double(nullable: false),
                        VATRates = c.String(),
                        CustomsValue = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.Countries", "VATRate", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Countries", "VATRate");
            DropTable("dbo.Duties");
        }
    }
}
