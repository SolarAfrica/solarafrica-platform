namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EquipmentUpdate02102015 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.BalanceSystems", "Conf_Value");
            DropColumn("dbo.Batteries", "Conf_Value");
            DropColumn("dbo.MountingStructures", "Conf_Value");
            DropColumn("dbo.PVModules", "Conf_Value");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PVModules", "Conf_Value", c => c.Int(nullable: false));
            AddColumn("dbo.MountingStructures", "Conf_Value", c => c.Int(nullable: false));
            AddColumn("dbo.Batteries", "Conf_Value", c => c.Int(nullable: false));
            AddColumn("dbo.BalanceSystems", "Conf_Value", c => c.Int(nullable: false));
        }
    }
}
