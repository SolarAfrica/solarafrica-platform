namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NVIFEES : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Fees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Prelim_Gen_02 = c.Int(nullable: false),
                        Prelim_Gen_05 = c.Int(nullable: false),
                        Prelim_Gen_1 = c.Int(nullable: false),
                        Prelim_Gen_5 = c.Int(nullable: false),
                        Prelim_Gen_51 = c.Int(nullable: false),
                        Prelim_Gen_DHybrid = c.Int(nullable: false),
                        Procurement_02 = c.Int(nullable: false),
                        Procurement_05 = c.Int(nullable: false),
                        Procurement_1 = c.Int(nullable: false),
                        Procurement_5 = c.Int(nullable: false),
                        Procurement_51 = c.Int(nullable: false),
                        Procurement_DHybrid = c.Int(nullable: false),
                        Work_Cap_02 = c.Int(nullable: false),
                        Work_Cap_05 = c.Int(nullable: false),
                        Work_Cap_1 = c.Int(nullable: false),
                        Work_Cap_5 = c.Int(nullable: false),
                        Work_Cap_51 = c.Int(nullable: false),
                        Work_Cap_DHybrid = c.Int(nullable: false),
                        Prep_Vet_02 = c.Int(nullable: false),
                        Prep_Vet_05 = c.Int(nullable: false),
                        Prep_Vet_1 = c.Int(nullable: false),
                        Prep_Vet_5 = c.Int(nullable: false),
                        Prep_Vet_51 = c.Int(nullable: false),
                        Prep_Vet_DHybrid = c.Int(nullable: false),
                        Tech_Vet_02 = c.Int(nullable: false),
                        Tech_Vet_05 = c.Int(nullable: false),
                        Tech_Vet_1 = c.Int(nullable: false),
                        Tech_Vet_5 = c.Int(nullable: false),
                        Tech_Vet_51 = c.Int(nullable: false),
                        Tech_Vet_DHybrid = c.Int(nullable: false),
                        Performance_GaranT_02 = c.Int(nullable: false),
                        Performance_GaranT_05 = c.Int(nullable: false),
                        Performance_GaranT_1 = c.Int(nullable: false),
                        Performance_GaranT_5 = c.Int(nullable: false),
                        Performance_GaranT_51 = c.Int(nullable: false),
                        Performance_GaranT_DHybrid = c.Int(nullable: false),
                        Project_Manage_02 = c.Int(nullable: false),
                        Project_Manage_05 = c.Int(nullable: false),
                        Project_Manage_1 = c.Int(nullable: false),
                        Project_Manage_5 = c.Int(nullable: false),
                        Project_Manage_51 = c.Int(nullable: false),
                        Project_Manage_DHybrid = c.Int(nullable: false),
                        Commissioning_02 = c.Int(nullable: false),
                        Commissioning_05 = c.Int(nullable: false),
                        Commissioning_1 = c.Int(nullable: false),
                        Commissioning_5 = c.Int(nullable: false),
                        Commissioning_51 = c.Int(nullable: false),
                        Commissioning_DHybrid = c.Int(nullable: false),
                        Margin_Contin_02 = c.Int(nullable: false),
                        Margin_Contin_05 = c.Int(nullable: false),
                        Margin_Contin_1 = c.Int(nullable: false),
                        Margin_Contin_5 = c.Int(nullable: false),
                        Margin_Contin_51 = c.Int(nullable: false),
                        Margin_Contin_DHybrid = c.Int(nullable: false),
                        Monitor_Software_02 = c.Int(nullable: false),
                        Monitor_Software_05 = c.Int(nullable: false),
                        Monitor_Software_1 = c.Int(nullable: false),
                        Monitor_Software_5 = c.Int(nullable: false),
                        Monitor_Software_51 = c.Int(nullable: false),
                        Monitor_Software_PBS = c.Int(nullable: false),
                        Monitor_Software_PPA = c.Int(nullable: false),
                        Monitor_Software_SDS = c.Int(nullable: false),
                        Monitor_Software_DHybrid = c.Int(nullable: false),
                        Admin_02 = c.Int(nullable: false),
                        Admin_05 = c.Int(nullable: false),
                        Admin_1 = c.Int(nullable: false),
                        Admin_5 = c.Int(nullable: false),
                        Admin_51 = c.Int(nullable: false),
                        Admin_PBS = c.Int(nullable: false),
                        Admin_PPA = c.Int(nullable: false),
                        Admin_SDS = c.Int(nullable: false),
                        Admin_DHybrid = c.Int(nullable: false),
                        Asset_Manage_02 = c.Int(nullable: false),
                        Asset_Manage_05 = c.Int(nullable: false),
                        Asset_Manage_1 = c.Int(nullable: false),
                        Asset_Manage_5 = c.Int(nullable: false),
                        Asset_Manage_51 = c.Int(nullable: false),
                        Asset_Manage_PBS = c.Int(nullable: false),
                        Asset_Manage_PPA = c.Int(nullable: false),
                        Asset_Manage_SDS = c.Int(nullable: false),
                        Asset_Manage_DHybrid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Fees");
        }
    }
}
