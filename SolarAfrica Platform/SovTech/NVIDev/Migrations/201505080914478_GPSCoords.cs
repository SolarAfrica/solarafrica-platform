namespace NVIDev.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GPSCoords : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "Details_GPSCoords", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "Details_GPSCoords");
        }
    }
}
