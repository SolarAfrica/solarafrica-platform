﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NVIDev.Startup))]
namespace NVIDev
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
