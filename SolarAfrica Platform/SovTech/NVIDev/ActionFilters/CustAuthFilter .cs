﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NVIDev.Models;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Web.ModelBinding;
using System.Security.Principal;
 
using Microsoft.Owin.Security;

namespace NVIDev.ActionFilters
{
    public class CustAuthFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                DBHelper db = new DBHelper();
                HttpCookie authCookie = HttpContext.Current.Request.Cookies["ASP.NETNVICookie"];
                string userid = HttpContext.Current.User.Identity.GetUserId();
                string UserName = HttpContext.Current.User.Identity.GetUserName();
                ApplicationUser au= db.GetUser(userid);
                HttpContext.Current.Session["UserName"] = UserName;
                HttpContext.Current.Session["CompanyName"] = au.Company;
            }
            else
            {
                HttpContext.Current.Session.Abandon();
                HttpContext.Current.Session.RemoveAll();
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
                  //HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}