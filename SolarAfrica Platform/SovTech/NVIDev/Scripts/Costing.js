﻿var a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12;

$(document).ready(function () {


    setTimeout(function () {
        setVals();
        valuesHaveBeenSet = true;
        CheckBoxes()
    },500)
       
  
 


   
})


function CheckBoxes() {
    var selectVal = $('#EnergySourceSelect').val()
    ////Grid-Tied", "Diesel", "Both

    //mdPrice   mdPerc   mdEff mdUse
    switch (selectVal) {
        case "Grid-Tied": $('#gridTarriffield').prop("disabled", false); $('#Costing_GeneratorEfficiencyfield').prop("disabled", true); $('#mdtar').addClass('active').removeClass('hidden'); $('#mdPrice').addClass('hidden').removeClass('active'); $('#Costing_DieselConsumerfield').prop("disabled", true); $('#mdUse').addClass('hidden').removeClass('active'); $('#Costing_GeneratorEfficiencyfield').prop("disabled", true); $('#mdEff').addClass('hidden').removeClass('active'); $('#Costing_DieselPercentfield').prop("disabled", true); $('#mdPerc').addClass('hidden').removeClass('active'); $('#Costing_DieselPercent').val(0); break;
        case "Diesel": $('#gridTarriffield').prop("disabled", true); $('#Costing_GeneratorEfficiencyfield').prop("disabled", false); $('#mdtar').addClass('hidden').removeClass('active'); $('#mdPrice').addClass('active').removeClass('hidden'); $('#Costing_DieselConsumerfield').prop("disabled", false); $('#mdUse').addClass('active').removeClass('hidden'); $('#Costing_GeneratorEfficiencyfield').prop("disabled", false); $('#mdEff').addClass('active').removeClass('hidden'); $('#Costing_DieselPercentfield').prop("disabled", true); $('#Costing_DieselPercentfield').val(100); $('#Costing_DieselPercent').val(100); $('#mdPerc').addClass('active').removeClass('hidden'); break;
        case "Both": $('#gridTarriffield').prop("disabled", false); $('#Costing_GeneratorEfficiencyfield').prop("disabled", false); $('#mdtar').addClass('active').removeClass('hidden'); $('#mdPrice').addClass('active').removeClass('hidden'); $('#Costing_DieselConsumerfield').prop("disabled", false); $('#mdUse').addClass('active').removeClass('hidden'); $('#Costing_GeneratorEfficiencyfield').prop("disabled", false); $('#mdEff').addClass('active').removeClass('hidden'); $('#Costing_DieselPercentfield').prop("disabled", false); $('#mdPerc').addClass('active').removeClass('hidden'); break;
    }
}//GeneratorEff DieselMonth DieselPerc DieselPrice

var valuesHaveBeenSet = false;
function CheckDemand() //if apply to all give each box the value of 1 , grey 2 - 12
{

    if ($('#ApplyAll').is(':checked') && !valuesHaveBeenSet)
    {

    }
   

    if ($('#ApplyAll').is(':checked')) {

        var values = removeCommas($("#Costing_ClientDemand1field").val());




        $('#Costing_ClientDemand2field').prop("disabled", true); $('#Costing_ClientDemand2field').val(numberWithCommas(values)); $('#Costing_ClientDemand2').val(parseFloat(removeCommas(values)));
        $('#Costing_ClientDemand3field').prop("disabled", true); $('#Costing_ClientDemand3field').val(numberWithCommas(values)); $('#Costing_ClientDemand3').val(parseFloat(removeCommas(values)));
        $('#Costing_ClientDemand4field').prop("disabled", true); $('#Costing_ClientDemand4field').val(numberWithCommas(values)); $('#Costing_ClientDemand4').val(parseFloat(removeCommas(values)));
        $('#Costing_ClientDemand5field').prop("disabled", true); $('#Costing_ClientDemand5field').val(numberWithCommas(values)); $('#Costing_ClientDemand5').val(parseFloat(removeCommas(values)));
        $('#Costing_ClientDemand6field').prop("disabled", true); $('#Costing_ClientDemand6field').val(numberWithCommas(values)); $('#Costing_ClientDemand6').val(parseFloat(removeCommas(values)));
        $('#Costing_ClientDemand7field').prop("disabled", true); $('#Costing_ClientDemand7field').val(numberWithCommas(values)); $('#Costing_ClientDemand7').val(parseFloat(removeCommas(values)));
        $('#Costing_ClientDemand8field').prop("disabled", true); $('#Costing_ClientDemand8field').val(numberWithCommas(values)); $('#Costing_ClientDemand8').val(parseFloat(removeCommas(values)));
        $('#Costing_ClientDemand9field').prop("disabled", true); $('#Costing_ClientDemand9field').val(numberWithCommas(values)); $('#Costing_ClientDemand9').val(parseFloat(removeCommas(values)));
        $('#Costing_ClientDemand10field').prop("disabled", true); $('#Costing_ClientDemand10field').val(numberWithCommas(values)); $('#Costing_ClientDemand10').val(parseFloat(removeCommas(values)));
        $('#Costing_ClientDemand11field').prop("disabled", true); $('#Costing_ClientDemand11field').val(numberWithCommas(values)); $('#Costing_ClientDemand11').val(parseFloat(removeCommas(values)));
        $('#Costing_ClientDemand12field').prop("disabled", true); $('#Costing_ClientDemand12field').val(numberWithCommas(values)); $('#Costing_ClientDemand12').val(parseFloat(removeCommas(values)));



    }
    else {
        $('#Costing_ClientDemand2field').prop("disabled", false);
        $('#Costing_ClientDemand3field').prop("disabled", false);
        $('#Costing_ClientDemand4field').prop("disabled", false);
        $('#Costing_ClientDemand5field').prop("disabled", false);
        $('#Costing_ClientDemand6field').prop("disabled", false);
        $('#Costing_ClientDemand7field').prop("disabled", false);
        $('#Costing_ClientDemand8field').prop("disabled", false);
        $('#Costing_ClientDemand9field').prop("disabled", false);
        $('#Costing_ClientDemand10field').prop("disabled", false);
        $('#Costing_ClientDemand11field').prop("disabled", false);
        $('#Costing_ClientDemand12field').prop("disabled", false);

 

        //if (a2 != null) {


           
        //}

    }
}

function setVals()
{
    a2 = $('#Costing_ClientDemand2field').val();
    a3 = $('#Costing_ClientDemand3field').val();
    a4 = $('#Costing_ClientDemand4field').val();
    a5 = $('#Costing_ClientDemand5field').val();
    a6 = $('#Costing_ClientDemand6field').val();
    a7 = $('#Costing_ClientDemand7field').val();
    a8 = $('#Costing_ClientDemand8field').val();
    a9 = $('#Costing_ClientDemand9field').val();
    a10 = $('#Costing_ClientDemand10field').val();
    a11 = $('#Costing_ClientDemand11field').val();
    a12 = $('#Costing_ClientDemand12field').val();
}


function getVals()
{

        $('#Costing_ClientDemand2field').val(a2);
        $('#Costing_ClientDemand3field').val(a3);
        $('#Costing_ClientDemand4field').val(a4);
        $('#Costing_ClientDemand5field').val(a5);
        $('#Costing_ClientDemand6field').val(a6);
        $('#Costing_ClientDemand7field').val(a7);
        $('#Costing_ClientDemand8field').val(a8);
        $('#Costing_ClientDemand9field').val(a9);
        $('#Costing_ClientDemand10field').val(a10);
        $('#Costing_ClientDemand11field').val(a11);
        $('#Costing_ClientDemand12field').val(a12);

        $('#Costing_ClientDemand2').val(parseFloat(removeCommas(a2)));
        $('#Costing_ClientDemand3').val(parseFloat(removeCommas(a3)));
        $('#Costing_ClientDemand4').val(parseFloat(removeCommas(a4)));
        $('#Costing_ClientDemand5').val(parseFloat(removeCommas(a5)));
        $('#Costing_ClientDemand6').val(parseFloat(removeCommas(a6)));
        $('#Costing_ClientDemand7').val(parseFloat(removeCommas(a7)));
        $('#Costing_ClientDemand8').val(parseFloat(removeCommas(a8)));
        $('#Costing_ClientDemand9').val(parseFloat(removeCommas(a9)));
        $('#Costing_ClientDemand10').val(parseFloat(removeCommas(a10)));
        $('#Costing_ClientDemand11').val(parseFloat(removeCommas(a11)));
        $('#Costing_ClientDemand12').val(parseFloat(removeCommas(a12)));
}


function CheckDemandA()
{
    if($('#ApplyAll').is(':checked'))
    {
        setVals();
    }

    else {

        getVals();
    }

    CheckDemand();
}