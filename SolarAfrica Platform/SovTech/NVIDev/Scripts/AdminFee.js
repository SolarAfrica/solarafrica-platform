﻿//checkbox and add total cost

$(document).ready(function()
{
    Tally();

})
var exch = $('#Exch').text();
var curr = $('#Curr').text();
function Tally()
{

    var TotalUpfront = 0;
    var TotalOngoing = 0;
    var MarginCont = 0;
    var vals = $('#project_UsePrelim').is(":checked");
    if(vals)
    {
      
        TotalUpfront += parseFloat($('#PrelimCost').text().replace(",", ""))
    }


   
     vals = $('#project_UseProcurement').is(":checked");
    if (vals) {
        
        TotalUpfront += parseFloat($('#ProcurementCost').text().replace(",",""))
    }

    vals = $('#project_UseWorkingCap').is(":checked");
    if (vals) {

        TotalUpfront += parseFloat($('#WorkingCost').text().replace(",", ""))
    }

    vals = $('#project_UsePrepVet').is(":checked");
    if (vals) {

        TotalUpfront += parseFloat($('#PrepareCost').text().replace(",", ""))
    }
    vals = $('#project_UseTechVet').is(":checked");
    if (vals) {

        TotalUpfront += parseFloat($('#TechnicalCost').text().replace(",", ""))
    }
    vals = $('#project_UsePerformanceG').is(":checked");
    if (vals) {

        TotalUpfront += parseFloat($('#PerformCost').text().replace(",", ""))
    }

    vals = $('#project_UseProjectManage').is(":checked");
    if (vals) {

        TotalUpfront += parseFloat($('#ProjectCost').text().replace(",", ""))
    }

    vals = $('#project_UseCommisioning').is(":checked");
    if (vals) {

        TotalUpfront += parseFloat($('#ComissioningCost').text().replace(",", ""))
    }


    //Ongoing Costs

    vals = $('#project_UseMonitorSoftware').is(":checked");
    if (vals) {

        TotalOngoing += parseFloat($('#MonitoringCost').text().replace(",", ""))
    }
    vals = $('#project_UseAdmin').is(":checked");
    if (vals) {

        TotalOngoing += parseFloat($('#AdminCost').text().replace(",", ""))
    }

    vals = $('#project_UseAssetManagement').is(":checked");
    if (vals) {

        TotalOngoing += parseFloat($('#AssetCost').text().replace(",", ""))
    }

    MarginCont = TotalUpfront * parseFloat($('#MarginDriver').text().replace(",", "") / 100)
    TotalUpfront += MarginCont;
    $('#MyUpfront').text('Upfront Cost : ' + curr + ' ' + TotalUpfront.toFixed(2))
    $('#MyOngoing').text('Ongoing Cost : ' + curr + ' ' + TotalOngoing.toFixed(2))
    $('#Upfront').val(TotalUpfront);
    $('#OnGoingFee_Admin').val(TotalOngoing.toFixed(2));
    //MarginDriver MarginCost
    $('#MarginCost').text(MarginCont.toFixed(2))
}