﻿function ChangeSolarCap(trig) {
    var oldRI = removeCommas($("#OldSolarCap").text())
    $("#NewSolarCap").text(numberWithCommas((oldRI * (1 + removeCommas($(trig).val()) / 100)).toFixed(2)))
}

function ChangeRI(trig) {


    var oldRI = parseFloat($("#OldRI").text())

    $("#NewRI").text(numberWithCommas((oldRI + parseFloat(removeCommas($(trig).val()))).toFixed(2)))

}

function ChangeAdmin(trig) {
    var oldRI = removeCommas($("#OldAdminFee").text())

    $("#NewAdminFee").text(numberWithCommas((oldRI * (1 + removeCommas($(trig).val()) / 100)).toFixed(2)))

}

function ChangeUpfront(trig) {
    var oldRI = parseFloat(removeCommas($("#OldUpfrontFee").text()))
    $("#NewUpfrontFee").text(numberWithCommas((oldRI * (1 + removeCommas($(trig).val()) / 100)).toFixed(2)))

}

function ChangeUpfrontClient(trig) {

    var oldRI = parseFloat(removeCommas($("#OldUpfrontClient").text()))

    $("#NewUpfrontClient").text(numberWithCommas((parseFloat(oldRI + removeCommas(parseFloat($(trig).val()))).toFixed(2))))

}

function ChangeFixedAnnual(trig) {


    $("#NewFixedAnnual").text(numberWithCommas((parseFloat(removeCommas($(trig).val()))).toFixed(2)))

}


function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function removeCommas(x) {
    return x.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "")
}