﻿setInterval(Tally, 4000)

var exch = $('#Exch').text();
var curr = $('#Curr').text();



function toggleTable(tab, row) {

    var id = '#' + tab;
    var hasclass = $(id).hasClass("active")
    if (hasclass) {
        $(id).addClass("hidden").removeClass("active")
    }

    else {
        $(id).addClass("active").removeClass("hidden")

    }


    var rd = "#r" + row;
   
    var hasPlus = $(rd).hasClass('glyphicon-plus-sign')


    if (hasPlus)
    {
        $(rd).removeClass("glyphicon-plus-sign")

        $(rd).addClass("glyphicon-minus-sign")
    }

    else {
        $(rd).removeClass("glyphicon-minus-sign")

        $(rd).addClass("glyphicon-plus-sign")
    }

}



function Changer(trig) {


    try {
        var pare = trig.parentNode.parentNode;
        $.ajax({
            url: '/Project/GetInverterSpecs/',
            data: { id: $(trig).val() },
            type: 'POST',
            dataType: 'json',
            success: function (data) {


               
                var itemNumID = '#' + pare.childNodes[1].childNodes[0].id

                $(itemNumID).text(data[7][0])

                var configID = '#' + pare.childNodes[3].childNodes[0].id
                var configs = data[0];
                var options = $(configID);
                options.empty();

                for (i = 0; i < configs.length; i++) {
                    options.append($('<option />').val(configs[i]).text(configs[i]));

                }



                var dclists = data[1];
                var dcvID = '#' + pare.childNodes[5].childNodes[0].id
                var dcVolts = $(dcvID);
                dcVolts.empty();

                for (i = 0; i < dclists.length; i++) {
                    dcVolts.append($('<option />').val(dclists[i]).text(dclists[i]));

                }

                var dLOADID = '#' + pare.childNodes[7].childNodes[0].id

                var dcLoad = $(dLOADID);
                dcLoad.empty();

                var dclodus = data[2];
                for (i = 0; i < dclists.length; i++) {
                    dcLoad.append($('<option />').val(dclodus[i]).text(dclodus[i]));

                }


                var ioOADID = '#' + pare.childNodes[9].childNodes[0].id

                var ioloads = $(ioOADID);
                ioloads.empty();

                var iolist = data[3];
                for (i = 0; i < dclists.length; i++) {
                    ioloads.append($('<option />').val(iolist[i]).text(iolist[i]));

                }


                var countLoadID = '#' + pare.childNodes[11].childNodes[0].id

                var countload = $(countLoadID);
                countload.empty();

                var countlist = data[3];
                for (i = 0; i < dclists.length; i++) {
                    countload.append($('<option />').val(countlist[i]).text(countlist[i]));

                }





                var amouns = '#' + pare.childNodes[17].childNodes[1].id
                var _amount = $(amouns);



                var Wattid = '#' + pare.childNodes[15].childNodes[1].id
                var wattContainer = $(Wattid);


                var moduleSystem = data[5];
                var wattage = data[6];
                var _wattage = wattage[0]/1000
                var _price = wattage[1];

                var PriceContainerID = '#' + pare.childNodes[19].childNodes[0].id



                _wattage = _wattage * $(amouns).val();
                _price = wattage[1] * $(amouns).val();



                $(Wattid).text('' + numberWithCommas(_wattage.toFixed(2)) + ' kW')
                $(PriceContainerID).text('' + numberWithCommas( parseFloat(_price * exch).toFixed(2)) + " " + curr)



            }
        }); Tally()

    }

    catch (ex) {
        Tally()
    }
    Tally()
}



function priceCheck(obs) {

    var pare = obs.parentNode.parentNode;
    var trig = pare.childNodes[3].childNodes[1]


    $.ajax({
        url: '/Project/GetInverterSpecs/',
        data: { id: $(trig).val() },
        type: 'POST',
        dataType: 'json',
        success: function (data) {







            var itemNumID = '#' + pare.childNodes[1].childNodes[0].id
            $(itemNumID).text(data[7][0])



            var amouns = '#' + pare.childNodes[17].childNodes[1].id
            var _amount = $(amouns);



            var Wattid = '#' + pare.childNodes[15].childNodes[1].id

            var wattContainer = $(Wattid);


            var moduleSystem = data[5];
            var wattage = data[6];
            var _wattage = wattage[0] / 1000
            var _price = wattage[1];

            var PriceContainerID = '#' + pare.childNodes[19].childNodes[0].id

            _wattage = _wattage * $(amouns).val();
            _price = wattage[1] * $(amouns).val();

            $(Wattid).text('' + numberWithCommas(_wattage.toFixed(2)) + ' kW')
            $(PriceContainerID).text('' + numberWithCommas(parseFloat(_price * exch).toFixed(2)) + " " + curr)



        }
    }); Tally()

}






function priceCheck2(obs) {
    var pare = obs.parentNode.parentNode;
    var trig = pare.childNodes[3].childNodes[0]


    $.ajax({
        url: '/Project/GetBiInverterSpecs/',
        data: { id: $(trig).val() },
        type: 'POST',
        dataType: 'json',
        success: function (data) {








            var amouns = '#' + pare.childNodes[13].childNodes[2].id




            var Wattid = '#' + pare.childNodes[11].childNodes[0].id
            var wattContainer = $(Wattid);


            var moduleSystem = data[3];
            var wattage = data[4];
            var _wattage = wattage[0] / 1000
            var _price = wattage[1];

            var PriceContainerID = '#' + pare.childNodes[15].childNodes[0].id

            _wattage = $(amouns).val() * _wattage;
            _price = _price * $(amouns).val();


            $(Wattid).text('' + numberWithCommas(_wattage.toFixed(2)) + ' kW')
            $(PriceContainerID).text('' + numberWithCommas(parseFloat(_price * exch).toFixed(2)) + " " + curr)



        }
    }); Tally()
    Tally()
}



function Changer2(trig) {

    var pare = trig.parentNode.parentNode;

    $.ajax({
        url: '/Project/GetBiInverterSpecs/',
        data: { id: $(trig).val() },
        type: 'POST',
        dataType: 'json',
        success: function (data) {


            var itemNumID = '#' + pare.childNodes[1].childNodes[0].id
            $(itemNumID).text(data[5][0])



            var comID = '#' + pare.childNodes[5].childNodes[0].id
            var comigs = data[0];
            var options = $(comID);
            options.empty();

            for (i = 0; i < comigs.length; i++) {
                options.append($('<option />').val(comigs[i]).text(comigs[i]));

            }



            var dclists = data[1];
            var dcvID = '#' + pare.childNodes[9].childNodes[0].id
            var dcVolts = $(dcvID);
            dcVolts.empty();

            for (i = 0; i < dclists.length; i++) {
                dcVolts.append($('<option />').val(dclists[i]).text(dclists[i]));

            }

            var dLOADID = '#' + pare.childNodes[7].childNodes[0].id

            var dcLoad = $(dLOADID);
            dcLoad.empty();

            var dclodus = data[2];
            for (i = 0; i < dclists.length; i++) {
                dcLoad.append($('<option />').val(dclodus[i]).text(dclodus[i]));

            }






            var amouns = '#' + pare.childNodes[13].childNodes[1].id




            var Wattid = '#' + pare.childNodes[11].id
            var wattContainer = $(Wattid);


            var moduleSystem = data[3];
            var wattage = data[4];
            var _wattage = wattage[0] / 1000
            var _price = wattage[1];

            var PriceContainerID = '#' + pare.childNodes[15].id

            _wattage = $(amouns).val() * _wattage;
            _price = _price * $(amouns).val();
            $(Wattid).text('' + numberWithCommas(_wattage.toFixed(2)) + ' kW')
            $(PriceContainerID).text('' + numberWithCommas(parseFloat(_price * exch).toFixed(2)) + " " + curr)




        }
    });
    Tally()

}



function Changer3(trig) {

    var pare = trig.parentNode.parentNode;
    trig = pare.childNodes[3].childNodes[0]
    $.ajax({
        url: '/Project/GetMountSpecs/',
        data: { id: $(trig).val() },
        type: 'POST',
        dataType: 'json',
        success: function (data) {








            var amouns = '#' + pare.childNodes[7].childNodes[0].id



            var Wattid = '#' + pare.childNodes[5].id
            var wattContainer = $(Wattid);



            var _wattage = data[0] / 1000
            var _price = data[1];

            var PriceContainerID = '#' + pare.childNodes[9].id
            _wattage = _wattage * $(amouns).val();
            _price = _price * $(amouns).val();

            $(Wattid).text('' + numberWithCommas(_wattage.toFixed(2)) + ' kW')
            $(PriceContainerID).text('' +numberWithCommas( parseFloat(_price * exch).toFixed(2)) + " " + curr)




        }
    });

    Tally()

}



function Changer4(trig) {

    var pare = trig.parentNode.parentNode;
    trig = pare.childNodes[3].childNodes[0]

    $.ajax({
        url: '/Project/GetBatSpecs/',
        data: { id: $(trig).val() },
        type: 'POST',
        dataType: 'json',
        success: function (data) {




            var itemNumID = '#' + pare.childNodes[1].childNodes[0].id;

            $(itemNumID).text(data[0])

            var amouns = '#' + pare.childNodes[7].childNodes[1].id



            var Wattid = '#' + pare.childNodes[5].childNodes[0].id
            var wattContainer = $(Wattid);



            var PriceContainerID = '#' + pare.childNodes[9].childNodes[0].id

            var moduleSystem = data[1];
            var wattage = data[2];
            var _wattage = wattage[0] / 1000
            var _price = wattage[1];
            _wattage = $(amouns).val() * _wattage;
            _price = _price * $(amouns).val();

            $(Wattid).text('' + numberWithCommas(_wattage.toFixed(2)) + ' kWh')
            $(PriceContainerID).text('' + numberWithCommas(parseFloat(_price * exch).toFixed(2)) + " " + curr)




        }
    });

    Tally()

}


function Tally() {
    var totalCost = 0;
    var totalWatts = 0;
    var totalStorage = 0;
    $("*[id*='price']").each(function () {


        totalCost += parseFloat(removeCommas($(this).text()))

    });


    $("*[id*='watt']").each(function () {

        totalWatts += parseFloat(removeCommas($(this).text()))

    });

    $("*[id*='storage']").each(function () {

        numberWithCommas()
        totalStorage +=  parseFloat(  removeCommas($(this).text()))

    });


    totalCost += $("#mounting").val() * exch;

    $('#totalCost').text('Total Cost: ' + numberWithCommas(totalCost) + " " + curr)
    $('#totalWatts').text('System Size: ' + " " + numberWithCommas(totalWatts) + ' kWp')
    $('#totalStorage').text('Total Storage: ' + " " + numberWithCommas(totalStorage / 1000) + ' kAh')



}


function ToggleMe(item) {
    $(item).next("div").slideToggle("slow");
}


function Changer5(trig) {

    var pare = trig.parentNode.parentNode;
    trig = pare.childNodes[3].childNodes[0]



    $.ajax({
        url: '/Project/GetBalanceSpecs/',
        data: { id: $(trig).val() },
        type: 'POST',
        dataType: 'json',
        success: function (data) {





            var itemNumID = '#' + pare.childNodes[1].id
            $(itemNumID).text(data[2][0])


            var amouns = '#' + pare.childNodes[5].childNodes[1].id



            //var Wattid = '#' + pare.childNodes[7].id
            //var wattContainer = $(Wattid);



            var PriceContainerID = '#' + pare.childNodes[7].childNodes[0].id

            var moduleSystem = data[0];
            var wattage = data[1];
            var _wattage = wattage[0] / 1000
            var _price = wattage[1];


            _wattage = $(amouns).val() * _wattage;
            _price = _price * $(amouns).val();


            //$(Wattid).text('' + numberWithCommas(_wattage.toFixed(2)) + 'W Capable')
            $(PriceContainerID).text('' + numberWithCommas(parseFloat(_price * exch).toFixed(2)) + " " + curr)




        }
    });

    Tally()

}

function Changer6(trig) {

    var pare = trig.parentNode.parentNode;
    trig = pare.childNodes[3].childNodes[0]

    $.ajax({
        url: '/Project/GetPVSpecs/',
        data: { id: $(trig).val() },
        type: 'POST',
        dataType: 'json',
        success: function (data) {



            var KeyID = '#' + pare.childNodes[1].childNodes[0].id

            $(KeyID).text(data[2][0])


            var amouns = '#' + pare.childNodes[7].childNodes[1].id



            var Wattid = '#' + pare.childNodes[5].childNodes[0].id
            var wattContainer = $(Wattid);



            var PriceContainerID = '#' + pare.childNodes[9].childNodes[0].id

            var moduleSystem = data[0];
            var wattage = data[1];
            var _wattage = wattage[0] / 1000
            var _price = wattage[1];

            _wattage = $(amouns).val() * _wattage;
            _price = _price * $(amouns).val();

            $(Wattid).text('' +numberWithCommas( _wattage.toFixed(2)) + ' kW')
            $(PriceContainerID).text('' + numberWithCommas(parseFloat(_price * exch).toFixed(2)) + " " + curr)




        }
    });

    Tally()

}


$(window).ready(function () {




    var amounts = $("*[id*='Amount']")



    for (i = 0; i < amounts.length; i++) {

        $(amounts[i]).trigger("change")
    }






    Tally();
})
//GetBalanceSpecs


function successCallPV(result) {
    $("#PVRows tr:last").after('<tr>' + result + '</tr>');

    createEVERYTHING();

}

function successCallINV(result) {
    $("#InverterRows tr:last").after('<tr>' + result + '</tr>');
    createEVERYTHING();

}
function successCallBINV(result) {
    $("#BiInverterRows tr:last").after('<tr>' + result + '</tr>');

    createEVERYTHING();

}
//successCallMount


function successCallMount(result) {
    $("#MountingRows tr:last").after('<tr>' + result + '</tr>');
    createEVERYTHING();


}

//successCallBatt


function successCallBatt(result) {
    $("#BattRows tr:last").after('<tr>' + result + '</tr>');
    createEVERYTHING();


}

function successBalance(data, typo) {


    $("#" + typo.replace(" ", "_").substring(0, 5) + " tr:last").after('<tr>' + data + '</tr>');

    createEVERYTHING();
}




function numberWithCommas(x) {
    var neg = "-";
    var a = parseFloat(x).toFixed(2);

    if (a < 0) {
        return neg + a.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    }
    return a.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function removeCommas(x) {
    var neg = "-";

    var c = x.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "")
    if (parseFloat(c) < 0) {


        return neg + parseFloat(c).toFixed(2);

    }
    return parseFloat(c).toFixed(2);
}