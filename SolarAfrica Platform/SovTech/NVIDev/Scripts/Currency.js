﻿function getCurrency(trigger)
{
    var id = $(trigger).val();

    $.ajax({
        url: '/Admin/GetCurrency/',
        data: { id: id },
        type: 'POST',
        dataType: 'json',
        success: function (data) {

            $('#currentValue').val(data);
        }
    });
}


window.onload(function ()
{
    $('#currentValue').attr('class', 'disabled');

    getCurrency($("#Currency"))
})