﻿
function ActiveStatus(cellvalue, options, rowObject) {
    if (cellvalue) return "Yes"; else return "No";
}

//For Sorting & Align 
//
function columnsData(Data) {

    var str = "[";
    str = str;
    for (var i = 0; i < Data["column"].length; i++) {

        str = str + "{name:'" + Data["column"][i] + "',    index:'" + Data["column"][i] + "'";

        // For Column hide
        if (Data["column"][i] == 'CostingSummaryId' || Data["column"][i] == 'RubberRMCostId' || Data["column"][i] == 'RoleNewId' || Data["column"][i] == 'RMSpecification' || Data["column"][i] == 'LevelId' || Data["column"][i] == 'Delete' || Data["column"][i] == 'ProcessCodeNew' || Data["column"][i] == 'CreatedBy' || Data["column"][i] == 'LevelUserId' || Data["column"][i] == 'CostingDetailPartwiseId' || Data["column"][i] == 'OperationId' || Data["column"][i] == 'CostingDetailPartwiseProcessId' || Data["column"][i] == 'pkFertCodeId' || Data["column"][i] == 'Modifiedby' || Data["column"][i] == 'Remark') {
            str = str + ", hidden: true , align:'center'";
        }    
   //For DateTime format
        if (Data["column"][i] == "CreatedOn"  || Data["column"][i] == "ReferenceDate" || Data["column"][i] == "CreatorTime") {
            str = str + ", formatter:'date', formatoptions:{ srcformat:'m/d/Y h:i:s', newformat:'d/m/Y' } }, ";            
        }
        if (Data["column"][i] == 'View'  ) {
            str = str + ", sortable: false },";
        }
        else {
            str = str + "},";
        }


    }
    str = str.substring(0, str.length - 1);
    str = str + "]";
    //console.log(str)
    return str;
}

// To change Name of Textbox 
function columnsName(Data) {
    var str = "[";
    str = str;
    for (var i = 0; i < Data.length; i++) {
        if (Data[i] == 'IsActive') { str += "'Active Status',"; }

        else if (Data[i] == 'DeleteLink') { str += "'Delete',"; }
        else if (Data[i] == 'CreatorPerson') { str += "'Creator',"; }
        else if (Data[i] == 'FirstName') { str += "'First Name',"; }
        else if (Data[i] == 'LastName') { str += "'Last Name',"; }
        else if (Data[i] == 'UserType') { str += "'User Type',"; }
        else if (Data[i] == 'CompanyName') { str += "'Company Name',"; }
        else if (Data[i] == 'MobileNo') { str += "'Mobile No',"; }
        else if (Data[i] == 'InitiatedOn') { str += "'Initiated On',"; }
        else if (Data[i] == 'UserType') { str += "'User Type',"; }
        else if (Data[i] == 'UserType') { str += "'User Type',"; }
        else if (Data[i] == 'UserType') { str += "'User Type',"; }

        else { str += "'" + Data[i] + "',";}
    }
    str = str.substring(0, str.length - 1);
    str = str + "]";
 
    return str;
}

// for redefining column width in  JQgrid
function reDefineColWidth(grid) {
    if (grid == "#CostSummaryReport") {
        var columnWidths = '10px|40px|40px|40px|100px|80px|100px|50px|100px|220px|100px|50px|50px|111px|70px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px|100px|150px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px||100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px||100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px||100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px|'
    }
    else if (grid == "#CostSummaryReport_1")
    {
        var columnWidths = '5px|40px|40px|40px|100px|80px|100px|50px|100px|220px|100px|50px|50px|111px|70px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px|100px|150px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px||100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px||100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px||100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px|'
    }
    else {
        var columnWidths = '30px|180px|155px|140px|150px|100px|175px|175px|130px|220px|125px|230px|200px|111px|70px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px|100px|150px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px||100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px||100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px||100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|100px|150px|100px|'
    }
    var widthsArr = columnWidths.split('|');
    for (var j = 0; j < widthsArr.length ; j++) {
        $('.ui-jqgrid-labels > th:eq(' + j + ')').css('width', widthsArr[j]); // will set the column header widths
        $(grid + ' tr').find('td:eq(' + j + ')').each(function () { $(this).css('width', widthsArr[j]); }) // will set the column widths
    }
}

//Main JQGRID Function
function JQGrid(gridName, pagerName, action, sortColumn, sortOrder, caption, width, height, loadtext, excelUrl, parentDivId) {
    var lastsel;
    var colname;
    var grid = $('#' + gridName);
    var pager = $('#' + pagerName);
    $.ajax({
        url: action + "?column=column",
        dataType: "json",
        mtype: 'POST',
        sortorder: 'asc',
        beforeSend: function () {
        },
        success: function (result) {

            if (result) {
                if (!result.Error) {
                    var colData = columnsData(result);
                    colname = columnsName(result.column);

                    colData = eval('{' + colData + '}');
                    colname = eval('{' + colname + '}');
                   

                    grid.jqGrid('GridUnload');
                    grid.jqGrid({
                        url: action,
                        datatype: 'json',
                        height: height,
                        colNames: colname,
                        colModel: colData,
                        rowNum: 100,
                        //autowidth: true,
                        scrollOffset: 0,
                        autowidth: true,
                        rownumbers: true,
                        rowList: [100, 500, 1000],
                        loadtext: loadtext,
                        pager: pager,
                        //viewrecords: true,
                        viewrecords: true,
                        sortorder: sortOrder,
                        sortname: sortColumn,
                        rownumbers: true,
                        caption: caption,
                        loadComplete: function () {
                            if (gridName == 'grdUser') {
                                $('#gs_View').hide();
                            }

                        }
                    });
                    grid.jqGrid('navGrid', '#' + pagerName, { view: false, edit: false, refreshtext: "", add: false, del: false, refresh: true, search: false });
                    //  grid.jqGrid('navGrid', '#' + parentDivId, { view: false, edit: false, refreshtext: "", add: false, del: false, refresh: true, search: true });

                    grid.jqGrid('filterToolbar', { searchOnEnter: false });

                   // $.extend($.jgrid.search, { Find: 'Search' });

                    if (parentDivId != '') {
                        $(window).bind('resize', function () {
                            if ($('#' + parentDivId).width() > 700) {
                                grid.setGridWidth($('#' + parentDivId).width() - 1, true);
                            } else {
                                grid.setGridWidth(900, true);
                            }
                        }).trigger('resize');
                    }
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr && thrownError) {
                alert('Status: ' + xhr.status + ' Error: ' + thrownError);
            }
        }
    });
}
// For Clearing Search Filter (*)

// JqGrid With Filter Options
function JQGridFilter(gridName, pagerName, action, sortColumn, sortOrder, caption, width, height, loadtext, excelUrl, parentDivId, Filter) {

    var lastsel;
    var colname;
    var grid = $('#' + gridName);
    var pager = $('#' + pagerName);
    $.ajax({
        url: action + "?column=column" + '&' + Filter,
        dataType: "json",
        mtype: 'POST',
        sortorder: 'asc',
        beforeSend: function () {
        },
        success: function (result) {

            if (result) {
                if (!result.Error) {
                    var colData = columnsData(result);
                    colname = columnsName(result.column);

                    colData = eval('{' + colData + '}');
                    colname = eval('{' + colname + '}');
                    grid.jqGrid('GridUnload');
                    grid.jqGrid({
                        url: action + '?' + Filter,
                        datatype: 'json',
                        height: height,
                        colNames: colname,
                        colModel: colData,
                        rowNum: 100,
                        scrollOffset: 0,
                        autowidth: true,
                        rownumbers: true,
                        rowList: [100, 500, 1000],
                        loadtext: loadtext,
                        pager: pager,
                        viewrecords: true,
                        sortorder: sortOrder,
                        sortname: sortColumn,
                        rownumbers: true,
                        caption: caption,
                        loadComplete: function () {                             
                            try{
                                if (parentDivId != '') {

                                    if ($('#' + parentDivId).width() > 700) {
                                        grid.setGridWidth($('#' + parentDivId).width() - 1, true);
                                    } else {
                                        grid.setGridWidth(900, true);
                                    }
                                }
                                if (gridName != 'tblMasterSimulationList') {
                                    reDefineColWidth('#' + gridName);
                                }
                                // reDefineColWidth('#' + gridName);
                                if ($('#gs_Date').parent().next('td').html() != null) {
                                    $('#gs_Date').parent().next('td').remove();
                                }
                                $('#gs_Date').remove();

                                if ($('#gs_Delete').parent().next('td').html() != null) {
                                    $('#gs_Delete').parent().next('td').remove();
                                }
                                $('#gs_Delete').remove();
                                if ($('#gs_CreatedOn').parent().next('td').html() != null) {
                                    $('#gs_CreatedOn').parent().next('td').remove();
                                }
                                $('#gs_CreatedOn').remove();
                                $('#gs_RateINRUOM').remove();
                                $('#gs_UOM').remove();

                                


                            } catch (e) {
                                console.log(e);
                            }

                            if (gridName == "tblApproval") {
                                $('#gs_Status').remove();
                                $("#tblApproval").hideCol("CreatedOn");
                            }

                            if (gridName == "tblSubGrid") {
                               
                                $("#tblSubGrid").hideCol("Edit");
                                $("#tblSubGrid").hideCol("Delete");
                                $("#tblSubGrid").hideCol("DeleteBOP");
                                $("#tblSubGrid").hideCol("DeleteData");

                                }

                        },
                        onSelectRow: function (rowid) {
                            
                            if (gridName == "tblMachineRateForgine" || gridName == "tblCEDOtherOperationCost" || gridName == "tblBenchMarking" || gridName == "tblMachineRate" || gridName == "tblRMSpec" || gridName == "tblMachineList" || gridName == "tblBOMList" || gridName == "tblOtherOperationList" || gridName == "tblOtherOperationList" || gridName == "tblMasterList" || gridName == "tblProprietaryCategory" || gridName == " tblCostCastingMHR" || gridName == "tblApproval") {

                                SetValue(rowid);
                            } else if (gridName == 'tblGlobal') {

                                var grid = $("#tblGlobal");
                                SetValue(rowid, grid);
                            } else {

}
                        },
                        gridComplete: function () {
                            if (gridName == "tblSubGrid") {
                                var filter1 = Filter.split('&');
                                var filter2 = filter1[0].split('='); // Get GridType

                                if (filter2[1] == 'SMOtherOperation' || filter2[1] == 'SMSurfaceOperation') {
                                    var parseTotal = $(this).jqGrid('getCol', 'NetPaintingCost', false, 'sum');
                                    document.getElementById('lblTotalCost').innerHTML = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);
                                } else if (filter2[1] == 'RubberRM') {
                                    var parseTotal = $(this).jqGrid('getCol', 'MaterialCost', false, 'sum');
                                    //var parseTotalWt = $(this).jqGrid('getCol', 'MaterialWt', false, 'sum');
                                    document.getElementById('lblTotalCost').innerHTML = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);
                                    //document.getElementById('lblGrossWt').innerHTML = isNaN(parseFloat(parseTotalWt)) ? 0.00 : parseFloat(parseTotalWt).toFixed(2);

                                } else if (filter2[1] == 'RubberBOP' || filter2[1] == 'ForgingBOP') {
                                    var parseTotal = $(this).jqGrid('getCol', 'TotalCost', false, 'sum');
                                    document.getElementById('lblTotalCost').innerHTML = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);

                                } else if (filter2[1] == 'ForgingVBCMHR') {
                                    var parseTotal = $(this).jqGrid('getCol', 'RatePerPC', false, 'sum');
                                    document.getElementById('lblTotalCost').innerHTML = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);

                                } else if (filter2[1] == 'RubberProcessCost') {
                                    var parseTotal = $(this).jqGrid('getCol', 'CostPerPc', false, 'sum');
                                    document.getElementById('lblTotalCost').innerHTML = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);

                                } else if (filter2[1] == 'RubberOtherCost' || filter2[1] == 'ForgingMachiningCost') {
                                    var parseTotal = $(this).jqGrid('getCol', 'Cost', false, 'sum');
                                    document.getElementById('lblTotalCost').innerHTML = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);

                                } else if (filter2[1] == 'NFecProcess') {
                                    var Technic = filter1[1].split('='); // Technic
                                    var SupplierId = filter1[2].split('='); // SupplierId
                                    var parseTotal = 0;
                                    if (SupplierId[1] == '8' && Technic[1] == 'PDC') {
                                        parseTotal = $(this).jqGrid('getCol', 'CostPerPc', false, 'sum');
                                    } else if (SupplierId[1] == '8') {
                                        parseTotal = $(this).jqGrid('getCol', 'Cost', false, 'sum');
                                    } else {
                                        parseTotal = $(this).jqGrid('getCol', 'CostPerPc', false, 'sum');
                                    }
                                    document.getElementById('lblTotalCost').innerHTML = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);

                                } else if (filter2[1] == 'FecCastingCost') {
                                    var parseTotal = $(this).jqGrid('getCol', 'Cost', false, 'sum');
                                    document.getElementById('lblTotalCost').innerHTML = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);
                                    $(this).jqGrid('footerData', 'set', { 'WtOfSand': "Shell Core: " + wtsandshell + "<br>Cold Core: " + wtsand });
                                }
                            }
                        }

                    });
                    grid.jqGrid('navGrid', '#' + pagerName, { view: false, edit: false, refreshtext: "", add: false, del: false, refresh: true, search: false });
                    grid.jqGrid('filterToolbar', { searchOnEnter: false });
                    $.extend($.jgrid.search, { Find: 'Search' });
                    if (parentDivId != '') {
                        $(window).bind('resize', function () {
                            if ($('#' + parentDivId).width() > 700) {
                                grid.setGridWidth($('#' + parentDivId).width() - 1, true);
                            } else {

                                grid.setGridWidth(900, true);
                            }
                        }).trigger('resize');
                    }
                    if (gridName == "RMFixed") {
                        jQuery('#' + gridName).jqGrid('sortableRows');
                        $("#RMFixed").hideCol("Sequence");
                        grid.jqGrid('showCol', "Delete");
                    }
                    if (gridName == "tblMachineRateForgine") {                   
                        $("#tblMachineRateForgine").hideCol("Edit");  
                    }
                    if (gridName == "tblForgingVBCMHR") {
                        //  jQuery('#' + gridName).jqGrid('sortableRows');
                        $("#tblForgingVBCMHR").hideCol("ForgingVBCMHRId");
                        $("#tblForgingVBCMHR").hideCol("ProcessId");
                        grid.jqGrid('showCol', "Delete");
                        $("#gs_Edit").hide();

                    }
                    //NetRMCostPerAssy
                    if (excelUrl != '') {
                        grid.jqGrid('navButtonAdd', pagerName,
                            {
                                caption: "Excel", title: "Export to Excel", buttonicon: "ui-icon-print",
                                onClickButton: function () {
                                    grid.jqGrid('excelExport', { url: excelUrl, oper: "oper", tag: "excel" });
                                }
                            });
                    }

                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr && thrownError) {
                alert('Status: ' + xhr.status + ' Error: ' + thrownError);
            }
        }
    });
}


// JqGrid With InLine Operation
function JQGridFilterWithInLineNavigation(gridName, pagerName, action, sortColumn, sortOrder, caption, width, height, loadtext, excelUrl, parentDivId, Filter) {

    var lastsel;
    var lastSel;
    var colname;
    var grid = $('#' + gridName);
    var pager = $('#' + pagerName);
    $.ajax({
        url: action + "?column=column" + Filter,
        dataType: "json",
        mtype: 'POST',
        sortorder: 'asc',
        beforeSend: function () {
        },
        success: function (result) {

            if (result) {
                if (!result.Error) {
                    var colData = columnsData(result);
                    colname = columnsName(result.column);
                    colData = eval('{' + colData + '}');
                    colname = eval('{' + colname + '}');
                    grid.jqGrid('GridUnload');
                    grid.jqGrid({
                        url: action + '?' + Filter,
                        datatype: 'json',
                        height: height,
                        colNames: colname,
                        colModel: colData,
                        rowNum: 100,
                        //scrollOffset: 0,
                        autowidth: true,
                        rownumbers: true,
                        rowList: [100, 500, 1000],
                        //loadtext: loadtext,
                        pager: pager,
                        //viewrecords: true,
                        //gridview: true,
                        sortorder: sortOrder,
                        sortname: sortColumn,
                        rownumbers: true,
                        caption: caption,
                        footerrow: true,
                        userDataOnFooter: true,
                        //altRows: true,
                         
                        //cellEdit: true,
                       
                        loadComplete: function () {
                            if (parentDivId != '') {
                                $(window).bind('resize', function () {
                                    if ($('#' + parentDivId).width() > 700) {
                                        grid.setGridWidth($('#' + parentDivId).width() - 1, true);
                                    } else {
                                        grid.setGridWidth(900, true);
                                    }
                                }).trigger('resize');
                            }
                            var ids = grid.jqGrid('getDataIDs');
                            if (gridName == "RMGrid") {
                                for (var i = 0; i < ids.length; i++) {
                                    var id = ids[i];
                                    var rowData = grid.jqGrid('getRowData', id);
                                    $("#RMGrid").setCell(id, 'Description', '', '', { 'title': 'Description' });
                                    $("#RMGrid").setCell(id, 'Cost', '', '', { 'title': 'Enter Cost' });

                                }
                            }
                            else if (gridName == "tblSubGrid")
                            {
                                $("#tblSubGrid").jqGrid('setLabel', 'NetRMCostPerAssy', 'RM Cost'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'TotalBOPCostPerAssy', 'BOP Cost'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'RM_CC', 'RM_CC'); // Dynamic Column name change 

                                $("#tblSubGrid").jqGrid('setLabel', 'GrossWtComp', 'Gross Wt'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'FinWtPerComponent', 'Fin Wt'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'BOPPerPc', 'BOP/Pc'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'ScrapRateRsPerKg', 'Scrape Rate'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'RMRatePerKg', 'RM/kg'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'NetPaintingCostPerAssy', 'Net Painting Cost/Assy'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'RawMaterial', 'RM'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'RawMaterialGrade', 'RM Grade'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'RawMaterialSpecification', 'Specification'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'CostingHead', 'Costing Head'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'NoOfParts', 'No Of Parts'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'NetSurfaceArea', 'Net Sur Area'); // Dynamic Column name change 
                                $("#tblSubGrid").jqGrid('setLabel', 'BlankLengthBL', 'Blank L'); // Dynamic Column name change 
                            }
                            else if (gridName == "MultipleCoreSize") {
                                for (var i = 0; i < ids.length; i++) {
                                    var id = ids[i];
                                    var rowData = grid.jqGrid('getRowData', id);
                                    if (gridName == "MultipleCoreSize") {
                                        $("#MultipleCoreSize").setCell(id, 'VolumeOfCoreRect', '', '', { 'title': 'Core Length * Core Breadth * Core Height' });
                                        $("#MultipleCoreSize").setCell(id, 'VolumeOfCylCore', '', '', { 'title': '3.14/4* Core Dia * Core Dia * Core Length' });
                                        $("#MultipleCoreSize").setCell(id, 'WtOfSand', '', '', {
                                            'title': '((Volume of core rect + Volume of cyl core )*Density of Sand  )*1.1'
                                        });
                                    }
                                }
                            } else if (gridName == "tblCostCalculation") {
                                for (var i = 0; i < ids.length; i++) {
                                    var id = ids[i];
                                    var rowData = grid.jqGrid('getRowData', id);
                                    $("#tblCostCalculation").setCell(id, 'Cost', '', '', { 'readonly': 'readonly' });
                                }
                            } else if (gridName == "tblCostCalculationA") {
                                reDefineColWidthA('#' + gridName);
                                $("#tblCostCalculationA_iledit").hide();
                                for (var i = 0; i < ids.length; i++) {
                                    var id = ids[i];
                                    var rowData = grid.jqGrid('getRowData', id);
                                    $("#tblCostCalculationA").setCell(id, 'Cost', '', '', { 'readonly': 'readonly' });                                    
                                }
                                //CostingCalculations();
                            } else if (gridName == "tblCostSummary") {
                                reDefineColWidthA('#' + gridName);
                                $("#tblCostSummary_iledit").hide();
                                for (var i = 0; i < ids.length; i++) {
                                    var id = ids[i];
                                    var rowData = grid.jqGrid('getRowData', id);
                                    $("#tblCostSummary").setCell(id, 'Cost', '', '', { 'readonly': 'readonly' });
                                }
                            }
                        },
                        onSelectRow: function (id) {
                           
                            if (gridName == "tblCostCalculation") {
                              
                                if (id && id != lastSel) {
                                    $('#tblCostCalculation').restoreRow(lastSel);
                                    lastSel = id;
                                    $('#tblCostCalculation').editRow(id, true);
                                }
                            } else if (gridName == "tblCostCalculationA") {
                              
                                if (id && id != lastSel) {
                                    $('#tblCostCalculationA').restoreRow(lastSel);
                                    lastSel = id;
                                    $('#tblCostCalculationA').editRow(id, true);
                                    $("#tblCostCalculationA_iledit").click();
                                }
                            } else if (gridName == "tblCostSummary") {

                                if (id && id != lastSel) {
                                    $('#tblCostSummary').restoreRow(lastSel);
                                    lastSel = id;
                                    $('#tblCostSummary').editRow(id, true);
                                    $("#tblCostSummary_iledit").click();
                                }
                            }
                        },

                        gridComplete: function () {
                            if (gridName == "RMGrid") {
                                var parseTotal = $(this).jqGrid('getCol', 'Cost', false, 'sum');
                                $(this).jqGrid('footerData', 'set', {
                                    'Cost': "Total RM Cost: " + parseFloat(parseTotal).toFixed(3)
                                });
                                total = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);
                            } else if (gridName == "tblOtherOperationCost") {
                                var parseTotal = $(this).jqGrid('getCol', 'NetPaintingCost', false, 'sum');
                                $(this).jqGrid('footerData', 'set', {
                                    'FC': "Total Cost: "
                                });
                                $(this).jqGrid('footerData', 'set', {
                                    'NetPaintingCost': parseFloat(parseTotal).toFixed(2)
                                });
                                total = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);
                            } else if (gridName == "tblSubGrid") {
                                var filter1 = Filter.split('&');
                                var filter2 = filter1[0].split('=');
                                if (filter2[1] == 'SMOtherOperation' || filter2[1] == 'SMSurfaceOperation') {
                                    var parseTotal = $(this).jqGrid('getCol', 'NetPaintingCost', false, 'sum');
                                    $(this).jqGrid('footerData', 'set', { 'FC': "Total Cost: " });
                                    $(this).jqGrid('footerData', 'set', { 'NetPaintingCost': parseFloat(parseTotal).toFixed(2) });
                                    total = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);
                                } else if (filter2[1] == 'SMProcessCost') {
                                    var parseTotal = $(this).jqGrid('getCol', 'TotalProcessCostAssy', false, 'sum');
                                    $(this).jqGrid('footerData', 'set', { 'QtyPerNoUnits': "Total Cost: " });
                                    $(this).jqGrid('footerData', 'set', { 'TotalProcessCostAssy': parseFloat(parseTotal).toFixed(2) });
                                    total = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);

                                }
                                else if (filter2[1] == 'ApprovalRMList' || filter2[1] == 'ApprovalBOPList') {
                                    var parseTotal = $(this).jqGrid('getCol', 'NetRMCostPerAssy', false, 'sum');
                                    $(this).jqGrid('footerData', 'set', { 'PartDescription': "Total Cost: " });
                                    $(this).jqGrid('footerData', 'set', { 'NetRMCostPerAssy': parseFloat(parseTotal).toFixed(2) });
                                    total = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);

                                    var parseBOPTotal = $(this).jqGrid('getCol', 'TotalBOPCostPerAssy', false, 'sum');
                                    $(this).jqGrid('footerData', 'set', { 'TotalBOPCostPerAssy': parseFloat(parseBOPTotal).toFixed(2) });
                                    // total = isNaN(parseFloat(parseTotal)) ? 0.00 : parseFloat(parseTotal).toFixed(2);
                                }
                            } else if (gridName == "MultipleCoreSize") {

                                var ids = $("#MultipleCoreSize").jqGrid('getDataIDs');
                                var totalColdCorwt = [];
                                var totalShellCore = [];
                                for (var i = 0; i < ids.length; i++) {
                                    var rowId = ids[i];
                                    var rowData = $("#MultipleCoreSize").jqGrid('getRowData', rowId);
                                    if (rowData.CoreType == "Cold Core 1" || rowData.CoreType == "Cold Core 2" || rowData.CoreType == "Cold Core 3" || rowData.CoreType == "Cold Core 4" || rowData.CoreType == "Cold Core 4" || rowData.CoreType == "Cold Core 6" || rowData.CoreType == "Cold Core 7" || rowData.CoreType == "Cold Core 8" || rowData.CoreType == "Cold Core 9" || rowData.CoreType == "Cold Core 10" || rowData.CoreType == "Cold Core 11" || rowData.CoreType == "Cold Core 12") {
                                        totalColdCorwt.push(parseFloat(rowData.WtOfSand).toFixed(2));
                                    }
                                    else {
                                        totalShellCore.push(parseFloat(rowData.WtOfSand).toFixed(2));
                                    }
                                }
                                var wtsand = eval(totalColdCorwt.join('+'));
                                var wtsandshell = eval(totalShellCore.join('+'));
                                //if wtsand and wtsandshell is undefine
                                if (typeof (wtsand) == "undefined") {
                                    wtsand = 0;
                                }
                                if (typeof (wtsandshell) == "undefined") {
                                    wtsandshell = 0;
                                }
                                //set text box of shell and cold core wt of sand
                                document.getElementById("txtWtOfSand").value = wtsand;
                                document.getElementById("txtCoreWt").value = wtsand;
                                document.getElementById("txtShellWtOfSand").value = wtsandshell
                                document.getElementById("txtShellCoreWt").value = wtsandshell;
                                $(this).jqGrid('footerData', 'set', { 'WtOfSand': "Shell Core: " + wtsandshell + "<br>Cold Core: " + wtsand });

                            }else   if (gridName == "tblCostCalculation") {
                                var parseTotal = $(this).jqGrid('getCol', 'Cost', false, 'sum');
                                $(this).jqGrid('footerData', 'set', {
                                    'Cost': "Total Cost: " + parseFloat(parseTotal).toFixed(2)
                                });
                                total = parseFloat(parseTotal).toFixed(2);
                            } else if (gridName == "tblCostCalculationA") {
                                var parseTotal = $(this).jqGrid('getCol', 'Cost', false, 'sum');
                                $(this).jqGrid('footerData', 'set', { 'Quantity': "Total Cost: " });
                                $(this).jqGrid('footerData', 'set', { 'Cost': parseFloat(parseTotal).toFixed(2) });
                                total = parseFloat(parseTotal).toFixed(2);
                            } else if (gridName == "tblCostSummary") {
                                var parseTotal = $(this).jqGrid('getCol', 'Cost', false, 'sum');
                                $(this).jqGrid('footerData', 'set', { 'OtherCost': "Total Cost: " });
                                $(this).jqGrid('footerData', 'set', { 'Cost': parseFloat(parseTotal).toFixed(2) });
                                total = parseFloat(parseTotal).toFixed(2);
                            }
                            else if (gridName == "tblAssyCost") {
                                var parseTotal = $(this).jqGrid('getCol', 'Cost', false, 'sum');
                                $(this).jqGrid('footerData', 'set', {
                                    'Cost': "Total Cost: " + parseFloat(parseTotal).toFixed(2)
                                });
                                total = parseFloat(parseTotal).toFixed(2);
                                document.getElementById('hdnTotalCost').value = total;
                            }

                        },
                        editurl: action + '?' +Filter,
                    });
                    if (gridName == "MultipleCoreSize") {
                        grid.jqGrid('navGrid', '#' + pagerName, { view: false, edit: false, refreshtext: "", add: false, del: false, refresh: false, search: false });
                        $("#MultipleCoreSize").jqGrid('inlineNav', '#' + pagerName,
                         {
                             edit: false,
                             //editicon: "ui-icon-pencil",
                             edittext: "<span class='btn btn-primary  btn-sm'>Edit Row</span>",
                             add: true,
                             addtype: 'button',
                             addicon: "ui-icon-add ",
                             addtext: "<span class='btn btn-primary  btn-sm'>Add </span>",
                             refresh: true,
                             save: true,
                             saveicon: "ui-icon-disk",
                             savetext: "<span class='btn btn-primary  btn-sm'>Save</span>",
                             // <span class="btn btn-primary">Add Row</span>
                             cancel: true,
                             canceltext: "<span class='btn btn-primary  btn-sm'>Cancel</span>",
                             cancelicon: "ui-icon-cancel",
                             editParams: {
                                 keys: false,
                                 oneditfunc: null,
                                 successfunc: function (val) {
                                     if (val.responseText != "") {
                                         // alert(val.responseText);
                                         $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                                     }
                                 },
                                 url: null,
                                 extraparam: {
                                     EmpId: function () {
                                         var sel_id = $('#MultipleCoreSize').jqGrid('getGridParam', 'selrow');
                                         var value = $('#MultipleCoreSize').jqGrid('getCell', sel_id, '_id');
                                         return value;
                                     }
                                 },
                                 aftersavefunc: null,
                                 errorfunc: null,
                                 afterrestorefunc: null,
                                 restoreAfterError: true,
                                 mtype: "POST"
                             },
                             addParams: {
                                 useDefValues: true,
                                 addRowParams: {
                                     keys: true,
                                     extraparam: {},
                                     oneditfunc: function (rawid) {
                                         document.getElementById(rawid + "_DensityOfSand").readOnly = true;
                                         document.getElementById(rawid + "_WtOfSand").readOnly = true;
                                         document.getElementById(rawid + "_VolumeOfCylCore").readOnly = true;
                                         document.getElementById(rawid + "_VolumeOfCoreRect").readOnly = true;
                                         document.getElementById(rawid + "_DensityOfSand").value = "0.0000017";

                                         $('#' + rawid + '_CoreLength').keyup(function () {
                                             CalculateCoreSizeInGrid(rawid);
                                         });
                                         $('#' + rawid + '_CoreBreadth').keyup(function () {
                                             CalculateCoreSizeInGrid(rawid);
                                         }); $('#' + rawid + '_CoreHeight').keyup(function () {
                                             CalculateCoreSizeInGrid(rawid);
                                         });
                                         $('#' + rawid + '_CoreDensity').keyup(function () {
                                             CalculateCoreSizeInGrid(rawid);
                                         });

                                     },
                                     successfunc: function (val) {
                                         if (val.responseText != "") {
                                             //alert(val.responseText)
                                             $("#MultipleCoreSize").jqGrid('setGridParam', { datatype: 'json', page: 1, current: true }).trigger('reloadGrid');

                                         }
                                     }
                                 }
                             }
                         }
                        );
                    }
                    else if (gridName == "RMGrid") {
                        grid.jqGrid('navGrid', '#' + pagerName,
                                {
                                    view: false, edit: false, refreshtext: "", add: false, del: false, refresh: false, search: false
                                });
                        $("#RMGrid").jqGrid('inlineNav', '#' + pagerName,
                            {
                                edit: false,
                                //editicon: "ui-icon-pencil",
                                edittext: "<span class='btn btn-primary  btn-sm'>Edit Row</span>",
                                add: true,
                                addtype: 'button',
                                addicon: "ui-icon-add ",
                                addtext: "<span class='btn btn-primary  btn-sm'>Add Row</span>",
                                refresh: true,
                                save: true,
                                saveicon: "ui-icon-disk",
                                savetext: "<span class='btn btn-primary  btn-sm'>Save</span>",
                                // <span class="btn btn-primary">Add Row</span>
                                cancel: true,
                                canceltext: "<span class='btn btn-primary  btn-sm'>Cancel</span>",
                                cancelicon: "ui-icon-cancel",
                                editParams: {
                                    keys: false,
                                    oneditfunc: null,
                                    successfunc: function (val) {
                                        if (val.responseText != "") {
                                            // alert(val.responseText);
                                            $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                                        }
                                    },
                                    url: null,
                                    extraparam: {
                                        EmpId: function () {
                                            var sel_id = $('#RMGrid').jqGrid('getGridParam', 'selrow');
                                            var value = $('#RMGrid').jqGrid('getCell', sel_id, '_id');
                                            return value;
                                        }
                                    },
                                    aftersavefunc: null,
                                    errorfunc: null,
                                    afterrestorefunc: null,
                                    restoreAfterError: true,
                                    mtype: "POST"
                                },
                                addParams: {
                                    useDefValues: true,
                                    addRowParams: {
                                        keys: true,
                                        extraparam: {},
                                        oneditfunc: function (rawid) {

                                        },
                                        successfunc: function (val) {
                                            if (val.responseText != "") {

                                                $("#RMGrid").jqGrid('setGridParam', { datatype: 'json', page: 1, current: true }).trigger('reloadGrid');

                                            }
                                        }
                                    }
                                }
                            }
            );
                    } else if (gridName == "tblAssyCost") {
                        grid.jqGrid('navGrid', '#' + pagerName,
                                {
                                    view: false, edit: false, refreshtext: "", add: false, del: false, refresh: false, search: false
                                });
                        $("#tblAssyCost").jqGrid('inlineNav', '#' + pagerName,
                            {
                                edit: false,
                                //editicon: "ui-icon-pencil",
                                edittext: "<span class='btn btn-primary  btn-sm'>Edit Row</span>",
                                add: true,
                                addtype: 'button',
                                addicon: "ui-icon-add ",
                                addtext: "<span class='btn btn-primary  btn-sm'>Add Row</span>",
                                refresh: true,
                                save: true,
                                saveicon: "ui-icon-disk",
                                savetext: "<span class='btn btn-primary  btn-sm'>Save</span>",
                                // <span class="btn btn-primary">Add Row</span>
                                cancel: true,
                                canceltext: "<span class='btn btn-primary  btn-sm'>Cancel</span>",
                                cancelicon: "ui-icon-cancel",
                                editParams: {
                                    keys: false,
                                    oneditfunc: null,
                                    successfunc: function (val) {
                                        if (val.responseText != "") {
                                            // alert(val.responseText);
                                            $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                                        }
                                    },
                                    url: null,
                                    extraparam: {
                                        EmpId: function () {
                                            var sel_id = $('#tblAssyCost').jqGrid('getGridParam', 'selrow');
                                            var value = $('#tblAssyCost').jqGrid('getCell', sel_id, '_id');
                                            return value;
                                        }
                                    },
                                    aftersavefunc: null,
                                    errorfunc: null,
                                    afterrestorefunc: null,
                                    restoreAfterError: true,
                                    mtype: "POST"
                                },
                                addParams: {
                                    useDefValues: true,
                                    addRowParams: {
                                        keys: true,
                                        extraparam: {},
                                        oneditfunc: function (rawid) {

                                        },
                                        successfunc: function (val) {
                                            if (val.responseText != "") {

                                                $("#tblAssyCost").jqGrid('setGridParam', { datatype: 'json', page: 1, current: true }).trigger('reloadGrid');

                                            }
                                        }
                                    }
                                }
                            }
            );
                    } else if (gridName == "tblCostCalculation") {
                        grid.jqGrid('navGrid', '#' + pagerName,
                                {
                                    view: false, edit: false, refreshtext: "", add: false, del: false, search: false
                                });
                        $("#tblCostCalculation").jqGrid('inlineNav', '#' + pagerName,
                            {
                                edit: false,
                                //editicon: "ui-icon-pencil",
                                edittext: "<span class='btn btn-primary  btn-sm'>Edit Row</span>",
                                add: true,
                                addtype: 'button',
                                addicon: "ui-icon-add ",
                                addtext: "<span class='btn btn-primary  btn-sm'>Add Row</span>",
                                refresh: true,
                                save: true,
                                saveicon: "ui-icon-disk",
                                savetext: "<span class='btn btn-primary  btn-sm'>Save</span>",
                                // <span class="btn btn-primary">Add Row</span>
                                cancel: true,
                                canceltext: "<span class='btn btn-primary  btn-sm'>Cancel</span>",
                                //cancelicon: "ui-icon-cancel",
                                editParams: {
                                    keys: false,
                                    oneditfunc: null,
                                    successfunc: function (val) {
                                        if (val.responseText != "") {
                                            // alert(val.responseText);
                                            //$(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                                            var PartNo = document.getElementById('PartNo').value;
                                            var CostingSummaryID = document.getElementById('hdn_CostingSummaryId').value;
                                            $("#tblCostCalculation").jqGrid('setGridParam', { datatype: 'json', url: oApp + '/Proprietary/CostSummary?fkCostingSummaryID=' + CostingSummaryID + '&AssyPartNo=' + PartNo }).trigger('reloadGrid');

                                        }
                                    },
                                    url: null,
                                    extraparam: {
                                        AssyPartNo: function () {
                                            var AssyPartNo = document.getElementById("PartNo").value;

                                            return AssyPartNo;
                                        },
                                        fkCostingSummaryID: document.getElementById("hdn_CostingSummaryId").value
                                    },
                                    aftersavefunc: null,
                                    errorfunc: null,
                                    afterrestorefunc: null,
                                    restoreAfterError: true,
                                    mtype: "POST"
                                },
                                addParams: {
                                    useDefValues: false,
                                    addRowParams: {
                                        keys: true,
                                        errorfunc: null,
                                        extraparam: {
                                            AssyPartNo: function () {
                                                var AssyPartNo = document.getElementById("PartNo").value;

                                                return AssyPartNo;
                                            },
                                            CostingSummaryID: function () { return document.getElementById("hdn_CostingSummaryId").value },
                                            ravish: 123
                                        },
                                        oneditfunc: function (rawid) {

                                        },
                                        aftersavefunc: null,

                                        afterrestorefunc: null,
                                        restoreAfterError: true,
                                        successfunc: function (val) {
                                            if (val.responseText != "") {
                                                var PartNo = document.getElementById('PartNo').value;
                                                var CostingSummaryID = document.getElementById('hdn_CostingSummaryId').value;
                                                $("#tblCostCalculation").jqGrid('setGridParam', { datatype: 'json', url: oApp + '/Proprietary/CostSummary?fkCostingSummaryID=' + CostingSummaryID + '&AssyPartNo=' + PartNo }).trigger('reloadGrid');

                                            }
                                        }
                                    }
                                }
                            }
            );
                    } else if (gridName == "tblCostCalculationA") {
                        grid.jqGrid('navGrid', '#' + pagerName,
                                {
                                    view: false, edit: false, refreshtext: "", add: false, del: false, search: false
                                });
                        $("#tblCostCalculationA").jqGrid('inlineNav', '#' + pagerName,
                            {
                                edit: true,
                                //editicon: "ui-icon-pencil",
                                edittext: "<span class='btn btn-primary  btn-sm'>Edit Row</span>",
                                add: true,
                                addtype: 'button',
                                addicon: "ui-icon-add ",
                                addtext: "<span class='btn btn-primary  btn-sm'>Add Row</span>",
                                refresh: true,
                                save: true,
                                saveicon: "ui-icon-disk",
                                savetext: "<span class='btn btn-primary  btn-sm'>Save</span>",
                                // <span class="btn btn-primary">Add Row</span>
                                cancel: true,
                                canceltext: "<span class='btn btn-primary  btn-sm'>Cancel</span>",
                                //cancelicon: "ui-icon-cancel",
                                editParams: {
                                    keys: true,
                                    oneditfunc: function (rawid) {

                                    },
                                    successfunc: function (val) {
                                        if (val.responseText != "") {
                                            // alert(val.responseText);
                                            //$(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                                            var PartNo = document.getElementById('PartNo').value;
                                            var CostingSummaryID = document.getElementById('hdn_CostingSummaryId').value;
                                            $("#tblCostCalculationA").jqGrid('setGridParam', { datatype: 'json', url: oApp + '/Assembly/CostSummary?fkCostingSummaryID=' + CostingSummaryID + '&AssyPartNo=' + PartNo }).trigger('reloadGrid');

                                        }
                                    },
                                    url: null,
                                    extraparam: {
                                        AssyPartNo: function () {
                                            var AssyPartNo = document.getElementById("PartNo").value;

                                            return AssyPartNo;
                                        },
                                        fkCostingSummaryID: document.getElementById("hdn_CostingSummaryId").value
                                    },
                                    aftersavefunc: null,
                                    errorfunc: null,
                                    afterrestorefunc: null,
                                    restoreAfterError: true,
                                    mtype: "POST"
                                },
                                addParams: {
                                    useDefValues: false,
                                    addRowParams: {
                                        keys: true,
                                        errorfunc: null,
                                        extraparam: {
                                            AssyPartNo: function () {
                                                var AssyPartNo = document.getElementById("PartNo").value;

                                                return AssyPartNo;
                                            },
                                            CostingSummaryID: function () { return document.getElementById("hdn_CostingSummaryId").value } 
                                        },
                                        oneditfunc: function (rawid) {

                                        },
                                        aftersavefunc: null,

                                        afterrestorefunc: null,
                                        restoreAfterError: true,
                                        successfunc: function (val) {
                                            if (val.responseText != "") {
                                                var PartNo = document.getElementById('PartNo').value;
                                                var CostingSummaryID = document.getElementById('hdn_CostingSummaryId').value;
                                                $("#tblCostCalculationA").jqGrid('setGridParam', { datatype: 'json', url: oApp + '/Assembly/CostSummary?fkCostingSummaryID=' + CostingSummaryID + '&AssyPartNo=' + PartNo }).trigger('reloadGrid');

                                            }
                                        }
                                    }
                                }
                            }
            );
                    } else if (gridName == "tblCostSummary") {
                        grid.jqGrid('navGrid', '#' + pagerName,
                                {
                                    view: false, edit: false, refreshtext: "", add: false, del: false, search: false
                                });
                        $("#tblCostSummary").jqGrid('inlineNav', '#' + pagerName,
                            {
                                edit: true,
                                //editicon: "ui-icon-pencil",
                                edittext: "<span class='btn btn-primary  btn-sm'>Edit Row</span>",
                                add: true,
                                addtype: 'button',
                                addicon: "ui-icon-add ",
                                addtext: "<span class='btn btn-primary  btn-sm'>Add Row</span>",
                                refresh: true,
                                save: true,
                                saveicon: "ui-icon-disk",
                                savetext: "<span class='btn btn-primary  btn-sm'>Save</span>",
                                // <span class="btn btn-primary">Add Row</span>
                                cancel: true,
                                canceltext: "<span class='btn btn-primary  btn-sm'>Cancel</span>",
                                //cancelicon: "ui-icon-cancel",
                                editParams: {
                                    keys: true,
                                    oneditfunc: function (rawid) {

                                    },
                                    successfunc: function (val) {
                                        if (val.responseText != "") {
                                            // alert(val.responseText);
                                            //$(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                                            var PartNo = document.getElementById('PartNo').value;
                                            var CostingSummaryID = document.getElementById('hdn_CostingSummaryId').value;
                                            $("#tblCostSummary").jqGrid('setGridParam', { datatype: 'json', url: oApp + '/Assembly/CostSummaryVBC?fkCostingSummaryID=' + CostingSummaryID + '&AssyPartNo=' + PartNo }).trigger('reloadGrid');

                                        }
                                    },
                                    url: null,
                                    extraparam: {
                                        AssyPartNo: function () {
                                            var AssyPartNo = document.getElementById("PartNo").value;

                                            return AssyPartNo;
                                        },
                                        fkCostingSummaryID: document.getElementById("hdn_CostingSummaryId").value
                                    },
                                    aftersavefunc: null,
                                    errorfunc: null,
                                    afterrestorefunc: null,
                                    restoreAfterError: true,
                                    mtype: "POST"
                                },
                                addParams: {
                                    useDefValues: false,
                                    addRowParams: {
                                        keys: true,
                                        errorfunc: null,
                                        extraparam: {
                                            AssyPartNo: function () {
                                                var AssyPartNo = document.getElementById("PartNo").value;

                                                return AssyPartNo;
                                            },
                                            CostingSummaryID: function () { return document.getElementById("hdn_CostingSummaryId").value } 
                                        },
                                        oneditfunc: function (rawid) {

                                        },
                                        aftersavefunc: null,

                                        afterrestorefunc: null,
                                        restoreAfterError: true,
                                        successfunc: function (val) {
                                            if (val.responseText != "") {
                                                var PartNo = document.getElementById('PartNo').value;
                                                var CostingSummaryID = document.getElementById('hdn_CostingSummaryId').value;
                                                $("#tblCostSummary").jqGrid('setGridParam', { datatype: 'json', url: oApp + '/Assembly/CostSummaryVBC?fkCostingSummaryID=' + CostingSummaryID + '&AssyPartNo=' + PartNo }).trigger('reloadGrid');

                                            }
                                        }
                                    }
                                }
                            }
            );
                    }
                    else {
                        grid.jqGrid('navGrid', '#' + pagerName, { view: false, edit: false, refreshtext: "", add: false, del: false, refresh: true, search: false });
                    }
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr && thrownError) {
                alert('Status: ' + xhr.status + ' Error: ' + thrownError);
            }
        }
    });
}

// JqGrid With Multiple Grid Options
function JQGridFilterSubGrid(gridName, pagerName, action, sortColumn, sortOrder, caption, width, height, loadtext, excelUrl, parentDivId, Filter, ProcessURL, OtherOperationURL) {

    var lastsel;
    var colname;
    var grid = $('#' + gridName);
    var pager = $('#' + pagerName);
    $.ajax({
        url: action + "?column=column" + Filter,
        dataType: "json",
        mtype: 'POST',
        sortorder: 'asc',
        beforeSend: function () {
        },
        success: function (result) {

            if (result) {
                if (!result.Error) {
                    var colData = columnsData(result);
                    colname = columnsName(result.column);

                    colData = eval('{' + colData + '}');
                    colname = eval('{' + colname + '}');
                    grid.jqGrid('GridUnload');
                    grid.jqGrid({
                        url: action + '?' + Filter,
                        datatype: 'json',
                        height: height,
                        colNames: colname,
                        colModel: colData,
                        rowNum: 100,
                        scrollOffset: 0,

                        autowidth: true,
                        rownumbers: true,
                            rowList: [100, 500, 1000],
                        loadtext: loadtext,
                        pager: pager,
                        //viewrecords: true,
                        viewrecords: true,
                        sortorder: sortOrder,
                        sortname: sortColumn,
                        rownumbers: true,
                        caption: caption,
                        subGrid: true,
                        loadComplete: function () {
                            reDefineColWidth('#' + gridName);
                            if ($('#gs_Date').parent().next('td').html() != null) {
                                $('#gs_Date').parent().next('td').remove();
                            }
                            $('#gs_Date').remove();
                            if ($('#gs_Delete').parent().next('td').html() != null) {
                                $('#gs_Delete').parent().next('td').remove();
                            }
                            $('#gs_Delete').remove();
                            if ($('#gs_CreatedOn').parent().next('td').html() != null) {
                                $('#gs_CreatedOn').parent().next('td').remove();
                            }
                            $('#gs_CreatedOn').remove();
                            $('#gs_RateINRUOM').remove();
                            $('#gs_UOM').remove();
                        },
                        subGridRowExpanded: function (subgrid_id, row_id) {
                            var final = grid.jqGrid('getCell', row_id, 'CostingDetailTotalId');
                            //Other technology
                            if (final == "" || final == "0") {
                                final = grid.jqGrid('getCell', row_id, 'PK_CostingSummary');
                            }
                            //----------SubGrid One---------
                            var subgrid_table_id, pager_id;
                            subgrid_table_id = subgrid_id + "_t";
                            pager_id = "p_" + subgrid_table_id;
                            $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                            var Subgrid = $('#' + subgrid_table_id);
                            var Subpager = $('#' + pager_id);
                            $.ajax({
                                url: ProcessURL + "?column=column" + final,
                                dataType: "json",
                                mtype: 'POST',
                                sortorder: 'asc',
                                beforeSend: function () {
                                },
                                success: function (result) {

                                    if (result) {
                                        if (!result.Error) {
                                            var colData = columnsData(result);
                                            colname = columnsName(result.column);

                                            colData = eval('{' + colData + '}');
                                            colname = eval('{' + colname + '}');
                                            Subgrid.jqGrid('GridUnload');
                                            Subgrid.jqGrid({
                                                url: ProcessURL + "?final=" + final,
                                                datatype: 'json',
                                                height: 150,
                                                colNames: colname,
                                                colModel: colData,
                                                rowNum: 12,
                                                scrollOffset: 0,
                                                autowidth: true,
                                                rownumbers: true,
                                                rowList: [12, 100, 150],
                                                loadtext: loadtext,
                                                pager: Subpager,
                                                viewrecords: true,
                                                sortorder: sortOrder,
                                                sortname: sortColumn,
                                                rownumbers: true,
                                                caption: "Process",
                                                shrinkToFit: false,
                                                loadComplete: function () {
                                                    //alert(subgrid_id);
                                                    //reDefineColWidth('#' + subgrid_id);
                                                }
                                            });

                                        }
                                    }
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    if (xhr && thrownError) {
                                        alert('Status: ' + xhr.status + ' Error: ' + thrownError);
                                    }
                                }
                            });
                            //----------SubGrid Two---------
                            var subgrid_table_id2, pager_id2;
                            subgrid_table_id2 = subgrid_id + "_tt";
                            pager_id2 = "pp_" + subgrid_table_id;
                            $("#" + subgrid_id).append("<br><table id='" + subgrid_table_id2 + "' class='scroll'></table><div id='" + pager_id2 + "' class='scroll'></div>");
                            var Subgrid2 = $('#' + subgrid_table_id2);
                            var Subpager2 = $('#' + pager_id2);
                            $.ajax({
                                url: OtherOperationURL + "?column=column" + final,
                                dataType: "json",
                                mtype: 'POST',
                                sortorder: 'asc',
                                beforeSend: function () {
                                },
                                success: function (result) {

                                    if (result) {
                                        if (!result.Error) {
                                            var colData = columnsData(result);
                                            colname = columnsName(result.column);

                                            colData = eval('{' + colData + '}');
                                            colname = eval('{' + colname + '}');
                                            Subgrid2.jqGrid('GridUnload');
                                            Subgrid2.jqGrid({
                                                url: OtherOperationURL + "?final=" + final,
                                                datatype: 'json',
                                                height: 150,
                                                colNames: colname,
                                                colModel: colData,
                                                rowNum: 12,
                                                scrollOffset: 0,
                                                autowidth: true,
                                                rownumbers: true,
                                                rowList: [12, 100, 150],
                                                loadtext: loadtext,
                                                pager: Subpager2,
                                                viewrecords: true,
                                                sortorder: sortOrder,
                                                sortname: sortColumn,
                                                rownumbers: true,
                                                caption: "Other Operation",
                                                shrinkToFit: false
                                            });

                                        }
                                    }
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    if (xhr && thrownError) {
                                        alert('Status: ' + xhr.status + ' Error: ' + thrownError);
                                    }
                                }
                            });
                            //----------SubGrid Three---------
                            if (gridName == "CostSummaryReport")
                            {
                                //----------SubGrid Three---------
                                //Taken refrence from SubGrid.js
                                var subgrid_table_id3, pager_id3;
                                subgrid_table_id3 = subgrid_id + "_ttt";
                                pager_id3 = "ppp_" + subgrid_table_id;
                                $("#" + subgrid_id).append("<br><table id='" + subgrid_table_id3 + "' class='scroll'></table><div id='" + pager_id3 + "' class='scroll'></div>");
                                var Subgrid3 = $('#' + subgrid_table_id3);
                                var Subpager3 = $('#' + pager_id3);
                                $.ajax({
                                    url: oApp + '/SheetMetal/ApprovalRMList' + '?column=column' + "GridType=ApprovalRMList&CostingDetailTotalID=" + final,
                                    dataType: "json",
                                    mtype: 'POST',
                                    sortorder: 'asc',
                                    beforeSend: function () {
                                    },
                                    success: function (result) {

                                        if (result) {
                                            if (!result.Error) {
                                                var colData = columnsData(result);
                                                colname = columnsName(result.column);

                                                colData = eval('{' + colData + '}');
                                                colname = eval('{' + colname + '}');
                                                Subgrid3.jqGrid('GridUnload');
                                                Subgrid3.jqGrid({
                                                    url: oApp + '/SheetMetal/ApprovalRMList' + '?' + "GridType=ApprovalRMList&CostingDetailTotalID=" + final,
                                                    datatype: 'json',
                                                    height: 150,
                                                    colNames: colname,
                                                    colModel: colData,
                                                    rowNum: 12,
                                                    scrollOffset: 0,
                                                    autowidth: true,
                                                    rownumbers: true,
                                                    rowList: [12, 100, 150],
                                                    loadtext: loadtext,
                                                    pager: Subpager3,
                                                    viewrecords: true,
                                                    sortorder: sortOrder,
                                                    sortname: sortColumn,
                                                    rownumbers: true,
                                                    caption: "RM & BOP",
                                                    shrinkToFit: false,
                                                });
                                            }
                                        }
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        if (xhr && thrownError) {
                                            alert('Status: ' + xhr.status + ' Error: ' + thrownError);
                                        }
                                    }
                                });
                            }
                            //-------------End-------------

                            //jQuery("#" + subgrid_table_id).jqGrid('navGrid', "#" + Subpager, { edit: false, add: false, del: false })
                        },
                        subGridRowColapsed: function (subgrid_id, row_id) {
                            // this function is called before removing the data
                            var subgrid_table_id;
                            subgrid_table_id = subgrid_id + "_t";
                            jQuery("#" + subgrid_table_id).remove();
                        },
                    });
                    grid.jqGrid('navGrid', '#' + pagerName, {
                        view: false, edit: false, refreshtext: "", add: false, del: false, refresh: true, search: false
                    });
                    if (gridName != "MultipleCoreSize") {
                        grid.jqGrid('filterToolbar', {
                            searchOnEnter: false
                        });

                        $.extend($.jgrid.search, { Find: 'Search' });

                        if (parentDivId != '') {
                            $(window).bind('resize', function () {
                                if ($('#' + parentDivId).width() > 700) {
                                    grid.setGridWidth($('#' + parentDivId).width() - 1, true);
                                } else {
                                    grid.setGridWidth(900, true);
                                }
                            }).trigger('resize');
                        }
                        if (gridName != "RMGrid") {
                            grid.jqGrid('filterToolbar', { searchOnEnter: false });

                            $.extend($.jgrid.search, {
                                Find: 'Search'
                            });

                            if (parentDivId != '') {
                                $(window).bind('resize', function () {
                                    if ($('#' + parentDivId).width() > 700) {
                                        grid.setGridWidth($('#' + parentDivId).width() - 1, true);
                                    } else {
                                        grid.setGridWidth(900, true);
                                    }
                                }).trigger('resize');
                            }
                            if (gridName == "TechnologyReport" || gridName == "CostSummaryReport") {
                                // jQuery('#' + gridName).jqGrid('sortableRows');
                                grid.jqGrid('hideCol', 'PK_CostingSummary');
                                grid.jqGrid('showCol', 'CreatedBy');

                            }
                            if (gridName != "masterreport-grid-json") {

                                if (excelUrl != '') {

                                    grid.jqGrid('navButtonAdd', pagerName,
                                    {
                                        caption: "Excel", title: "Export to Excel", buttonicon: "ui-icon-print",
                                        onClickButton: function () {
                                            grid.jqGrid('excelExport', {
                                                url: excelUrl, oper: "oper", tag: "excel"
                                            });
                                        }
                                    });

                                }
                            }
                        }
                    }
                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr && thrownError) {
                alert('Status: ' + xhr.status + ' Error: ' + thrownError);
            }
        }
    });
}



// JqGrid With Check box selection 
function JQGridCheckBox(gridName, pagerName, action, sortColumn, sortOrder, caption, width, height, loadtext, excelUrl, parentDivId, Filter) {

    var lastsel;
    var colname;
    var grid = $('#' + gridName);
    var pager = $('#' + pagerName);
    $.ajax({
        url: action + "?column=column" + '&' + Filter,
        dataType: "json",
        mtype: 'POST',
        sortorder: 'asc',
        beforeSend: function () {
        },
        success: function (result) {

            if (result) {
                if (!result.Error) {
                    var colData = columnsData(result);
                    colname = columnsName(result.column);

                    colData = eval('{' + colData + '}');
                    colname = eval('{' + colname + '}');
                    grid.jqGrid('GridUnload');
                    grid.jqGrid({
                        url: action + '?' + Filter,
                        datatype: 'json',
                        height: height,
                        colNames: colname,
                        colModel: colData,
                        rowNum: 100,
                        scrollOffset: 0,
                        autowidth: true,
                        rownumbers: true,
                        rowList: [100, 500, 1000],
                        loadtext: loadtext,
                        pager: pager,
                        viewrecords: true,
                        sortorder: sortOrder,
                        sortname: sortColumn,
                        rownumbers: true,
                        caption: caption,
                        multiselect:true,
                        loadComplete: function () {
                            if (parentDivId != '') {

                                if ($('#' + parentDivId).width() > 700) {
                                    grid.setGridWidth($('#' + parentDivId).width() - 1, true);
                                } else {
                                    grid.setGridWidth(900, true);
                                }
                            }
                            //if (gridName != 'tblMasterSimulationList') {
                            //    reDefineColWidth('#' + gridName);
                            //}
                            $("#tblApproval").hideCol("CreatedOn"); 
                            $("#tblApproval").hideCol("FkProcessTableId"); // hide token no
                            //$("#tblApproval").hideCol("PkApprovalId");
                            //$("#tblApproval").hideCol("PushToSap");

                            //$("#tblApproval").jqGrid('setLabel', 'CreatedOn', 'Initiator On'); // Dynamic Column name change 

                        },

                    });
                    grid.jqGrid('navGrid', '#' + pagerName, { view: false, edit: false, refreshtext: "", add: false, del: false, refresh: true, search: false });
              
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr && thrownError) {
                alert('Status: ' + xhr.status + ' Error: ' + thrownError);
            }
        }
    });
}


//Main JQGRID Function
function ItemPOGrid(gridName, pagerName, action, sortColumn, sortOrder, caption, width, height, loadtext, excelUrl, parentDivId, Filter) {
    var lastsel;
    var colname;
    var grid = $('#' + gridName);
    var pager = $('#' + pagerName);
    $.ajax({
      //  url: action + "?column=column",
        url: action + "?column=column" + '&' + Filter,
        dataType: "json",
        mtype: 'POST',
        sortorder: 'asc',
        beforeSend: function () {
        },
        success: function (result) {
            if (result) {
                if (!result.Error) {
                    var colData = columnsData(result);
                    colname = columnsName(result.column);
                    colData = eval('{' + colData + '}');
                    colname = eval('{' + colname + '}');
                    grid.jqGrid('GridUnload');
                    grid.jqGrid({
                        url: action + '?' + Filter,
                        datatype: 'json',
                        height: height,
                        colNames: colname,
                        colModel: colData,
                        rowNum: 100,
                        //autowidth: true,
                        scrollOffset: 0,
                        autowidth: true,
                        rownumbers: true,
                        rowList: [100, 500, 1000],
                        loadtext: loadtext,
                        pager: pager,
                        hidegrid: false,
                        viewrecords: true,
                        sortorder: sortOrder,
                        sortname: sortColumn,
                        rownumbers: true,
                        caption: caption,
                        loadComplete: function () {
                            if (parentDivId != '') {
                                //grid.jqGrid('setGridWidth', $('#' + parentDivId).width() - 10, true);
                                if ($('#' + parentDivId).width() > 700) {
                                    grid.setGridWidth($('#' + parentDivId).width() - 1, true);
                                } else {
                                    grid.setGridWidth(900, true);
                                }
                            }
                            if (gridName == 'tblItemPO') {

                                //$("#tblItemPO").hideCol("ItemPOId");                             
                                reDefineColWidth('#' + gridName);
                              

                                var gid = $("#tblItemPO").jqGrid('getDataIDs');

                                for (i = 0; i < gid.length; i++) {

                                    var AMO2 = document.getElementById("DOC_TYPE").value;
                                    var plant = document.getElementById("Plant").value;

                                        var doc_Type = AMO2.substr(0, 4);
                                        var plantValue = plant.substr(0, 4);
                                    //alert(AMO2.charAt(1))
                                    //alert(AMO2.charAt(2))
                                    if (AMO2.charAt(1) == "A") {
                                        grid.setColProp('Amo2', { editable: true });
                                        //$("#tblItemPO").jqGrid('editRow', gid[i], true, null, null, 'clientArray')
                                    }
                                    else {
                                        grid.setColProp('Amo2', { editable: false });
                                        //$("#tblItemPO").jqGrid('editRow', gid[i], true, null, null, 'clientArray');
                                }

                                    if (plantValue = "1000" && (doc_Type == "1ABD" || doc_Type == "1ASD" || doc_Type=="1LBD" || doc_Type=="1LSD" || doc_Type=="1LID")) {
                                            grid.setColProp('DDVend', { editable: true });
                                    } else if (plantValue = "1400" && (doc_Type == "1ABD" || doc_Type == "1ASD" || doc_Type == "1LBD" || doc_Type == "1LSD")) {
                                        grid.setColProp('DDVend', { editable: true });
                                    } else {
                                        grid.setColProp('DDVend', { editable: false });
                                    }


                                    if (AMO2.charAt(2) == "S") {
                                        grid.setColProp('DrAmount', { editable: true });
                                        $("#tblItemPO").jqGrid('editRow', gid[i], true, null, null, 'clientArray')
                                    }
                                    else {
                                        grid.setColProp('DrAmount', { editable: false });
                                        $("#tblItemPO").jqGrid('editRow', gid[i], true, null, null, 'clientArray');
                                    }
                                }
                            }

                        }
                    });
                    grid.jqGrid('navGrid', '#' + pagerName, { view: false, edit: false, refreshtext: "", add: false, del: false, refresh: true, search: false });
                    //  grid.jqGrid('navGrid', '#' + parentDivId, { view: false, edit: false, refreshtext: "", add: false, del: false, refresh: true, search: true });               

                    if (parentDivId != '') {
                        $(window).bind('resize', function () {
                            if ($('#' + parentDivId).width() > 700) {
                                grid.setGridWidth($('#' + parentDivId).width() - 1, true);
                            } else {
                                grid.setGridWidth(900, true);
                            }
                        }).trigger('resize');
                    }               
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr && thrownError) {
                alert('Status: ' + xhr.status + ' Error: ' + thrownError);
            }
        }
    });
}


// JqGrid With Multiple Grid PO Options
function JQGridFilterSubGridPO(gridName, pagerName, action, sortColumn, sortOrder, caption, width, height, loadtext, excelUrl, parentDivId, Filter, ProcessURL, OtherOperationURL) {

    var lastsel;
    var colname;
    var grid = $('#' + gridName);
    var pager = $('#' + pagerName);
    $.ajax({
        url: action + "?column=column" + Filter,
        dataType: "json",
        mtype: 'POST',
        sortorder: 'asc',
        beforeSend: function () {
        },
        success: function (result) {

            if (result) {
                if (!result.Error) {
                    var colData = columnsData(result);
                    colname = columnsName(result.column);

                    colData = eval('{' + colData + '}');
                    colname = eval('{' + colname + '}');
                    grid.jqGrid('GridUnload');
                    grid.jqGrid({
                        url: action + '?' + Filter,
                        datatype: 'json',
                        height: height,
                        colNames: colname,
                        colModel: colData,
                        rowNum: 100,
                        scrollOffset: 0,

                        autowidth: true,
                        rownumbers: true,
                        rowList: [100, 500, 1000],
                        loadtext: loadtext,
                        pager: pager,
                        //viewrecords: true,
                        viewrecords: true,
                        sortorder: sortOrder,
                        sortname: sortColumn,
                        rownumbers: true,
                        caption: "PO Header Line",
                        subGrid: true,
                        loadComplete: function () {
                            //  reDefineColWidth('#' + gridName);
                            if ($('#gs_Date').parent().next('td').html() != null) {
                                $('#gs_Date').parent().next('td').remove();
                            }
                            $('#gs_Date').remove();
                            if ($('#gs_Delete').parent().next('td').html() != null) {
                                $('#gs_Delete').parent().next('td').remove();
                            }
                            $('#gs_Delete').remove();
                            if ($('#gs_CreatedOn').parent().next('td').html() != null) {
                                $('#gs_CreatedOn').parent().next('td').remove();
                            }
                            $('#gs_CreatedOn').remove();
                            $('#gs_RateINRUOM').remove();
                            $('#gs_UOM').remove();
                        },
                        subGridRowExpanded: function (subgrid_id, row_id) {
                            var final = grid.jqGrid('getCell', row_id, 'POHeaderID');

                            var subgrid_table_id, pager_id;
                            subgrid_table_id = subgrid_id + "_t";
                            pager_id = "p_" + subgrid_table_id;
                            $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                            var Subgrid = $('#' + subgrid_table_id);
                            var Subpager = $('#' + pager_id);
                            $.ajax({
                                url: ProcessURL + "?column=column" + final,
                                dataType: "json",
                                mtype: 'POST',
                                sortorder: 'asc',
                                beforeSend: function () {
                                },
                                success: function (result) {

                                    if (result) {
                                        if (!result.Error) {
                                            var colData = columnsData(result);
                                            colname = columnsName(result.column);

                                            colData = eval('{' + colData + '}');
                                            colname = eval('{' + colname + '}');
                                            Subgrid.jqGrid('GridUnload');
                                            Subgrid.jqGrid({
                                                url: ProcessURL + "?final=" + final,
                                                datatype: 'json',
                                                height: 150,
                                                colNames: colname,
                                                colModel: colData,
                                                rowNum: 12,
                                                scrollOffset: 0,
                                                autowidth: true,
                                                rownumbers: true,
                                                rowList: [12, 100, 150],
                                                loadtext: loadtext,
                                                pager: Subpager,
                                                viewrecords: true,
                                                sortorder: sortOrder,
                                                sortname: sortColumn,
                                                rownumbers: true,
                                                caption: "PO Line Item",
                                            });

                                        }
                                    }
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    if (xhr && thrownError) {
                                        alert('Status: ' + xhr.status + ' Error: ' + thrownError);
                                    }
                                }
                            });
                            //jQuery("#" + subgrid_table_id).jqGrid('navGrid', "#" + Subpager, { edit: false, add: false, del: false })
                        },
                        subGridRowColapsed: function (subgrid_id, row_id) {
                            // this function is called before removing the data
                            var subgrid_table_id;
                            subgrid_table_id = subgrid_id + "_t";
                            jQuery("#" + subgrid_table_id).remove();
                        },
                    });
                    grid.jqGrid('navGrid', '#' + pagerName, {
                        view: false, edit: false, refreshtext: "", add: false, del: false, refresh: true, search: false
                    });
                    if (excelUrl != '') {
                        grid.jqGrid('navButtonAdd', pagerName,
                            {
                                caption: "Excel", title: "Export to Excel", buttonicon: "ui-icon-print",
                                onClickButton: function () {
                                    grid.jqGrid('excelExport', { url: excelUrl, oper: "oper", tag: "excel" });
                                }
                            });
                    }
                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr && thrownError) {
                alert('Status: ' + xhr.status + ' Error: ' + thrownError);
            }
        }
    });
}


    //Use in Mould core cost in Ferrous casting
    function getAllSelectOptions() {

        var checktpye = { 'Cold Core 1': 'Cold Core 1', 'Cold Core 2': 'Cold Core 2', 'Cold Core 3': 'Cold Core 3', 'Cold Core 4': 'Cold Core 4', 'Cold Core 5': 'Cold Core 5', 'Cold Core 6': 'Cold Core 6', 'Cold Core 7': 'Cold Core 7', 'Cold Core 8': 'Cold Core 8', 'Cold Core 9': 'Cold Core 9', 'Cold Core 10': 'Cold Core 10', 'Cold Core 11': 'Cold Core 11', 'Cold Core 12': 'Cold Core 12', 'Shell Core 1': 'Shell Core 1', 'Shell Core 2': 'Shell Core 2', 'Shell Core 3': 'Shell Core 3', 'Shell Core 4': 'Shell Core 4', 'Shell Core 5': 'Shell Core 5', 'Shell Core 6': 'Shell Core 6', 'Shell Core 7': 'Shell Core 7', 'Shell Core 8': 'Shell Core 8', 'Shell Core 9': 'Shell Core 9', 'Shell Core 10': 'Shell Core 10', 'Shell Core 11': 'Shell Core 11', 'Shell Core 12': 'Shell Core 12'
        };
        return checktpye
        }


// Get All tehnology added by ravish Patel
function GetTechnology() {

    var checktpye = { 'Casting-Ferrous': 'Casting-Ferrous', 'Casting-Non Ferrous': 'Casting-Non Ferrous', 'Forging': 'Forging', 'Machining': 'Machining', 'Plastics': 'Plastics', 'Proprietary': 'Proprietary', 'Proprietary-Electrical': 'Proprietary-Electrical', 'Proprietary-Mechanical': 'Proprietary-Mechanical', 'Rubber': 'Rubber', 'Sheet Metal': 'Sheet Metal' };
    return myTechnology;
}

//on 24 March 2015 by nupur k
function getAllRMCost() {

    var checktpye = {'Vibration Welding': 'Vibration Welding', 'Assy': 'Assy', 'Mould Maintenance': 'Mould Maintenance', 'Velcro Pasting': 'Velcro Pasting', 'Assembly of Felt Shoddy': 'Assembly of Felt Shoddy', 'Inspection & Packaging': 'Inspection & Packaging', 'Post Curing': 'Post Curing', 'Packing Labour Cost': 'Packing Labour Cost', 'Boding Cost': 'Boding Cost', 'Cleaning(washing)': 'Cleaning(washing) ', 'Cutting': 'Cutting ', 'Dismendreling': 'Dismendreling ', 'Lable Cost': 'Lable Cost', 'Finishing': 'Finishing', 'Paint': 'Paint', 'Plating': 'Plating', 'Consumables': 'Consumables', 'Wastage': 'Wastage', 'Camlock Application': 'Camlock Application', 'Others': 'Others'
    };
    return checktpye
}

//For Removing cross from search text box in JqGrid 
 function Example() {
//define grid
    var grid = $("#list");
    //get all column names
    var columnNames = grid[0].p.colNames;
    //iterate through each and disable
for (i = 0; i < columnNames.length; i++) {
    grid.setColProp(columnNames[i], {
        sortable: false });
        }
        }
    jQuery.extend(jQuery.jgrid.defaults, { cmTemplate: { searchoptions: { clearSearch: false } } });


    function IsNumeric(e, id) {
        var value = document.getElementById('' + id + '').value;

        var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
        var ret = ((keyCode >= 48 && keyCode <= 57) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || (keyCode == 46));

        if (value.contains('.') && keyCode == 46) {
            return false;
        }

        else {
            return ret;
        }
    }

    function AllowFloat(event,t) {    
        
        if ((event.which != 46 || $(t).val().indexOf('.') != -1) && //if the keypress is not a . or there is already a decimal point
          ((event.which < 48 || event.which > 57) && //and you try to enter something that isn't a number
            (event.which != 45 || ($(t)[0].selectionStart != 0 || $(t).val().indexOf('-') != -1)) && //and the keypress is not a -, or the cursor is not at the beginning, or there is already a -
            (event.which != 0 && event.which != 8))) { //and the keypress is not a backspace or arrow key (in FF)
            event.preventDefault(); //cancel the keypress
        }

        if (($(t).val().indexOf('.') != -1) && ($(t).val().substring($(t).val().indexOf('.')).length > 2) && //if there is a decimal point, and there are more than two digits after the decimal point
          (($(t)[0].selectionStart - $(t)[0].selectionEnd) == 0) && //and no part of the input is selected
          ($(t)[0].selectionStart >= $(t).val().length - 2) && //and the cursor is to the right of the decimal point
          (event.which != 45 || ($(t)[0].selectionStart != 0 || $(t).val().indexOf('-') != -1)) && //and the keypress is not a -, or the cursor is not at the beginning, or there is already a -
          (event.which != 0 && event.which != 8)) { //and the keypress is not a backspace or arrow key (in FF)
            event.preventDefault(); //cancel the keypress
        }
    }
    function getSelectionStart(o) {
        if (o.createTextRange) {
            var r = document.selection.createRange().duplicate()
            r.moveEnd('character', o.value.length)
            if (r.text == '') return o.value.length
            return o.value.lastIndexOf(r.text)
        } else return o.selectionStart
    }

     













