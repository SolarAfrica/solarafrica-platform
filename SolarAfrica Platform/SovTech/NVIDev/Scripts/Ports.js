﻿function GetPortData(trig) {

    $.ajax({
        url: oApp + "/Project/GetPorts/",
        data: { "id": $(trig).val() },
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            var options = $('#portList')
            var configs = data[0];
            var gps = data[1];
            options.empty();

            for (i = 0; i < configs.length; i++) {
                options.append($('<option />').val(gps[i]).text(configs[i]));

            }
        }
    })
}

//            href: oApp + '/Account/CompanyAdd',

$(document).ready(function () {

    GetPortData($("#countryList"));
})