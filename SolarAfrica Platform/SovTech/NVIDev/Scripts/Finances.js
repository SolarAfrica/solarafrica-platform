﻿function ChangePlan(plan)
{

    $("#txtplan").text(plan);
    $("#mplan").val(plan)
    showPlan(plan);
}


$(document).ready(function () {

    ChangePlan($("#mplan").val());

}
    )

function showPlan(plan)
{
    
    var plansToChange = new Array();
    var HeadsToChange = new Array();
    var head = "";

    switch(plan)
    {
       
        case "Pay-By-Solar": $("#PBS").addClass("active").removeClass("hidden"); plansToChange[0] = "CSH"; plansToChange[1] = "PPA"; HeadsToChange[0] = "cash"; HeadsToChange[1] = "ppa"; head = "pbs";  break;
        case "Cash Offer": $("#CSH").addClass("active").removeClass("hidden"); plansToChange[0] = "PBS"; plansToChange[1] = "PPA"; HeadsToChange[0] = "pbs"; HeadsToChange[1] = "ppa"; head = "cash";break;
        case "Power Purchase Agreement": $("#PPA").addClass("active").removeClass("hidden"); plansToChange[0] = "CSH"; plansToChange[1] = "PBS"; HeadsToChange[0] = "cash"; HeadsToChange[1] = "pbs"; head = "ppa"; break;
    }
    $("#mplan").val(plan)
    for(var a = 0; a < plansToChange.length  ; a++)
    {

        $("#" + HeadsToChange[a]).removeClass("offer-on")
    }

    for (var b = 0; b < HeadsToChange.length ; b++)
    {
        $("#" + HeadsToChange[b] + "fields").find("*").attr('disabled', true)

    }


    $("#" + head).addClass("offer-on").find("*").attr('disabled', false)

    assignValues(head);

}


function assignValues(item)
{

    $("#indicativeTarrif").text($("#indicativeTarrif" + item).text())
    $("#fixedPayment").text($("#fixedPayment" + item).text())
    $("#upfrontPayment").text($("#upfrontPayment" + item).text())
    $("#term").text($("#term" + item).text())
    $("#firstYearSaving").text($("#firstYearSaving" + item).text())
    $("#paybackperiod").text($("#paybackperiod" + item).text())

}
