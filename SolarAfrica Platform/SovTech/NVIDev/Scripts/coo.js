﻿function HideCOO()
{
    $('#Coo-form').toggle()
    $('#Name').text("")
}
function HideCurrency() {
    $('#Currency-form').toggle()
    $('#id').text("")
    $('#ValueInDollars').text("")
}

function successCOO(data)
{
    if (data == "Success")
    {
        HideCOO();
        location.reload();
    }

    else if(data =="Already Exists")
    {
        $('#myMessage').show();
        $('#myMessage').text("Country Already Registered!")
    }

    else if(data == "Fail")
    {
        $('#myMessage').show();
        $('#myMessage').text("Country names may only consist of letters and spaces.")
    }
   

}

function successCurrency(data)
{
    if (data == "Success") {
        HideCurrency();
        location.reload();
    }

    else if (data == "Already Exists") {
        $('#myMessage1').show();
        $('#myMessage1').text("Currency Already Registered!")
    }

    else if (data == "Fail") {
        $('#myMessage1').show();
        $('#myMessage1').text("Currency Id must be a letters only abbreviation, e.g EUR = euro")
    }

}