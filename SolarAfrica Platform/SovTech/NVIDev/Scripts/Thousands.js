﻿

function CreateItem(element)
{
    var aID = element.id;
    var paren = element.parentElement;

    var newShiz = document.createElement('INPUT')
    newShiz.id = aID + 'field'
    var onis = $(element).attr('oninput')

    if (onis == null)
    {
        $(newShiz).attr('oninput', 'updateValue(this)')
    }

    else {
        $(newShiz).attr('oninput', onis + ';updateValue(this)')
    }
    var cls = $(element).attr('class').split(' ');
    for (var a = 0; a < cls.length;a++ )
    {
        newShiz.classList.add(cls[a])
    }

    newShiz.classList.add('ths')

 paren.insertBefore(newShiz,element)
}



$(document).ready(function()
{

   

    createEVERYTHING();
    PopulateCreatures();

})

var coID = 'elemID'
var coINT =0;

function createEVERYTHING()
{

   $('input').each(function(){


      
     
       if(this.type =='hidden' && this.classList.length > 0 && !hasInput(this) && !this.classList.contains('NT'))
       {
           //if this is a hidden type, and has classes in to, so not an invalid hidden for, only ones I've converted
           if (this.id == null)
           {
               this.id = coID + coINT;
         

               coINT++;
           }
           this.setAttribute('data-has', 'yes')
           var creature = document.getElementById(this.id);
           CreateItem(creature);

       }
      

    })

}

function hasInput(element)
{
    var a = element.getAttribute('data-has')

    if(a == null || a=='no')
    {
        return false;
    }

    return true;
   
}
function PopulateCreatures()
{
    $("*[id*='field']").each(function () {
      

        var hiddenElementID = "#" + this.id.split('field')[0]
        var newVal = numberWithCommas($(hiddenElementID).val());


        
        $("#" + this.id).val(newVal)
    })

}


function updateValue(element)
{

    var hiddenElementID ="#" + element.id.split('field')[0]


    var newVal = removeCommas(removeLetters($(element).val()));
    if (newVal == null || newVal == ' ' || newVal == '' || isNaN(newVal))
    {
        newVal = 0.00;
    }
    $(hiddenElementID).val(parseFloat(newVal));

    $(element).val(numberWithCommas(numberWithCommas(newVal)))

}

function numberWithCommas(x) {

    if (x == null)
    {
        return 0;
    }

    else {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    }
}


function removeCommas(x) {
    var c = x.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "")

    return c;
}

function removeLetters(x)
{
    var c = x.replace(/[A-Za-z$-]/g, "");
    return c;
}