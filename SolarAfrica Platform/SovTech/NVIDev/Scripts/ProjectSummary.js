﻿function showTable()
{
    $('#summary-content').toggle();
}

function UpdateTarrif(data)
{
    //sum-       tarrif    upfront    size
    
    var tfr = parseFloat(data[0]).toPrecision(3)
    var up = parseFloat(data[1]).toFixed(2)
    var size = parseFloat(data[2]).toPrecision(2)
    var fixed = parseFloat(data[3]).toFixed(2)
    $('#sum-tarrif').text(tfr)
    $('#sum-upfront').text(up)
    $('#sum-size').text(size)
    $('#sum-fixed').text(fixed)

}