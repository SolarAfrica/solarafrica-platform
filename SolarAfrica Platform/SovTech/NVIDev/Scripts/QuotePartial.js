﻿function CalculateMargins() {

    //get all cost items        cost...markup...fixed...sellprice
    //get current tariff, target tariff, current system cost, new system costs
    //calculate diff factor
    //multiply all unfixed markups by factor
    //recalculate . then repeat

    getAllLineItems();
    //HidePopup();
}

var setMarkup;

var curr = $('#curr').text()


function getAllLineItems() {


    if ($('#UseMargin').is(':checked')) {

        GetToMargin();
        HidePopup();

    }

    else if ($('#UseWP').is(':checked')) {

        GetToWP();
        HidePopup();

    }

    else {

    }




}

function GetToWP() {
    var wpToHit = parseFloat(removeCommas( $('#myWp').val()));
    var currentWP = parseFloat(removeCommas($("#CurrentWP").text()))
    var currentSYS = parseFloat(removeCommas($("#currentSystemPrice").text()))
    var difference = wpToHit / currentWP;
    var targetValToHit = currentSYS * ( difference)

    var allLineItems = new Array();
    var allCosts = $('.cost-text');
    var allMarkups = $('.m-markup.ths');
    var allSellItems = $('.line-item-amount')
    var allFixeds = $('.checkbox')
    var costAmountCount = allCosts.length;
    var totalCost = 0;
    for (var a = 0; a < costAmountCount; a++) {
        var costLine = {

            Cost: parseFloat(removeCommas($(allCosts[a]).text()).replace(curr, '')),
            Markup: parseFloat(removeCommas($(allMarkups[a]).val())),
            SellPrice: parseFloat(removeCommas($(allSellItems[a]).text()).replace(curr, '')),
            Fixed: $(allFixeds[a]).attr('checked')
        }

        totalCost += costLine.SellPrice;
        allLineItems[a] = costLine;
    }
    var mult = 0;
    if (difference > 1)
    {
        mult = 1;
        while (targetValToHit > currentSYS) {
            for (var a = 0; a < costAmountCount; a++) {



                //if not fixed set to new margin and if not zero
                var mCost = parseFloat(removeCommas($(allCosts[a]).text()).replace(curr, ''))
                var mMarkup = parseFloat(removeCommas($(allMarkups[a]).val())) + mult;
                var mFixed = $(allFixeds[a]).attr('checked')
                if (mCost > 0 && mFixed != 'checked') {

                    var id = allMarkups[a].id
                    $("#" + allMarkups[a].id).val(mMarkup)

                    updateLineSellPrice(document.getElementById(id))
                    updateValue(document.getElementById(id))
                    tally();

                    currentSYS = parseFloat(removeCommas($("#currentSystemPrice").text()))
                    targetValToHit = currentSYS * (difference);
                }
            }
        }
    }

    else {
        mult = -1;

        while (targetValToHit < currentSYS) {
            for (var a = 0; a < costAmountCount; a++) {



                //if not fixed set to new margin and if not zero
                var mCost = parseFloat(removeCommas($(allCosts[a]).text()).replace(curr, ''))
                var mMarkup = parseFloat(removeCommas($(allMarkups[a]).val())) + mult;
                var mFixed = $(allFixeds[a]).attr('checked')
                if (mCost > 0 && mFixed != 'checked') {

                    var id = allMarkups[a].id
                    $("#" + allMarkups[a].id).val(mMarkup)

                    updateLineSellPrice(document.getElementById(id))
                    updateValue(document.getElementById(id))
                    tally();

                    currentSYS = parseFloat(removeCommas($("#currentSystemPrice").text()))
                    
                    targetValToHit = currentSYS * (difference);
                }
            }
        }
    }

   
    //loop until all markups are right

    


}

function GetToMargin() {
    var marginToHit = $('#myMargin').val();

    var allLineItems = new Array();

    var allCosts = $('.cost-text');
    var allMarkups = $('.m-markup.ths');
    var allSellItems = $('.line-item-amount')
    var allFixeds = $('.checkbox')
    var costAmountCount = allCosts.length;
    var totalCost = 0;

    for (var a = 0; a < costAmountCount; a++) {
        var costLine = {

            Cost: parseFloat(removeCommas($(allCosts[a]).text()).replace(curr, '')),
            Markup: parseFloat(removeCommas($(allMarkups[a]).val())),
            SellPrice: parseFloat(removeCommas($(allSellItems[a]).text()).replace(curr, '')),
            Fixed: $(allFixeds[a]).attr('checked')
        }

        totalCost += costLine.SellPrice;
        allLineItems[a] = costLine;



        //var  mSellPrice = parseFloat(removeCommas($(allSellItems[a]).text()).replace(curr, ''))

    }


    for (var a = 0; a < costAmountCount; a++) {

        //if not fixed set to new margin and if not zero
        var mCost = parseFloat(removeCommas($(allCosts[a]).text()).replace(curr, ''))
        var mMarkup = parseFloat(removeCommas($(allMarkups[a]).val()))

        var mFixed = $(allFixeds[a]).prop('checked')

        if (mCost > 0 && !mFixed ) {
            var id =  allMarkups[a].id
            mMarkup = marginToHit;
            $("#" + allMarkups[a].id).val(mMarkup)
  
            updateLineSellPrice(document.getElementById(id))
            updateValue(document.getElementById(id))


        }

        tally();

    }
}