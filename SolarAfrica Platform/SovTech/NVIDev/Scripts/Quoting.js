﻿function OnSuccessCost(result) {

    //$("#CostTable > tbody > tr").eq(10).after('<tr>' + result + '</tr>');
    $("#addhere").after('<tr>' + result + '</tr>');
    createEVERYTHING();


    HidePopup();
    populateNewMarkup();
}
var  curr = $('#curr').text().replace(/\s/g, '')


function findCostInfo(trig) {
    var a = $(trig).val();


    if (a != 'None' && a != 'New Item')
       
    {
        a = a.replace(' ', '_')

        var pare = trig.parentNode.parentNode;
        $.ajax({
            url: '/Project/FindCostInfo/',
            data: { id: a },
            type: 'POST',
            dataType: 'json',
            success: function (data) {

                //costItem.Name, costItem.Cost.ToString(), costItem.Markup.ToString(), costItem.SellPrice.ToString(), costItem.Removeable.ToString() };
                $('#Name').val(data[0]);

                $('#Costfield').val(numberWithCommas(parseFloat(data[1])));
                $('#Markupfield').val(numberWithCommas(parseFloat(data[2])));

                $('#Cost').val(parseFloat(data[1]));
                $('#Markup').val(parseFloat(data[2]));

                var sellprice = parseFloat(data[1]) * (1 + parseFloat(data[2]) / 100);

                $('#sellprice').text(numberWithCommas(curr + ' ' +sellprice ));

                var fixed = data[3] == "true" ? true : false;
                $('#Fixed').attr('checked', fixed);
            }
        })
    }
  
    else if (a == 'New Item') {
        $('#Name').val(" ");

        $('#Costfield').val('0');
        $('#Markupfield').val('0');


        $('#sellprice').text(numberWithCommas('0.00' + ' ' + curr));

      
        $('#Fixed').attr('checked', false);
    }
}


function populateNewMarkup()
{
    $("*[id*='field']").each(function () {


        var hiddenElementID = "#" + this.id.split('field')[0]
        var newVal = numberWithCommas($(hiddenElementID).val());



        $("#" + this.id).val(newVal)
    })
}

function updateLineSellPrice(trig) {
    //used for the cost line item

    var id = trig.id.split('__Markupfield')[0]
  var markID = '#' + id + '__Markupfield'; 
  var costID = '#' + id + '__Cost';

    a = trig.parentNode.parentNode.childNodes.length;


    var cost = parseFloat($(costID).val().replace(curr),'');
    var markup = parseFloat(removeCommas($(markID).val()));
    if (markup == ' ' || markup=='NaN' || isNaN(markup))
    {
        markup = 0
    }
    var sellprice = (cost * (1 + markup / 100)).toFixed(2);
  
    $(trig.parentNode.parentNode).find(".lower-text:last").text(curr + ' '+  numberWithCommas(sellprice) ) //this is the sellprice column

    tally();
}


function tally()
{
    curr = $('#curr').text().replace(/\s/g, '')

    var totalCost = 0.00;
    $('.line-item-amount').each(function () {

    
        var text = $(this).text();
        var number = parseFloat(removeCommas(text).replace(curr, ''))


        if(number == NaN || number =='NaN ')
        {
            
        }

        else{
            totalCost += number;

        }

    });


    $("#currentSystemPrice").text(curr + ' ' + numberWithCommas(totalCost.toFixed(2)) )
}


$(document).ready(function () {

    tally();
})




function numberWithCommas(x) {
    var neg = "-";
    var a = parseFloat(x).toFixed(2);

    if (a < 0)
    {
        return neg + a.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    }
    return a.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function removeCommas(x) {
    var c = x.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "")
   
    return parseFloat(c).toFixed(2);
}

function OtherUpdate(trig, id)
{
    id = "#" + id;
    var x =$(trig).val();
    if ( x == '' || x == ' ' || x == 0 || isNaN(x))
    {
        $(id).text(curr + ' ' + 0)

    }

    else {
        var a = removeCommas($(trig).val());
        a = parseFloat(a).toFixed(2);

        a = numberWithCommas(a);



        $(id).text(curr + ' ' + a)
    }
  
    tally();
}