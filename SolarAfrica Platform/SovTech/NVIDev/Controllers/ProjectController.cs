﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NVIDev.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;
using iTextSharp.text;
using iTextSharp.text.pdf;
using MvcRazorToPdf;
using System.Web.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.IO;
using Excel;
using NVIDev.ActionFilters;




namespace NVIDev.Controllers
{
    [Authorize(Roles = "NVIAdmin,NVIUser,TPAdmin,TPUser")]
    [CustAuthFilter]
    public class ProjectController : Controller
    {

        private DBHelper dbhelper = new DBHelper();
        public ActionResult Index()
        {
            var currentUserID = User.Identity.GetUserId();

            List<Project> myProjects = dbhelper.GetUserProjects(currentUserID);

            foreach (var project in myProjects)
            {
                project.isEnergyDone();
            }
            return View(myProjects);
        }

        public ActionResult Test(int id)
        {
            var project = dbhelper.GetProject(id);
            SuperProject model = new SuperProject(project);
            return View(model);
        }

        public ActionResult Test2(int id)
        {
            var project = dbhelper.GetProject(id);
            SuperProject model = new SuperProject(project);
            return View(model);
        }


        #region Project Core
        public ActionResult Details(int id)
        {

            Project project = dbhelper.GetProject(id);
            if (project == null)
            {
                return HttpNotFound();
            }

            if (User.Identity.GetUserId() == project.CurrentUserID || User.IsInRole("NVIAdmin"))
            {
                return View(project);

            }

            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
        }
        // GET: /Project/Create
        public ActionResult Create()
        {
            ViewBag.Titles = dbhelper.PersonTitles();
            ViewBag.Ports = dbhelper.GetAllPortNames();
            ViewBag.Countries = dbhelper.GetAllCountryNames();

            Project model = new Project();
            return View(model);
        }





        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "Costing, CurrentUserID,CreatorID, Code, Id")]Project project)
        {

            if (ModelState.IsValid)
            {


                bool Success = dbhelper.CreateProject(project, User.Identity.GetUserId());

                if (Success)
                {
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Created a new project : {1}.", CurrentUsername(), project.Details.ProjectName));

                    return RedirectToAction("EditCosting", new { @id = project.Id });
                }

            }
            ViewBag.Titles = dbhelper.PersonTitles();
            ViewBag.Ports = dbhelper.GetAllPortNames();
            ViewBag.Countries = dbhelper.GetAllCountryNames();
            return View(project);
        }

        public ActionResult ShowMap()
        {

            return PartialView("~/Views/Project/_Map.cshtml");
        }


        public ActionResult Audits(int id)
        {
            List<Audit> projectsAudits = dbhelper.GetProjectAudits(id);
            return View(projectsAudits);
        }
        public ActionResult Maps()
        {
            return View();
        }
        public JsonResult GetPorts(string id)
        {
            List<string> ports = dbhelper.GetAllPortNames(id);
            List<string> gps = dbhelper.GetAllPorts(id).Select(m => m.GPS).ToList();
            if (ports == null || ports.Count <= 0)
            {
                ports.Add("NONE");
            }
            return Json(new[] { ports, gps });
        }

        // GET: /Project/Edit/5
        public ActionResult EditDetails(int id)
        {
            ViewBag.Titles = dbhelper.PersonTitles();
            ViewBag.Countries = dbhelper.GetAllCountryNames();
            ViewBag.Ports = dbhelper.GetAllPortNames();

            Project project = dbhelper.GetProject(id);
            if (project == null)
            {
                return HttpNotFound();
            }

            if (project.CurrentUserID != User.Identity.GetUserId())
            {
                return RedirectToAction("Index");
            }


            return View(project);
        }

        // POST: /Project/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDetails([Bind(Exclude = "Costing,Code")]Project project)
        {
            if (ModelState.IsValid)
            {
                bool success = dbhelper.EditProjectDetails(project);

                if (success)
                {
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited  : {1} - Proposal Info.", CurrentUsername(), project.Details.ProjectName));
                    return RedirectToAction("EditDetails", new { Id = project.Id });
                }

            }

            ViewBag.Titles = dbhelper.PersonTitles();
            ViewBag.Countries = dbhelper.GetAllCountryNames();
            ViewBag.Ports = dbhelper.GetAllPortNames();

            return View(project);
        }



        public ActionResult EditCosting(int id)
        {
            ViewBag.Terms = dbhelper.Terms();
            ViewBag.Plans = dbhelper.ProjectPlans();
            ViewBag.Types = dbhelper.ProjectTypes();
            ViewBag.Sources = dbhelper.EnergySources();


            Project project = dbhelper.GetProject(id);
            ViewBag.Currency = dbhelper.GetCurrency(project.Details.P_Country);
            SuperProject summary = new SuperProject(project);
            ViewBag.Summary = new[] { summary.TFR.ToString("0.###"), summary.Up.ToString("0.##"), summary.prosize.ToString("0.##"), (summary.FP / 12.00).ToString("0.##") };


            if (project == null)
            {
                return HttpNotFound();
            }

            if (project.CurrentUserID != User.Identity.GetUserId())
            {
                //project is not assigned to current user
                return RedirectToAction("Index");
            }


            return View(project);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCosting([Bind(Exclude = "Details")]Project project)
        {
            if (ModelState.IsValid)
            {

                bool success = dbhelper.EditProjectCosting(project);

                if (success)
                {
                    Project a = dbhelper.GetProject(project.Id);
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited  : {1} - Energy Costs.", CurrentUsername(), a.Details.ProjectName));
                    return RedirectToAction("EditCosting", new { Id = project.Id });
                    //return Json(new[] { summary.TFR, summary.Up, summary.prosize, summary.FP / 12.00 });
                }

            }
            ViewBag.Terms = dbhelper.Terms();
            ViewBag.Plans = dbhelper.ProjectPlans();
            ViewBag.Types = dbhelper.ProjectTypes();
            ViewBag.Sources = dbhelper.EnergySources();
            return View(project);
        }





        [HttpGet]
        public ActionResult Quoting(int id)
        {

            Project project = dbhelper.GetProject(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            SuperProject super = new SuperProject(project);
            super.Project = super.PopulateCosts(super.Project);
            QuotingViewModel model = new QuotingViewModel();
            model.Populate(super);


            ViewBag.Currency = dbhelper.GetCurrency(project.Details.P_Country);
            ViewBag.Suppliers = dbhelper.Suppliers(User.Identity.GetUserId());

            if (project.CurrentUserID != User.Identity.GetUserId())
            {
                return RedirectToAction("Index");
            }


            return View(model);
        }



        [HttpPost]
        public ActionResult Quoting(QuotingViewModel model)
        {
            ViewBag.Suppliers = dbhelper.Suppliers(User.Identity.GetUserId());

            if (ModelState.IsValid)
            {

                try
                {


                    bool Success = dbhelper.EditProjectQuoting(model);

                    Project a = dbhelper.GetProject(model.id);
                    ViewBag.Currency = dbhelper.GetCurrency(a.Details.P_Country);

                    if (Success)
                    {
                        AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited  : {1} - Quoting.", CurrentUsername(), a.Details.ProjectName));
                        return RedirectToAction("Quoting", new { id = model.id });
                    }


                }

                catch
                {
                    return View(model);

                }
            }            

            return View(model);
        }


        public ActionResult AddCostLine(QuotingCost model)
        {
            model.Removeable = true;
            model.Assigned = true;
            model.Supplier = "NA";
            model.Main = false;
            model.WPMark = "Markup";
            model.UserID = User.Identity.GetUserId();
            model.CalculateSellPrice();
            string mess = dbhelper.TryInsertCostItem(model);
            return PartialView("~/Views/Project/_QuoteCost.cshtml", model);
        }


        public ActionResult ShowCostPartial()
        {
            QuotingCost model = new QuotingCost();
            var a = dbhelper.GetUserCostItems(User.Identity.GetUserId());
            List<string> costNames = new List<string>();


            int b = a.Count();
            if (a.Count() < 1)
            {

                ViewBag.UserCosts = new string[] { "None" };
            }

            else
            {
                costNames.Add("New Item");

                var c = dbhelper.GetUserCostItems(User.Identity.GetUserId()).Select(m => m.Name);
                costNames.AddRange(c);
                //model = a.First();
                ViewBag.UserCosts = costNames;
            }


            return PartialView("~/Views/Project/_Cost.cshtml", model);
        }


        public ActionResult ShowQuotePartial()
        {



            return PartialView("~/Views/Project/QuotePartial.cshtml");
        }

        //public ActionResult ShowQuotingPartial()

        public ActionResult FindCostInfo(string id)
        {
            id = id.Replace('_', ' ');
            var costItem = dbhelper.GetQuoteCost(id, User.Identity.GetUserId());

            if (costItem != null)
            {
                string[] items = new string[] { costItem.Name, costItem.Cost.ToString(), costItem.Markup.ToString(), costItem.SellPrice.ToString(), costItem.Removeable.ToString() };
                return Json(items);

            }
            return null;
        }



        [HttpGet]
        public ActionResult Finance(int id)
        {

            ViewBag.Terms = dbhelper.Terms();

            Project project = dbhelper.GetProject(id);
            ViewBag.Currency = dbhelper.GetCurrency(project.Details.P_Country);
            SuperProject super = new SuperProject(project);
            ViewBag.Super = super;
            project.in_UpfrontClient = super.UpInitial;
            ViewBag.Curreny = super.Country.Currency;


            //create values for the project for each of the plans
            Project dummy1 = new Project();
            dummy1.Populate(project);
            dummy1.Costing.Plan = "Pay-By-Solar";

            SuperProject pbs = new SuperProject(dummy1);
            ViewBag.PBS = pbs;

            Project dummy2 = new Project();
            dummy2.Populate(project);
            dummy2.Costing.Plan = "Power Purchase Agreement";
            SuperProject ppa = new SuperProject(dummy2);


            ViewBag.PPA = ppa;
            Project dummy3 = new Project();
            dummy3.Populate(project);
            dummy3.Costing.Plan = "Cash Offer";
            SuperProject csh = new SuperProject(dummy3);


            ViewBag.CSH = csh;


            if (project == null)
            {
                return HttpNotFound();
            }

            if (project.CurrentUserID != User.Identity.GetUserId())
            {
                return RedirectToAction("Index");
            }


            return View(project);
        }


        [HttpPost]
        public ActionResult Finance([Bind(Exclude = "Details")] Project model)
        {
            ViewBag.Terms = dbhelper.Terms();

            Project projectToUpdate = dbhelper.GetProject(model.Id);


            if (ModelState.IsValid)
            {

                bool success = dbhelper.EditProjectFinance(model);
                if (success)
                {
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited  : {1} - Finance.", CurrentUsername(), projectToUpdate.Details.ProjectName));

                }

                return RedirectToAction("Finance", new { Id = model.Id });



            }

            ViewBag.Currency = dbhelper.GetCurrency(projectToUpdate.Details.P_Country);
            SuperProject super = new SuperProject(projectToUpdate);
            ViewBag.Super = super;
            projectToUpdate.in_UpfrontClient = super.UpInitial;
            ViewBag.Curreny = super.Country.Currency;



            //create values for the project for each of the plans
            Project dummy1 = new Project();
            dummy1.Populate(projectToUpdate);
            dummy1.Costing.Plan = "Pay-By-Solar";

            SuperProject pbs = new SuperProject(dummy1);
            ViewBag.PBS = pbs;

            Project dummy2 = new Project();
            dummy2.Populate(projectToUpdate);
            dummy2.Costing.Plan = "Power Purchase Agreement";
            SuperProject ppa = new SuperProject(dummy2);


            ViewBag.PPA = ppa;
            Project dummy3 = new Project();
            dummy3.Populate(projectToUpdate);
            dummy3.Costing.Plan = "Cash Offer";
            SuperProject csh = new SuperProject(dummy3);


            ViewBag.CSH = csh;



            return View(projectToUpdate);
        }

        [HttpGet]
        public ActionResult Pronumbers()
        {

            string UserID = User.Identity.GetUserId();
            string pronumbers = dbhelper.ProjectNumbers(UserID);
            return Content(pronumbers);
        }



        [HttpGet]
        public ActionResult Review(int id)
        {
            Project pro = dbhelper.GetProject(id);
            Fees fee = dbhelper.CurrentFees();
            FeeViewModel model = new FeeViewModel(pro, fee);
            model.Populate();

            ViewBag.Currency = dbhelper.GetCurrency(pro.Details.P_Country);
            ViewBag.Exch = dbhelper.CurrencyPerDollar(pro.Details.P_Country);
            return View(model);
        }

        [HttpPost]
        public ActionResult Review(FeeViewModel model)
        {

            if (ModelState.IsValid)
            {

                bool success = dbhelper.ReviewProject(model);


                if (success)
                {

                    Project pro = dbhelper.GetProject(model.project.Id);
                    string userID = User.Identity.GetUserId();
                    AddAudit(userID, string.Format("{0} Reviewd  : {1} .", userID, pro.Details.ProjectName));
                    return RedirectToAction("Index");

                }


            }


            return View(model);
        }


        // POST: /Project/Delete/5

        public ActionResult DeleteProject(int id)
        {

            Project project = dbhelper.GetProject(id);
            if (User.Identity.GetUserId() == project.CurrentUserID)
            {
                bool success = dbhelper.DeleteProject(project);


            }


            return RedirectToAction("Index");




        }


        [HttpGet]
        public ActionResult TransferProject(int id)
        {
            TransferViewModel model = new TransferViewModel();
            Project pro = dbhelper.GetProject(id);
            model.ProjectID = id;
            model.ProjectName = pro.Details.ProjectName;
            var currentUser = dbhelper.GetUser(User.Identity.GetUserId());
            model.CurrentUserID = currentUser.Id;
            model.AllCompanyUsers = dbhelper.GetAllCompanyUserFullNames(currentUser.Company);
            return PartialView(model); //send project transfer to view
        }

        [HttpPost]
        public ActionResult TransferProject(TransferViewModel model)
        {

            if (ModelState.IsValid)
            {

                Project project = dbhelper.GetProject(model.ProjectID);
                if (User.Identity.GetUserId() == project.CurrentUserID)
                {
                    try
                    {
                        dbhelper.TransferProject(project, model.NewUserName);
                        AddAudit(User.Identity.GetUserId(), string.Format("{0} Transfered : {1} to {2}.", CurrentUsername(), project.Details.ProjectName, model.NewUserName));
                        return RedirectToAction("Index");
                    }

                    catch (Exception)
                    { //failed possibly due to bad user information
                        return RedirectToAction("Index");
                    }

                }


            }
            return RedirectToAction("Index");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbhelper.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion



        #region PDF Generation



        [AllowAnonymous]

        public ActionResult Quote(int id)
        {
            var project = dbhelper.GetProject(id);
            var superMode = new SuperProject(project);
            ViewBag.Currency = dbhelper.GetCurrency(project.Details.P_Country);
            var a = dbhelper.GetUser(User.Identity.GetUserId());

            if (a.Image != null)
            {
                string imageBase64 = Convert.ToBase64String(a.Image);
                string imageSrc = string.Format("data:image/gif;base64,{0}", imageBase64);
                ViewBag.Image = imageSrc;
            }

            QuoteModel qm = new QuoteModel()
            {
                BatterySize = (int)superMode.BattCap,
                Country = project.Details.P_Country,
                ExchangeRate = project.Costing.ExchangeRate,
                DesignQuote = project.Costing.DesignFee,
                ExpEnergyYield = project.Costing.Yield,
                InstallationQuote = project.Costing.InstallFee,
                InstallType = project.Costing.Type,
                Term = project.Costing.Term,
                SunnyIslandCap = (int)superMode.sunnyIslandCap,
                OMWork = project.Costing.OMWorks,
                OMRate = superMode.OMRate,
                TPRebate = project.Costing.TPRate,
                UpfrontPay = project.UpfrontXI,
                Tariff = superMode.TFR,
                ProjectType = project.Costing.Plan,
                StorageCharge = project.Costing.StorageCost,
                SystemSize = superMode.prosize,
                inclOM = project.OMYN,
                inIn = project.InsureYN,
                FixedMonthlyCharges = superMode.FPMT,
                EstInsurance = (double)(superMode.InsTotal / 12)

            };

            return View(qm);
        }


        [AllowAnonymous]
        public ActionResult QuoteASPDF(int id)
        {


            var project = dbhelper.GetProject(id);
            var superMode = new SuperProject(project);
            ViewBag.Currency = dbhelper.GetCurrency(project.Details.P_Country);
            var a = dbhelper.GetUser(User.Identity.GetUserId());

            if (a.Image != null)
            {
                string imageBase64 = Convert.ToBase64String(a.Image);
                string imageSrc = string.Format("data:image/gif;base64,{0}", imageBase64);
                ViewBag.Image = imageSrc;
            }

            QuoteModel qm = new QuoteModel()
            {
                BatterySize = (int)superMode.BattCap,
                Country = project.Details.P_Country,
                ExchangeRate = superMode.Exch,
                DesignQuote = project.Costing.DesignFee,
                ExpEnergyYield = project.Costing.Yield,
                InstallationQuote = project.Costing.InstallFee,
                InstallType = project.Costing.Type,
                Term = project.Costing.Term,
                SunnyIslandCap = (int)superMode.sunnyIslandCap,
                OMWork = project.Costing.OMWorks / 12,
                OMRate = superMode.OMRate,
                TPRebate = project.Costing.TPRate,
                UpfrontPay = superMode.Up * 1000,
                Tariff = superMode.TFR,
                ProjectType = project.Costing.Plan,
                StorageCharge = project.Costing.StorageCost,
                SystemSize = superMode.prosize,
                inclOM = project.OMYN,
                inIn = project.InsureYN,
                FixedMonthlyCharges = (project.Costing.Plan == "Solar Development Services") ? (superMode.TFR * superMode.prosize * project.Costing.Yield) : (project.real_FixedAnualCharge / 12),
                EstInsurance = (superMode.InsTotal * 1000 * superMode.NVIStatic.InsR / 100 / 12),
                TPCosts = (project.Costing.Plan == "Solar Development Services") ? 0.0 : (((project.Costing.InstallFee / 1000 + project.Costing.DesignFee / 1000) * superMode.TPF) / 100)

            };

            return new MvcRazorToPdf.PdfActionResult(qm);

        }


        #endregion

        #region EquipmentSelection

        public ActionResult updateAllProjectCodes()
        {
            string a = dbhelper.UpdateAllProjectCodes();
            return Content(a);
        }

        [HttpGet] //get the equipment selection page
        public ActionResult EditEquipment(int id)
        {
            ViewBag.Packages = dbhelper.Packages();
            ViewBag.Types = dbhelper.ProjectTypes();
            ViewBag.BOSTypes = dbhelper.BalanceCategories();

            //View stuff
            ViewBag.Inverters = dbhelper.GetAllInverters();
            ViewBag.BiInverters = dbhelper.GetAllBiInverters();
            ViewBag.MountingStructure = dbhelper.GetAllMounting();
            ViewBag.Batteries = dbhelper.GetAllBatteries();
            ViewBag.PVModules = dbhelper.GetAllPVModules();
            ViewBag.BalanceSystems = dbhelper.GetAllBalanceSystems();

            Project pro = dbhelper.GetProject(id);

            ViewBag.Currency = dbhelper.GetCurrency(pro.Details.P_Country);
            ViewBag.Exch = dbhelper.CurrencyPerDollar(pro.Details.P_Country);
            EquipmentSelectionViewModel viewModel = new EquipmentSelectionViewModel();
            viewModel.CurrencyPerDollar = dbhelper.CurrencyPerDollar(pro.Details.P_Country);
            viewModel.Populate(pro, dbhelper.db);

            if (pro == null)
            {
                return HttpNotFound();
            }
            if (User.Identity.GetUserId() == pro.CurrentUserID)
            {
                return View(viewModel);

            }

            else
            {
                return RedirectToAction("Index");

            }
        }

        [HttpPost]
        public ActionResult EditEquipment(EquipmentSelectionViewModel Model)
        {
            ViewBag.Packages = dbhelper.Packages();
            ViewBag.Types = dbhelper.ProjectTypes();

            //View stuff
            ViewBag.Inverters = dbhelper.GetAllInverters();
            ViewBag.BiInverters = dbhelper.GetAllBiInverters();
            ViewBag.MountingStructure = dbhelper.GetAllMounting();
            ViewBag.Batteries = dbhelper.GetAllBatteries();
            ViewBag.PVModules = dbhelper.GetAllPVModules();
            ViewBag.BalanceSystems = dbhelper.GetAllBalanceSystems();

            ViewBag.BOSTypes = dbhelper.BalanceCategories();
            //inverter ids = name!config!dco!dcl!io!co , amounts = packagin!amount
            //bi inverter ids = name!com!app!interface , amounts = normal
            //mount ids = name, amounts = amount
            //pv modules ids = name , amonts = normal
            //batts - pv
            //balance id = name!parentlist

            //Binding Inverters to project
            if (ModelState.IsValid)
            {
                try
                {
                    bool success = dbhelper.EditProjectEquipment(Model);
                    if (success)
                    {
                        string projectName = dbhelper.GetProject(Model.Id).Details.ProjectName;
                        AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited : {1} - Equipment .", CurrentUsername(), projectName));
                        return RedirectToAction("EditEquipment", new { Id = Model.Id });
                    }
                }
                catch (Exception)
                {
                }
            }
            return View(Model);
        }





        #region Ajax
        public JsonResult GetInverterSpecs(string id)
        {

            try
            {
                var a = dbhelper.GetInverter(id);
                var currencyMultiplier = dbhelper.GetCurrencyC(a.CCY);

                List<string> configs = string.IsNullOrEmpty(a.ConfigurationList) ? new List<string>() : a.ConfigurationList.Split(',').ToList();
                List<string> dcovers = string.IsNullOrEmpty(a.ConfigurationList) ? new List<string>() : a.DCOverVoltageList.Split(',').ToList();
                List<string> dcDiscos = string.IsNullOrEmpty(a.ConfigurationList) ? new List<string>() : a.DCLoadDisconnectList.Split(',').ToList();
                List<string> ioport = string.IsNullOrEmpty(a.ConfigurationList) ? new List<string>() : a.IOPortModulesList.Split(',').ToList();
                List<string> country = string.IsNullOrEmpty(a.ConfigurationList) ? new List<string>() : a.CountrySpecificGridList.Split(',').ToList();
                List<string> moduleSytem = new List<string>() { a.ModulesPerPallet.ToString() };
                List<string> watt_Price = new List<string>() { Convert.ToInt32(a.UnitWP).ToString(), (a.ItemCost * currencyMultiplier.ValueInDollars).ToString("0.##") };
                List<string> ItemNum = new List<string>() { a.Key };


                return Json(new[] { configs, dcovers, dcDiscos, ioport, country, moduleSytem, watt_Price, ItemNum });
            }

            catch (Exception)
            {
                return null;
            }

        }

        public JsonResult GetBiInverterSpecs(string id)
        {
            try
            {
                var a = dbhelper.GetBiInverter(id);
                var currencyMultiplier = dbhelper.GetCurrencyC(a.CCY);

                List<string> coms = string.IsNullOrEmpty(a.CommunicationList) ? new List<string>() : a.CommunicationList.Split(',').ToList();
                List<string> apps = string.IsNullOrEmpty(a.ApplicationList) ? new List<string>() : a.ApplicationList.Split(',').ToList();
                List<string> interfac = string.IsNullOrEmpty(a.InterfaceList) ? new List<string>() : a.InterfaceList.Split(',').ToList();
                List<string> moduleSytem = new List<string>() { a.ModulesPerPallet.ToString() };
                List<string> watt_Price = new List<string>() { Convert.ToInt32(a.UnitWP).ToString(), Convert.ToInt32(a.ItemCost * currencyMultiplier.ValueInDollars).ToString("0.##") };
                List<string> itemNum = new List<string>() { a.Key };


                return Json(new[] { coms, apps, interfac, moduleSytem, watt_Price, itemNum });
            }

            catch (Exception)
            {
                return null;
            }

        }

        public JsonResult GetMountSpecs(string id)
        {

            try
            {
                var a = dbhelper.GetMounting(id);
                var currencyMultiplier = dbhelper.GetCurrencyC(a.CCY);
                a.Validate();

                List<string> watt_Price = new List<string>() { Convert.ToInt32(a.UnitWP).ToString(), Convert.ToInt32(a.ItemCost * currencyMultiplier.ValueInDollars).ToString("0.##") };


                return Json(watt_Price);
            }

            catch (Exception)
            {
                return null;
            }


        }

        public JsonResult GetPVSpecs(string id)
        {
            try
            {
                //equipment cost 1 euro = 1.12 dollars 1 * 1.12 = 1.12 dollars
                var a = dbhelper.GetPVModule(id);
                var currencyMultiplier = dbhelper.GetCurrencyC(a.CCY);

                List<string> moduleSytem = new List<string>() { a.ModulesPerPallet.ToString() };                     //Unit cost in eCCY to dollar
                List<string> watt_Price = new List<string>() { Convert.ToInt32(a.UnitWP).ToString(), Convert.ToInt32(a.ItemCost * currencyMultiplier.ValueInDollars).ToString("0.##") };
                List<string> ItemNo = new List<string>() { a.Key };

                return Json(new[] { moduleSytem, watt_Price, ItemNo });
            }

            catch (Exception)
            {
                return null;
            }

        }

        public JsonResult GetBatSpecs(string id)
        {
            try
            {
                var a = dbhelper.GetBattery(id);
                var currencyMultiplier = dbhelper.GetCurrencyC(a.CCY);

                List<string> moduleSytem = new List<string>() { a.ModulesPerPallet.ToString() };
                List<string> watt_Price = new List<string>() { Convert.ToInt32(a.UnitAh_C10).ToString(), Convert.ToInt32(a.ItemCost * currencyMultiplier.ValueInDollars).ToString("0.##") };
                List<string> key = new List<string>() { a.Key };

                return Json(new[] { key, moduleSytem, watt_Price });
            }

            catch (Exception)
            {
                return null;
            }

        }
        //
        public JsonResult GetBalanceSpecs(string id)
        {

            try
            {
                var a = dbhelper.GetBalanceSystem(id);
                var currencyMultiplier = dbhelper.GetCurrencyC(a.CCY);

                List<string> moduleSytem = new List<string>() { a.ModulesPerPallet.ToString() };
                List<string> watt_Price = new List<string>() { Convert.ToInt32(a.Units).ToString(), Convert.ToInt32(a.ItemCost * currencyMultiplier.ValueInDollars).ToString("0.##") };
                List<string> keys = new List<string>() { a.Key };

                return Json(new[] { moduleSytem, watt_Price, keys });
            }

            catch (Exception)
            {
                return null;
            }

        }



        #endregion


        public ActionResult AddInverter()
        {
            ViewBag.Packages = dbhelper.Packages();



            ViewBag.Inverters = dbhelper.GetAllInverters();
            InverterSub isub = new InverterSub();
            isub.ParentList = "InvertList";
            isub.Key = dbhelper.GetAllInverters().FirstOrDefault().Key;
            isub.Watt /= 1000;
            return PartialView("ISub", isub);
        }

        public ActionResult AddBiInverter()
        {
            ViewBag.Packages = dbhelper.Packages();

            ViewBag.BiInverters = dbhelper.GetAllBiInverters();
            BiInverterSub BIisub = new BiInverterSub();
            BIisub.Key = dbhelper.GetAllBiInverters().FirstOrDefault().Key;
            BIisub.ParentList = "BiInverterList";
            BIisub.Watt /= 1000;
            return PartialView("BiSub", BIisub);
        }

        public ActionResult AddBasic(string id)
        {
            ViewBag.Packages = dbhelper.Packages();
            switch (id)
            {
                case "MountingStructureList":
                    ViewBag.MountingStructure = dbhelper.GetAllMounting();
                    Basic bas = new Basic();
                    bas.Watt /= 1000;
                    bas.Key = dbhelper.GetAllMounting().FirstOrDefault().Key;
                    bas.ParentList = id;
                    return PartialView("MonSub", bas);


                case "BatteryList": ViewBag.Batteries = dbhelper.GetAllBatteries(); bas = new Basic(); bas.Key = dbhelper.GetAllBatteries().FirstOrDefault().Key; bas.Watt /= 1000; return PartialView("BatSub", bas);
                case "PVModuleList": ViewBag.PVModules = dbhelper.GetAllPVModules(); bas = new Basic(); bas.Key = dbhelper.GetAllPVModules().FirstOrDefault().Key; bas.Watt /= 1000; return PartialView("PVSub", bas);

            }

            return View();

        }

        public ActionResult AddBalanceSystem(string id)
        {
            ViewBag.Packages = dbhelper.Packages();
            ViewBag.CurrentProducts = dbhelper.GetAllBalanceSystems();
            Basic bos = new Basic();
            bos.ParentList = id.Replace("_", " ");

            var keyarrs = from a in dbhelper.GetAllBalanceSystems() where a.Type.Substring(0, 5) == id.Replace("_", " ").Substring(0, 5) select a.Key;
            List<string> kss = keyarrs.ToList();
            bos.Key = kss.FirstOrDefault();
            bos.Watt /= 1000;
            ViewBag.BalanceSystems = dbhelper.GetAllBalanceSystems();
            return PartialView("BalanceSub", bos);


        }


        #endregion


        #region Proposal Generation
        //proposal data to pdf generation
        /// The user needs to specify what they want to see in the proposal
        /// 1.The project information is retrieved from the projects ID
        /// 2.A super project , with all the calculations is then generated, providing the data
        /// 3.If necessary, the user may want to see various costing models such as PPA, SSA, or PBS
        /// 4.This will require additional models, which will need to be recosted using fees/reviews
        /// 5.The user will be able to dynamically create more parts to the proposal,specifying the name and an uploaded PDF
        /// 6.From there the requested PDF's will be generated, the extra information will be added in the respective places
        /// This will all be done using the Document class from ITextsharp, styled and formatted accordingly in a code-only approach
        /// The information can be retrieved by creating a class with a specifications page and module
        /// 



        [HttpGet]
        public ActionResult ProposalGeneration(int id)
        {
            var project = dbhelper.GetProject(id);
            var feeModel = dbhelper.CurrentFees();
            SuperProject superProject = new SuperProject(project); //The super project contains all the calculated information about 
            //Project and its proposal
            ProposalInputModel model = new ProposalInputModel();
            model.project = project;
            model.superProject = superProject;
            model.preparer = dbhelper.GetUser(User.Identity.GetUserId());

            model.ProjectID = project.Id;
            model.preparerID = User.Identity.GetUserId();

            if (User.IsInRole("NVIAdmin") || User.IsInRole("NVIUser"))
            {
                //NVI can do any proposal Type
                ViewBag.Types = new string[] { "Sales - Direct", "Sales + Financed - Direct", "Financed - Direct" };
            }

            else
            {
                ViewBag.Types = new string[] { "Financed - Indirect" };

            }

            ViewBag.GenerateProposalType = new string[] { "Complete Proposal", "Finance Only" };
            //Set up the input model with all defaults

            ///
            /// Main Sections
            /// TECHNICAL DATA , VISUAL OVERLAYS & DESIGN , DATASHEETS & YIELD ANALYSIS , SUPPLEMENTAL INFO
            ///
            ///
            #region Technical Data
            model.TechData = new PSection()
            {
                Name = "TECHNICAL DATA",
                IncludeMe = false,
                Letter = "B"


            };

            PSubSection subsect = new PSubSection()
            {
                Name = "BILL OF QUANTITIES (BOQ)",
                LetterCode = "B1",
                Standard = true,
                ParentName = "TechData"

            };

            model.TechData.SubSections.Add(subsect);

            subsect = new PSubSection()
            {
                Name = "SYSTEM REQUIREMENTS",
                LetterCode = "B2",
                Standard = true,
                ParentName = "TechData"
            };

            model.TechData.SubSections.Add(subsect);



            #endregion

            #region Visual Overlays & design
            model.VisualData = new PSection()
            {
                Name = "VISUAL OVERLAYS & DESIGN",
                IncludeMe = false,
                Letter = "C",

            };





            #endregion

            #region Data Sheets and Yield Analysis

            model.DataSheets = new PSection()
            {
                Name = "DATA SHEETS & YIELD ANALYSIS",
                IncludeMe = false,
                Letter = "D"

            };


            //subsect = new PSubSection()
            //{
            //    Name = "PVSYS YIELD MODEL(S)",
            //    IncludeMe = false,
            //    LetterCode = "D1",
            //    Standard = false,
            //    ParentName = "DataSheets"

            //};

            //model.DataSheets.SubSections.Add(subsect);
            subsect = new PSubSection()
            {
                Name = "MODULE DATA SHEETS",
                IncludeMe = false,
                LetterCode = "D2",
                Standard = false,
                ParentName = "DataSheets"
            };

            model.DataSheets.SubSections.Add(subsect);
            subsect = new PSubSection()
            {
                Name = "INVERTER DATA SHEETS",
                IncludeMe = false,
                LetterCode = "D3",
                Standard = false,
                ParentName = "DataSheets"
            };

            model.DataSheets.SubSections.Add(subsect);

            subsect = new PSubSection()
            {
                Name = "FRAMING DATA SHEETS",
                IncludeMe = false,
                LetterCode = "D4",
                Standard = false,
                ParentName = "DataSheets"
            };

            model.DataSheets.SubSections.Add(subsect);




            #endregion

            #region Supplemental Info

            model.SupplemData = new PSection()
            {
                Name = "SUPPLEMENTAL INFO",
                IncludeMe = false,
                Letter = "E"

            };
            subsect = new PSubSection()
            {
                Name = "Program Of Works",
                IncludeMe = false,
                LetterCode = "E1",
                Standard = true,
                ParentName = "SupplemData"

            };
            model.SupplemData.SubSections.Add(subsect); subsect = new PSubSection()
            {
                Name = "Cost of Energy Calculation",
                IncludeMe = false,
                LetterCode = "E2",
                Standard = true,
                ParentName = "SupplemData"

            };
            model.SupplemData.SubSections.Add(subsect);
            subsect = new PSubSection()
            {
                Name = "Levelised Cost of Energy Calculation",
                IncludeMe = false,
                LetterCode = "E3",
                Standard = true,
                ParentName = "SupplemData"

            };
            model.SupplemData.SubSections.Add(subsect);
            #endregion

            model.Calculate();
            return View(model);
        }


        [HttpPost]
        public ActionResult ProposalGeneration(ProposalInputModel model)
        {
            //I NEED to get in the model, do validation
            //Then create pdf's for all the required pages
            //Update the contents page with any added/removed pages
            //take in the pdfs that the user uploads, add them to the project
            //format the PDF nicely
            //send PDF back to the user
            //Keep up with the clients unrealistic demands



            if (ModelState.IsValid)
            {
                //sends the pdf to be downloaded

                MemoryStream stream = Proposal_Logic.Proposal(model);
                string projectName = dbhelper.GetProject(model.ProjectID).Details.ProjectName;
                string userID = User.Identity.GetUserId();
                AddAudit(userID, string.Format("{0} Generated Proposol For : {1}  .", CurrentUsername(), projectName));
                stream.Position = 0;
                string filename = string.Format("Solar For Africa Proposal");

                stream.Flush();


                Response.Clear();
                //Response.AddHeader("Content-Disposition", ("attachment; filename=" + "Solar 4 Africa.pdf"));
                return File(stream, "application/pdf", "Solar 4 Africa.pdf");

            }

            if (User.IsInRole("NVIAdmin") || User.IsInRole("NVIUser"))
            {
                //NVI can do any proposal Type
                ViewBag.Types = new string[] { "Sales - Direct", "Sales + Financed - Direct", "Financed - Direct" };
            }

            else
            {
                ViewBag.Types = new string[] { "Financed - Indirect" };

            }

            ViewBag.GenerateProposalType = new string[] { "Complete Proposal", "Finance Only" };

            var project = dbhelper.GetProject(model.ProjectID);
            var feeModel = dbhelper.CurrentFees();
            SuperProject superProject = new SuperProject(project); //The super project contains all the calculated information about 
            //Project and its proposal
            model.project = project;
            model.superProject = superProject;
            model.preparer = dbhelper.GetUser(User.Identity.GetUserId());

            model.ProjectID = project.Id;
            model.preparerID = User.Identity.GetUserId();
            return View(model);
        }


        public ActionResult AddSubsection(string id)
        {

            PSubSection subsection = new PSubSection();
            subsection.LetterCode = id;
            subsection.Standard = false;


            switch (id.ToUpper())
            {
                case "A": break;
                case "B": subsection.ParentName = "TechData"; break; //Technical Data
                case "C": subsection.ParentName = "VisualData"; break; //Visual Overlays & design
                case "D": subsection.ParentName = "DataSheets"; break; //Data Sheets and Yield Analysis
                case "E": subsection.ParentName = "SupplemData"; break; //Supplemental Info
            }
            return PartialView("~/Views/Project/_subsection.cshtml", subsection);
        }


        //this method will contain the logic to generate a proposal pdf

        public ActionResult AddExclusion()
        {
            Exclusion model = new Exclusion();
            return PartialView("_TC", model);
        }

        #endregion

        #region Audits

        public int AddAudit(string userID, string Content)
        {
            string company = dbhelper.GetUser(userID).Company;
            Audit audit = new Audit(userID, company, Content);


            return dbhelper.AddAudit(audit);


        }



        #endregion




        [Authorize]
        public string CurrentUsername()
        {

            string id = User.Identity.GetUserId();
            return dbhelper.GetUser(id).UserName;

        }

    }

}
