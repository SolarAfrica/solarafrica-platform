﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NVIDev.ActionFilters;
using NVIDev.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Data.Entity.SqlServer;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.IO;

namespace NVIDev.Controllers
{
    //[Authorize(Roles = "NVIAdmin, TPAdmin")]
    //[CustAuthFilter]
    public class DocumentController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        DBHelper dbHelper = new DBHelper();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Document
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [Authorize(Roles = "NVIAdmin, TPAdmin")]
        public ActionResult ApprovedDocument()
        {
            var Users = db.ApprovalDocument.ToList();
            IEnumerable<ApprovalDocument> oList = (IEnumerable<ApprovalDocument>)Users;

            List<Vw_User> UserList = db.Database.SqlQuery<Vw_User>("SELECT  [UserId],[Email],[PasswordHash],[PhoneNumber],[UserName],[Company],[Image],[Road],[City],[Country],[Salutation],[FirstName],[LastName],[UserType],[Designation],[CreatedOn],[RoleId],[RoleName]  FROM  [Vw_Users]  Where [RoleName]!= 'NVIAdmin' ").ToList();
            var raw = UserList.Select(x => new SelectListItem { Text = x.FirstName + " " + x.LastName, Value = x.UserId }).ToList();
            ViewBag.UserList = raw;

            var rawCompany = db.Company.Select(x => new SelectListItem { Value = x.CompanyID.ToString(), Text = x.CompanyName }).ToList();
            ViewBag.CompanyList = rawCompany;

            return View(oList);

        }

        /// <summary>
        /// Bind Method for Binding Approved Document
        /// </summary>
        /// <returns></returns>
        public ApprovalDocumentModelLayer ApprovedDocumentMethod()
        {
            ApprovalDocumentModelLayer oApprovalDocument = new ApprovalDocumentModelLayer();

            var Users = db.ApprovalDocument.ToList();
            //ViewBag.List = db.Users.Join(db.Roles, a => a.Id, b => b.Id, (a, b) => new SelectListItem { Text = a.FirstName, Value = a.Id }).ToList();

            //List<Vw_User> UserList = db.Database.SqlQuery<Vw_User>("SELECT  [UserId],[Email],[PasswordHash],[PhoneNumber],[UserName],[Company],[Image],[Road],[City],[Country],[Salutation],[FirstName],[LastName],[UserType],[Designation],[CreatedOn],[RoleId],[RoleName]  FROM  [Vw_Users]  Where [RoleName]!= 'NVIAdmin' ").ToList();
            var raw = dbHelper.GetUserList().Select(x => new SelectListItem { Text = x.FirstName + " " + x.LastName, Value = x.UserId }).ToList();
            ViewBag.UserList = raw;

            var rawCompany = db.Company.Select(x => new SelectListItem { Value = x.CompanyID.ToString(), Text = x.CompanyName }).ToList();
            ViewBag.CompanyList = rawCompany;


            //ViewBag.UserList = db.Roles.Select(x => x.Users.Select(y => new { y.UserId, y.RoleId })
            //    .Join(db.Users, a => a.UserId, b => b.Id, (a, b) => new SelectListItem { Text = b.FirstName + " " + b.LastName, Value = b.Id }).ToList());

            //ViewBag.UserList = db.Users.Join(db.Roles.Where(y => y.Name != "NVIAdmin").Select(x => x.Users), a => a.Id, b => b.Select(g => g.UserId), (a, b) =>
            //    new SelectListItem { Text = a.FirstName + " " + a.LastName, Value = a.Id }).OrderBy(x => x.Text).ToList();

            //ViewBag.UserList = db.Users.Where(x => x.RoleID != 1)
            //    .Select(x => new SelectListItem { Text = x.FirstName + " " + x.LastName, Value = x.UserID.ToString() }).OrderBy(x => x.Text).ToList();

            oApprovalDocument.AdminApprovalDocumentList = (IEnumerable<ApprovalDocument>)Users;
            return oApprovalDocument;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        [AllowAnonymous]
        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize(Roles = "NVIAdmin, TPAdmin")]
        public ActionResult ApprovedDocument(ApprovalDocumentModelLayer oApprovalDocument)
        {
            if (ModelState.IsValid)
            {
                ApprovalDocument nApprovalDocument = new ApprovalDocument();
                nApprovalDocument.ApprovalDocumentId = oApprovalDocument.AdminApprovalDocument.ApprovalDocumentId;
                nApprovalDocument.ApprovalDocumentMenu = "";// oApprovalDocument.AdminApprovalDocument.ApprovalDocumentMenu;
                nApprovalDocument.ApprovalDocumentSubMenu = oApprovalDocument.AdminApprovalDocument.ApprovalDocumentSubMenu;
                nApprovalDocument.IsActive = true;
                nApprovalDocument.CreatedBy = User.Identity.GetUserId();
                nApprovalDocument.CreatedOn = DateTime.Now;
                db.ApprovalDocument.Add(nApprovalDocument);
                db.SaveChanges();

                ModelState.AddModelError("AdminApprovalDocument.TriggerOnLoadMessage", "Record save successfully.");

                oApprovalDocument.AdminApprovalDocument.ApprovalDocumentMenu = string.Empty;
                oApprovalDocument.AdminApprovalDocument.ApprovalDocumentSubMenu = string.Empty;
                oApprovalDocument.AdminApprovalDocument.TriggerOnLoadMessage = "Record save successfully.";
                //ViewBag["SuccessMessage"] = "Record save successfully.";
            }

            oApprovalDocument = ApprovedDocumentMethod();
            return View(oApprovalDocument);
        }

        [AllowAnonymous]
        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize(Roles = "NVIAdmin, TPAdmin")]
        public ActionResult AddDocument()
        {
            ApprovalDocumentModel oApprovalDocument = new ApprovalDocumentModel();
            oApprovalDocument.ApprovalDocumentMenu = string.Empty;
            //bind dropdown from database for country list
            List<SelectListItem> list = new List<SelectListItem>();
           // list.Add(new SelectListItem { Text = "-Please select-", Value = "Selects items" });
            var cat = (from c in db.Countries select c).ToArray();
            for (int i = 0; i < cat.Length; i++)
            {
                list.Add(new SelectListItem
                {
                    Text = cat[i].Id,
                    Value = cat[i].Id.ToString()
                });
            }
            ViewData["Countries"] = list;
            //bind organisation dropdown from database
            List<SelectListItem> listOrg = new List<SelectListItem>();
          //  listOrg.Add(new SelectListItem { Text = "-Please select-", Value = "Selects items" });
            var org = (from c in db.Company select c).ToArray();
            for (int i = 0; i < org.Length; i++)
            {
                listOrg.Add(new SelectListItem
                {
                    Text = org[i].CompanyName,
                    Value = org[i].CompanyID.ToString()
                });
            }
            ViewData["Organisation"] = listOrg;
            return PartialView(oApprovalDocument);
        }

        [AllowAnonymous]
        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize(Roles = "NVIAdmin, TPAdmin")]
        public JsonResult AddDocument(ApprovalDocumentModel oApprovalDocument)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    ApprovalDocument nApprovalDocument = new ApprovalDocument();
                    nApprovalDocument.ApprovalDocumentId = oApprovalDocument.ApprovalDocumentId;
                    nApprovalDocument.ApprovalDocumentMenu =  oApprovalDocument.ApprovalDocumentMenu;
                    nApprovalDocument.ApprovalDocumentSubMenu = oApprovalDocument.ApprovalDocumentSubMenu;
                    nApprovalDocument.ApprovalDocumentDesc = oApprovalDocument.ApprovalDocumentDesc;
                    if(oApprovalDocument.ApprovalDocumentMenu.ToString() == "Country Specific")
                    nApprovalDocument.CounOrgSpecificName = oApprovalDocument.CounSpecificName;
                    else
                        nApprovalDocument.CounOrgSpecificName = oApprovalDocument.OrgSpecificName;
                    nApprovalDocument.IsActive = true;
                    nApprovalDocument.CreatedBy = User.Identity.GetUserId();
                    nApprovalDocument.CreatedOn = DateTime.Now;
                    db.ApprovalDocument.Add(nApprovalDocument);
                    db.SaveChanges();
                    return Json("Success");
                }
                catch (Exception ex)
                {
                    return Json(ex.Message);
                }
            }
            return Json("Fail");
        }


        /// <summary>
        /// Show All TP User Information
        /// </summary>
        /// <param name="column"></param>
        /// <param name="sidx"></param>
        /// <param name="sord"></param>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public JsonResult TPUserList(string column, string sidx = "UserID", string sord = "asc", int page = 1, int rows = 50) // Gets the list of Labours
        {
            int pageIndex = Convert.ToInt16(page) - 1;
            int pageSize = rows;
            var LabourList1 = (dynamic)null;
            var totalRecords = (dynamic)null;
            var totalPages = (dynamic)null;
            var colNames = (dynamic)null;

            if (string.IsNullOrEmpty(column))
            {
                //For Linq Query 

                string Salutation = Request.Params.Get("Salutation");
                string FirstName = Request.Params.Get("FirstName");
                string LastName = Request.Params.Get("LastName");
                string UserType = Request.Params.Get("UserType");
                string Designation = Request.Params.Get("Designation");
                string Email = Request.Params.Get("Email");
                string MobileNo = Request.Params.Get("MobileNo");
                string CompanyName = Request.Params.Get("CompanyName");
                string Country = Request.Params.Get("Country");
                string City = Request.Params.Get("City");
                string Role = Request.Params.Get("Role");
                string Status = Request.Params.Get("Status");
                string InitiatedOn = Request.Params.Get("InitiatedOn");

                var raw = db.Users.Select(b => new
                {
                    id = b.Id,
                    UserID = b.Id,
                    b.UserType,
                    b.Salutation,
                    b.FirstName,
                    b.LastName,
                    b.Designation,
                    b.Email,
                    MobileNo = b.PhoneNumber,
                    CompanyName = b.Company,
                    Country = b.Address_Country,
                    City = b.Address_City,
                    Role = b.Roles.FirstOrDefault(),
                    Status = b.IsFinancedApproved,
                    InitiatedOn = b.CreatedOn.ToString(),
                    View = "<a href='#' onclick='javascript:ViewDetail(\"" + b.Company + "\",\"" + b.Id + "\",this);' style='color:blue;'>View Details</a>"
                }).Select(x => new
                {
                    x.id,
                    x.UserID,
                    x.UserType,
                    x.Salutation,
                    x.FirstName,
                    x.LastName,
                    x.Designation,
                    x.Email,
                    x.MobileNo,
                    x.CompanyName,
                    x.Country,
                    x.City,
                    Role = x.Role.RoleId,
                    x.InitiatedOn,
                    x.Status,
                    x.View
                });

                //For Searching  in JQgrid 

                if (!string.IsNullOrEmpty(Salutation))
                    raw = raw.Where(x => x.Salutation.StartsWith(Salutation));
                if (!string.IsNullOrEmpty(UserType))
                    raw = raw.Where(x => x.UserType.StartsWith(UserType));
                if (!string.IsNullOrEmpty(FirstName))
                    raw = raw.Where(x => x.FirstName.StartsWith(FirstName));
                if (!string.IsNullOrEmpty(LastName))
                    raw = raw.Where(x => x.LastName.StartsWith(LastName));
                if (!string.IsNullOrEmpty(Designation))
                    raw = raw.Where(x => x.Designation.StartsWith(Designation));
                if (!string.IsNullOrEmpty(Email))
                    raw = raw.Where(x => x.Email.StartsWith(Email));
                if (!string.IsNullOrEmpty(MobileNo))
                    raw = raw.Where(x => x.MobileNo.StartsWith(MobileNo));
                if (!string.IsNullOrEmpty(CompanyName))
                    raw = raw.Where(x => x.CompanyName.StartsWith(CompanyName));
                if (!string.IsNullOrEmpty(Country))
                    raw = raw.Where(x => x.Country.StartsWith(Country));

                if (!string.IsNullOrEmpty(City))
                    raw = raw.Where(x => x.City.StartsWith(City));
                if (!string.IsNullOrEmpty(Role))
                    raw = raw.Where(x => x.Role.StartsWith(Role));
                if (!string.IsNullOrEmpty(Status))
                    raw = raw.Where(x => x.Status.StartsWith(Status));
                if (!string.IsNullOrEmpty(InitiatedOn))
                    raw = raw.Where(x => x.InitiatedOn.ToString().StartsWith(InitiatedOn));

                totalRecords = raw.Count();
                totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);

                if (sord.ToUpper().Contains("DESC"))
                {
                    if (sidx.Equals("Salutation"))
                        raw = raw.OrderByDescending(b => b.Salutation);
                    else if (sidx.Equals("FirstName"))
                        raw = raw.OrderByDescending(b => b.FirstName);
                    else if (sidx.Equals("LastName"))
                        raw = raw.OrderByDescending(b => b.LastName);
                    else if (sidx.Equals("UserType"))
                        raw = raw.OrderByDescending(b => b.UserType);
                    else if (sidx.Equals("Designation"))
                        raw = raw.OrderByDescending(b => b.Designation);
                    else if (sidx.Equals("Email"))
                        raw = raw.OrderByDescending(b => b.Email);
                    else if (sidx.Equals("MobileNo"))
                        raw = raw.OrderByDescending(b => b.MobileNo);
                    else if (sidx.Equals("CompanyName"))
                        raw = raw.OrderByDescending(b => b.CompanyName);
                    else if (sidx.Equals("Country"))
                        raw = raw.OrderByDescending(b => b.Country);
                    else if (sidx.Equals("City"))
                        raw = raw.OrderByDescending(b => b.City);
                    else if (sidx.Equals("Role"))
                        raw = raw.OrderByDescending(b => b.Role);
                    else if (sidx.Equals("Status"))
                        raw = raw.OrderByDescending(b => b.Status);
                    else if (sidx.Equals("InitiatedOn"))
                        raw = raw.OrderByDescending(b => b.InitiatedOn);
                    else
                        raw = raw.OrderByDescending(b => b.UserID);
                }
                else
                {
                    if (sidx.Equals("Salutation"))
                        raw = raw.OrderBy(b => b.Salutation);
                    else if (sidx.Equals("FirstName"))
                        raw = raw.OrderBy(b => b.FirstName);
                    else if (sidx.Equals("LastName"))
                        raw = raw.OrderBy(b => b.LastName);
                    else if (sidx.Equals("UserType"))
                        raw = raw.OrderBy(b => b.UserType);
                    else if (sidx.Equals("Designation"))
                        raw = raw.OrderBy(b => b.Designation);
                    else if (sidx.Equals("Email"))
                        raw = raw.OrderBy(b => b.Email);
                    else if (sidx.Equals("MobileNo"))
                        raw = raw.OrderBy(b => b.MobileNo);
                    else if (sidx.Equals("CompanyName"))
                        raw = raw.OrderBy(b => b.CompanyName);
                    else if (sidx.Equals("Country"))
                        raw = raw.OrderBy(b => b.Country);
                    else if (sidx.Equals("City"))
                        raw = raw.OrderBy(b => b.City);
                    else if (sidx.Equals("Role"))
                        raw = raw.OrderBy(b => b.Role);
                    else if (sidx.Equals("Status"))
                        raw = raw.OrderBy(b => b.Status);
                    else if (sidx.Equals("InitiatedOn"))
                        raw = raw.OrderBy(b => b.InitiatedOn);
                    else
                        raw = raw.OrderBy(b => b.UserID);
                }

                raw = raw.Skip(pageIndex * pageSize).Take(pageSize);
                LabourList1 = raw.ToList();

            }
            else
            {
                colNames = typeof(RegistrationModel).GetProperties().Select(b => b.Name).ToList();
            }
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = LabourList1,
                column = colNames
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Show All TP User Information
        /// </summary>
        /// <param name="column"></param>
        /// <param name="sidx"></param>
        /// <param name="sord"></param>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public JsonResult TPUserInfoList(string column, string sidx = "UserID", string sord = "asc", int page = 1, int rows = 50) // Gets the list of Labours
        {
            int pageIndex = Convert.ToInt16(page) - 1;
            int pageSize = rows;
            var LabourList1 = (dynamic)null;
            var totalRecords = (dynamic)null;
            var totalPages = (dynamic)null;
            var colNames = (dynamic)null;

            if (string.IsNullOrEmpty(column))
            {
                //For Linq Query 
                var raw = db.Users.Select(b => new
                {
                    id = b.Id,
                    UserID = b.Id,
                    b.UserType,
                    b.Salutation,
                    b.FirstName,
                    b.LastName,
                    b.Designation,
                    b.Email,
                    MobileNo = b.PhoneNumber,
                    CompanyName = b.Company,
                    Country = b.Address_Country,
                    City = b.Address_City,
                    Role = b.Roles.FirstOrDefault(),
                    Status = b.IsFinancedApproved,
                    InitiatedOn = b.CreatedOn,
                    View = "<a href='#' onclick='javascript:ViewDetail(\"" + b.Id + "\",this);' style='color:blue;'>View Details</a>"
                }).Select(x => new
                {
                    x.id,
                    x.UserID,
                    x.UserType,
                    x.Salutation,
                    x.FirstName,
                    x.LastName,
                    x.Designation,
                    x.Email,
                    x.MobileNo,
                    x.CompanyName,
                    x.Country,
                    x.City,
                    Role = x.Role.RoleId,
                    x.InitiatedOn,
                    x.Status
                });
                totalRecords = 10;// raw.Count();
                totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);

                raw = raw.Skip(pageIndex * pageSize).Take(pageSize);
                LabourList1 = raw;

            }
            else
            {
                colNames = typeof(RegistrationModel).GetProperties().Where(x => x.Name != "View").Select(b => b.Name).ToList();
            }
            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = LabourList1,
                column = colNames
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ApprovalDocument(string CompanyName)
        {
            GetApprovalDocumentDetails(CompanyName);
            //var Users = db.ApprovalDocument.OrderBy(x => new { x.ApprovalDocumentMenu, x.ApprovalDocumentSubMenu }).ToList();
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ApprovalDocument(AttachDocModel model)
        {
            try
            {
                GetApprovalDocumentDetails(TempData["CompanyName"].ToString());

                List<AttachDocModel> datalist = new List<AttachDocModel>();
                datalist = ViewBag.DocDetails;

                var file = (from x in datalist where x.ApprovalDocumentId == model.ApprovalDocumentId select x.AttachedFile).ToList();
                var filename = (from f in datalist where f.ApprovalDocumentId == model.ApprovalDocumentId select f.Attachedfilename).ToList();
                Response.Clear();
                Response.ContentType = "application/force-download";
                Response.AddHeader("content-disposition", "attachment; filename=" + filename[0] + "");
                Response.BinaryWrite(file[0]);
                Response.End();
            }
            catch (Exception)
            { }
            return View();
        }

        /// <summary>
        /// This function is for approval or rejection of documents uploaded by TPUsers or TPAdmin
        /// </summary>
        /// <param name="companyid"></param>
        /// <param name="docid"></param>
        /// <param name="isapproved"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public JsonResult ApproveRejectDocument(string companyid, string docid, string isapproved)
        {
            var query = from doc in db.UserApprovalDocument
                        where doc.CompanyID == companyid && doc.DocumentID == docid
                        select doc;

            foreach (UserApprovalDocumentModel doc in query)
            {
                doc.IsApproved = isapproved;
            }
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To save comments of admin for document
        /// </summary>
        /// <param name="companyid"></param>
        /// <param name="docid"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public JsonResult AddCommentsForDocument(string companyid, string docid, string comment, string commentType)
        {
            comment = comment.Replace(';', ',');
            string username = Session["UserName"].ToString();

            var loginname = db.Users.Where(x => username.Contains(x.UserName.ToString())).Select(s => s.FirstName + " " + s.LastName).FirstOrDefault();
            //loginname = logindetails;
            var query = from doc in db.UserApprovalDocument
                        where doc.CompanyID == companyid && doc.DocumentID == docid
                        select doc;
            if (commentType == "Admin")
            {
                foreach (UserApprovalDocumentModel doc in query)
                {
                    doc.AdminComment += loginname + " [" + DateTime.Now.ToString("dd/MM/yyyy HH:MM") + "] " + comment + ";";
                }
            }

            if (commentType == "TPuser")
            {
                foreach (UserApprovalDocumentModel doc in query)
                {
                    doc.TPUserComment += loginname + " [" + DateTime.Now.ToString("dd/MM/yyyy HH:MM") + "] " + comment + ";";
                }
            }
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This function is for adding or removing document on basis of check uncheck of input
        /// </summary>
        /// <returns>Json data</returns>
        [AllowAnonymous]
        [CustAuthFilter]
        public ActionResult SaveUserDocument()
        {
            try
            {
                var Obj = Request.Params.Get("oUserDocumentModel");
                var oUserDocumentModel = JsonConvert.DeserializeObject<List<UserDocumentModel>>(Obj);

                string compId = oUserDocumentModel[0].CompanyID;
                var docids = db.UserApprovalDocument.Where(x => x.CompanyID == compId).Select(s => s.DocumentID).ToList();

                ////Adding new document required from company 
                foreach (var raw in oUserDocumentModel)
                {
                    if ((from x in docids where x == raw.DocumentID select x).ToList().Count == 0)
                    {
                        UserApprovalDocumentModel oUserApprovalDocumentModel = new UserApprovalDocumentModel();
                        oUserApprovalDocumentModel.UserID = raw.UserID;
                        oUserApprovalDocumentModel.CompanyID = raw.CompanyID;
                        oUserApprovalDocumentModel.DocumentID = raw.DocumentID;
                        oUserApprovalDocumentModel.CreatedOn = DateTime.Now;
                        db.UserApprovalDocument.Add(oUserApprovalDocumentModel);
                        db.SaveChanges();
                    }
                }

                ////Removing document required earlier, but now these document are unchecked by admin
                foreach (string i in docids)
                {
                    var doc = (from y in oUserDocumentModel where y.DocumentID == i select y.DocumentID).ToList();
                    if (doc.Count == 0)
                    {
                        var deleteDocsDetails = from details in db.UserApprovalDocument where details.CompanyID == compId && details.DocumentID == i select details;
                        foreach (var detail in deleteDocsDetails)
                        {
                            db.UserApprovalDocument.Remove(detail);
                        }
                        db.SaveChanges();
                    }
                }

                var docnames = db.ApprovalDocument.Where(x => docids.Contains(x.ApprovalDocumentId.ToString())).ToList();

                var userdetails = db.Database.SqlQuery<Vw_User>("SELECT  [UserId],[Email],[PasswordHash],[PhoneNumber],[UserName],[Company],[Image],[Road],[City],[Country],[Salutation],[FirstName],[LastName],[UserType],[Designation],[CreatedOn],[RoleId],[RoleName]  FROM  [Vw_Users]  Where Userid =  '" + oUserDocumentModel[0].UserID + "' ").FirstOrDefault();
                EmailModel sendemail = new EmailModel();

                sendemail.SendEMail("Documents Required", userdetails.FirstName + " " + userdetails.LastName, userdetails.Email, docnames);
                return Json("Success");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        /// <summary>
        /// This is for fetching user by company name
        /// </summary>
        /// <param name="CompName"></param>
        /// <returns>Json data</returns>
        [AllowAnonymous]
        public JsonResult GetUserByCompanyname(string CompName)
        {
            List<Vw_User> UserList = db.Database.SqlQuery<Vw_User>("SELECT  [UserId],[Email],[PasswordHash],[PhoneNumber],[UserName],[Company],[Image],[Road],[City],[Country],[Salutation],[FirstName],[LastName],[UserType],[Designation],[CreatedOn],[RoleId],[RoleName]  FROM  [Vw_Users]  Where [RoleName]!= 'NVIAdmin' and Company='" + CompName + "' ").ToList();
            var raw = UserList.Select(x => new SelectListItem { Text = x.FirstName + " " + x.LastName, Value = x.UserId }).ToList();

            return Json(new { raw }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This fucntion is for check uncheck of input by companyid on the basis of document required or not
        /// </summary>
        /// <param name="CompId"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public JsonResult GetRequiredDocuments(string CompId)
        {
            var raw = db.UserApprovalDocument.Where(x => x.CompanyID == CompId).Select(x => new
            {
                x.DocumentID

            }).ToList();
            return Json(new { raw }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// For return to attachdocument page
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [CustAuthFilter]
        public ActionResult AttachDocument()
        {
            GetApprovalDocumentDetails();
            return View();
        }

        /// <summary>
        /// For saving document in DB in bytes
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult AttachDocument(AttachDocModel model)
        {
            if (Session["CompanyName"] != null)
            {
                string companyname = Session["CompanyName"].ToString();
                var compny = db.Company.Where(c => c.CompanyName == companyname).Select(c => c.CompanyID).FirstOrDefault();
                string compnyid = compny.ToString();
                if (Session["command"] != null)
                {
                    if (Session["command"] == "Remove")
                    {
                        var query = from doc in db.UserApprovalDocument
                                    where doc.CompanyID == compnyid && doc.DocumentID == model.ApprovalDocumentId
                                    select doc;

                        foreach (UserApprovalDocumentModel doc in query)
                        {
                            doc.AttachmentName = null;
                            doc.AttachedFile = null;
                            doc.IsApproved = null;
                        }
                        db.SaveChanges();
                        Session.Remove("command");
                    }
                }
                GetApprovalDocumentDetails();
            }
            return View();
        }


        // delete the uploaded document from database
        [HttpPost]
        public ActionResult SetCommandToDelete()
        {
            Session["command"] = "Remove";
            return RedirectToAction("AttachDocument");
        }

        [AllowAnonymous]
        [CustAuthFilter]
        public ActionResult PopupUploadDoc()
        {
            TempData["ApprovalDocumentId"] = Request.QueryString["docid"];
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult PopupUploadDoc(AttachDocModel model)
        {
            string companyname = Session["CompanyName"].ToString();
            var compny = db.Company.Where(c => c.CompanyName == companyname).Select(c => c.CompanyID).FirstOrDefault();
            string compnyid = compny.ToString();

            string ApprovalDocumentId = TempData["ApprovalDocumentId"].ToString();

            var validFileTypes = new string[]
                    {
                        "image/gif",
                        "image/jpeg",
                        "image/pjpeg",
                        "image/png",
                        "image/bmp"
                    };
            var img = new byte[] { };
            //------------------------------------------------------------------------------------------------------//
            //if (model.ImageUpload != null)
            //{
            //    if (!validImageTypes.Contains(model.ImageUpload.ContentType))
            //    {
            //        ModelState.AddModelError("ImageUpload", "Please choose either a GIF, JPG or PNG image.");
            //    }
            //}
            //------------------------------------------------------------------------------------------------------//
            if (model.AttachmentName != null && model.AttachmentName.ContentLength > 0)
            {
                using (BinaryReader reader = new BinaryReader(model.AttachmentName.InputStream))
                {
                    img = reader.ReadBytes(model.AttachmentName.ContentLength);
                }

                var query = from doc in db.UserApprovalDocument
                            where doc.CompanyID == compnyid && doc.DocumentID == ApprovalDocumentId
                            select doc;

                foreach (UserApprovalDocumentModel doc in query)
                {
                    doc.AttachmentName = model.AttachmentName.FileName;
                    doc.AttachedBy = Session["UserName"].ToString();
                    doc.IsApproved = "0";
                    doc.AttachedFile = img;
                }

                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            return Content("<script>parent.$.fancybox.close();</script>");
        }

        /// <summary>
        /// For getting approval status, attachment details, document details.
        /// </summary>
        /// <param name="CompanyName"></param>
        public void GetApprovalDocumentDetails(string CompanyName = null)
        {
            string companyname = CompanyName == null ? Session["CompanyName"].ToString() : CompanyName;
            var compny = db.Company.Where(c => c.CompanyName == companyname).Select(c => c.CompanyID).FirstOrDefault();
            string compnyid = compny.ToString();

            //------------------------------------------------------------------------------------------------------//
            //var docid = db.UserApprovalDocument.Where(b => b.CompanyID == compnyid).Select(b => b.DocumentID).ToList();
            //var ReqDocs = db.ApprovalDocument.Where(x => docid.Contains(x.ApprovalDocumentId.ToString())).ToList();
            //IEnumerable<ApprovalDocument> oList = (IEnumerable<ApprovalDocument>)ReqDocs;
            //ViewBag.ReqDocs = oList;


            //var docdetails = db.UserApprovalDocument.Where(b => b.CompanyID == compnyid).ToList();
            //IEnumerable<UserApprovalDocumentModel> oDocDetailList = (IEnumerable<UserApprovalDocumentModel>)docdetails;
            //------------------------------------------------------------------------------------------------------//

            List<AttachDocModel> datalist = new List<AttachDocModel>();
            var docDatalist = (from x in db.UserApprovalDocument
                               join y in db.ApprovalDocument on x.DocumentID equals y.ApprovalDocumentId.ToString() into t
                               from rt in t.DefaultIfEmpty()
                               where x.CompanyID == compnyid
                               select new
                               {
                                   x.DocumentID,
                                   rt.ApprovalDocumentMenu,
                                   rt.ApprovalDocumentSubMenu,
                                   rt.ApprovalDocumentDesc,
                                   x.CompanyID,
                                   x.AttachmentName,
                                   x.IsApproved,
                                   x.AttachedFile
                               }).ToList();
            for (int i = 0; i < docDatalist.Count; i++)
            {
                AttachDocModel oDocsData = new AttachDocModel();
                oDocsData.ApprovalDocumentId = docDatalist[i].DocumentID;
                oDocsData.ApprovalDocumentMenu = Convert.ToString(docDatalist[i].ApprovalDocumentMenu);
                oDocsData.ApprovalDocumentSubMenu = Convert.ToString(docDatalist[i].ApprovalDocumentSubMenu);
                oDocsData.CompanyID = Convert.ToString(docDatalist[i].CompanyID);
                oDocsData.IsApproved = Convert.ToString(docDatalist[i].IsApproved);
                oDocsData.ApprovalDocumentDesc = Convert.ToString(docDatalist[i].ApprovalDocumentDesc);
                oDocsData.AttachedFile = docDatalist[i].AttachedFile;
                datalist.Add(oDocsData);
            }
            TempData["CompanyName"] = CompanyName;
            ViewBag.DocDetails = datalist;
        }

        [AllowAnonymous]
        [CustAuthFilter]
        public ActionResult ShowAllComments(string docid, string companyid)
        {
            List<AttachDocModel> datalist = new List<AttachDocModel>();
            var docDatalist = (from x in db.UserApprovalDocument
                               where x.CompanyID == companyid && x.DocumentID == docid
                               select new
                               {
                                   x.AdminComment,
                                   x.TPUserComment
                               }).ToList();

            for (int i = 0; i < docDatalist.Count; i++)
            {
                AttachDocModel oDocsData = new AttachDocModel();
                oDocsData.AdminComment = docDatalist[i].AdminComment;
                oDocsData.TPUserComment = docDatalist[i].TPUserComment;
                datalist.Add(oDocsData);
            }
            ViewBag.DocDetails = datalist;
            return View();
        }


    }
}