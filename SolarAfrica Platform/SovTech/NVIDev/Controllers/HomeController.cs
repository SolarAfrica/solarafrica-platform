﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NVIDev.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Data.Entity.SqlServer;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.IO;

namespace NVIDev.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            RemoveAllTransportAndInstall();
            if (User.Identity.IsAuthenticated)
            {
                var userdetails = db.Users.Where(m => m.UserName == User.Identity.Name).FirstOrDefault();
                byte[] Image = userdetails.Image;
                ViewBag.Image = Image;
                ViewBag.Salutation = userdetails.Salutation;
                ViewBag.Name = userdetails.FirstName + " " + userdetails.LastName;
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public void RemoveAllTransportAndInstall()
        {

            List<Project> allpros = db.Projects.ToList();

            foreach(var pro in allpros)
            {


                try
                {
                    pro.RemoveCost("Transport & Logistics");
                    pro.RemoveCost("Installation & Labour Costs");
                    pro.RemoveCost("Design Costs");

                    db.Entry(pro).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }

                catch
                {

                }

            }



        }


    }
}