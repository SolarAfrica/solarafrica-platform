﻿using Microsoft.AspNet.Identity.EntityFramework;
using NVIDev.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Data.OleDb;
using System.IO;
using System.Data;
using Excel;
using NVIDev.ActionFilters;

namespace NVIDev.Controllers
{
    [Authorize(Roles = "NVIAdmin")]
    [CustAuthFilter]
    public class AdminController : Controller
    {

        #region User Stuff//
        // GET: /Admin/
        private ApplicationDbContext VarDB = new ApplicationDbContext();

        private ApplicationUserManager _userManager;
        DBHelper dbhelper = new DBHelper();

        public ActionResult Index()
        {
            var Users = dbhelper.GetAllUsers().OrderBy(x => x.UserName);
            return View(Users);
        }


        [HttpGet]
        public ActionResult Edit(string id)
        {
            var UserToChange = dbhelper.GetUser(id);

            RoleViewModel model = new RoleViewModel(RolesAsNormalText(UserToChange.Roles.Select(m => m.RoleId).ToArray()).ToList(), dbhelper.UserRoles());
            model.UserID = UserToChange.Id;
            model.UserName = UserToChange.UserName;
            return View(model);
        }

        public string[] RolesAsNormalText(string[] roles)
        {
            string[] asNorms = new string[roles.Count()];
            string _roller = "";
            for (int a = 0; a < roles.Count(); a++)
            {
                switch (roles[a])
                {
                    case "NVIAdmin": _roller = "NVI Admin"; break;
                    case "NVIUser": _roller = "NVI User"; break;
                    case "TPAdmin": _roller = "TP Admin"; break;
                    case "TPUser": _roller = "TP User"; break;

                }
                asNorms.OrderBy(m => m);
                asNorms[a] = _roller;
            }

            return asNorms;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        [HttpPost]
        public async Task<ActionResult> Edit(RoleViewModel model)
        {
            string roleid = "";


            //current role IDS NVIAdmin , NVIUser, TPAdmin, TPUser

            try
            {


                var oldRolls = await UserManager.GetRolesAsync(model.UserID);
                foreach (var role in oldRolls)
                {
                    await UserManager.RemoveFromRoleAsync(model.UserID, role);

                }

                foreach (var role in model.Roles)
                {

                    switch (role.Role)
                    {
                        case "NVI Admin": roleid = "NVIAdmin"; break;
                        case "NVI User": roleid = "NVIUser"; break;
                        case "TP Admin": roleid = "TPAdmin"; break;
                        case "TP User": roleid = "TPUser"; break;
                    }
                    if (role.includeMe)
                    {
                        await UserManager.AddToRoleAsync(model.UserID, roleid);
                    }

                }

                return RedirectToAction("Index");
            }

            catch (Exception ex)
            {
                ViewBag.Message = "Could not change Role :" + ex.ToString();
                return View(model);
            }

        }
        public async Task<ActionResult> Delete(string id)
        {
            ApplicationUser User = dbhelper.GetUser(id);
            var oldRolls = await UserManager.GetRolesAsync(id);
            foreach (var role in oldRolls)
            {
                await UserManager.RemoveFromRoleAsync(id, role);
            }


            await UserManager.DeleteAsync(User);

            return View("Index");
        }


        public async Task<ActionResult> Suspend(string id)
        {
            ApplicationUser User = VarDB.Users.Find(id);
            var oldRolls = await UserManager.GetRolesAsync(id);
            foreach (var role in oldRolls)
            {
                await UserManager.RemoveFromRoleAsync(id, role);
            }


            await UserManager.AddToRoleAsync(User.Id, "Basic");

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Allow(string id)
        {
            ApplicationUser User = VarDB.Users.Find(id);
            var oldRolls = await UserManager.GetRolesAsync(id);
            foreach (var role in oldRolls)
            {
                await UserManager.RemoveFromRoleAsync(id, role);
            }


            await UserManager.AddToRoleAsync(User.Id, "TPUser");


            return RedirectToAction("Index");
        }


        public ActionResult Audits()
        {
            int numOfAudits = 20;
            ViewBag.Audits = VarDB.Audits.ToList().Take(numOfAudits).OrderBy(m => m.Id).ToList();

            string comp = VarDB.Users.Find(User.Identity.GetUserId()).Company;
            ViewBag.AdminAudits = CompanyAudits(comp).Take(numOfAudits).ToList();
            return View();
        }

        #endregion

        #region Variable Stuff (NVI Static)


        public ActionResult Variables()
        {

            ViewBag.Countries = VarDB.Countries.ToList();
            ViewBag.ProTypes = VarDB.Prospec.ToList();

            try
            {
                if (VarDB.NVIStatic.FirstOrDefault().RevisionDate != null)
                {
                    ViewBag.Rev = VarDB.NVIStatic.FirstOrDefault().RevisionDate;

                }


                else
                {
                    ViewBag.Rev = null;
                }

            }

            catch (Exception) { }

            return View();
        }
        public ActionResult CreateNVIStatic()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNVIStatic([Bind(Include = "Id,RevisionDate")] NVIStatic nvistatic)
        {
            nvistatic.RevisionDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                VarDB.NVIStatic.Add(nvistatic);
                VarDB.SaveChanges();

                AddAudit(User.Identity.GetUserId(), string.Format("{0} Created The NVI Static Data.", CurrentUsername()));

                return RedirectToAction("Variables");
            }

            return View(nvistatic);
        }

        // GET: /Variable/Edit/5
        public ActionResult EditNVIStatic(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NVIStatic nvistatic = VarDB.NVIStatic.Find(id);
            if (nvistatic == null)
            {
                return HttpNotFound();
            }
            return View(nvistatic);
        }

        // POST: /Variable/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditNVIStatic(NVIStatic nvistatic)
        {

            nvistatic.RevisionDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                VarDB.Entry(nvistatic).State = EntityState.Modified;
                VarDB.SaveChanges();
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited The NVI Static Data.", CurrentUsername()));

                return RedirectToAction("Index");
            }
            return View(nvistatic);
        }

        // GET: /Variable/Delete/5
        public ActionResult DeleteNVIStatic(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NVIStatic nvistatic = VarDB.NVIStatic.Find(id);
            if (nvistatic == null)
            {
                return HttpNotFound();
            }
            return View(nvistatic);
        }

        // POST: /Variable/Delete/5
        [HttpPost, ActionName("DeleteNVIStatic")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmedNVIStatic(int id)
        {
            NVIStatic nvistatic = VarDB.NVIStatic.Find(id);
            VarDB.NVIStatic.Remove(nvistatic);
            VarDB.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                VarDB.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion

        #region Variable Stuff (Project Specific)
        public ActionResult CreateProSpecs()
        {
            return View();
        }

        // POST: /Variable/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProSpecs([Bind(Include = "Id,FPT,BOQ_Asset_101,BOQ_Asset_390,BOQ_Asset_500")] ProSpec prospec)
        {
            if (ModelState.IsValid)
            {
                VarDB.Prospec.Add(prospec);
                VarDB.SaveChanges();
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Created Project Type{1}.", CurrentUsername(), prospec.Id));

                return RedirectToAction("Index");
            }

            return View(prospec);
        }

        // GET: /Variable/Edit/5
        public ActionResult EditProspecs(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProSpec prospec = VarDB.Prospec.Find(id);
            if (prospec == null)
            {
                return HttpNotFound();
            }
            return View(prospec);
        }

        // POST: /Variable/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProspecs([Bind(Include = "Id,FPT,BOQ_Asset_101,BOQ_Asset_390,BOQ_Asset_500")] ProSpec prospec)
        {
            if (ModelState.IsValid)
            {
                VarDB.Entry(prospec).State = EntityState.Modified;
                VarDB.SaveChanges();
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited Project Type {1}", CurrentUsername(), prospec.Id));

                return RedirectToAction("Index");
            }
            return View(prospec);
        }

        // GET: /Variable/Delete/5
        public ActionResult DeleteProSpecs(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProSpec prospec = VarDB.Prospec.Find(id);
            if (prospec == null)
            {
                return HttpNotFound();
            }
            return View(prospec);
        }

        // POST: /Variable/Delete/5
        [HttpPost, ActionName("DeleteProSpecs")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmedProspecs(string id)
        {
            ProSpec prospec = VarDB.Prospec.Find(id);
            VarDB.Prospec.Remove(prospec);
            VarDB.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Varialbe Stuff (Country Specific)
        // GET: /Variable/Create
        [HttpGet]
        public ActionResult CreateCountry()
        {
            ViewBag.Currencies = VarDB.Currencies.ToList().Select(m => m.id);
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCountry(Country country)
        {
            if (ModelState.IsValid)
            {
                country.Currency_Per_Dollar = VarDB.Currencies.Find(country.Currency).ValueInDollars;
                VarDB.Countries.Add(country);
                VarDB.SaveChanges();
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Created Country : {1}", CurrentUsername(), country.Id));

                return RedirectToAction("Variables");
            }
            ViewBag.Currencies = VarDB.Currencies.ToList().Select(m => m.id);
            return View(country);
        }

        public JsonResult GetCurrency(string id)
        {
            var currencyValue = VarDB.Currencies.Find(id).ValueInDollars;
            return Json(currencyValue);
        }

        public ActionResult EditCountry(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Country country = VarDB.Countries.Find(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            ViewBag.Currencies = VarDB.Currencies.ToList().Select(m => m.id);

            return View(country);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCountry(Country country)
        {
            if (ModelState.IsValid)
            {
                country.Currency_Per_Dollar = VarDB.Currencies.Find(country.Currency).ValueInDollars;
                VarDB.Entry(country).State = EntityState.Modified;
                VarDB.SaveChanges();
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited Country : {1}", CurrentUsername(), country.Id));

                return RedirectToAction("Index");
            }
            ViewBag.Currencies = VarDB.Currencies.ToList().Select(m => m.id);

            return View(country);
        }

        public ActionResult DeleteCountry(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Country country = VarDB.Countries.Find(id);


            if (country == null)
            {
                return HttpNotFound();
            }

            VarDB.Entry(country).State = EntityState.Deleted;
            VarDB.SaveChanges();
            AddAudit(User.Identity.GetUserId(), string.Format("{0} Removed Country : {1}", CurrentUsername(), id));

            return RedirectToAction("Variables");
        }

        // POST: /Variable/Delete/5
        [HttpPost, ActionName("DeleteCountry")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmedCountry(string id)
        {
            Country country = VarDB.Countries.Find(id);
            VarDB.Countries.Remove(country);
            VarDB.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Equipment Creation + Editing

        public ActionResult EquipmentList()
        {
            EquipmentListviewModel emod = new EquipmentListviewModel();
            emod.BalanceSystems = VarDB.BalanceSystems.ToList();
            emod.Batteries = VarDB.Batteries.ToList();
            emod.BiInverters = VarDB.BiInverters.ToList();
            emod.Inverters = VarDB.Inverters.ToList();
            emod.MountingStructures = VarDB.MountingStructures.ToList();
            emod.PVModules = VarDB.PVModules.ToList();


            return View(emod);

        }

        #region Inverters
        [HttpGet]
        public ActionResult CreateInverter()
        {
            InverterViewModel model = new InverterViewModel();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }


        [HttpPost]
        public ActionResult CreateInverter(InverterViewModel model)
        {
            if (ModelState.IsValid)
            {

                Inverters inverter = new Inverters();
                inverter = model.ReversePopulate();
                inverter.Id = inverter.Id.Replace(" ", "_");
                inverter.Validate();
                VarDB.Inverters.Add(inverter);
                VarDB.SaveChanges();
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Created Inverter : {1}", CurrentUsername(), model.Inverter.Id));

                return RedirectToAction("EquipmentList");
            }
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            return View(model);
        }


        [HttpGet]
        public ActionResult EditInverter(string id)
        {
            Inverters model = VarDB.Inverters.Find(id);
            InverterViewModel vm = new InverterViewModel();
            vm.Populate(model);
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(vm);
        }


        [HttpPost]
        public ActionResult EditInverter(InverterViewModel model)
        {
            if (ModelState.IsValid)
            {

                Inverters inverter = new Inverters();
                inverter = model.ReversePopulate();
                inverter.Validate();
                VarDB.Entry(inverter).State = EntityState.Modified;
                VarDB.SaveChanges();
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited Inverter : {1}", CurrentUsername(), model.Inverter.Id));

                return RedirectToAction("EquipmentList");
            }
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }

        #endregion

        #region BiInverters
        [HttpGet]
        public ActionResult CreateBiInverter()
        {
            BiInverterViewModel model = new BiInverterViewModel();

            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }


        [HttpPost]
        public ActionResult CreateBiInverter(BiInverterViewModel model)
        {
            if (ModelState.IsValid)
            {

                BiInverters inverter = new BiInverters();
                inverter = model.ReversePopulate();
                inverter.Validate();
                VarDB.BiInverters.Add(inverter);
                VarDB.SaveChanges();
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Created Bi-Inverter : {1}", CurrentUsername(), model.BiInverter.Id));

                return RedirectToAction("EquipmentList");
            }
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }


        [HttpGet]
        public ActionResult EditBiInverter(string id)
        {
            BiInverters model = VarDB.BiInverters.Find(id);
            BiInverterViewModel vm = new BiInverterViewModel();
            vm.Populate(model);
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(vm);
        }


        [HttpPost]
        public ActionResult EditBiInverter(BiInverterViewModel model)
        {
            if (ModelState.IsValid)
            {

                model.BiInverter.Validate();
                BiInverters Biinverter = new BiInverters();
                Biinverter = model.ReversePopulate();
                Biinverter.Validate();
                VarDB.Entry(Biinverter).State = EntityState.Modified;
                VarDB.SaveChanges();
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited Bi-Inverter : {1}", CurrentUsername(), model.BiInverter.Id));

                return RedirectToAction("EquipmentList");
            }
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }
        #endregion

        #region PVModules
        [HttpGet]
        public ActionResult CreatePVModule()
        {
            PVModules model = new PVModules();
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }


        [HttpPost]
        public ActionResult CreatePVModule(PVModules model)
        {
            if (ModelState.IsValid)
            {

                model.Validate();
                model.Id = model.Id.Replace(" ", "_");

                VarDB.PVModules.Add(model);
                VarDB.SaveChanges();
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Created PV Module : {1}.", CurrentUsername(), model.Id));

                return RedirectToAction("EquipmentList");
            }
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }


        [HttpGet]
        public ActionResult EditPVModule(string id)
        {
            PVModules model = VarDB.PVModules.Find(id);
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }


        [HttpPost]
        public ActionResult EditPVModule(PVModules model)
        {
            if (ModelState.IsValid)
            {

                model.Validate();

                VarDB.Entry(model).State = EntityState.Modified;
                VarDB.SaveChanges();
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited PV Module : {1}.", CurrentUsername(), model.Id));

                return RedirectToAction("EquipmentList");
            }
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }

        #endregion

        #region MountingStructures
        [HttpGet]
        public ActionResult CreateMountingStructure()
        {
            MountingStructures model = new MountingStructures();
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }


        [HttpPost]
        public ActionResult CreateMountingStructure(MountingStructures model)
        {
            if (ModelState.IsValid)
            {

                bool success = dbhelper.CreateMounting(model);

                if (success)
                {
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Created Mounting : {1}", CurrentUsername(), model.Id));

                    return RedirectToAction("EquipmentList");
                }

            }

            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }


        [HttpGet]
        public ActionResult EditMountingStructure(string id)
        {
            MountingStructures model = dbhelper.GetMounting(id);
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            model.Validate();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }


        [HttpPost]
        public ActionResult EditMountingStructure(MountingStructures model)
        {
            if (ModelState.IsValid)
            {

                bool success = dbhelper.EditMounting(model);

                if (success)
                {
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited Mounting : {1}", CurrentUsername(), model.Id));

                    return RedirectToAction("EquipmentList");
                }

            }
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }

        #endregion

        #region Batteries
        [HttpGet]
        public ActionResult CreateBattery()
        {
            Batteries model = new Batteries();
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }


        [HttpPost]
        public ActionResult CreateBattery(Batteries model)
        {
            if (ModelState.IsValid)
            {

                bool success = dbhelper.CreateBattery(model);

                if (success)
                {
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Created Battery : {1}", CurrentUsername(), model.Id));

                    return RedirectToAction("EquipmentList");
                }

            }
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }


        [HttpGet]
        public ActionResult EditBattery(string id)
        {
            Batteries model = dbhelper.GetBattery(id);
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            model.Validate();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }


        [HttpPost]
        public ActionResult EditBattery(Batteries model)
        {
            if (ModelState.IsValid)
            {

                bool success = dbhelper.EditBattery(model);
                if (success)
                {
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited Battery : {1}", CurrentUsername(), model.Id));

                    return RedirectToAction("EquipmentList");
                }

            }
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }

        #endregion

        #region Balance of System
        [HttpGet]
        public ActionResult CreateBalanceSystem()
        {
            ViewBag.BOSTypes = dbhelper.BalanceCategories();
            ViewBag.Subs = dbhelper.EquipmentTypes();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            BalanceSystem model = new BalanceSystem();

            return View(model);
        }


        [HttpPost]
        public ActionResult CreateBalanceSystem(BalanceSystem model)
        {
            if (ModelState.IsValid)
            {

                bool success = dbhelper.CreateBalanceSystem(model);

                if (success)
                {
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Created Balance of System : {1}", CurrentUsername(), model.Id));

                    return RedirectToAction("EquipmentList");
                }

            }
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }


        [HttpGet]
        public ActionResult EditBalanceSystem(string id)
        {
            ViewBag.BOSTypes = dbhelper.BalanceCategories();

            ViewBag.Subs = dbhelper.EquipmentTypes();

            BalanceSystem model = VarDB.BalanceSystems.Find(id);
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames();
            model.Validate();

            return View(model);
        }


        [HttpPost]
        public ActionResult EditBalanceSystem(BalanceSystem model)
        {
            if (ModelState.IsValid)
            {

                bool Success = dbhelper.EditBalanceSystem(model);

                if (Success)
                {
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited Balance of System : {1}", CurrentUsername(), model.Id));

                    return RedirectToAction("EquipmentList");
                }


            }
            ViewBag.Currencies = dbhelper.GetAllCurrenciesNames();
            ViewBag.COO = dbhelper.GetAllCOOSNames(); ViewBag.Subs = dbhelper.EquipmentTypes();
            ViewBag.Subs = dbhelper.EquipmentTypes();

            return View(model);
        }

        #endregion

        #region Currencies

        [HttpGet]
        public ActionResult Currencies()
        {
            var currencies = dbhelper.GetAllCurrencies();
            return View(currencies);
        }

        [HttpGet]
        public ActionResult CreateCurrency()
        {
            Currency model = new Currency();
            return PartialView(model); //shows the currency partial view
        }

        [HttpPost]
        public JsonResult CreateCurrency(Currency model)
        {

            if (ModelState.IsValid)
            {
                var d = CheckForDuplicationCurrency(model.id);
                var e = d.Data;
                if (e.ToString().Contains("Sorry"))
                {
                    return Json("Already Exists");
                }

                try
                {
                    bool success = dbhelper.CreatCurrency(model);
                    if (success)
                    {
                        AddAudit(User.Identity.GetUserId(), string.Format("{0} Created Currency : {1}", CurrentUsername(), model.id));
                        return Json("Success");
                    }

                }

                catch (Exception)
                {

                }


            }
            return Json("Fail");
        }

        [HttpGet]
        public ActionResult EditCurrency(string id)
        {
            try
            {
                var model = dbhelper.GetCurrencyC(id);
                ViewBag.Countries = dbhelper.GetAllCountryNames();
                return View(model);
            }

            catch
            {
                //cant find currency
                ViewBag.Message = "Could Not Find Currency";
                return RedirectToAction("Currencies");
            }

        }

        [HttpPost]
        public ActionResult EditCurrency(Currency model)
        {
            if (ModelState.IsValid)
            {

                try
                {

                    bool success = dbhelper.EditCurrency(model);

                    if (success)
                    {
                        AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited Currency : {1}", CurrentUsername(), model.id));


                        success = dbhelper.ChangeAllCountryCurrencies(model);

                        if (success)
                        {
                            return RedirectToAction("Currencies");
                        }
                    }



                    //change all country values



                }

                catch (Exception)
                {
                    //something went wrong
                    return View(model);
                }


            }
            return View(model);
        }

        public ActionResult DeleteCurrency(string id)
        {
            bool success = dbhelper.DeleteCurrency(id);

            if (success)
            {
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Deleted Currency : {1}", CurrentUsername(), id));

            }

            return RedirectToAction("Currencies");
        }


        public JsonResult CheckForDuplicationCurrency(string id)
        {
            var data = VarDB.Currencies.Where(p => p.id.Equals(id, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

            if (data != null)
            {
                return Json("Sorry, this name already exists", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region helpers
        public ActionResult AddFiller(string id)
        {
            string partial = "Filler";
            Filler fill = new Filler();
            fill.ParentList = id;


            return PartialView(partial, fill);
        }


        public ActionResult DeleteItem(string id)
        {

            bool success = dbhelper.DeleteItem(id);

            if (success)
            {
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Deleted Item : {1}", CurrentUsername(), id.Split('!')[0]));

            }
            return RedirectToAction("EquipmentList");
        }
        #endregion


        #endregion

        #region CountryOfOrigin

        public ActionResult COO()
        {
            var coos = dbhelper.GetAllCoos();
            return View(coos);
        }

        [HttpGet]
        public ActionResult CreateCOO()
        {
            CountryOfOrigin model = new CountryOfOrigin();
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult CreateCOO(CountryOfOrigin model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var d = CheckForDuplication(model.Name);
                    var e = d.Data;
                    if (e.ToString().Contains("Sorry"))
                    {
                        return Json("Already Exists");
                    }
                    bool success = dbhelper.CreateCOO(model);

                    if (success)
                    {
                        AddAudit(User.Identity.GetUserId(), string.Format("{0} Created Country (origin) : {1}", CurrentUsername(), model.Name));

                        return Json("Success");
                    }

                }
                return Json("Fail");

            }

            catch (Exception)
            {
                return Json("Fail");

            }

        }


        public ActionResult DeleteCOO(int id)
        {
            CountryOfOrigin coo = dbhelper.GetCOO(id);

            bool Success = dbhelper.DeleteCOO(id);

            if (Success)
            {
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Deleted Country (origin) : {1}", CurrentUsername(), coo.Name));

            }

            return RedirectToAction("COO");
        }

        public JsonResult CheckForDuplication(string Country)
        {
            var data = VarDB.CountryOfOrigins.Where(p => p.Name.Equals(Country, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

            if (data != null)
            {
                return Json("Sorry, this name already exists", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }



        [HttpGet]
        public ActionResult CreatePort()
        {
            List<string> allCOO = dbhelper.GetAllCOOSNames();
            PortViewModel portviewmodel = new PortViewModel();
            portviewmodel.Populate(allCOO);
            ViewBag.Countries = dbhelper.GetAllCountryNames();

            return View(portviewmodel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePort(PortViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Port port = new Port()
                    {
                        Name = model.Name,
                        GPS = model.GPS,
                        Country = model.Country,
                        COOs = string.Join("!", model.Coo2Ports.Select(m => m.Country).ToList()),
                        DeliveryCosts20 = string.Join("!", model.Coo2Ports.Select(m => m.Amount20).ToList()),
                        DeliveryCosts40 = string.Join("!", model.Coo2Ports.Select(m => m.Amount40).ToList())

                    };


                    VarDB.Ports.Add(port);
                    VarDB.SaveChanges();
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Created Port : {1}", CurrentUsername(), model.Name));

                    return RedirectToAction("Ports");
                }

                catch (Exception ex)
                {
                    ViewBag.Message = ex.ToString();
                    ViewBag.Countries = VarDB.Countries.Select(m => m.Id).ToList();
                    return View(model);
                }

            }
            ViewBag.Countries = VarDB.Countries.Select(m => m.Id).ToList();

            return View(model);
        }


        [HttpGet]
        public ActionResult Ports()
        {
            IEnumerable<Port> ports = VarDB.Ports.ToList();
            return View(ports);
        }



        public ActionResult DeletePort(int id)
        {
            try
            {
                var port = VarDB.Ports.Find(id);
                VarDB.Entry(port).State = EntityState.Deleted;
                VarDB.SaveChanges();
            }

            catch (Exception)
            {
                ViewBag.Message = "Port does not exist or could not be deleted!";
            }

            return RedirectToAction("Ports");
        }


        [HttpGet]
        public ActionResult EditPort(int id)
        {
            List<string> allCOO = VarDB.CountryOfOrigins.Select(m => m.Name).ToList();
            Port portToEdit = VarDB.Ports.Find(id);
            PortViewModel portviewmodel = new PortViewModel();
            portviewmodel.Populate(portToEdit, allCOO);
            ViewBag.Countries = VarDB.Countries.Select(m => m.Id).ToList();

            return View(portviewmodel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPort(PortViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Port port = VarDB.Ports.Find(model.id);


                    port.Name = model.Name;
                    port.GPS = model.GPS;
                    port.Country = model.Country;
                    port.COOs = string.Join("!", model.Coo2Ports.Select(m => m.Country).ToList());
                    port.DeliveryCosts20 = string.Join("!", model.Coo2Ports.Select(m => m.Amount20).ToList());
                    port.DeliveryCosts40 = string.Join("!", model.Coo2Ports.Select(m => m.Amount40).ToList());

                    VarDB.Entry(port).State = EntityState.Modified;
                    VarDB.SaveChanges();
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited Port : {1}", CurrentUsername(), model.Name));

                    return RedirectToAction("Ports");
                }

                catch (Exception ex)
                {
                    ViewBag.Message = ex.ToString();
                    ViewBag.Countries = VarDB.Countries.Select(m => m.Id).ToList();
                    return View(model);
                }

            }
            ViewBag.Countries = VarDB.Countries.Select(m => m.Id).ToList();

            return View(model);
        }
        #endregion

        #region Project Stuff + FEES REVIEWS
        public ActionResult Projects()
        {
            List<Project> Projs = VarDB.Projects.OrderBy(m => m.CurrentUserID).ToList();

            return View(Projs);
        }



        [HttpGet]
        public ActionResult Fees()
        {
            try
            {
                NVIDev.Models.Fees model = VarDB.Fees.FirstOrDefault();
                if (model != null)
                {
                    return View(model);
                }

                else
                {
                    model = new Fees();
                    return View(model);
                }
            }

            catch (Exception)
            {
                NVIDev.Models.Fees model = new Fees();
                return View(model);
            }

        }

        [HttpPost]
        public ActionResult Fees(Fees model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    VarDB.Entry(model).State = EntityState.Modified;
                    VarDB.SaveChanges();
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited Fees.", CurrentUsername()));


                }

                catch (Exception)
                {
                    VarDB.Fees.Add(model);
                    VarDB.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            return View(model);

        }

        public ActionResult GetFullname(string id)
        {
            string username = "";

            var userdetails = VarDB.Users.Find(id);

            if (userdetails != null)
            {
                username = userdetails.UserName;
            }

            return Content(username);
        }


        [HttpGet]
        public ActionResult Review(int id)
        {
            Project pro = VarDB.Projects.Find(id);
            Fees fee = VarDB.Fees.FirstOrDefault();
            FeeViewModel model = new FeeViewModel(pro, fee);
            model.Populate();

            ViewBag.Currency = VarDB.Countries.Find(pro.Details.P_Country).Currency;
            ViewBag.Exch = VarDB.Countries.Find(pro.Details.P_Country).Currency_Per_Dollar;
            return View(model);
        }

        [HttpPost]
        public ActionResult Review(FeeViewModel model)
        {

            if (ModelState.IsValid)
            {

                Project pro = UpdateFees(model);
                VarDB.Entry(pro).State = EntityState.Modified;
                VarDB.SaveChanges();
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Reviewed Project : {1} ", CurrentUsername(), model.project.Id));

                return RedirectToAction("Projects");

            }


            return View(model);
        }



        public Project UpdateFees(FeeViewModel model)
        {
            NVIDev.Models.Fees fee = VarDB.Fees.FirstOrDefault();
            model = new FeeViewModel(model.project, fee);
            model.Populate();

            model.Submission();
            Project pro = VarDB.Projects.Find(model.project.Id);

            pro.UseAdmin = model.project.UseAdmin;
            pro.UseAssetManagement = model.project.UseAssetManagement;
            pro.UseCommisioning = model.project.UseCommisioning;
            pro.UseMargin = model.project.UseMargin;
            pro.UseMonitorSoftware = model.project.UseMonitorSoftware;
            pro.UsePerformanceG = model.project.UsePerformanceG;
            pro.UsePrelim = model.project.UsePrelim;
            pro.UsePrepVet = model.project.UsePrepVet;
            pro.UseProcurement = model.project.UseProcurement;
            pro.UseProjectManage = model.project.UseProjectManage;
            pro.UseTechVet = model.project.UseTechVet;
            pro.UseWorkingCap = model.project.UseWorkingCap;

            //EPC Check 

            pro.in_UpfrontClient = model.superPro.UpInitial;
            pro.in_UpfrontFee = model.Upfront;
            pro.in_AdminFee = model.OnGoingFee_Admin;
            pro.AdminXII = model.OnGoingFee_Admin;
            pro.UpfrontXI = model.Upfront;

            return pro;
        }

        public ActionResult EditVars(int id) //variable adjustments
        {

            var model = VarDB.Projects.Find(id);
            SuperProject sm = new SuperProject(model);
            model.in_UpfrontClient = sm.UpInitial;
            ViewBag.Currency = sm.Country.Currency;
            return View(model);
        }

        [HttpPost]
        public ActionResult EditVars(Project model)
        {

            var project = VarDB.Projects.Find(model.Id);

            project.xn1 = model.xn1;
            project.xn2 = model.xn2;
            project.xn3 = model.xn3;
            project.xn4 = model.xn4;
            project.xn5 = model.xn5;
            project.xn6 = model.xn6;
            project.xn7 = model.xn7;
            project.xn8 = model.xn8;

            project.real_FixedAnualCharge = model.real_FixedAnualCharge;
            project.adj_SolarCapexFee = model.adj_SolarCapexFee;
            project.RIAdjustment = model.RIAdjustment;
            project.UpAdj = model.UpAdj;
            project.FAdmin = model.FAdmin;
            project.UpAdj = model.UpAdj;
            var country = VarDB.Countries.Find(project.Details.P_Country);

            if (project.UtilityInflation1 == 0) //set first time utility inflation
            {
                project.UtilityInflation1 = country.UtilityInflation1;
                project.UtilityInflation2 = country.UtilityInflation2;
                project.UtilityInflation3 = country.UtilityInflation3;
                project.UtilityInflation4 = country.UtilityInflation4;
                project.UtilityInflation5 = country.UtilityInflation5;
                project.UtilityInflation6 = country.UtilityInflation6;
                project.UtilityInflation7 = country.UtilityInflation7;
                project.UtilityInflation8 = country.UtilityInflation8;
                project.UtilityInflation9 = country.UtilityInflation9;
                project.UtilityInflation10 = country.UtilityInflation10;
                project.UtilityInflation11 = country.UtilityInflation11;
                project.UtilityInflation12 = country.UtilityInflation12;
                project.UtilityInflation13 = country.UtilityInflation13;
                project.UtilityInflation14 = country.UtilityInflation14;
                project.UtilityInflation15 = country.UtilityInflation15;
                project.UtilityInflation16 = country.UtilityInflation16;
                project.UtilityInflation17 = country.UtilityInflation17;
                project.UtilityInflation18 = country.UtilityInflation18;
                project.UtilityInflation19 = country.UtilityInflation19;
                project.UtilityInflation20 = country.UtilityInflation20;

            }
            project.real_UpfrontClient = project.in_UpfrontClient + project.UpAdj;

            project.real_AdminFee = project.in_AdminFee * (1 + project.FAdmin / 100);
            project.real_SolarCapexFee = project.in_SolarCapexFee * (1 + project.adj_SolarCapexFee / 100);
            project.real_RI = project.In_Ri + project.RIAdjustment;
            project.real_UpfrontFee = project.in_UpfrontFee * (1 + project.UpAdj / 100);

            VarDB.Entry(project).State = EntityState.Modified;
            VarDB.SaveChanges();
            AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited : {1} Inputs", CurrentUsername(), project.Id));


            return RedirectToAction("Projects");
        }
        #endregion

        #region Duties and Taxes

        #region Duties
        public ActionResult Duties() //show a list of all duties
        {
            IEnumerable<Duty> allDuties = VarDB.Duties.ToList();
            return View(allDuties);
        }


        [HttpGet]
        public ActionResult CreateDuty()
        {
            List<DutyItem> possibleCountries = new List<DutyItem>();
            List<DutyItem> possibleEquipment = new List<DutyItem>();
            ViewBag.Customs = new string[] { "FOB", "CIF", "CIF + VAT" };
            if (VarDB.Countries.Count() > 0)
            {
                foreach (var country in VarDB.Countries)
                {
                    DutyItem counter = new DutyItem()
                    {
                        UseMe = false,
                        Name = country.Id
                    };

                    possibleCountries.Add(counter);
                }

                if (VarDB.Catergories.Count() > 0)
                {
                    foreach (var cat in VarDB.Catergories)
                    {
                        DutyItem cate = new DutyItem()
                        {
                            UseMe = false,
                            Name = cat.Name
                        };

                        possibleEquipment.Add(cate);
                    }


                }

                DutyViewModel dvmodel = new DutyViewModel();
                dvmodel.PossibleCountries = possibleCountries;
                dvmodel.PossibleEquipment = possibleEquipment;
                return View(dvmodel);


            }

            ViewBag.Message = "No countries to add duties to.";
            return View(); //Something went wrong

        }

        [HttpPost]
        public ActionResult CreateDuty(DutyViewModel model)
        {
            if (ModelState.IsValid)
            {

                try
                {

                    List<string> vrates = new List<string>();
                    List<string> names = new List<string>();
                    foreach (var country in model.UseCountries)
                    {
                        Country c = VarDB.Countries.Find(country.Name);
                        vrates.Add(c.VATRate.ToString());
                        names.Add(country.Name);
                    }
                    Duty myDuty = new Duty()
                    {
                        AllCountries = model.AllCountries,
                        VATRates = string.Join("!", vrates),
                        Name = model.Name,
                        CustomsValue = model.CustomsValue,
                        Countries = string.Join("!", names),
                        Rate = model.Rate

                    };


                    VarDB.Duties.Add(myDuty);

                    foreach (var catego in model.UseEquipment) //add duty to equipment
                    {
                        Category cat = VarDB.Catergories.Where(m => m.Name == catego.Name).First();

                        List<string> mytaxes = cat.Taxes.Split('!').ToList();
                        if (!mytaxes.Contains(catego.Name))
                        {
                            mytaxes.Add(catego.Name);

                        }
                        cat.Taxes = string.Join("!", mytaxes);
                        VarDB.Entry(cat).State = EntityState.Modified;

                    }

                    VarDB.SaveChanges();
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Created Duty : {1}.", CurrentUsername(), model.Name));

                    return RedirectToAction("Duties");
                }

                catch (Exception ex)
                {
                    ViewBag.Message = ex.ToString();
                    ViewBag.Customs = new string[] { "FOB", "CIF", "CIF + VAT" };

                    return View(model);
                }

            }
            return View(model);
        }


        [HttpGet]
        public ActionResult EditDuty(int id)
        {
            ViewBag.Customs = new string[] { "FOB", "CIF", "CIF + VAT" };

            var model = VarDB.Duties.Find(id);
            DutyViewModel dutyViewModel = new DutyViewModel();
            dutyViewModel.Populate(model, VarDB);
            if (VarDB.Countries.Count() > 0)
            {
                foreach (var country in VarDB.Countries)
                {
                    DutyItem counter = new DutyItem()
                    {
                        UseMe = false,
                        Name = country.Id
                    };

                    dutyViewModel.PossibleCountries.Add(counter);
                }

                if (VarDB.Catergories.Count() > 0)
                {
                    foreach (var cat in VarDB.Catergories)
                    {
                        DutyItem cate = new DutyItem()
                        {
                            UseMe = false,
                            Name = cat.Name
                        };

                        dutyViewModel.PossibleEquipment.Add(cate);
                    }


                }

                return View(dutyViewModel);



            }


            return RedirectToAction("Duties"); //something super bad
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDuty(DutyViewModel model)
        {
            if (ModelState.IsValid)
            {

                try
                {

                    List<string> vrates = new List<string>();
                    List<string> names = new List<string>();
                    foreach (var country in model.UseCountries)
                    {
                        if (country.UseMe)
                        {
                            Country c = VarDB.Countries.Find(country.Name);
                            vrates.Add(c.VATRate.ToString());
                            names.Add(country.Name);
                        }

                    }
                    Duty myDuty = VarDB.Duties.Find(model.Id);

                    myDuty.AllCountries = model.AllCountries;
                    myDuty.VATRates = string.Join("!", vrates);
                    myDuty.Name = model.Name;
                    myDuty.CustomsValue = model.CustomsValue;
                    myDuty.Countries = string.Join("!", names);
                    myDuty.Rate = model.Rate;





                    //iterates through a list of categories
                    //Each category must now be assigned this tax
                    //It must be added to the list, or removed if not used
                    foreach (var catego in model.UseEquipment) //add duty to equipment
                    {
                        if (catego.UseMe)
                        {
                            Category cat = VarDB.Catergories.Where(m => m.Name == catego.Name).First();

                            List<string> currentTaxes = cat.Taxes.Split('!').ToList();

                            if (!currentTaxes.Contains(myDuty.Name))
                            {
                                currentTaxes.Add(myDuty.Name);
                            }
                            cat.Taxes = string.Join("!", currentTaxes);
                            VarDB.Entry(cat).State = EntityState.Modified;
                        }


                    }
                    VarDB.Entry(myDuty).State = EntityState.Modified;

                    VarDB.SaveChanges();
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited Duty : {1}.", CurrentUsername(), model.Name));

                    return RedirectToAction("Duties");
                }

                catch (Exception ex)
                {
                    ViewBag.Message = ex.ToString();
                    ViewBag.Customs = new string[] { "FOB", "CIF", "CIF + VAT" };

                    return View(model);
                }

            }
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteDuty(int id)
        {
            var dutyToDelete = VarDB.Duties.Find(id);
            if (dutyToDelete != null)
            {
                VarDB.Entry(dutyToDelete).State = EntityState.Deleted;
                VarDB.SaveChanges();
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Deleted Duty : {1}.", CurrentUsername(), dutyToDelete.Name));

                return RedirectToAction("Duties");
            }
            return RedirectToAction("Duties");


        }

        #endregion
        #region Categories
        [HttpGet]
        public ActionResult Categories()
        {
            List<Category> myCategories = VarDB.Catergories.Count() > 0 ? VarDB.Catergories.ToList().OrderBy(m => m.Type).ToList() : new List<Category>();
            return View(myCategories);
        }

        [HttpGet]
        public ActionResult CreateCategory()
        {


            ViewBag.Types = new[] { "Inverters", "Bi-Inverter", "Mounting Structure", "Balance Of System", "PV Panel", "Battery" };
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCategory(CategoryViewModel model)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    List<string> taxes = new List<string>();

                    foreach (var tax in model.UseTaxes)
                    {
                        taxes.Add(tax.Name);
                    }
                    Category cat = new Category()
                    {
                        Name = model.Name,
                        Type = model.Type,
                        Taxes = string.Join("!", taxes)
                    };

                    VarDB.Catergories.Add(cat);
                    VarDB.SaveChanges();
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Created Category : {1}.", CurrentUsername(), model.Name));

                    return RedirectToAction("Categories");
                }

                catch (Exception ex)
                {
                    ViewBag.Types = new[] { "Inverters", "Bi-Inverter", "Mounting Structure", "Balance Of System", "PV Panel", "Battery" };

                    ViewBag.Message = ex.ToString();
                    return View(model);
                }


            }
            ViewBag.Types = new[] { "Inverters", "Bi-Inverter", "Mounting Structure", "Balance Of System", "PV Panel", "Battery" };

            return View(model);
        }


        [HttpGet]
        public ActionResult EditCategory(int id)
        {
            ViewBag.Types = new[] { "Inverters", "Bi-Inverter", "Mounting Structure", "Balance Of System", "PV Panel", "Battery" };

            var possibleTaxes = VarDB.Duties.Count() > 0 ? VarDB.Duties.ToList() : null;
            var realTaxes = new List<string>();

            foreach (var dud in possibleTaxes)
            {


                realTaxes.Add(dud.Name);
            }
            ViewBag.PossibleTaxes = realTaxes;


            var category = VarDB.Catergories.Find(id);
            List<string> myTaxes = new List<string>();
            if (!string.IsNullOrEmpty(category.Taxes))
            {
                myTaxes = category.Taxes.Split('!').ToList();
            }

            List<DutyItem> myDutyItems = new List<DutyItem>();

            foreach (string i in myTaxes)
            {
                DutyItem dud = new DutyItem()
                {
                    UseMe = true,
                    Name = i

                };
                if (VarDB.Duties.Where(m => m.Name == i).FirstOrDefault() != null)
                {
                    dud.Extra = VarDB.Duties.Where(m => m.Name == i).First().Rate.ToString();

                }
                else
                {
                    dud.Extra = null;
                }
                myDutyItems.Add(dud);
            }
            if (category != null)
            {
                CategoryViewModel categoryViewModel = new CategoryViewModel()
                {
                    Name = category.Name,
                    UseTaxes = myDutyItems,
                    Type = category.Type,
                    id = category.id
                };

                return View(categoryViewModel);
            }
            return RedirectToAction("Categories");

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCategory(CategoryViewModel model)
        {
            ViewBag.Types = new[] { "Inverters", "Bi-Inverter", "Mounting Structure", "Balance Of System", "PV Panel", "Battery" };

            if (ModelState.IsValid)
            {
                Category category = VarDB.Catergories.Find(model.id);
                try
                {
                    List<string> taxes = new List<string>();

                    if (model.UseTaxes == null)
                    {
                        model.UseTaxes = new List<DutyItem>();
                    }
                    foreach (var tax in model.UseTaxes)
                    {
                        taxes.Add(tax.Name);
                    }

                    category.Type = model.Type;
                    category.Taxes = string.Join("!", taxes);
                    if (string.IsNullOrEmpty(category.Taxes))
                    {
                        category.Taxes = null;
                    }
                    VarDB.Entry(category).State = EntityState.Modified;
                    VarDB.SaveChanges();
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Edited Category : {1}.", CurrentUsername(), model.Name));

                    return RedirectToAction("Categories");
                }

                catch (Exception ex)
                {
                    ViewBag.Types = new[] { "Inverters", "Bi-Inverter", "Mounting Structure", "Balance Of System", "PV Panel", "Battery" };

                    ViewBag.Message = ex.ToString();
                    return View(model);
                }


            }
            var possibleTaxes = VarDB.Duties.Count() > 0 ? VarDB.Duties.ToList() : null;
            var realTaxes = new List<string>();

            foreach (var dud in possibleTaxes)
            {


                realTaxes.Add(dud.Name);
            }
            ViewBag.PossibleTaxes = realTaxes;
            ViewBag.Types = new[] { "Inverters", "Bi-Inverter", "Mounting Structure", "Balance Of System", "PV Panel", "Battery" };

            return View(model);
        }



        public ActionResult DeleteCategory(int id)
        {
            var categoryToDelete = VarDB.Catergories.Find(id);
            VarDB.Entry(categoryToDelete).State = EntityState.Deleted;
            VarDB.SaveChanges();
            AddAudit(User.Identity.GetUserId(), string.Format("{0} Deleted Category : {1}.", CurrentUsername(), categoryToDelete.Name));

            return RedirectToAction("Categories");
        }

        #endregion
        [HttpGet]
        public ActionResult AddTax()
        {
            var possibleTaxes = VarDB.Duties.Count() > 0 ? VarDB.Duties.ToList() : null;
            var realTaxes = new List<string>();

            foreach (var dud in possibleTaxes)
            {


                realTaxes.Add(dud.Name);
            }
            ViewBag.PossibleTaxes = realTaxes;

            DutyItem duty = new DutyItem();
            duty.Name = (possibleTaxes.First() != null) ? possibleTaxes.FirstOrDefault().Name : "Empty";
            duty.Extra = (possibleTaxes.First() != null) ? possibleTaxes.FirstOrDefault().Rate.ToString() : "0";
            return PartialView("UseTax", duty);
        }

        public ActionResult GetTax(string id)
        {

            var myDuty = (VarDB.Duties.Count() > 0) ? VarDB.Duties.Where(model => model.Name == id).First() : null;
            if (myDuty != null)
            {
                return Json(myDuty.Rate);
            }

            return null;

        }
        #endregion

        #region Clear All
        public ActionResult ClearAllEquipment()
        //
        {
            try
            {
                VarDB.Database.ExecuteSqlCommand("delete from Inverters");
                VarDB.Database.ExecuteSqlCommand("delete from BiInverters");
                VarDB.Database.ExecuteSqlCommand("delete from BalanceSystems");
                VarDB.Database.ExecuteSqlCommand("delete from Batteries");
                VarDB.Database.ExecuteSqlCommand("delete from PVModules");
                VarDB.Database.ExecuteSqlCommand("delete from MountingStructures");
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Cleared All Database Equipment..", CurrentUsername()));

            }

            catch (Exception)
            {

            }


            return RedirectToAction("EquipmentList");



        }
        #endregion

        #region Excel Importing

        //get the excel file
        [HttpPost]
        public ActionResult ImportExcel(HttpPostedFileBase file)
        {
            //PROCESS
            //Modules, Inverters, Batteries, Mounting, Bi-Inverters, Balance of system
            //Actually alphabetizes

            if (file != null && file.ContentLength > 0) //file has content
            {


                string fileext = System.IO.Path.GetExtension(file.FileName);

                if (fileext == ".xls" || fileext == ".xlsx")
                {


                    DataSet ds = new DataSet();

                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(file.InputStream);

                    excelReader.IsFirstRowAsColumnNames = true;
                    ds = excelReader.AsDataSet();


                    //Modules inverters batteries bi-direcitonal mounting balance 







                    int keyCount = VarDB.Batteries.Count() + 1000;
                    keyCount++;
                    //if an item cant be added. add it to this list and reinform
                    List<string> equipmentErrors = new List<string>();

                    List<string> currentCategories = VarDB.Catergories.Select(m => m.Name).ToList();
                    Dictionary<string, string> categoriesToAdd = new Dictionary<string, string>();
                    //string query = string.Format("Select * from [{0}]", excelsheets[0]);

                    //OleDbCommand command = new OleDbCommand(query,excelConnection);
                    //OleDbDataReader dataReader = command.ExecuteReader();
                    //0       1              2           3    4              5           6    7                     8                9             10                   11                   12      13
                    //Item	Manufacturer	Description	UOM	  Unit (Ah -C10)	Unit cost	CCY  	Units per pallet	Pallets Per 20 Ft	 Inco terms 	Lead Time (days)	Country of Origin	Category
                    //If a category does not exist, add it to a list, create

                    //for batteries

                    #region Add Batteries

                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        object[] items = row.ItemArray;

                        string name = items[0].ToString();
                        if (string.IsNullOrWhiteSpace(name))
                        {
                            continue;
                        }
                        string manufacturer = items[1].ToString();
                        string description = items[2].ToString();
                        string UOM = items[3].ToString();
                        string unitAH = items[4].ToString(); //conver to double
                        string unitCost = items[5].ToString(); //conver to double
                        string ccy = items[6].ToString(); //currency of import, must be converted to dollars
                        string unitPerPallet = items[7].ToString(); //conver to int
                        string palletsPer20ft = items[8].ToString(); //conver to int
                        string incoTerms = items[9].ToString();
                        string leadTime = items[10].ToString();// delivery time
                        string countryOfOrigin = items[11].ToString();
                        string category = items[12].ToString();

                        name = name.Replace('.', ' ');
                        name = name.Replace(',', ' ');

                        if (palletsPer20ft == "0" || string.IsNullOrWhiteSpace(palletsPer20ft) || unitPerPallet == "0" || string.IsNullOrWhiteSpace(unitPerPallet))
                        {
                            string erro = string.Format("Could not add Battery {0} . Must have a value greater than 0 for Units per pallet and Pallets per 20f", name);
                            equipmentErrors.Add(erro);
                            continue;
                        }
                        if (!currentCategories.Contains("Battery") && !categoriesToAdd.Contains(new KeyValuePair<string, string>("Battery", "Battery")))
                        {
                            categoriesToAdd.Add("Battery", "Battery");
                        }

                        Batteries battery = new Batteries()
                        {
                            CountryOfOrigin = countryOfOrigin,
                            Description = description,
                            Dimensions = "None",
                            UnitAh_C10 = double.Parse(unitAH),
                            Id = name.Replace(' ', '_'),
                            IncoTerms = incoTerms,
                            ItemCost = double.Parse(unitCost),
                            UnitCost_WP = 0.00,
                            Key = "Ba" + keyCount.ToString(),
                            Manufacturer = manufacturer,
                            ModulesPerPallet = int.Parse(unitPerPallet),
                            PalletsPer20ft = int.Parse(palletsPer20ft),
                            SubCategory = "Battery",
                            UOM = UOM,
                            LeadTime = int.Parse(leadTime),
                            CCY = ccy
                        };

                        battery.Validate();

                        try
                        {
                            VarDB.Batteries.Add(battery);
                            VarDB.SaveChanges();
                            keyCount++;
                        }

                        catch (Exception)
                        {
                            VarDB.Batteries.Remove(battery);
                            string erro = string.Format("Could not add Battery {0} . It may be incomplete or already exists", name);
                            equipmentErrors.Add(erro);
                        }

                    }



                    //#region Add Bi-Inverters
                    //query = string.Format("Select * from [{0}]", excelsheets[1]);
                    //command = new OleDbCommand(query, excelConnection);


                    //dataReader = command.ExecuteReader();
                    /////Binverters
                    //keyCount = VarDB.BiInverters.Count() + 1001;
                    foreach (DataRow row in ds.Tables[3].Rows)
                    {
                        object[] items = row.ItemArray;
                        string name = items[0].ToString();
                        if (string.IsNullOrWhiteSpace(name))
                        {
                            continue;
                        }
                        string manufacturer = items[1].ToString();
                        string description = items[2].ToString();
                        string UOM = items[3].ToString();
                        string unitWP = items[4].ToString(); //conver to double
                        string unitCost = items[5].ToString(); //conver to double
                        string ccy = items[6].ToString(); //currency of import, must be converted to dollars
                        string unitPerPallet = items[7].ToString(); //conver to int
                        string palletsPer20ft = items[8].ToString(); //conver to int
                        string incoTerms = items[9].ToString();
                        string leadTime = items[10].ToString();// delivery time
                        string countryOfOrigin = items[11].ToString();
                        string category = items[12].ToString();
                        string comList = items[13].ToString();
                        string interf = items[14].ToString();
                        string applic = items[15].ToString();
                        name = name.Replace('.', ' ');
                        name = name.Replace(',', ' ');

                        if (palletsPer20ft == "0" || string.IsNullOrWhiteSpace(palletsPer20ft) || unitPerPallet == "0" || string.IsNullOrWhiteSpace(unitPerPallet))
                        {
                            string erro = string.Format("Could not add Bi-Inverter {0} . Must have a value greater than 0 for Units per pallet and Pallets per 20f", name);
                            equipmentErrors.Add(erro);
                            continue;
                        }
                        if (!currentCategories.Contains("Bi-Inverter") && !categoriesToAdd.Contains(new KeyValuePair<string, string>("Bi-Inverter", "Bi-Inverter")))
                        {
                            categoriesToAdd.Add("Bi-Inverter", "Bi-Inverter");
                        }

                        BiInverters biInverter = new BiInverters()
                        {
                            CountryOfOrigin = countryOfOrigin,
                            Description = description,
                            Dimensions = "None",
                            UnitWP = double.Parse(unitWP),
                            Id = name.Replace(' ', '_'),
                            IncoTerms = incoTerms,
                            ItemCost = double.Parse(unitCost),
                            UnitCost_WP = 0.00,
                            Key = "B" + keyCount.ToString(),
                            Manufacturer = manufacturer,
                            ModulesPerPallet = int.Parse(unitPerPallet),
                            PalletsPer20ft = int.Parse(palletsPer20ft),
                            SubCategory = "Bi-Inverter",
                            UOM = UOM,
                            LeadTime = int.Parse(leadTime),
                            CCY = ccy,
                            CommunicationList = comList,
                            ApplicationList = applic,
                            InterfaceList = interf
                        };

                        biInverter.Validate();
                        try
                        {
                            VarDB.BiInverters.Add(biInverter);
                            VarDB.SaveChanges();
                            keyCount++;
                        }

                        catch (Exception)
                        {
                            VarDB.BiInverters.Remove(biInverter);
                            string erro = string.Format("Could not add Bi-Inverter {0} . It may be incomplete or already exists", name);
                            equipmentErrors.Add(erro);
                        }
                    }

                    #endregion

                    #region Add Balance of System

                    ///Binverters
                    keyCount = VarDB.BalanceSystems.Count() + 1001;
                    foreach (DataRow row in ds.Tables[5].Rows)
                    {
                        object[] items = row.ItemArray;

                        string name = items[0].ToString();
                        if (string.IsNullOrWhiteSpace(name))
                        {
                            continue;
                        }
                        string manufacturer = items[1].ToString();
                        string description = items[2].ToString();
                        string UOM = items[3].ToString();
                        string unitCost = items[4].ToString(); //conver to double
                        string ccy = items[5].ToString(); //currency of import, must be converted to dollars
                        string unitPerPallet = items[6].ToString(); //conver to int
                        string palletsPer20ft = items[7].ToString(); //conver to int
                        string incoTerms = items[8].ToString();
                        string leadTime = items[9].ToString();// delivery time
                        string countryOfOrigin = items[10].ToString();
                        string category = items[11].ToString();

                        name = name.Replace('.', ' ');
                        name = name.Replace(',', ' ');

                        if (palletsPer20ft == "0" || string.IsNullOrWhiteSpace(palletsPer20ft) || unitPerPallet == "0" || string.IsNullOrWhiteSpace(unitPerPallet))
                        {
                            string erro = string.Format("Could not add BOS {0} . Must have a value greater than 0 for Units per pallet and Pallets per 20f", name);
                            equipmentErrors.Add(erro);
                            continue;
                        }
                        if (!currentCategories.Contains("Balance Of System") && !categoriesToAdd.Contains(new KeyValuePair<string, string>("Balance Of System", "Balance Of System")))
                        {
                            categoriesToAdd.Add("Balance Of System", "Balance Of System");
                        }

                        BalanceSystem BOS = new BalanceSystem()
                        {
                            CountryOfOrigin = countryOfOrigin,
                            Description = description,
                            Dimensions = "None",
                            Id = name.Replace(' ', '_'),
                            IncoTerms = incoTerms,
                            ItemCost = Math.Round(double.Parse(unitCost), 2),
                            Units = 1,
                            UnitCost = Math.Round(double.Parse(unitCost), 2),
                            Type = category,
                            Key = "Bo" + keyCount.ToString(),
                            Manufacturer = manufacturer,
                            ModulesPerPallet = int.Parse(unitPerPallet),
                            PalletsPer20ft = int.Parse(palletsPer20ft),
                            SubCategory = "Balance Of System",
                            UOM = UOM,
                            LeadTime = int.Parse(leadTime),
                            CCY = ccy
                        };


                        try
                        {
                            VarDB.BalanceSystems.Add(BOS);
                            VarDB.SaveChanges();
                            keyCount++;
                        }

                        catch (Exception)
                        {
                            VarDB.BalanceSystems.Remove(BOS);
                            string erro = string.Format("Could not add BOS {0} . It may be incomplete or already exists", name);
                            equipmentErrors.Add(erro);
                        }



                    }


                    #endregion

                    #region Add Inverters


                    keyCount = VarDB.Inverters.Count() + 1001;
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        object[] items = row.ItemArray;

                        string name = items[0].ToString();
                        if (string.IsNullOrWhiteSpace(name))
                        {
                            continue;
                        }
                        string manufacturer = items[1].ToString();
                        string description = items[2].ToString();
                        string UOM = items[3].ToString();
                        string unitWP = items[4].ToString(); //conver to double
                        string unitCost = items[5].ToString(); //conver to double
                        string ccy = items[6].ToString(); //currency of import, must be converted to dollars
                        string unitPerPallet = items[7].ToString(); //conver to int
                        string palletsPer20ft = items[8].ToString(); //conver to int
                        string incoTerms = items[9].ToString();
                        string leadTime = items[10].ToString();// delivery time
                        string countryOfOrigin = items[11].ToString();
                        string category = items[12].ToString();

                        string configuration = items[13].ToString();
                        string DCOverVolt = items[14].ToString();
                        string DCLoad = items[15].ToString();
                        string IOPort = items[16].ToString();
                        string CountrySpec = items[17].ToString();
                        name = name.Replace('.', ' ');
                        name = name.Replace(',', ' ');

                        if (palletsPer20ft == "0" || string.IsNullOrWhiteSpace(palletsPer20ft) || unitPerPallet == "0" || string.IsNullOrWhiteSpace(unitPerPallet))
                        {
                            string erro = string.Format("Could not add Inverter {0} . Must have a value greater than 0 for Units per pallet and Pallets per 20f", name);
                            equipmentErrors.Add(erro);
                            continue;
                        }
                        if (!currentCategories.Contains(category) && !categoriesToAdd.Contains(new KeyValuePair<string, string>("Inverters", "Inverters")))
                        {
                            categoriesToAdd.Add("Inverters", "Inverters");
                        }

                        Inverters invert = new Inverters()
                        {
                            CountryOfOrigin = countryOfOrigin,
                            Description = description,
                            Dimensions = "None",
                            Id = name.Replace(' ', '_'),
                            IncoTerms = incoTerms,
                            ItemCost = double.Parse(unitCost),
                            UnitWP = double.Parse(unitWP),
                            Key = "I" + keyCount.ToString(),
                            Manufacturer = manufacturer,
                            ModulesPerPallet = int.Parse(unitPerPallet),
                            PalletsPer20ft = int.Parse(palletsPer20ft),
                            SubCategory = "Inverters",
                            UOM = UOM,
                            LeadTime = int.Parse(leadTime),
                            CCY = ccy,
                            ConfigurationList = configuration,
                            IOPortModulesList = IOPort,
                            CountrySpecificGridList = CountrySpec,
                            DCLoadDisconnectList = DCLoad,
                            DCOverVoltageList = DCOverVolt,
                        };

                        invert.Validate();

                        try
                        {
                            VarDB.Inverters.Add(invert);
                            VarDB.SaveChanges();
                            keyCount++;
                        }

                        catch (Exception)
                        {
                            VarDB.Inverters.Remove(invert);
                            string erro = string.Format("Could not add Inverter {0} . It may be incomplete or already exists", name);
                            equipmentErrors.Add(erro);
                        }

                    }
                    #endregion

                    #region Modules

                    keyCount = VarDB.PVModules.Count() + 1001;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        object[] items = row.ItemArray;

                        string name = items[0].ToString();
                        if (string.IsNullOrWhiteSpace(name))
                        {
                            continue;
                        }
                        string manufacturer = items[1].ToString();
                        string description = items[2].ToString();
                        string UOM = items[3].ToString();
                        string unitWP = items[4].ToString(); //conver to double
                        string unitCost = items[5].ToString(); //conver to double
                        string ccy = items[6].ToString(); //currency of import, must be converted to dollars
                        string unitPerPallet = items[7].ToString(); //conver to int
                        string palletsPer20ft = items[8].ToString(); //conver to int
                        string incoTerms = items[9].ToString();
                        string leadTime = items[10].ToString();// delivery time
                        string countryOfOrigin = items[11].ToString();
                        string category = items[12].ToString();
                        name = name.Replace('.', ' ');
                        name = name.Replace(',', ' ');

                        if (palletsPer20ft == "0" || string.IsNullOrWhiteSpace(palletsPer20ft) || unitPerPallet == "0" || string.IsNullOrWhiteSpace(unitPerPallet))
                        {
                            string erro = string.Format("Could not add Module {0} . Must have a value greater than 0 for Units per pallet and Pallets per 20f", name);
                            equipmentErrors.Add(erro);
                            continue;
                        }
                        if (!currentCategories.Contains("Modules") && !categoriesToAdd.Contains(new KeyValuePair<string, string>("Modules", "PV Panel")))
                        {
                            categoriesToAdd.Add("Modules", "PV Panel");
                        }
                        PVModules pvMod = new PVModules()
                        {
                            CountryOfOrigin = countryOfOrigin,
                            Description = description,
                            Dimensions = "None",
                            Id = name.Replace(' ', '_'),
                            IncoTerms = incoTerms,
                            ItemCost = double.Parse(unitCost),
                            UnitWP = double.Parse(unitWP),
                            Key = "P" + keyCount.ToString(),
                            Manufacturer = manufacturer,
                            ModulesPerPallet = int.Parse(unitPerPallet),
                            PalletsPer20ft = int.Parse(palletsPer20ft),
                            SubCategory = "Modules",
                            UOM = UOM,
                            LeadTime = int.Parse(leadTime),
                            CCY = ccy
                        };

                        pvMod.Validate();

                        try
                        {
                            VarDB.PVModules.Add(pvMod);
                            VarDB.SaveChanges();
                            keyCount++;
                        }

                        catch (Exception)
                        {
                            VarDB.PVModules.Remove(pvMod);
                            string erro = string.Format("Could not add PV Module {0} . It may be incomplete or already exists", name);
                            equipmentErrors.Add(erro);
                        }

                    }
                    #endregion

                    #region Mounting

                    ///Binverters
                    keyCount = VarDB.MountingStructures.Count() + 1001;
                    foreach (DataRow row in ds.Tables[4].Rows)
                    {
                        object[] items = row.ItemArray;
                        string name = items[0].ToString();
                        if (string.IsNullOrWhiteSpace(name))
                        {
                            continue;
                        }
                        string manufacturer = items[1].ToString();
                        string description = items[2].ToString();
                        string UOM = items[3].ToString();

                        string unitCost = items[4].ToString(); //conver to double
                        string ccy = items[5].ToString(); //currency of import, must be converted to dollars
                        string unitPerPallet = items[6].ToString(); //conver to int
                        string palletsPer20ft = items[7].ToString(); //conver to int
                        string incoTerms = items[8].ToString();
                        string leadTime = items[9].ToString();// delivery time
                        string countryOfOrigin = items[10].ToString();
                        string category = items[11].ToString();
                        string type = items[12].ToString();
                        name = name.Replace('.', ' ');
                        name = name.Replace(',', ' ');
                        if (palletsPer20ft == "0" || string.IsNullOrWhiteSpace(palletsPer20ft) || unitPerPallet == "0" || string.IsNullOrWhiteSpace(unitPerPallet))
                        {
                            string erro = string.Format("Could not add Mounting {0} . Must have a value greater than 0 for Units per pallet and Pallets per 20f", name);
                            equipmentErrors.Add(erro);
                            continue;
                        }
                        if (!currentCategories.Contains("Mounting Structure") && !categoriesToAdd.Contains(new KeyValuePair<string, string>("Mounting Structure", "Mounting Structure")))
                        {
                            categoriesToAdd.Add("Mounting Structure", "Mounting Structure");
                        }
                        MountingStructures mount = new MountingStructures()
                        {
                            CountryOfOrigin = countryOfOrigin,

                            Id = name.Replace(' ', '_'),
                            IncoTerms = incoTerms,
                            ItemCost = double.Parse(unitCost),
                            UnitWP = 1,
                            Key = "M" + keyCount.ToString(),
                            Manufacturer = manufacturer,
                            ModulesPerPallet = int.Parse(unitPerPallet),
                            PalletsPer20ft = int.Parse(palletsPer20ft),
                            SubCategory = "Mounting Structure",
                            Type = category,

                            LeadTime = int.Parse(leadTime),
                            CCY = ccy
                        };

                        mount.Validate();

                        try
                        {
                            VarDB.MountingStructures.Add(mount);
                            VarDB.SaveChanges();
                            keyCount++;
                        }

                        catch (Exception)
                        {
                            VarDB.MountingStructures.Remove(mount);
                            string erro = string.Format("Could not add Mounting {0} . It may be incomplete or already exists", name);
                            equipmentErrors.Add(erro);
                        }

                    }
                    #endregion


                    #region Add Categories



                    excelReader.Close();

                    foreach (var cat in categoriesToAdd)
                    {
                        Category cate = new Category()
                        {
                            Name = cat.Key,
                            Type = cat.Value,
                            Taxes = ""

                        };

                        //null check

                        Category temp = VarDB.Catergories.Where(m => m.Name == cate.Name).FirstOrDefault();
                        if (temp == null)
                        {
                            try
                            {
                                VarDB.Catergories.Add(cate);
                                VarDB.SaveChanges();
                                AddAudit(User.Identity.GetUserId(), string.Format("{0} Imported Database Equipment..", CurrentUsername()));

                            }

                            catch (Exception)
                            {
                                string err = string.Format("{0} Category is invalid or already exists", cate.Name);
                                equipmentErrors.Add(err);
                            }
                        }

                    }

                    //if(equipmentErrors.Count > 0)
                    //{
                    //    return ViewExcel(equipmentErrors);
                    //}
                    return RedirectToAction("EquipmentList");
                    //return RedirectToAction("ViewExcel");

                }

                return RedirectToAction("EquipmentList");


            }


            else
            {
                return RedirectToAction("EquipmentList");
            }
        }

        [HttpGet]
        public ActionResult ViewExcel(List<string> equipmentErrors)
        {
            ViewBag.Errors = equipmentErrors;
            return View();
        }
                    #endregion

        #endregion

        #region Audits

        public int AddAudit(string userID, string Content)
        {

            string company = VarDB.Users.Find(userID).Company;
            Audit audit = new Audit(userID, company, Content);

            try
            {
                VarDB.Audits.Add(audit);
                VarDB.SaveChanges();
                return 1;
            }

            catch (Exception)
            {
                VarDB.Audits.Remove(audit);
                return 0;
            }
        }


        public List<Audit> UserAudits(string userID)
        {
            List<Audit> mAudits = new List<Audit>();


            mAudits.AddRange(VarDB.Audits.Where(m => m.UserID == userID));

            return mAudits;
        }
        public List<Audit> CompanyAudits(string company)
        {
            List<Audit> mAudits = new List<Audit>();


            mAudits.AddRange(VarDB.Audits.Where(m => m.Company == company));

            return mAudits;

        }

        public string CurrentUsername()
        {
            string id = User.Identity.GetUserId();
            return VarDB.Users.Find(id).UserName;
        }



        #endregion


        #region Tester
        public ActionResult TestOrders(int id)
        {
            try
            {
                Project p = VarDB.Projects.Find(id);
                Order_Details business = new Order_Details();
                business.CalculateEquipment(p);
                var model = business.order;

                return View(model);
            }

            catch (Exception)
            {
                return RedirectToAction("Projects");
            }

        }
        #endregion
    }
}