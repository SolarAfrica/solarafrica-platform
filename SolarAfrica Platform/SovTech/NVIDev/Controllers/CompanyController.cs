﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NVIDev.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Threading.Tasks;
using NVIDev.ActionFilters;


namespace NVIDev.Controllers
{
    [Authorize(Roles = "NVIAdmin, TPAdmin")]
    [CustAuthFilter]
    public class CompanyController : Controller
    {
        // GET: Company

        ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Index()
        {
            var userDeets = db.Users.Find(User.Identity.GetUserId());

            ViewBag.CompanyUsers = CompanyUsers(userDeets.Company);
            ViewBag.CompanyProjects = CompanyProjects(userDeets.Company);
            ViewBag.Company = userDeets.Company;
            ViewBag.Audits = CompanyAudits(userDeets.Company);
            return View();
        }

        #region User Management

        [HttpGet]
        public ActionResult Edit(string id)
        {
            var UserToChange = db.Users.Find(id);

            RoleViewModel model = new RoleViewModel(RolesAsNormalText(UserToChange.Roles.Select(m => m.RoleId).ToArray()).ToList(), new string[] { "Admin", "User" });
            model.UserID = UserToChange.Id;
            model.UserName = UserToChange.UserName;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(RoleViewModel model)
        {

            string roleid = "";


            //current role IDS NVIAdmin , NVIUser, TPAdmin, TPUser

            try
            {


                var oldRolls = await UserManager.GetRolesAsync(model.UserID);
                var userdeets = db.Users.Find(model.UserID);
                foreach (var role in oldRolls)
                {
                    await UserManager.RemoveFromRoleAsync(model.UserID, role);

                }

                foreach (var role in model.Roles.Where(m => m.includeMe == true))
                {
                    if (userdeets.Company.Contains("nvi"))
                    {
                        switch (role.Role)
                        {
                            case "Admin": roleid = "NVIAdmin"; break;
                            case "User": roleid = "NVIUser"; break;

                        }
                    }

                    else
                    {
                        switch (role.Role)
                        {

                            case "Admin": roleid = "TPAdmin"; break;
                            case "User": roleid = "TPUser"; break;
                        }
                    }

                    if (role.includeMe)
                    {
                        await UserManager.AddToRoleAsync(model.UserID, roleid);
                    }

                }
                AddAudit(User.Identity.GetUserId(), string.Format("{0} Changed {1} User Roles.", CurrentUsername(), userdeets.UserName));
                return RedirectToAction("Index");
            }

            catch (Exception ex)
            {
                ViewBag.Message = "Could not change Role :" + ex.ToString();
                return View(model);
            }

        }

        /// <summary>
        /// Show Company Information in Selete Item List
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> CompanyList(string id)
        {
            var raw = db.Company.Where(x => x.IsAcceptTerm == true && x.CompanyName.StartsWith(id)).Select(x => new SelectListItem { Text = x.CompanyName, Value = x.CompanyName }).ToList();
            return raw;
        }


        #endregion


        #region Project Management
        [HttpGet]
        public ActionResult TransferProject(int id)
        {
            TransferViewModel model = new TransferViewModel();
            Project pro = db.Projects.Find(id);
            model.ProjectID = id;
            model.ProjectName = pro.Details.ProjectName;
            var currentUser = db.Users.Find(User.Identity.GetUserId());
            model.CurrentUserID = currentUser.Id;
            model.AllCompanyUsers = db.Users.Where(m => m.Company == currentUser.Company).Select(a => a.UserName).ToList();
            return PartialView(model); //send project transfer to view
        }

        [HttpPost]
        public ActionResult TransferProject(TransferViewModel model)
        {

            if (ModelState.IsValid)
            {

                Project project = db.Projects.Find(model.ProjectID);

                try
                {
                    project.CurrentUserID = db.Users.Where(m => m.UserName == model.NewUserName).Select(m => m.Id).First();
                    db.SaveChanges();
                    AddAudit(User.Identity.GetUserId(), string.Format("{0} Transfered : {1} to {2}.", CurrentUsername(), project.Details.ProjectName, model.NewUserName));

                    return RedirectToAction("Index");
                }

                catch (Exception)
                { //failed possibly due to bad user information
                    return RedirectToAction("Index");
                }

            }



            return RedirectToAction("Index");

        }

        #endregion

        #region Audits

        public int AddAudit(string userID, string Content)
        {
            string company = db.Users.Find(userID).Company;
            Audit audit = new Audit(userID, company, Content);

            try
            {
                db.Audits.Add(audit);
                db.SaveChanges();
                return 1;
            }

            catch (Exception)
            {
                db.Audits.Remove(audit);
                return 0;
            }
        }


        public List<Audit> UserAudits(string userID)
        {
            List<Audit> mAudits = new List<Audit>();


            mAudits.AddRange(db.Audits.Where(m => m.UserID == userID));

            return mAudits;
        }
        public List<Audit> CompanyAudits(string company)
        {
            List<Audit> mAudits = new List<Audit>();


            mAudits.AddRange(db.Audits.Where(m => m.Company == company));

            return mAudits;

        }
        #endregion


        #region Helpers

        public string[] RolesAsNormalText(string[] roles)
        {
            string[] asNorms = new string[roles.Count()];
            string _roller = "";
            for (int a = 0; a < roles.Count(); a++)
            {
                switch (roles[a])
                {
                    case "NVIAdmin": _roller = "Admin"; break;
                    case "NVIUser": _roller = "User"; break;
                    case "TPAdmin": _roller = "Admin"; break;
                    case "TPUser": _roller = "User"; break;

                }
                asNorms.OrderBy(m => m);
                asNorms[a] = _roller;
            }

            return asNorms;
        }

        public List<CompanyUserView> CompanyUsers(string Company)
        {
            List<CompanyUserView> myCompanyUsers = new List<CompanyUserView>();

            var aUsers = db.Users.Where(m => m.Company == Company).ToList();

            if (aUsers != null && aUsers.Count > 0)
            {
                foreach (var uSer in aUsers)
                {
                    CompanyUserView model = new CompanyUserView()
                    {
                        FullName = uSer.UserName,
                        id = uSer.Id,
                        Username = uSer.UserName,
                        Roles = string.Join(",", RolesAsNormalText(uSer.Roles.Select(m => m.RoleId).ToArray()).ToList())

                    };

                    myCompanyUsers.Add(model);
                }

            }
            return myCompanyUsers;
        }


        public List<CompanyProjView> CompanyProjects(string Company)
        {

            List<CompanyUserView> myUsers = CompanyUsers(Company);

            List<CompanyProjView> myCompanyProjects = new List<CompanyProjView>();

            foreach (var uSer in myUsers)
            {
                var aCompanyProjects = db.Projects.Where(m => m.CurrentUserID == uSer.id).ToList();

                if (aCompanyProjects != null && aCompanyProjects.Count > 0)
                {

                    foreach (var proj in aCompanyProjects)
                    {
                        proj.isEnergyDone();
                        CompanyProjView proView = new CompanyProjView()
                        {
                            Client = proj.Details.ProjectName,
                            CurrentUser = uSer.FullName,
                            id = proj.Id,
                            Status = proj.Valid ? "Ready" : "Not Ready",
                            Type = proj.Costing.Plan,
                            Country = proj.Details.P_Country
                        };

                        myCompanyProjects.Add(proView);
                    }

                }
            }



            return myCompanyProjects;
        }


        [Authorize]
        public string CurrentUsername()
        {
            string id = User.Identity.GetUserId();
            return db.Users.Find(id).UserName;
        }
        #endregion
    }
}