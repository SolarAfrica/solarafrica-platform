﻿using System.Web;
using System.Web.Optimization;

namespace NVIDev
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                //"~/Scripts/jquery-{version}.js"));
                           "~/Scripts/jquery.1.9.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
          "~/Scripts/jquery-migrate-1.0.0.js",

                "~/Scripts/jquery-ui-1.10.2.min.js",
                 "~/Scripts/bootstrap.min.js",
                "~/Content/fancybox/jquery.fancybox-1.3.4.min.js",
                "~/Scripts/jquery.bs_grid.min.js",
                 "~/Scripts/en.js",
               "~/Scripts/respond.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqGrid").Include(
                   "~/Scripts/jquery-ui.1.9.2.min.js",
            "~/Scripts/grid.locale-en.js",
                  "~/Scripts/jquery.jqGrid.min.js",
                  "~/Scripts/grid.subgrid.js",
            "~/Scripts/jqGridScript.js"
                    ));

            bundles.Add(new ScriptBundle("~/plugins/metsiMenu").Include(
                    "~/Content/plugins/metisMenu-master/src/jquery.slimscroll.min.js"));

            bundles.Add(new ScriptBundle("~/plugins/slimScroll").Include(

                     "~/Content/plugins/slimscroll/metisMenu.js"));

            // CSS style (bootstrap/inspinia)
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                //  "~/Content/bootstrap-theme.min.css",
                      "~/Content/animate.css",
                      "~/Content/Site.css",
                      "~/Content/style.css",
                      "~/Content/jquery-ui-1.8.7.css",
                      "~/Content/fancybox/jquery.fancybox-1.3.4.min.css",
                      "~/Content/ui.jqgrid.css"));

            bundles.Add(new StyleBundle("~/plugins/jasnyBootstrapStyles").Include(
                    "~/Content/plugins/jasny/jasny-bootstrap.min.css"));
            bundles.Add(new StyleBundle("~/font-awesome/css").Include(
"~/Content/plugins/font-awesome-4.4.0/css/font-awesome.min.css"
                   ));

            bundles.Add(new ScriptBundle("~/bundles/Admin").Include(
          "~/Scripts/Admin/Index.js"));
            bundles.Add(new ScriptBundle("~/bundles/UserInfo").Include(
                     "~/Scripts/Admin/UserInfo.js"));
            bundles.Add(new ScriptBundle("~/bundles/Register").Include(
                     "~/Scripts/Register/Registration.js"));

            BundleTable.EnableOptimizations = false;

        }
    }
}
